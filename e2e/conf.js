// An example configuration file.

exports.config = {

    framework: 'jasmine2', // mocha, cucumber are other frameworks available and can be used

    // These are the params we are passing from CMD
    params: {
        host:"",
        isemu:"",
        fc:""
    },

    //Initial setup before test suite execution starts
    onPrepare: function() {
        var resultsPath;
        protractorHostName=browser.params.host; //reading host from protractor and declared global to access everywhere
        emuFlag=browser.params.isemu;
        fullChassis=browser.params.fc;
        console.log(protractorHostName+","+emuFlag+",",fullChassis);
        if(protractorHostName=="" || protractorHostName==undefined){
            hostName="bolt02";
            emuFlag="false";
            fullChassis="true";
            resultsPath="./reports/";
        }
        else {
            hostName=protractorHostName;
            resultsPath="./reports/";
        }

        dependencies = require('./dependencies.js');// declared global to load dependencies all over the suite execution// Add a screenshot reporter and store screenshots to `/tmp/screnshots`:
        var exec = require('ssh-exec');
        exec("vsu list",dependencies.getCLIData().getConf(hostName), function (err,rolelog) {
        }).pipe(process.stdout);
        // to integrate reports with jenkins
        var jasmineReporters = require('jasmine-reporters');
        var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
        return browser.getProcessedConfig().then(function(config) {
            // you could use other properties here if you want, such as platform and version
            var browserName = config.capabilities.browserName;

            var junitReporter = new jasmineReporters.JUnitXmlReporter({
                consolidateAll: true,
                savePath: resultsPath+'/testReportsForJenkins',
                // this will produce distinct xml files for each capability
                filePrefix: browserName + '-xmloutput',
                modifySuiteName: function(generatedSuiteName, suite) {
                    // this will produce distinct suite names for each capability,
                    // e.g. 'firefox.login tests' and 'chrome.login tests'
                    return browserName + '.' + generatedSuiteName;
                }
            });

            var localReporter = new Jasmine2HtmlReporter({
                savePath: resultsPath + '/testReportsLocal/'+browserName+'/',
                screenshotsFolder: '/images/',
                takeScreenshotsOnlyOnFailures: true
            });
            jasmine.getEnv().addReporter(junitReporter);
            jasmine.getEnv().addReporter(localReporter);
        });
    },

    onComplete: function () {
      browser.close();
    },

    restartBrowserBetweenTests: false,
    //directConnect: true,

    //seleniumServerJar:'../../node_modules/protractor/selenium/selenium-server-standalone-2.45.0.jar' ,
    seleniumAddress: 'http://127.0.0.1:4444/wd/hub',

    //rootElement: 'body',

    // Capabilities to be passed to the webdriver instance.
    //capabilities:
    //{
    //    'browserName': 'firefox'
    //    //'firefox_profile':'default'
    //    //shardTestFiles: true,
    //    //maxInstances: 2
    //},

    multiCapabilities: [
        //{
        //    'browserName': 'phantomjs',
        //    'phantomjs.binary.path':require('phantomjs').path,
        //    'phantomjs.cli.args': ['--web-security=false', '--ignore-ssl-errors=true', '--webdriver-loglevel=DEBUG'],
        //    'phantomjs.ghostdriver.cli.args': ['--loglevel=DEBUG'],
        //
        //    //shardTestFiles: true,
        //    //    maxInstances: 3
        //},
        {
            'browserName': 'chrome',
            'chromeOptions': {
                args: ['--no-sandbox', '--disable-web-security']
                //args: ['--no-sandbox', "--headless", "--disable-gpu", "--window-size=1920x1080" ]
            },
        //shardTestFiles: true,
        //        maxInstances: 2
        },
        //{
        //    'browserName': 'firefox',
        //    //firefox_profile:'default',
        //    //prefs: {
        //    //    'config.http.use-cache': true
        //    //},
        //    'marionette': true,
        //    //'acceptSslCerts':true,
        //    'acceptInsecureCerts':true
        //
			//////shardTestFiles: true,
        ////     //   maxInstances: 3
        //}

    ],

    suites: {

        logicalAssets: ['./specs/logicalAssets/volumes/*Spec.js','./specs/logicalAssets/initiators/*Spec.js','./specs/logicalAssets/ports/*Spec.js',
            './specs/readOnlyUser/*Spec.js',
            './specs/logicalAssets/volumeGroups/*Spec.js',
            './specs/logicalAssets/portGroups/*Spec.js',
            './specs/logicalAssets/initiatorGroups/*Spec.js',
            './specs/system/settings/*Spec.js',
            './specs/logicalAssets/exportGroups/*Spec.js',
            './specs/dashboard/multiArrayScenarioSpec.js',
            './specs/policyManagement/*Spec.js'
            ],
        //logicalAssets: ['./specs/logicalAssets/volumes/*Spec.js','./specs/logicalAssets/ports/*Spec.js','./specs/login/*Spec.js','./specs/readOnlyUser/*Spec.js'],
        analytics:[
            './specs/system/health/*Spec.js',
            './specs/logicalAssets/storageArray/*Spec.js',
            './specs/layout/*Spec.js',
            './specs/dashboard/dashboardScenarioSpec.js',
            './specs/fault/*/*Spec.js',
             './specs/node/*Spec.js',
            './specs/login/*Spec.js',
            './specs/analytics/ports/*Spec.js',
            './specs/analytics/volumes/*Spec.js',
            './specs/analytics/capacityPlaning/*Spec.js'
            ],
        monitor:['./specs/analytics/*Spec.js'],
        full: ['./specs/*/*Spec.js','./specs/*/*/*Spec.js'],
        debugDryRun:['./test.js']
    },


    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 3000000
    },
    allScriptsTimeout: 300000,
    getPageTimeout:200000,
    //setScriptTimout:20000


};
