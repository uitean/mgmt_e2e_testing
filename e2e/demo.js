/**
 * Created by Ajay(Vexata) on 09-06-2017.
 */


var volumePage = dependencies.getVolumePage(),
    dataForVolumes = dependencies.getDataForVolumes(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage(),
    commonData = dependencies.getCommonData(),
    volumeService = dependencies.getVolumeService(),
    cliData=dependencies.getCLIData(),
    exec=require('ssh-exec');

//just to show the usage of ssh-exec library to run cli commands and capture the details from CLI
exec("vxcli sa show",cliData.getConf(hostName), function (err,stdout) {
    console.log("printing sa details from cli \n"+stdout);
    console.log("\n Sa name in CLI is "+stdout.split("Name:")[1].split("\n")[0].trim());
});

describe("Tests For Demo", function() {
    var volumeName;

    beforeAll(function () {
        //    Steps to be executed before all test cases inside this describe block
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToVolumesPage();
    });

    beforeEach(function () {
        //    Steps to be executed before each test case inside this describe block
    });

    describe("Tests to delete Volume", function () {

        beforeAll(function () {
            //steps to be executed before all tests in this describe block
        });

        beforeEach(function () {
            //Steps to be executed before each test case
            volumeName = webElementActionLibrary.getTimeStampAttached(dataForVolumes.volumeName);// appending volume name with time to make it unique and to avoid the usage with other tests
            volumeService.createVolume(volumeName, dataForVolumes.volumeDescription, dataForVolumes.volumeSize);
            webElementActionLibrary.refresh();
        });

        it("Tests to Delete Volume", function () {
            volumePage.deleteVolume(volumeName, true);
        });

        it("Tests to validate failure", function () {
            volumeService.getVolumeDetails("64");
            webElementActionLibrary.verifyPresenceOfElement(volumePage.cancelButton());
        });

        afterEach(function () {
            //   steps to be executed after each test case
        });

        afterAll(function () {
            //   steps to be executed after all test cases in this describe block
        });
    });

    xdescribe("another set of test cases", function () {

    });

    afterEach(function () {
        //    Steps to be executed after each test case inside this describe block
    });

    afterAll(function () {
        //    Steps to be executed after all test cases inside this describe block
        commonAssetsControlsPage.navigateToDashboardPage();
    });
});
