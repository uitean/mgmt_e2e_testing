'use strict';

var webElementActionLibrary = {

    loadUrl: function (url) {
        browser.ignoreSynchronization = false;
        browser.driver.manage().window().maximize();
        browser.get(url);
    },

    loadPageUrl: function (url) {
       browser.get(url);
    },
    zoomOut: function () {
        for(var i=0; i<2; i++){
            browser.actions().keyDown(protractor.Key.CONTROL).sendKeys(protractor.Key.SUBTRACT).keyUp(protractor.Key.CONTROL).perform();
        }
    },

    zoomIn:function () {
        for(var i=0; i<2; i++){
            browser.actions().keyDown(protractor.Key.CONTROL).sendKeys(protractor.Key.ADD).keyUp(protractor.Key.CONTROL).perform();
        }
    },

    refresh:  function(){
        browser.driver.navigate().refresh();
        //webElementActionLibrary.waitForAngular();
        // browser.executeScript("document.body.style.transform='scale(0.8)'");
    },

    verifySelectedDropDownValue: function (parent,value) {
        parent.$('option[selected="selected"]').getText().then(function (val) {
            expect(val).toBe(value);
        });
    },

    selectByVisibleText: function(element,text){
        browser.driver.findElement(By.xpath('//option[text()="'+text+'"]')).click();
    },

    waitForAngular: function () {
        browser.waitForAngular();
    },


    type: function (webElement, text) {
        webElementActionLibrary.waitForElement(webElement);
        webElement.clear();
        webElement.sendKeys(text);
    },

    verifyCurrentUrl: function (expectedUrl) {
        expect(browser.getCurrentUrl()).toEqual(expectedUrl);
    },

    mouseOver: function (webElement) {
        browser.actions().mouseMove(webElement).perform();
    },

    reduceResolution: function (webElement) {
        browser.actions().keyDown(protractor.Key.CONTROL).sendKeys(protractor.Key.SUBTRACT).perform();

    },

    getImmediateParent: function (xpath) {
        return  browser.driver.findElement(By.xpath(xpath+"/.."));
    },

    getGrandParent: function (xpath) {
        return  browser.driver.findElement(By.xpath(xpath+"/../.."));
    },

    verifyTextPresence: function (webElement, textToVerify) {
        expect(webElement.getText()).toMatch(textToVerify);
    },

    verifyTextPresenceUsingContain: function (webElement,textToContain) {
        expect(webElement.getText()).toContain(textToContain);
    },

    verifyTextPresenceUsingContainByGetAttr: function (attr,webElement,textToContain) {
        expect(webElement.getAttribute(attr)).toContain(textToContain);
    },

    verifyTextPresenceUsingContainByGetAttribute: function (webElement,textToContain) {
        expect(webElement.getAttribute('value')).toContain(textToContain);
    },

    verifyExactTextUsingContainByGetAttribute: function (webElement,textToContain) {
        expect(webElement.getAttribute('value')).toEqual(textToContain);
    },

    verifyTextAbsenceUsingContains: function (webElement,textToContain) {
        expect(webElement.getText()).not.toContain(textToContain);
    },

    verifyExactText: function (webElement, textToVerify) {
        expect(webElement.getText()).toEqual(textToVerify);
    },

    verifyPresenceOfElement: function (webElement) {
        expect(webElement.isDisplayed()).toBe(true);
    },

    verifyValueNotToBeEmpty: function (webElement) {
        expect(webElement.getText()).not.toBe("");
    },

    verifyAbsenceOfElement: function (webElement) {
        expect(webElement.isPresent()).toBe(false);
    },

    getTimeStampAttached: function(name){
        var temp = name + new Date().toISOString().replace(" ","");
        name = (temp.split(":")[0]+temp.split(":")[1]+Math.random().toString(16).slice(2,4));
        return name;
    },

    getElementByXpath: function(xpath){
        return browser.driver.findElement(By.xpath(xpath));
    },

    getElementsByXpath: function(xpath){
        return browser.driver.findElements(By.xpath(xpath));
    },

    waitForElement: function(webElement){
        browser.wait(function () {
            return webElement.isDisplayed();
        })
    },

    wait: function (sec) {
        browser.sleep(sec*1000);
    },

    waitForElementUsingExpectedConditions: function (webElement) {
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.presenceOf(webElement), 10000);
    },

    performDoubleClickActionOn: function(webElement){
        browser.actions().doubleClick(webElement).perform();
    },

    verifyElementsCount: function (webElementList,count) {
        webElementList.count().then(function (val) {
            expect(val).toBe(count);
        });
    },

    waitForElementAndClick: function(webElement){
        browser.wait(function () {
            return webElement.isPresent();
        }).then (function () {
            webElement.click();
        });
    },

    driverQuit:function(){
        browser.driver.close();
    },

    getElementByLabel: function (label) {
        return browser.driver.findElement(By.xpath('//label[text()="'+label+'"]'));
    },

    getElementContainsText: function (text) {
        return browser.driver.findElement(By.xpath("//*[contains(text(),'"+text+"')]"));
    },

    getElementByVisibleText: function (text) {
        return browser.driver.findElement(By.xpath("//*[text()='"+text+"']"));
    },

    navigateBack: function () {
      browser.navigate().back();
    }



};

module.exports = webElementActionLibrary;