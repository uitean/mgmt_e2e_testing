/**
 * Created by Ajay(Vexata) on 20-04-2016.
 */

var  commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    volumePage=dependencies.getVolumePage(),
    commonData= dependencies.getCommonData();

var volumeAnalyticsPage={

    verifyVolumeAnalyticsActiveBreadCrumbHeader: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.activeHeaderInBreadCrumb(),commonData.volumeAnalyticsHeader);
    },

    verifyGraphSection: function () {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getSectionHeader(volumeAnalyticsPage.graphSection()),commonData.top64VolumesGraphHeader);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getSectionHeader(volumeAnalyticsPage.graphSection()),commonData.top64VolumesBWGraphHeader);
        webElementActionLibrary.verifyPresenceOfElement(volumeAnalyticsPage.pickDifferentVolumeButton());
        volumeAnalyticsPage.pickDifferentVolumeButton().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.popUpTitle(), commonData.selectVolume);
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBoxInForm(volumeAnalyticsPage.pickVolumeGrid()),64);
        volumePage.verifyGridColumnTitles(volumeAnalyticsPage.pickVolumeGrid());
        commonAssetsControlsPage.closeWindow();
        commonAssetsControlsPage.verifyGraphSectionElements(volumeAnalyticsPage.graphSection(),"Volume Analytics");
    },

    verifyBandwidthUtilizationSectionElements: function () {
        webElementActionLibrary.wait(2);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(volumeAnalyticsPage.bwUtilizationSection()),commonData.bandwidthUtilization);
        commonAssetsControlsPage.getPagerInfo(volumeAnalyticsPage.topNVolumesGridSection()).getText().then(function (rowCount) {
            rowCount=rowCount.split("of")[1].split("items")[0].trim();
            volumeAnalyticsPage.donutSegments().count().then(function (donutSegmentCount) {
                expect(Number(rowCount)).toBe(donutSegmentCount);
                //if(donutSegmentCount>1) {
                //    //volumeAnalyticsPage.donutSegments().last().click();
                //    webElementActionLibrary.mouseOver(volumeAnalyticsPage.donutSegments().last());
                //    webElementActionLibrary.wait(3);
                //    commonAssetsControlsPage.getBWTooltip().getText().then(function (val) {
                //        console.log(val);
                //        expect(val).toContain("ID:");
                //        expect(val).toContain("Name:");
                //        expect(val).toContain("Provisioned:");
                //        expect(val).toContain("Used:");
                //        expect(val).toContain("Bandwidth Value:");
                //        expect(val).toContain("Bandwidth Percent:");
                //        webElementActionLibrary.mouseOver(commonAssetsControlsPage.dashboardLogo());
                //    });
                //}
            });
            webElementActionLibrary.verifyPresenceOfElement(webElementActionLibrary.getElementByVisibleText(commonData.top));
            webElementActionLibrary.verifyPresenceOfElement(volumeAnalyticsPage.bwPercentageValues().first());
            if(Number(rowCount)>Number(commonData.defaultTopNVolumesCount)) {
                webElementActionLibrary.verifyPresenceOfElement(webElementActionLibrary.getElementByVisibleText(commonData.other));
                webElementActionLibrary.verifyPresenceOfElement(volumeAnalyticsPage.bwPercentageValues().last());
                volumeAnalyticsPage.bwUtilizationSection().getText().then(function (val) {
                    var topValue=val.split(commonData.top)[1].split("%")[0];
                    var otherValue=val.split(commonData.other)[1].split("%")[0];
                    var total = (Number(topValue)+Number(otherValue));
                    expect(total).toBe(100);
                });
            }
            else
            webElementActionLibrary.verifyExactText(volumeAnalyticsPage.bwPercentageValues().get(1),"100");
        });
    },

    verifyTopNVolumesGridElements: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(volumeAnalyticsPage.topNVolumesGridSection()),commonData.top+" "+commonData.defaultTopNVolumesCount+" "+commonData.volumesTabText);
        volumeAnalyticsPage.verifyTopNVolumesGridTitlesAndValues();
    },

    verifyTopNVolumesGridTitlesAndValues: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(volumeAnalyticsPage.topNVolumesGridSection()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(volumeAnalyticsPage.topNVolumesGridSection()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getProvisionedTitle(volumeAnalyticsPage.topNVolumesGridSection()),commonData.provisionedTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getBWValueTitle(volumeAnalyticsPage.topNVolumesGridSection()),commonData.bwValueTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getBWPercentageTitle(volumeAnalyticsPage.topNVolumesGridSection()),commonData.bwPercentageTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getVolumeUsedTitle(volumeAnalyticsPage.topNVolumesGridSection()),commonData.usedTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPortsTitle(volumeAnalyticsPage.topNVolumesGridSection()),commonData.noOfPortsTitle);
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(volumeAnalyticsPage.topNVolumesGridSection(),8);
    },

    pickDifferentVolumeButton: function () {
      return $('[ng-click="pickDifferentVolume()"]');
    },

    pickVolumeGrid:  function () {
        return $('#vx_volume_analytics_choose_volume_form_id');
    },

    graphSection: function () {
      return $('.vx-transparent-container-volume-analytics-panel4');
    },

    bwPercentageValues: function () {
        return volumeAnalyticsPage.bwUtilizationSection().$$('.ng-binding');
    },

    donutSegments: function () {
        return volumeAnalyticsPage.bwUtilizationSection().$$('g path[fill*="url"]');
    },

    bwUtilizationSection: function () {
        return $('.vx-transparent-container-volume-analytics-panel2');
    },

    topNDropDown: function () {
        return $('.vx-light-dd-arrow');
    },

    defaultTopNValue: function () {
        return volumeAnalyticsPage.topNDropDown().$('option');
    },

    topNDropDownOptions: function () {
        return $$('ul[aria-live="polite"] li');
    },

    
    verifyTopNDefaultValue: function () {
        webElementActionLibrary.verifyExactText(volumeAnalyticsPage.defaultTopNValue(),commonData.defaultTopNVolumesCount)
    },

    verifyTopNDropDownOptions: function () {
        volumeAnalyticsPage.defaultTopNValue().click().then(function () {
            var options = volumeAnalyticsPage.topNDropDownOptions();
            expect(options.count()).toBe(Number(commonData.maxTopNVolumesCount));
            var j=1;
            options.count().then(function (count) {
                for(var i=0;i<count;i++){
                    options.get(i).getText().then(function (val) {
                        expect(Number(val)).toBe(j);
                        j++;
                    });
                }
            });
        });
    },

    topNVolumesGridSection: function () {
        return $('.vx-transparent-container-volume-analytics-panel3');
    }

};

module.exports=volumeAnalyticsPage;
