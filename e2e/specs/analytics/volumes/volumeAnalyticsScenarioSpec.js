/**
 * Created by Ajay(Vexata) on 20-04-2016.
 */

'use strict';

var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    volumeAnalyticsPage = dependencies.getVolumeAnalyticsPage();


describe("Tests for verifying presence of volume analytics page elements", function() {

    describe("Tests for readonly user", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
            commonAssetsControlsPage.navigateToVolumeAnalyticsPage();
        });

        it("Verifying page header ", function () {
            volumeAnalyticsPage.verifyVolumeAnalyticsActiveBreadCrumbHeader();
        });

        it("Verifying bw utilisation section elements like header, donut segments, top and other text and values,  ", function () {
            volumeAnalyticsPage.verifyBandwidthUtilizationSectionElements();
        });

        it("Verifying top n volumes grid section elements like header, grid column titles and values and their presence,  ", function () {
            volumeAnalyticsPage.verifyTopNVolumesGridElements();
        });

        it("Verifying time selector elements like default selected, and other selectors and +/- buttons for readonly user ", function () {
            commonAssetsControlsPage.verifyPresenceOfTimeSelectorElements();
        });

        it("Verifying graph section elements like header, r/w graph labels, avg values and tooltips, graph and tooltip, legends readonly user ", function () {
            volumeAnalyticsPage.verifyGraphSection();
        });

        it("Verifying top n default value and the drop down values", function () {
            volumeAnalyticsPage.verifyTopNDefaultValue();
            volumeAnalyticsPage.verifyTopNDropDownOptions();
        });

        xit("Verifying +/- icon changes, graph label changes, graph elements, selected interval while changing time selector ", function () {
            commonAssetsControlsPage.verifyGraphAndLabelsOnChangeOfTimeSelector(volumeAnalyticsPage.graphSection(),"VA BW");
        });

        it("Verifying time selector changes on clicking +/- icons", function () {
            commonAssetsControlsPage.verifyPlusMinusIconClickChanges();
        });

    });

    describe("Tests for admin", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToVolumeAnalyticsPage();
        });

        it("Verifying page header ", function () {
            volumeAnalyticsPage.verifyVolumeAnalyticsActiveBreadCrumbHeader();
        });

        it("Verifying bw utilisation section elements like header, donut segments, top and other text and values,  ", function () {
            volumeAnalyticsPage.verifyBandwidthUtilizationSectionElements();
        });

        it("Verifying top n volumes grid section elements like header, grid column titles and values and their presence,  ", function () {
            volumeAnalyticsPage.verifyTopNVolumesGridElements();
        });

        it("Verifying time selector elements like default selected, and other selectors and +/- buttons for readonly user ", function () {
            commonAssetsControlsPage.verifyPresenceOfTimeSelectorElements();
        });

        it("Verifying graph section elements like header, r/w graph labels, avg values and tooltips, graph and tooltip, legends readonly user ", function () {
            volumeAnalyticsPage.verifyGraphSection();
        });

        it("Verifying top n default value and the drop down values", function () {
            volumeAnalyticsPage.verifyTopNDefaultValue();
            volumeAnalyticsPage.verifyTopNDropDownOptions();
        });

        xit("Verifying +/- icon changes, graph label changes, graph elements, selected interval while changing time selector ", function () {
            commonAssetsControlsPage.verifyGraphAndLabelsOnChangeOfTimeSelector(volumeAnalyticsPage.graphSection());
        });

        it("Verifying time selector changes on clicking +/- icons", function () {
            commonAssetsControlsPage.verifyPlusMinusIconClickChanges();
        });
    });


});

