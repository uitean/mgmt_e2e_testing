/**
 * Created by Ajay(Vexata) on 01-06-2016.
 */

'use strict';

var  commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
//volumePage=dependencies.getVolumePage(),
    commonData= dependencies.getCommonData();

var portAnalyticsPage={

    verifyPresenceOfElementsOnPortAnalyticsPage: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.activeHeaderInBreadCrumb(),commonData.portAnalyticsHeader);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(portAnalyticsPage.portAnalyticsMainSection()),commonData.analyticsAllPortsHeader);
        portAnalyticsPage.portAnalyticsMenuDD().click();
        var i;
        for(i=0;i<3;i++)
        webElementActionLibrary.verifyExactText(portAnalyticsPage.ddHeadItems().get(i),commonData.portAnalyticsDDHeaders[i]);
        for(i=0;i<8;i++)
            webElementActionLibrary.verifyExactText(portAnalyticsPage.ddMenuItems().get(i),commonData.portAnalyticsDDCheckList[i]);
    },

    switchToIopsSection: function () {
        portAnalyticsPage.iopsButton().click();
        webElementActionLibrary.waitForAngular();
    },

    verifyRadialGuageSection: function () {
        webElementActionLibrary.verifyExactText(portAnalyticsPage.selectedView(),commonData.bandwidth);
        webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.iopsButton());
        webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.bandwidthButton());
        portAnalyticsPage.verifyLegends(portAnalyticsPage.radialGuageSection());
        //webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.getRadialGuagePointer(commonAssetsControlsPage.readColor));
        //webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.getRadialGuagePointer(commonAssetsControlsPage.writeColor));
        //webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.getRadialGuageArch(commonAssetsControlsPage.readColor));
        //webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.getRadialGuageArch(commonAssetsControlsPage.writeColor));

    },

    verifyLegends: function (section) {
        webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.getReadLegend(section));
        webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.getWriteLegend(section));
    },

    verifyCylinderSection: function () {
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(portAnalyticsPage.cylindersSection()),commonData.cylinderSectionHeader);
        //portAnalyticsPage.verifyLegends(portAnalyticsPage.cylindersSection());
        portAnalyticsPage.verifyIOCSectionElements(1);
        portAnalyticsPage.verifyIOCSectionElements(0);
        portAnalyticsPage.getPorts();
    },

    getPorts: function () {
        var disabledPorts=[];
        var barrierCount;
        portAnalyticsPage.disabledPorts().count().then(function (count) {
            if(count!=0) {
                barrierCount = count;
                for (var i = 0; i < count; i++)
                    portAnalyticsPage.disabledPorts().get(i).getText().then(function (val) {
                        disabledPorts.push(val);
                        barrierCount = barrierCount - 1;
                        if (barrierCount === 0) {
                            var enabledPorts = [];
                            if (disabledPorts[0] != 0) {
                                for (var j = 0; j < disabledPorts[0]; j++)
                                    enabledPorts.push(j);
                            }
                            for (var i = 1; i < disabledPorts.length; i++) {
                                var x = disabledPorts[i], y = disabledPorts[i - 1];
                                while (x - y != 1) {
                                    enabledPorts.push(y + 1);
                                    y++;
                                }
                            }
                            portAnalyticsPage.verifyCylinderTooltip(enabledPorts);
                        }
                    });
            }
            else
                portAnalyticsPage.verifyCylinderTooltip([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]);
        });
    },

    verifyCylinderTooltip: function (enabledPorts) {
        var j= 0,k=0;
        console.log(enabledPorts);
        for(var i=0;i<enabledPorts.length;i++){
            webElementActionLibrary.mouseOver(portAnalyticsPage.getPortReadBar(enabledPorts[i]));
            webElementActionLibrary.wait(1);
            commonAssetsControlsPage.getTooltip().last().getText().then(function (val) {
                console.log(" port "+enabledPorts[j]+" read tooltip value is "+val);
                j++;
            });
        }

        for(var i=0;i<enabledPorts.length;i++){
            webElementActionLibrary.mouseOver(portAnalyticsPage.getPortWriteBar(enabledPorts[i]));
            webElementActionLibrary.wait(1);
            commonAssetsControlsPage.getTooltip().last().getText().then(function (val) {
                console.log(" port " +enabledPorts[k]+ " write tooltip value is " + val);
                k++;
            });
        }
    },

    verifyGraphSection: function () {
        webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.getSectionHeader(portAnalyticsPage.graphSection()),commonData.portAnalyticsGraphSectionHeader);
        commonAssetsControlsPage.verifyGraphSectionElements(portAnalyticsPage.graphSubSection(),"Port Analytics");
    },

    verifyIOCSectionElements: function (iocNumber) {
        webElementActionLibrary.verifyExactText(portAnalyticsPage.getInitiatorText(iocNumber),commonData.initiatorsTabText);
        webElementActionLibrary.verifyExactText(portAnalyticsPage.getVolumeText(iocNumber),commonData.volumesTabText);
        webElementActionLibrary.verifyExactText(portAnalyticsPage.getIOCHeading(iocNumber),commonData.ioc+" "+iocNumber);
        var j=0;
        for(var i=0;i<8;i++){
            portAnalyticsPage.getPortNumber(iocNumber,i).getText().then(function (portNumber) {
                webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.getPortReadBar(iocNumber*8+j));
                webElementActionLibrary.verifyPresenceOfElement(portAnalyticsPage.getPortWriteBar(iocNumber*8+j));
                if(fullChassis=="true"){
                    webElementActionLibrary.verifyValueNotToBeEmpty(portAnalyticsPage.getPortITNInitiatorsCount(iocNumber,j));
                    webElementActionLibrary.verifyValueNotToBeEmpty(portAnalyticsPage.getPortITNVolumesCount(iocNumber,j));
                }
                expect(Number(portNumber)).toBe(8*iocNumber+j);
                j++;
            });
        }
    },

    ddHeadItems: function () {
      return $$('li[role="headitem"]');
    },


    ddMenuItems: function () {
        return $$('li[role="menuitem"] label');
    },

    portAnalyticsMenuDD: function () {
      return $('.portAnalyticsDropMenu');
    },

    portAnalyticsMainSection: function () {
      return $('.vx-transparent-container-port-analytics-main-panel');
    },


    getRadialGuagePointer: function (color) {
        return $('[stroke="+'+color+'"][stroke-width="0.5"]');
    },

    getRadialGuageArch: function (color) {
        return $('[stroke="+'+color+'"][stroke-width="12"]');
    },

    getReadLegend: function (section) {
        return section.$('.read-legend');
    },

    getWriteLegend: function (section) {
        return section.$('.write-legend');
    },

    getPortBarSection: function (iocNumber,portNumber) {
        return portAnalyticsPage.getIOCSection(iocNumber).$$('.bar-wrapper').get(portNumber);
    },

    getPortNumber: function (iocNumber,portNumber) {
        return portAnalyticsPage.getPortBarSection(iocNumber,portNumber).$('span.ng-binding');
    },

    getPortITNInitiatorsCount: function (iocNumber,portNumber) {
        return portAnalyticsPage.getPortBarSection(iocNumber,portNumber).$$('label').last();
    },

    getPortITNVolumesCount: function (iocNumber,portNumber) {
        return portAnalyticsPage.getPortBarSection(iocNumber,portNumber).$$('label').first();
    },

    getPortReadBar: function (portNumber) {
        return $$('.readBar').get(portNumber);
    },

    getPortWriteBar: function (portNumber) {
        return $$('.writeBar').get(portNumber);
    },

    getIOCHeading: function (iocNumber) {
        return $$('.heading').get(iocNumber);
    },

    graphSection: function () {
        return $$('.col-xs-7').get(1);
    },

    graphSubSection: function () {
        return $('.chart-container-pa-trend-left');
    },

    bandwidthButton: function () {
        return $('[ng-class*="bandwidthAnalytics"]');
    },

    iopsButton: function () {
        return $('[ng-class*="iopsAnalytics"]');
    },

    selectedView: function () {
        return portAnalyticsPage.radialGuageSection().$('.vx-tab-button.active');
    },

    cylindersSection: function () {
        return $$('.col-xs-7').get(0);
    },

    radialGuageSection: function () {
        return $('.pa-bw-iops-gauage');
    },

    iocs: function () {
        return $$('.ioc-cylinder-box');
    },

    getIOCSection: function (iocNumber) {
        return portAnalyticsPage.iocs().get(iocNumber);
    },

    getInitiatorText: function (iocNumber) {
        return portAnalyticsPage.iocs().get(iocNumber).$$('.asset-heading').first();
    },

    getVolumeText: function (iocNumber) {
        return portAnalyticsPage.iocs().get(iocNumber).$$('.asset-heading').last();
    },

    disabledPorts: function () {
        return $$('.bar-wrapper.disabled');
    }

};

module.exports=portAnalyticsPage;