/**
 * Created by Ajay(Vexata) on 01-06-2016.
 */


'use strict';

var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    portAnalyticsPage = dependencies.getPortAnalyticsPage(),
    commonData=dependencies.getCommonData();


xdescribe("Tests for verifying presence of port analytics page elements", function() {

    describe("Tests for readonly user", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
            commonAssetsControlsPage.navigateToPortAnalyticsPage();
        });

        it("Verifying page header, selected bw view and IOPS button option  ", function () {
            portAnalyticsPage.verifyPresenceOfElementsOnPortAnalyticsPage();
        });

        describe("IN BW View", function(){

            it("Verifying  cylinder section header, ioc's headers, legends, initiators, volumes text, cylinder bars and tooltips for enabled ports and the port numbers", function () {
                portAnalyticsPage.verifyCylinderSection();
            });

            it("Verifying  radial guage section header, legends ", function () {
                portAnalyticsPage.verifyRadialGuageSection();
            });

            it("Verifying  time selector elements, like default value, options and +/- buttons ", function () {
                commonAssetsControlsPage.verifyPresenceOfTimeSelectorElementsForPortAnalytics();
            });

            xit("Verifying  graph section elements like header, r/w graph labels, avg values and tooltips, graph and tooltip, legends", function () {
                portAnalyticsPage.verifyGraphSection();
            });

            xit("Verifying +/- icon changes, graph label changes, graph elements, selected interval while changing time selector ", function () {
                portAnalyticsPage.verifyGraphAndLabelsOnChangeOfTimeSelector(portAnalyticsPage.graphSection());
            });

            xit("Verifying time selector changes on clicking +/- icons", function () {
                commonAssetsControlsPage.verifyPlusMinusIconClickChanges();
            });
        });

        describe("IN IOPS View", function(){

            beforeAll(function () {
                portAnalyticsPage.switchToIopsSection();
                commonAssetsControlsPage.selectTimeSelectorAndVerifySelected(commonData.portAnalyticsGraphTimeSelectors[0]);
            });

            it("Verifying cylinder section header, ioc's headers, legends, initiators, volumes text, cylinder bars and tooltips for enabled ports and the port numbers", function () {
                portAnalyticsPage.verifyCylinderSection();
            });

            it("Verifying radial guage section header, legends ", function () {
                portAnalyticsPage.verifyRadialGuageSection();
            });

            it("Verifying time selector elements, like default value, options and +/- buttons ", function () {
                commonAssetsControlsPage.verifyPresenceOfTimeSelectorElementsForPortAnalytics();
            });

            xit("Verifying graph section elements like header, r/w graph labels, avg values and tooltips, graph and tooltip, legends", function () {
                portAnalyticsPage.verifyGraphSection();
            });

            xit("Verifying +/- icon changes, graph label changes, graph elements, selected interval while changing time selector ", function () {
                commonAssetsControlsPage.verifyGraphAndLabelsOnChangeOfTimeSelector(portAnalyticsPage.graphSection());
            });

            xit("Verifying time selector changes on clicking +/- icons", function () {
                commonAssetsControlsPage.verifyPlusMinusIconClickChanges();
            });
        });

    });

    describe("Tests for admin", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToPortAnalyticsPage();
        });

        it("Verifying page header, selected bw view and IOPS button option  ", function () {
            portAnalyticsPage.verifyPresenceOfElementsOnPortAnalyticsPage();
        });


        describe("IN BW View", function(){

            it("Verifying  cylinder section header, ioc's headers, legends, initiators, volumes text, cylinder bars and tooltips for enabled ports and the port numbers", function () {
                portAnalyticsPage.verifyCylinderSection();
            });

            it("Verifying  radial guage section header, legends ", function () {
                portAnalyticsPage.verifyRadialGuageSection();
            });

            it("Verifying  time selector elements, like default value, options and +/- buttons ", function () {
                commonAssetsControlsPage.verifyPresenceOfTimeSelectorElementsForPortAnalytics();
            });

            xit("Verifying  graph section elements like header, r/w graph labels, avg values and tooltips, graph and tooltip, legends", function () {
                portAnalyticsPage.verifyGraphSection();
            });

            xit("Verifying +/- icon changes, graph label changes, graph elements, selected interval while changing time selector ", function () {
                portAnalyticsPage.verifyGraphAndLabelsOnChangeOfTimeSelector(portAnalyticsPage.graphSection());
            });

            xit("Verifying time selector changes on clicking +/- icons", function () {
                commonAssetsControlsPage.verifyPlusMinusIconClickChanges();
            });
        });

        describe("IN IOPS View", function(){

            beforeAll(function () {
                portAnalyticsPage.switchToIopsSection();
                commonAssetsControlsPage.selectTimeSelectorAndVerifySelected(commonData.portAnalyticsGraphTimeSelectors[0]);
            });

            it("Verifying cylinder section header, ioc's headers, legends, initiators, volumes text, cylinder bars and tooltips for enabled ports and the port numbers", function () {
                portAnalyticsPage.verifyCylinderSection();
            });

            it("Verifying radial guage section header, legends ", function () {
                portAnalyticsPage.verifyRadialGuageSection();
            });

            it("Verifying time selector elements, like default value, options and +/- buttons ", function () {
                commonAssetsControlsPage.verifyPresenceOfTimeSelectorElementsForPortAnalytics();
            });

            xit("Verifying graph section elements like header, r/w graph labels, avg values and tooltips, graph and tooltip, legends", function () {
                portAnalyticsPage.verifyGraphSection();
            });

            xit("Verifying +/- icon changes, graph label changes, graph elements, selected interval while changing time selector ", function () {
                commonAssetsControlsPage.verifyGraphAndLabelsOnChangeOfTimeSelector(portAnalyticsPage.graphSection());
            });

            xit("Verifying time selector changes on clicking +/- icons", function () {
                commonAssetsControlsPage.verifyPlusMinusIconClickChanges();
            });
        });

    });
});

