/**
 * Created by Ajay(Vexata) on 22-06-2016.
 */

'use strict';

var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    capacityPlaningPage = dependencies.getCapacityPlaningPage();


describe("Tests for verifying presence of capacity planing page page elements", function() {

    describe("Tests for readonly user", function () {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
            commonAssetsControlsPage.navigateToCapacityPlaningPage();
        });

        it("Verifying page header ", function () {
            capacityPlaningPage.verifyActiveBreadCrumbHeader();
        });

        it("Verifying top 10 space consuming volumes section like header, grid titles and values adn the tabs", function () {
            capacityPlaningPage.verifyTop10SpaceConsumingVolumesSection();
        });

        it("Verifying graph section like header, used and provisioned text and values and tooltips and labels and legends", function () {
            capacityPlaningPage.verifyGraphSection();
        });

        it("Verifying volume usage summary section like header, text and volumes count", function () {
            capacityPlaningPage.verifyVolumeUsageSummarySection();
        });

    });

    describe("Tests for admin", function () {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToCapacityPlaningPage();
        });

        it("Verifying page header ", function () {
            capacityPlaningPage.verifyActiveBreadCrumbHeader();
        });

        it("Verifying top 10 space consuming volumes section like header, grid titles and values adn the tabs", function () {
            capacityPlaningPage.verifyTop10SpaceConsumingVolumesSection();
        });

        it("Verifying graph section like header, used and provisioned text and values and tooltips and labels and legends", function () {
            capacityPlaningPage.verifyGraphSection();
        });

        it("Verifying volume usage summary section like header, text and volumes count", function () {
            capacityPlaningPage.verifyVolumeUsageSummarySection();
        });

    });
});
