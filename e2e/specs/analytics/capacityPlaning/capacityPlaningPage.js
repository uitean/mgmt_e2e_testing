/**
 * Created by Ajay(Vexata) on 22-06-2016.
 */


var  commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData();

var capacityPlaningPage= {

    usedLegendColor:"url(#purple-light-gradient)",
    provisionedLegendColor:"#B84BE8",
    usageTrendLegendColor:"url(#purple-light-gradient)",
    fullCapaityLegendColor:"#c00",
    fullCapacityGraphColor:"url(#red-gradient)",

    verifyActiveBreadCrumbHeader: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.activeHeaderInBreadCrumb(), commonData.capacityPlaningHeader);
    },

    verifyVolumeUsageSummarySection: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(capacityPlaningPage.volumeUsageSummarySection()),commonData.volumeUsageSummary);
        //webElementActionLibrary.wait(5);
        for(var i=0;i<4;i++){
            webElementActionLibrary.verifyExactText(capacityPlaningPage.getVolumeSectionsText(i),commonData.volumeSectionsText[i]);
            webElementActionLibrary.verifyValueNotToBeEmpty(capacityPlaningPage.getVolumeCount(i));
        }
    },

    verifyTop10SpaceConsumingVolumesSection: function () {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getSectionHeader(capacityPlaningPage.top10SpaceConsumingVolumesSection()),commonData.top10SpaceConsumingVolumes);
        webElementActionLibrary.verifyExactText(capacityPlaningPage.selectedView(),commonData.cpTop10VolumesViews[0]);
        for(var i=2;i>=0;i--){
            webElementActionLibrary.getElementContainsText(commonData.cpTop10VolumesViews[i]).click();
            webElementActionLibrary.waitForAngular();
            webElementActionLibrary.verifyExactText(capacityPlaningPage.selectedView(),commonData.cpTop10VolumesViews[i]);
            capacityPlaningPage.verifyGrid(capacityPlaningPage.getVXGrid(i));
        }
    },

    verifyGrid: function (grid) {
        grid.getAttribute("feature").then(function (val) {
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(grid),commonData.idTitle);
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(grid),commonData.nameTitle);
            if(val.split("top5")[1].split("volumes")[0]=="utilization")
                webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getUtilizationTitle(grid),commonData.utilizationTitle);
            else
                webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSizeTitle(grid),commonData.sizeTitle);
        });
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(grid,3);
    },

    verifyGraphSection: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(capacityPlaningPage.graphSection()),commonData.capacityTrend);
        capacityPlaningPage.capacitySummary().getText().then(function (val) {
            expect(val).toContain("PHYSICAL USED");
            expect(val).toContain("PROVISION USED");
            var used = val.split("PHYSICAL USED")[1].split("iB")[0].split(" ")[0].trim();
            expect(used).not.toBe("");
            var provisioned = val.split("PROVISION USED")[1].split("iB")[0].split(" ")[0].trim();
            expect(provisioned).not.toBe("");
            webElementActionLibrary.mouseOver(webElementActionLibrary.getElementContainsText("PHYSICAL USED"));
            webElementActionLibrary.wait(1);
            commonAssetsControlsPage.getTooltip().last().getText().then(function (val) {
                console.log("used tool tip value is "+val);
                webElementActionLibrary.mouseOver(webElementActionLibrary.getElementContainsText("PROVISION USED"));
                webElementActionLibrary.wait(1);
                commonAssetsControlsPage.getTooltip().last().getText().then(function (val1) {
                    console.log("provisioned tool tip value is "+val1);
                });
            });
        });
        capacityPlaningPage.verifyGraphSectionElements();
        capacityPlaningPage.verifyTimeSelectorElements();
    },

    verifyTimeSelectorElements: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorPlus());
        commonAssetsControlsPage.verifyDefaultSelectedInterval("1d");
        commonAssetsControlsPage.verifyTimeSelectorIntervals(commonData.capacityPlaningGraphTimeSelectors);
    },

    verifyGraphSectionElements: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getLegendByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.usedLegendColor));
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLegendTextByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.usedLegendColor),commonData.cpLegends[0]);
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getLegendByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.provisionedLegendColor));
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLegendTextByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.provisionedLegendColor),commonData.cpLegends[1]);
               webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getLegendByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.fullCapaityLegendColor));
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLegendTextByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.fullCapaityLegendColor),commonData.cpLegends[3]);
        //webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getRadientGraphByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.usedLegendColor));
        //webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getGraphByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.provisionedLegendColor));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getRadientGraphByColor(capacityPlaningPage.graphSection(),capacityPlaningPage.fullCapacityGraphColor));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getXYAxisLabels(capacityPlaningPage.graphSection()).get(0));
    },



    graphSection: function () {
        return $('.vx-transparent-container-cp-analytics-panel3');
    },

    capacitySummary: function () {
        return $('.vx-chart-details');
    },

    selectedView: function () {
        return capacityPlaningPage.top10SpaceConsumingVolumesSection().$('.active');
    },

    getVXGrid: function (i) {
        return $$('vx-grid').get(i);
    },

    volumeUsageSummarySection: function () {
        return $('.vx-transparent-container-cp-analytics-panel1');
    },

    top10SpaceConsumingVolumesSection: function () {
        return $('.vx-transparent-container-cp-analytics-panel2');
    },

    getVolumeSections: function (i) {
        return capacityPlaningPage.volumeUsageSummarySection().$$('.vx-cp-summary').get(i);
    },

    getVolumeSectionsText: function (i) {
        return capacityPlaningPage.getVolumeSections(i).$('label');
    },

    getVolumeCount: function (i) {
        return capacityPlaningPage.getVolumeSections(i).$('span');
    }

};
module.exports=capacityPlaningPage;
