/**
 * Created by Ajay(Vexata) on 29-06-2016.
 */

'use strict';
var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    volumePage=dependencies.getVolumePage(),
    dataForVolumes = dependencies.getDataForVolumes();

describe("Tests for performance monitor", function () {
    var volumeName;
    beforeAll(function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToVolumesPage();
    });

    beforeEach(function(){
        volumeName = webElementActionLibrary.getTimeStampAttached(dataForVolumes.volumeName);// appending volume name with time to make it unique and to avoid the duplicate object name error
    });

    for(var i=0;i<10000;i++){
        xit("Test to verify successful Volume Creation with size in MiB", function(){
            volumePage.createVolumeWithSizeUnit(volumeName,dataForVolumes.volumeDescription,dataForVolumes.volumeSize,dataForVolumes.MiB);
        });
    }

});

