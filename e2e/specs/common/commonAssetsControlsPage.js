/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var loginPage = dependencies.getLoginPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData();
    var frisby=require("frisby");

var commonAssetsControlsPage =  {

    readColor:"#A3C6E0",
    writeColor:"#B84BE8",
    whiteColor:"#FFFFFF",
    verticalSeparatorColor:"#7641D1",

    getLinkUsingUIsref: function (link) {
        return $('a[ui-sref="'+link+'"]');
    },

    getLinkUsingUIsrefInBreadcrumb: function (link) {
        return $('ol [ui-sref="'+link+'"]');
    },

    volumesLinkBreadCrumb: function(){
        return commonAssetsControlsPage.getLinkUsingUIsrefInBreadcrumb('volumes');
    },

    initiatorsLinkBreadCrumb: function(){
        return commonAssetsControlsPage.getLinkUsingUIsrefInBreadcrumb('initiators');
    },

    portsLinkBreadCrumb: function(){
        return commonAssetsControlsPage.getLinkUsingUIsrefInBreadcrumb('ports');
    },

    egLinkSubMenu: function(){
        return commonAssetsControlsPage.getLinkUsingUIsref('exportgroups');
    },

    saLinkSubMenu: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref('storageArray');
    },

    manageVolumesLink: function(){
        return commonAssetsControlsPage.getLinkUsingUIsref('volumes');
    },

    manageInitiatorsLink: function(){
        return commonAssetsControlsPage.getLinkUsingUIsref('initiators');
    },

    managePortsLink: function(){
        return commonAssetsControlsPage.getLinkUsingUIsref('ports');
    },

    assetsLogo: function () {
        return $('.logicalAsset');
    },

    selectedTabText: function(){
        return $$('[aria-selected="true"][role ="tab"]');
    },

    popUpTitle: function(){
        return $('[style*="display: block;"] .k-window-title');
    },

    notification: function () {
        return commonAssetsControlsPage.loginErrorNotifications().last();
    },

    loginErrorNotification: function () {
        return commonAssetsControlsPage.loginErrorNotifications().first();
    },

    loginErrorNotifications: function () {
        return $$('.k-notification');
    },

    invalidErrorMsgs: function () {
        return $$('.k-invalid-msg');
    },

    formErrors: function () {
        return commonAssetsControlsPage.invalidErrorMsgs();
    },

    getSearchBox: function (grid) {
        return grid.$('[ng-model*="search.nm"]');
    },

    getSearchBoxInForm: function () {
        return $$('vx-grid [ng-model*="search.nm"]');
    },

    applyNameFilter: function (name) {
        commonAssetsControlsPage.getNameFilterIcon().click();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.filterIconInDrillDown());
        webElementActionLibrary.type(commonAssetsControlsPage.filterInput(),name);
        commonAssetsControlsPage.filterBtn().click();
    },

    applyIdFilter: function (id) {
        commonAssetsControlsPage.getIdFilterIcon().click();
        commonAssetsControlsPage.filterIconInDrillDown().click();
        //webElementActionLibrary.mouseOver(commonAssetsControlsPage.filterIconInDrillDown());
        webElementActionLibrary.type(commonAssetsControlsPage.filterInput(),id);
        commonAssetsControlsPage.filterBtn().click();
    },

    filterBtn: function () {
        return $$('.k-primary').last();
    },

    filterInput: function () {
      return $('[data-bind="value:filters[0].value"]');
    },

    getNameFilterIcon: function () {
      return $$('th[data-field="name"] a span').last();
    },

    getIdFilterIcon: function () {
        return $$('th[data-field="id"] a span.k-i-arrowhead-s').last();
    },

    filterIconInDrillDown: function () {
        return $('.k-filter-item .k-i-arrow-e');
    },

    firstGridFilterIcon: function () {
      return  $('.k-i-filter');
    },

    getDetailedGridExpansionLink: function (row) {
        return row.$('.k-i-expand');
    },

    getDetailedGrid: function (grid) {
        return grid.$('.k-detail-cell');
    },

    getDisabledDeleteButton: function (grid) {
        return grid.$('.k-state-disabled .k-delete')
    },

    getEditButton: function (grid) {
        return grid.$('.k-i-edit');
    },

    getEditIcon: function (grid) {
        return grid.$('i[name="edit"]');
    },

    getColumnTitle: function (grid,dataTitle) {
        return grid.$('[data-title$="'+dataTitle+'"] .k-link');
    },

    getIdTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.idDataTitle);
    },

    getIDTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.iDDataTitle);
    },

    getHardwareTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.hardwareSummary);
    },

    getDGTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.dg);
    },

    getCapacityTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.capacity);
    },

    getHostTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.hostDataTitle);
    },

    getMAEventsTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.maEventsDataTitle);
    },

    getVersionTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.versionDataTitle);
    },


    getPortIdTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.portIdDataTitle);
    },

    getNameTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.nameDataTitle);
    },

    getPortListTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.portListDataTitle);
    },

    getNoOfPRTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfPRDataTitle);
    },

    getNoOfPRKeysTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfPRKeysDataTitle);
    },

    getDescriptionTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.descriptionDataTitle);
    },

    getVolumeIdTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.volumeIdDataTitle);
    },

    getVolumeUUIDTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.uuidDataTitle);
    },

    getSizeTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.sizeDataTitle);
    },

    getProvisionedTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.provisionedDataTitle);
    },

    getBWValueTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.bwValueDataTitle);
    },

    getBWPercentageTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.bwPercentageDataTitle);
    },

    getUtilizationTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.utilizationDataTitle);
    },

    getVolumeUsedTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.usedDataTitle);
    },

    getNoOfVGsTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfVGsDataTitle);
    },

    getIOReadsTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.ioReadsDataTitle);
    },

    getIOWritesTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.ioWritesDataTitle);
    },

    getRemoveButtonFirstRow: function (parentElement) {
        return parentElement.$$('.k-delete').first();
    },

    getRemoveButton: function (parentElement) {
        return parentElement.$('.k-grid-delete');
    },

    getITNexusButtonFirstRow: function (parentElement) {
        return parentElement.$$('button[name*="itnexus"]').first();
    },

    getNoOfVolumesTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfVolumesDataTitle);
    },

    getNoOfEGsTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfEGsDataTitle);
    },

    getWWNTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.wwnDataTitle);
    },

    getInitiatorTypeTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.initiatorTypeDataTitle);
    },

    getDiscoveryMethodTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.discoveryMethodDataTitle);
    },

    getNoOfIGsTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfIGsDataTitle);
    },

    getNoOfInitiatorsTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfInitiatorsDataTitle);
    },

    getNoOfPortsTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfPortsDataTitle);
    },

    getStateTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.stateDataTitle);
    },

    getTopologyTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.topologyDataTitle);
    },

    getTypeTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.typeDataTitle);
    },


    getESMStateTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.esmStateDataTitle);
    },

    getDGDriveStateTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.dgDriveStateDataTitle);
    },

    getBackUpTimeLeftTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.backUpTimeLeftDataTitle);
    },

    getChargeStatusTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.chargeStatusDataTitle);
    },

    getPortsIOCIdTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.portIOCIdDataTitle);
    },

    getPortPhysicalPortIdTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.physicalPortIdDataTitle);
    },

    getNoOfPGsTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.noOfPGsDataTitle);
    },

    getIdValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.idNGBindProp);
    },

    getNameValue: function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.nameNGBindProp);
    },

    getDescriptionValue: function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.descNGBindProp);
    },

    getProvisionedValue: function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.provisionedNGBindProp);
    },

    getUsedValue: function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.usedNGBindProp);
    },

    get1stColumnValue: function(parentElement){return parentElement.$('td:nth-child(1)')},
    get2ndColumnValue: function(parentElement){return parentElement.$('td:nth-child(2)')},
    get3rdColumnValue: function(parentElement){return parentElement.$('td:nth-child(3)')},
    get4thColumnValue: function(parentElement){return parentElement.$('td:nth-child(4)')},
    get5thColumnValue: function(parentElement){return parentElement.$('td:nth-child(5)')},
    get6thColumnValue: function(parentElement){return parentElement.$('td:nth-child(6)')},
    get7thColumnValue: function(parentElement){return parentElement.$$('td:nth-child(7)').last()},
    get8thColumnValue: function(parentElement){return parentElement.$('td:nth-child(8)')},
    get9thColumnValue: function(parentElement){return parentElement.$('td:nth-child(9)')},
    get10thColumnValue: function(parentElement){return parentElement.$('td:nth-child(10)')},
    get11thColumnValue: function(parentElement){return parentElement.$('td:nth-child(11)')},

    getDetailedGridNoOfPGs : function (parentElement) {
        return parentElement.$('td:nth-child(6)');
    },

    getDetailedGridNoOfIGs : function (parentElement) {
        return parentElement.$('td:nth-child(6)');
    },

    getPagerInfo: function (grid) {
        return grid.$('.k-pager-info');
    },

    getValueUsingNGBind: function (parentElement,ngBindProperty) {
        return parentElement.$('[ng-bind="'+ngBindProperty+'"]');
    },

    getNoOfVolumesValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.numOfVolsNGBindPropNGBindProp);
    },

    getVolumeUUIDValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.voluuidNGBindProp);
    },

    getIOReadsValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.ioReadsNGBindProp);
    },

    getIOWritesValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.ioWritesNGBindProp);
    },

    getPortsListValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.numOfInitiatorNGBindProp);
    },

    getNoOfInitiatorsValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.numOfInitiatorNGBindProp);
    },

    getWWNValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.memberIdNGBindProp);
    },

    getInitiatorTypeValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.initiatorTypeNGBindProp);
    },

    getInitiatorMethodValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.initiatorConfigMethodNGBindProp);
    },

    getInitiatorLoggedInPortValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.loggedInPortNGBindProp);
    },

    getPortStateValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.stateNGBindProp);
    },

    getPortIOCIdValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.phyIoControllerIdNGBindProp);
    },

    getPortTypeValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.typeNGBindProp);
    },

    getPortPhysicalIdValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.phyPortIdNGBindProp);
    },

    getNoOfPortsValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.numPortsNGBindProp);
    },

    getEGCombinationValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.currEGCopyNGBindProp);
    },

    getITNInitiatorsValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.initiatorCountViaITNexusNGBindProp);
    },

    getITNVolumesValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.volumeCountViaITNexusNGBindProp);
    },

    getITNPortsValue : function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.portCountViaITNexusNGBindProp);
    },

    getPortListValue: function(parentElement){
        return commonAssetsControlsPage.getValueUsingNGBind(parentElement,commonData.portListNGBindProp);
    },

    searchBox: function () {
        return element(by.model('serachValue'));

    },

    closeIcon: function(){
        return commonAssetsControlsPage.notification().$('.notification-remove-icon');
    },

    closeWindow: function () {
        $$('.k-window .k-i-close').last().click();
        webElementActionLibrary.waitForAngular();
    },

    assetsHeader: function(){
        return $('.vx-title-header');
    },

    activeHeaderInBreadCrumb: function(){
        return $('ol li.active');
    },

    getSectionHeader: function(section){
        return section.$('.vx-panel-heading');
    },

    getLabelHeadings: function (section) {
        return section.$$('.vx-label-heading');
    },


    getAlarmsSection: function (parent) {
        return parent.$('[ng-show*="alarmCount"]');
    },

    getWarningsSection: function (parent) {
        return parent.$('[ng-show*="warningCount"]');
    },

    getErrorsSection: function (parent) {
        return parent.$('[ng-show*="errorCount"]');
    },

    getCriticalSection: function (parent) {
        return parent.$('[ng-show*="criticalCount"]');
    },

    getEventText: function (parent) {
        return parent.$('label');
    },

    getEventIcon: function (parent) {
        return parent.$('i');
    },

    getVXButton: function () {
        return $('.vx-button');
    },

    getAlarmIcon: function (parent) {
        return commonAssetsControlsPage.getEventIcon(parent);
    },

    getCriticalIcon: function (parent) {
        return commonAssetsControlsPage.getEventIcon(parent);
    },

    getAlarmText: function (parent) {
        return commonAssetsControlsPage.getEventText(parent);
    },

    getWarningIcon: function (parent) {
        return commonAssetsControlsPage.getEventIcon(parent);
    },

    getWarningText: function (parent) {
        return commonAssetsControlsPage.getEventText(parent);
    },

    getErrorIcon: function (parent) {
        return commonAssetsControlsPage.getEventIcon(parent);
    },

    getErrorText: function (parent) {
        return commonAssetsControlsPage.getEventText(parent);
    },

    getReadAvgValue: function (parent) {
        return commonAssetsControlsPage.getAVGLabels(parent).get(2);
    },

    getWriteAvgValue: function (parent) {
        return commonAssetsControlsPage.getAVGLabels(parent).get(4);
    },

    getTooltip: function () {
        return $$('.k-tooltip-content.ng-scope');
    },

    getGraphTooltip: function () {
        return $$('.k-chart-tooltip');
    },

    getAVGLabels: function (parent) {
        return parent.$$('label');
    },

    getReadAVGLabel: function (parent) {
        return commonAssetsControlsPage.getAVGLabels(parent).get(1);
    },

    getWriteAVGLabel: function (parent) {
        return commonAssetsControlsPage.getAVGLabels(parent).get(3);
    },

    getYAxisLinesByColor: function (parent,color) {
        return  parent.$$('g path[stroke="'+color+'"][stroke-width="1"]');
    },

    getKendoPaginationDropDown: function (grid) {
        return grid.$('.k-dropdown-wrap');
    },

    getGraphByColor: function (parent, color) {
        return parent.$('path[stroke="'+color+'"][stroke-width="2"]');
    },

    getRadientGraphByColor: function (parent,color) {
        return parent.$('path[fill="'+color+'"]');
    },

    getWriteGraphDataPoint: function (parent) {
        return parent.$$('circle').get(3);
    },

    getReadGraphDataPoint: function (parent) {
        return parent.$$('circle').get(2);
    },

    getLegendByColor: function (parent,color) {
        return parent.$('circle[stroke="'+color+'"][stroke-width="1"][fill="'+color+'"]');
    },

    getLegendTextByColor: function (parent,color) {
        return parent.$('circle[stroke="'+color+'"][stroke-width="1"][fill="'+color+'"] + text');
    },

    getXYAxisLabels: function (parent) {
        return parent.$$('g text[fill="'+commonAssetsControlsPage.whiteColor+'"]');
    },

    getCheckbox: function (parentGrid) {
        return parentGrid.$('label[for*="rid"]');
    },

    timeSelectorMinus: function () {
        return $('.minus');
    },

    timeSelectorMinusDisabled: function () {
        return $('.minus.disabled');
    },

    timeSelectorPlus: function () {
        return $('.plus');
    },

    timeSelectorPlusDisabled: function () {
        return $('.plus.disabled');
    },



    timeSelectorIntervals: function() {
        return $$('.time-range span span');
    },

    getGridFirstRowColumns: function (grid) {
        return grid.$("tbody tr").$$("td");
    },

    getGridRows:function (grid) {
        return grid.$$("tbody tr");
    },


    selectedTimeInterval: function () {
        return $('.selection-circle');
    },

    selectAllOptionInKendoPagination: function (grid) {
        grid.element(by.css('.k-dropdown-wrap')).click();
        $$('li[data-offset-index="0"]').first().click();
    },

    kendoDropDown: function () {
        return $('.k-dropdown');
    },

    kendoDDOptions: function (parent) {
        return parent.$$('li[data-offset-index]');
    },

    getKendoDDOption: function (parent,num) {
        return parent.$('li[data-offset-index="'+num+'"]');
    },

    autoRefreshDropDown: function () {
        return $('.footerSelect');
    },

    detailedGrid: function () {
        return $('.k-detail-cell');
    },

    eventsLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("events");
    },

    dashboardLogo: function () {
        return $('a .dashboard');
    },

    vexataLogo: function () {
      return $('[ng-click="gotoHomePage()"]');
    },

    local: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("dashboard");
    },

    maDashboard: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("arrayManagement");
    },

    policyLogo: function () {
      return commonAssetsControlsPage.getLinkUsingUIsref("policyManagement");
    },

    nodeLogo: function () {
        return $('label.node');
    },

    analyticsLogo: function () {
        return $('label.analytics');
    },

    faultLogo: function () {
        return $('label.events');
    },

    systemLogo: function () {
        return $('label.system');
    },

    healthDetailsLink: function () {
        return $('.sub-menu [ui-sref="healthDetails"]');
        //return commonAssetsControlsPage.getLinkUsingUIsref("healthDetails");
    },

    settingsLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("settings");
    },

    hardwareDetailsLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("chassisDetails");
    },

    dgDetailsLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("driveGroupDetails");
    },

    eventCatalogLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("eventsCatalogDetails");
    },

    eventPolicyLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("eventsConfiguration");
    },

    detailedGridLabel: function(){
        return commonAssetsControlsPage.detailedGrid().$('.k-pager-info');
    },

    advancedDropDown: function () {
        return $('.downArrow');
    },

    volumeAnalyticsLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("volumeAnalytics");
    },

    capacityPlaningLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("capacityPlanning");
    },

    portAnalyticsLink: function () {
        return commonAssetsControlsPage.getLinkUsingUIsref("portAnalytics");
    },

    getGridPagerInfoLabel: function (grid) {
        return grid.$('.k-pager-info');
    },

    getTimeTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.timeDataTitle);
    },

    getSourceTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.sourceDataTitle);
    },

    getSeverityTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.severityDataTitle);
    },

    getCategoryTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.categoryDataTitle);
    },

    getMessageTitle: function (grid) {
        return commonAssetsControlsPage.getColumnTitle(grid,commonData.messageDataTitle);
    },

    getAcknowledgedTitle: function (parent) {
        return parent.$('[for="ackCheckAll"]');
    },

    getClearedTitle: function (parent) {
        return parent.$('[for="clrCheckAll"]');
    },

    getExportToExcelButton: function (grid) {
        return grid.$('.k-grid-excel');
    },

    getExportToPdfButton: function (grid) {
        return grid.$('.k-grid-pdf');
    },

    deleteSelectedButton:  function (parentGrid) {
        return parentGrid.$('.k-i-delete');
    },

    getDeletePasswordInput: function (popup) {
        return popup.$('input[type="password"]');
    },

    getDeleteButton: function (popup) {
        return popup.$$('[ng-click*="elete"]').last();
    },

    exportToExcelButton: function () {
        return $('.k-grid-excel');
    },

    exportToPdfButton: function () {
        return $('.k-grid-pdf');
    },

    loadLoginUrlAndLoginAsAdmin: function () {
        webElementActionLibrary.loadUrl(commonData.homePageUrl,20000);
        loginPage.loginAs(commonData.validUserName, commonData.validPassword);
        //browser.sleep(2000);
    },

    loadLoginUrlAndLoginAsReadOnlyUser: function () {
        webElementActionLibrary.loadUrl(commonData.homePageUrl,20000);
        loginPage.loginAs(commonData.readOnlyUserName, commonData.readOnlyUserPassword);
        browser.sleep(2000);
    },

    mouseOverOnAssetsLogo: function(){
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.assetsLogo());
        webElementActionLibrary.waitForAngular();
    },

    mouseOverOnSystemLogo: function(){
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.systemLogo());
        webElementActionLibrary.waitForAngular();
    },

    navigateToHealthDetailsPage: function () {
        commonAssetsControlsPage.mouseOverOnSystemLogo();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.healthDetailsLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.healthDetailsHeader);
    },

    navigateToSettingsPage: function () {
        commonAssetsControlsPage.mouseOverOnSystemLogo();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.settingsLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.settingsHeader);
    },

    navigateToHardwareDetailsPage: function () {
        commonAssetsControlsPage.mouseOverOnNodeLogo();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.hardwareDetailsLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.hardwareDetailsHeader);
    },

    navigateToDGDetailsPage: function () {
        commonAssetsControlsPage.mouseOverOnNodeLogo();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.dgDetailsLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.dgHeader);
    },

    navigateToEventCatalogPage: function () {
        commonAssetsControlsPage.mouseOverOnFaultLogo();
        commonAssetsControlsPage.eventCatalogLink().click();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.eventCatalogHeader);
    },

    navigateToEventPolicyPage: function () {
        commonAssetsControlsPage.mouseOverOnFaultLogo();
        commonAssetsControlsPage.eventPolicyLink().click();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.eventPolicyHeader);
    },

    navigateToEventsPage: function(){
        commonAssetsControlsPage.mouseOverOnFaultLogo();
        commonAssetsControlsPage.eventsLink().click();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.eventsHeader);
    },

    navigateToVolumeAnalyticsPage: function () {
        commonAssetsControlsPage.mouseOverOnAnalyticsLogo();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.volumeAnalyticsLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
    },

    navigateToCapacityPlaningPage: function () {
        commonAssetsControlsPage.mouseOverOnAnalyticsLogo();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.capacityPlaningLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
    },

    navigateToPortAnalyticsPage: function () {
        commonAssetsControlsPage.mouseOverOnAnalyticsLogo();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.portAnalyticsLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
    },

    verifyTimeSelectorIntervals: function (expectedIntervals) {
        var intervals = commonAssetsControlsPage.timeSelectorIntervals();
        expect(intervals.count()).toBe(Number(expectedIntervals.length));
        var j=0;
        intervals.count().then(function (count) {
            for(var i=0;i<count;i++){
                intervals.get(i).getText().then(function (val) {
                    expect(val).toBe(expectedIntervals[j]);
                    j++;
                });
            }
        });
    },

    verifyDefaultSelectedInterval: function (selected) {
        commonAssetsControlsPage.verifySelectedInterval(selected);
    },

    verifySelectedInterval: function (selected) {
        commonAssetsControlsPage.selectedTimeInterval().getText().then(function (val) {
            expect(val).toBe(selected);
            console.log("selected is "+val);
        })
    },

    verifyPresenceOfTimeSelectorElements: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorMinus());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorPlus());
        commonAssetsControlsPage.verifyDefaultSelectedInterval(commonData.defaultGraphTimeSelectors[0]);
        commonAssetsControlsPage.verifyTimeSelectorIntervals(commonData.defaultGraphTimeSelectors);
    },

    verifyPresenceOfTimeSelectorElementsForPortAnalytics: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorMinus());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorPlus());
        commonAssetsControlsPage.verifyDefaultSelectedInterval(commonData.portAnalyticsGraphTimeSelectors[0]);
        commonAssetsControlsPage.verifyTimeSelectorIntervals(commonData.portAnalyticsGraphTimeSelectors);
    },



    verifyPlusMinusIconClickChanges: function () {
        commonAssetsControlsPage.selectTimeSelectorAndVerifySelected(commonData.defaultGraphTimeSelectors[0]);
        for(var i=0;i<7;i++){
            commonAssetsControlsPage.timeSelectorPlus().click();
            webElementActionLibrary.waitForAngular();
            webElementActionLibrary.wait(2);
            commonAssetsControlsPage.verifySelectedInterval(commonData.defaultGraphTimeSelectors[i+1]);
        }
        for(var j=6;j>=0;j--){
            commonAssetsControlsPage.timeSelectorMinus().click();
            webElementActionLibrary.waitForAngular();
            webElementActionLibrary.wait(2);
            commonAssetsControlsPage.verifySelectedInterval(commonData.defaultGraphTimeSelectors[j]);
        }
    },

    verifyGraphAndLabelsOnChangeOfTimeSelector: function (section,name) {
        //commonAssetsControlsPage.offAutoRefresh();
        commonAssetsControlsPage.selectTimeSelectorAndVerifySelected(commonData.defaultGraphTimeSelectors[0]);
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorPlus());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorMinusDisabled());
        commonAssetsControlsPage.verifyGraphLabelChangesTimeSelectorChanges(section,commonData.defaultGraphTimeSelectors[0]);
        commonAssetsControlsPage.verifyGraphSectionElements(section,name);
        for(var i=1;i<7;i++){
            commonAssetsControlsPage.selectTimeSelectorAndVerifySelected(commonData.defaultGraphTimeSelectors[i]);
            webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorPlus());
            webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorMinus());
            if(i==1)
                commonAssetsControlsPage.verifyGraphLabelChangesTimeSelectorChanges(section,commonData.defaultGraphTimeSelectors[i]);
            else
                commonAssetsControlsPage.verifyGraphLabelChangesTimeSelectorChanges(section,commonData.defaultGraphTimeSelectors[i]);
            commonAssetsControlsPage.verifyGraphSectionElements(section,name);
        }
        commonAssetsControlsPage.selectTimeSelectorAndVerifySelected(commonData.defaultGraphTimeSelectors[7]);
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorPlusDisabled());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorPlus());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.timeSelectorMinus());
        commonAssetsControlsPage.verifyGraphLabelChangesTimeSelectorChanges(section,commonData.defaultGraphTimeSelectors[7]);
        commonAssetsControlsPage.verifyGraphSectionElements(section,name);
    },

    verifyGridColumnsValuesExistence: function (section, noOfColumns, blankColumnList) {
        commonAssetsControlsPage.getGridRows(section).count().then(function (val) {
            if(val!="0"){
                var columns = commonAssetsControlsPage.getGridFirstRowColumns(section);
                var j,k=0;
                columns.count().then(function (count) {
                    expect(count).toBe(noOfColumns);
                    for(var i=0;i<count;i++) {
                        columns.get(i).getText().then(function (val) {
                            if(blankColumnList==undefined){
                                expect(val).not.toBe("");
                            }
                            else {
                                for (j = 0; j < blankColumnList.length; j++) {
                                    //console.log(blankColumnList[j]+","+k);
                                    if (blankColumnList[j]==k)
                                        expect(val).toBe("");
                                    else
                                        expect(val).not.toBe("");
                                }
                            }
                            k++;
                        });
                    }
                });
            }
        });
    },

    mouseOverOnAnalyticsLogo: function () {
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.analyticsLogo());
    },

    mouseOverOnFaultLogo: function () {
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.faultLogo());
    },

    mouseOverOnNodeLogo: function(){
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.nodeLogo());
        webElementActionLibrary.waitForAngular();
    },

    navigateToDashboardPage: function(){
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.dashboardLogo());
        commonAssetsControlsPage.local().click();
        webElementActionLibrary.waitForAngular();
    },

    navigateToMADashboardPage: function(){
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.dashboardLogo());
        commonAssetsControlsPage.maDashboard().click();
        webElementActionLibrary.waitForAngular();
    },

    navigateToPMPage: function(){
        commonAssetsControlsPage.policyLogo().click();
        webElementActionLibrary.waitForAngular();
    },

    mouseOverOnAdvanced: function(){
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.advancedDropDown());
        webElementActionLibrary.waitForAngular();
    },

    loginAsValidUser: function(){
        webElementActionLibrary.loadUrl(commonData.homePageUrl);
        loginPage.loginAs(commonData.validUserName, commonData.validPassword);
    },

    navigateToVolumesPage: function(){
        commonAssetsControlsPage.mouseOverOnAssetsLogo();
        commonAssetsControlsPage.mouseOverOnAdvanced();
        commonAssetsControlsPage.manageVolumesLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.volumesTabText);

    },

    navigateToExportGroupsPage: function(){
        webElementActionLibrary.loadPageUrl(commonData.egPageUrl());
        // commonAssetsControlsPage.mouseOverOnAssetsLogo();
        // commonAssetsControlsPage.egLinkSubMenu().click();
        // webElementActionLibrary.waitForAngular();
        // webElementActionLibrary.mouseOver(commonAssetsControlsPage.dashboardLogo());
        //commonAssetsControlsPage.mouseOverOnAssetsLogo();
        // commonAssetsControlsPage.egLinkSubMenu().click();
        // webElementActionLibrary.loadUrl(commonData.egPageUrl(),20000);
        webElementActionLibrary.waitForAngular();
        // webElementActionLibrary.mouseOver(commonAssetsControlsPage.dashboardLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.exportGroupsHeader);
    },

    navigateToInitiatorsPage: function(){
        commonAssetsControlsPage.mouseOverOnAssetsLogo();
        commonAssetsControlsPage.mouseOverOnAdvanced();
        commonAssetsControlsPage.manageInitiatorsLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.initiatorsTabText);
    },

    navigateToPortsPage: function(){
        commonAssetsControlsPage.mouseOverOnAssetsLogo();
        commonAssetsControlsPage.mouseOverOnAdvanced();
        commonAssetsControlsPage.managePortsLink().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.vexataLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.portsTabText);
    },

    navigateToSAPage: function(){
        commonAssetsControlsPage.mouseOverOnAssetsLogo();
        commonAssetsControlsPage.saLinkSubMenu().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.dashboardLogo());
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.assetsHeader(),commonData.saHeader);
    },



    verifyCommonElementsInLogicalAssetsPage:  function(){ // verifies bread crumb, search box, grid pagination and drag and drop column
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.volumesLinkBreadCrumb());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.initiatorsLinkBreadCrumb());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.portsLinkBreadCrumb());
    },

    verifyBreadCrumbLinkNavigation: function(){ //verifies the navigation between assets
        commonAssetsControlsPage.initiatorsLinkBreadCrumb().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.initiatorsTabText);
        commonAssetsControlsPage.portsLinkBreadCrumb().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.portsTabText);
        commonAssetsControlsPage.volumesLinkBreadCrumb().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.volumesTabText);
    },

    selectTabUsingLabel: function (label) {
        webElementActionLibrary.getElementByLabel(label).click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.wait(2);
    },

    verifyGraphSectionElements: function (section,name) {
        //commonAssetsControlsPage.offAutoRefresh();
        webElementActionLibrary.verifyElementsCount(commonAssetsControlsPage.getYAxisLinesByColor(section,commonAssetsControlsPage.whiteColor),1);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getWriteAVGLabel(section),commonData.write);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getReadAVGLabel(section),commonData.read);
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.getWriteAvgValue(section));
        webElementActionLibrary.wait(1);
        commonAssetsControlsPage.getTooltip().getText().then(function (val) {
            console.log(name+" write avg tool tip value is "+val);
            webElementActionLibrary.mouseOver(commonAssetsControlsPage.getReadAvgValue(section));
            webElementActionLibrary.wait(1);
            commonAssetsControlsPage.getTooltip().getText().then(function (val1) {
                console.log(name+" read avg tool tip value is "+val1);
            });
        });
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getWriteAvgValue(section));
        commonAssetsControlsPage.getWriteAvgValue(section).getText().then(function (val) {
            if (val.charAt(0) != "0") {
                webElementActionLibrary.mouseOver(commonAssetsControlsPage.getWriteGraphDataPoint(section));
                webElementActionLibrary.wait(2);
                //webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getGraphTooltip());
                commonAssetsControlsPage.getGraphTooltip().getText().then(function (val) {
                    console.log(name + " write graph tool tip value is " + val);
                });
            }
        });
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getReadAvgValue(section));
        commonAssetsControlsPage.getReadAvgValue(section).getText().then(function (val) {
            if(val.charAt(0)!="0") {
                webElementActionLibrary.mouseOver(commonAssetsControlsPage.getReadGraphDataPoint(section));
                webElementActionLibrary.wait(1);
                //webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getGraphTooltip());
                commonAssetsControlsPage.getGraphTooltip().getText().then(function (val1) {
                    console.log(name + " read graph tool tip value is " + val1);
                });
            }
        });
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getGraphByColor(section,commonAssetsControlsPage.readColor));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getGraphByColor(section,commonAssetsControlsPage.writeColor));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getLegendByColor(section,commonAssetsControlsPage.readColor));
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLegendTextByColor(section,commonAssetsControlsPage.readColor),commonData.read);
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getLegendByColor(section,commonAssetsControlsPage.writeColor));
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLegendTextByColor(section,commonAssetsControlsPage.writeColor),commonData.write);
        //webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getXYAxisLabels(section));
        //commonAssetsControlsPage.selectAutoRefreshInterval(commonData.autoRefreshDropDown[1]);
    },

    verifyGraphLabelChangesTimeSelectorChanges: function (section,timeSelector) {
        commonAssetsControlsPage.getYAxisLinesByColor(section,commonAssetsControlsPage.verticalSeparatorColor).count().then(function (stepCount) {
            commonAssetsControlsPage.getXYAxisLabels(section).get(0).getText().then(function (firstXAxisLabel) {
                commonAssetsControlsPage.getXYAxisLabels(section).get(stepCount).getText().then(function (lastXAxisLabel){
                    console.log(timeSelector);
                    console.log(firstXAxisLabel+","+lastXAxisLabel);
                    var firstHours,firstMinutes,lastHours,lastMinutes,firstDay,lastDay,dayDiff,hrDiff,minDiff;
                    if(timeSelector==commonData.defaultGraphTimeSelectors[4]){
                        commonAssetsControlsPage.getXYAxisLabels(section).get(stepCount-1).getText().then(function (lastButOneXAxisLabel){
                            firstHours=firstXAxisLabel.split(":")[0];
                            lastHours=lastButOneXAxisLabel.split(":")[0];
                            expect(3<(new Date((new Date("","","",lastHours).getTime()) - new Date("","","",firstHours).getTime()).getUTCHours())<=5).toBeTruthy();
                        });
                    }
                    else if(timeSelector==commonData.defaultGraphTimeSelectors[6] || timeSelector==commonData.defaultGraphTimeSelectors[5] || timeSelector==commonData.defaultGraphTimeSelectors[7]){
                        if(timeSelector==commonData.defaultGraphTimeSelectors[5]){
                            firstDay=firstXAxisLabel.split("/")[1].split(" ")[0];
                            lastDay=lastXAxisLabel.split("/")[1].split(" ")[0];
                            console.log(firstDay+","+lastDay);
                            console.log((new Date(new Date("", "", lastDay).getTime() - new Date("", "", firstDay).getTime()).getUTCDate()));
                            expect(1<=(new Date(new Date("", "", lastDay).getTime() - new Date("", "", firstDay).getTime()).getUTCDate())<=2).toBeTruthy();
                        }
                        else{
                            if(timeSelector==commonData.defaultGraphTimeSelectors[6]) {
                                commonAssetsControlsPage.getXYAxisLabels(section).get(stepCount-1).getText().then(function (lastButOneXAxisLabel) {
                                    firstDay = firstXAxisLabel.split("/")[1];
                                    lastDay = lastButOneXAxisLabel.split("/")[1];
                                    console.log(firstDay+","+lastDay);
                                    console.log((new Date(new Date("", "", lastDay).getTime() - new Date("", "", firstDay).getTime()).getUTCDate()));
                                    expect(5<=(new Date(new Date("", "", lastDay).getTime() - new Date("", "", firstDay).getTime()).getUTCDate())<=7).toBeTruthy();
                                });
                            }

                            else {
                                commonAssetsControlsPage.getXYAxisLabels(section).get(stepCount-1).getText().then(function (lastButOneXAxisLabel) {
                                    firstDay = firstXAxisLabel.split("/")[1];
                                    lastDay = lastXAxisLabel.split("/")[1];
                                    console.log(firstDay+","+lastDay);
                                    expect((new Date(new Date("", lastDay, "").getTime() - new Date("", firstDay, "").getTime()).getUTCDate())<=1).toBeTruthy();
                                });
                            }
                        }
                    }
                    else{
                        firstHours=firstXAxisLabel.split(":")[0];
                        firstMinutes=firstXAxisLabel.split(":")[1].split(":")[0];
                        lastHours=lastXAxisLabel.split(":")[0];
                        lastMinutes=lastXAxisLabel.split(":")[1].split(":")[0];
                        var diff =new Date(new Date("","","",lastHours,lastMinutes) - new Date("","","",firstHours,firstMinutes)).getUTCMinutes();
                        if(timeSelector==commonData.defaultGraphTimeSelectors[0])
                            expect(diff>=4).toBeTruthy();
                        else if(timeSelector==commonData.defaultGraphTimeSelectors[1])
                            expect(diff>=12).toBeTruthy();
                        else if(timeSelector==commonData.defaultGraphTimeSelectors[2])
                            expect(diff>=25).toBeTruthy();
                        else if(timeSelector==commonData.defaultGraphTimeSelectors[3])
                            expect(diff>=50).toBeTruthy();
                    }
                });
            });
        });

    },



    selectTimeSelectorAndVerifySelected: function (selector) {
        webElementActionLibrary.getElementByVisibleText(selector).click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.wait(2);
        commonAssetsControlsPage.verifySelectedInterval(selector);
    },

    offAutoRefresh: function () {
        webElementActionLibrary.selectByVisibleText(commonAssetsControlsPage.autoRefreshDropDown(),commonData.autoRefreshDropDown[0]);
    },

    selectAutoRefreshInterval: function (interval) {
        webElementActionLibrary.selectByVisibleText(commonAssetsControlsPage.autoRefreshDropDown(),interval);
    },

    verifyEventIconPresence: function (section,icon) {
        section.getAttribute("class").then(function (val) {
            if (val.indexOf("ng-hide") == false) {
                webElementActionLibrary.verifyPresenceOfElement(icon);
            }
        });
    },

    verifyEventTextPresence: function (section,textElement) {
        section.getAttribute("class").then(function (val) {
            if (val.indexOf("ng-hide") == false) {
                webElementActionLibrary.verifyPresenceOfElement(textElement);
            }
        });
    },

    verifyEventIconsSectionElementsPresence: function (parent,temp) {//temp =1 represents other sections like hardware summary section
        if(temp==1){
            //commonAssetsControlsPage.verifyEventIconPresence(commonAssetsControlsPage.getAlarmsSection(parent),commonAssetsControlsPage.getAlarmIcon(parent));
            //commonAssetsControlsPage.verifyEventTextPresence(commonAssetsControlsPage.getAlarmsSection(parent),commonAssetsControlsPage.getAlarmText(parent));
            commonAssetsControlsPage.verifyEventTextPresence(commonAssetsControlsPage.getWarningsSection(parent),commonAssetsControlsPage.getWarningText(parent));
            commonAssetsControlsPage.verifyEventTextPresence(commonAssetsControlsPage.getErrorsSection(parent),commonAssetsControlsPage.getErrorText(parent));
        }
        else
            commonAssetsControlsPage.verifyEventIconPresence(commonAssetsControlsPage.getCriticalSection(parent),commonAssetsControlsPage.getCriticalIcon(parent));
        commonAssetsControlsPage.verifyEventIconPresence(commonAssetsControlsPage.getWarningsSection(parent),commonAssetsControlsPage.getWarningIcon(parent));
        commonAssetsControlsPage.verifyEventIconPresence(commonAssetsControlsPage.getErrorsSection(parent),commonAssetsControlsPage.getErrorIcon(parent));
    },

    navigateBetweenGraphPages: function () {
        commonAssetsControlsPage.navigateToPortAnalyticsPage();
        commonAssetsControlsPage.navigateToVolumeAnalyticsPage();
        commonAssetsControlsPage.navigateToCapacityPlaningPage();
        commonAssetsControlsPage.navigateToDashboardPage();
    },

    verifyPresenceOfExportButtons: function (section) {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getExportToPdfButton(section));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getExportToExcelButton(section));
    },

    searchByValueAndVerifyPresenceInSearchResults: function (grid,value) {
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBoxInForm(),value);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getGridFirstRowColumns(grid),value);
    },

    verifyGridItemsCount: function (grid, expectedCount) {
        commonAssetsControlsPage.getGridPagerInfoLabel(grid).getText().then(function (val) {
            var count = val.split("of")[1].split("items")[0].trim();
            expect(expectedCount).toBe(count);
        });
    },

    selectAndDeleteAsset: function (parentGrid,popup) {
        commonAssetsControlsPage.getCheckbox(parentGrid).click();
        commonAssetsControlsPage.deleteSelectedButton(parentGrid).click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.wait(2);
        webElementActionLibrary.type(commonAssetsControlsPage.getDeletePasswordInput(popup),commonData.validPassword);
        commonAssetsControlsPage.getDeleteButton(popup).click();
        webElementActionLibrary.waitForAngular();
    },

    getSuccessfullyCreatedText: function () {
        return 'Successfully Created!'
    },

    getSuccessfullyUpdatedText: function () {
        return 'Successfully Updated!'
    },

    getSuccessfullyDeletedText: function () {
        return 'Successfully Deleted!'
    },

    getIdFromNotification: function (notification) {
        var id = notification.split('#(')[1].split(')')[0];
        return id;
    },


    setServiceHeader: function () {
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        frisby.globalSetup({
            request: {
                headers: {
                    'Authorization': commonData.validAdminAuthentication,
                    'Content-Type': commonData.validContentType
                },
                strictSSL: false,
            }
        });
    },

    deletePopup:function () {
        return $('#confirm_popup_window');
    },

    deletePopupOkButton:function () {
        return commonAssetsControlsPage.deletePopup().$$('button').get(0);
    },

    deleteRecord:function () {
        commonAssetsControlsPage.deletePopupOkButton().click();
        webElementActionLibrary.wait(2);
    },

    getTableContent: function (grid) {
        return grid.$$("table").get(1);
    }
};

module.exports = commonAssetsControlsPage;