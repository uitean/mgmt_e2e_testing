/**
 * Created by Ajay(Vexata) on 01-04-2016.
 */

'use strict';

var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    nodeDetailsPage = dependencies.getNodeDetailsPage();


describe("Tests for verifying presence of hardware details page elements", function() {

    describe("Tests for readonly user", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
            commonAssetsControlsPage.navigateToHardwareDetailsPage();
        });

        it("Verifying node page header", function () {
            nodeDetailsPage.verifyNodePageHeader();
        });

        it("Verifying events grid titles and values for all, errors, warnings and alarms ", function () {
            nodeDetailsPage.verifyEventsGrid();
        });

        it("Verifying inventory grid titles and values for all the tabs ", function () {
            nodeDetailsPage.verifyPresenceOfInventoryTabElements();
        });

        it("Verifying all svg elements irrespective of their status ", function () {
            nodeDetailsPage.verifyPresenceOfNodeSVGElements();
        });

    });

    describe("Tests for admin", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToHardwareDetailsPage();
        });

        it("Verifying node page header", function () {
            nodeDetailsPage.verifyNodePageHeader();
        });

        it("Verifying events grid titles and values for all, errors, warnings and alarms ", function () {
            nodeDetailsPage.verifyEventsGrid();
        });

        it("Verifying inventory grid titles and values for all the tabs ", function () {
            nodeDetailsPage.verifyPresenceOfInventoryTabElements();
        });

        it("Verifying all svg elements irrespective of their status ", function () {
            nodeDetailsPage.verifyPresenceOfNodeSVGElements();
        });

    });

});
