/**
 * Created by Ajay(Vexata) on 01-04-2016.
 */

'use strict';

var  commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData();

var nodeDetailsPage= {

    verifyNodePageHeader: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.hardwareDetailsHeader);
    },

    verifyEventsGrid: function () {
        nodeDetailsPage.selectAndVerifyEventsSubTabElements(commonData.errorsTabText);
        nodeDetailsPage.selectAndVerifyEventsSubTabElements(commonData.warningsTabText);
        nodeDetailsPage.selectAndVerifyEventsSubTabElements(commonData.allTabText);
        nodeDetailsPage.selectAndVerifyEventsSubTabElements(commonData.alarmsTabText);
    },

    verifyPresenceOfInventoryTabElements: function () {
        nodeDetailsPage.selectAndVerifyPortsTabElements();
        nodeDetailsPage.selectAndVerifyESMsTabElements();
        nodeDetailsPage.selectAndVerifyFansTabElements();
        nodeDetailsPage.selectAndVerifySCAPTabElements();
        nodeDetailsPage.selectAndVerifyNetworksTabElements();
        nodeDetailsPage.selectAndVerifyPowerTabElements();
        nodeDetailsPage.selectAndVerifyControllersTabElements();
    },

    selectAndVerifyEventsSubTabElements: function (tabText) {
        nodeDetailsPage.getTabElement(tabText).click();
        webElementActionLibrary.wait(2);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.selectedTabText().first(),tabText);
        nodeDetailsPage.verifyEventsSectionColumnTitlesAndValues(tabText);
    },

    selectInventoryTabAndVerifyTabTextSelected: function (tabText) {
        nodeDetailsPage.getTabElement(tabText).click();
        webElementActionLibrary.wait(2);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.selectedTabText().last(),tabText);
    },

    selectAndVerifyControllersTabElements: function () {
        nodeDetailsPage.selectInventoryTabAndVerifyTabTextSelected(commonData.ioControllersTabText);
        //commonAssetsControlsPage.verifyPresenceOfExportButtons(nodeDetailsPage.controllersContent());
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIDTitle(nodeDetailsPage.controllersContent()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getUpSinceTitle(nodeDetailsPage.controllersContent()),commonData.upSinceTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getSystemTimeTitle(nodeDetailsPage.controllersContent()),commonData.sysTimeTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(nodeDetailsPage.controllersContent()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getHealthStatusTitle(nodeDetailsPage.controllersContent()),commonData.healthStatusTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getPresentTitle(nodeDetailsPage.controllersContent()),commonData.presentTitle);
        //webElementActionLibrary.verifyExactText(nodeDetailsPage.getModelTitle(nodeDetailsPage.controllersContent()),commonData.modelTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getSWVersionTitle(nodeDetailsPage.controllersContent()),commonData.swVersionTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getMgmtRoleTitle(nodeDetailsPage.controllersContent()),commonData.mgmtRoleTitle);
        //webElementActionLibrary.verifyExactText(nodeDetailsPage.getBusMasterTitle(nodeDetailsPage.controllersContent()),commonData.busMasterTitle);
        //webElementActionLibrary.verifyExactText(nodeDetailsPage.getStatusTitle(nodeDetailsPage.controllersContent()),commonData.statusTitle);
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(nodeDetailsPage.controllersContent(),8);
    },

    selectAndVerifyPortsTabElements: function () {
        nodeDetailsPage.selectInventoryTabAndVerifyTabTextSelected(commonData.portsTabText);
        //commonAssetsControlsPage.verifyPresenceOfExportButtons(nodeDetailsPage.portsContent());
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getIOCIdTitle(nodeDetailsPage.portsContent()),commonData.iocIDTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortIdTitle(nodeDetailsPage.portsContent()),commonData.portIdTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getWWNTitle(nodeDetailsPage.portsContent()),commonData.wwnTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getLinkSpeedTitle(nodeDetailsPage.portsContent()),commonData.portLinkSpeedTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getStateTitle(nodeDetailsPage.portsContent()),commonData.stateTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getTopologyTitle(nodeDetailsPage.portsContent()),commonData.topologyTitle);
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(nodeDetailsPage.portsContent(),7);
    },

    selectAndVerifyESMsTabElements: function () {
        nodeDetailsPage.selectInventoryTabAndVerifyTabTextSelected(commonData.esmsTabText);
        //commonAssetsControlsPage.verifyPresenceOfExportButtons(nodeDetailsPage.esmsContent());
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getSlotTitle(nodeDetailsPage.esmsContent()),commonData.slotTitle);
        //webElementActionLibrary.verifyExactText(nodeDetailsPage.getdgTitle(nodeDetailsPage.esmsContent()),commonData.dgTitle);
        //webElementActionLibrary.verifyExactText(nodeDetailsPage.getTotalCapacityTitle(nodeDetailsPage.esmsContent()),commonData.totalCapacityTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getESMStateTitle(nodeDetailsPage.esmsContent()),commonData.esmStateTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDGDriveStateTitle(nodeDetailsPage.esmsContent()),commonData.dgDriveStateTitle);
        //webElementActionLibrary.verifyExactText(webElementActionLibrary.getElementContainsText(commonData.actionsDataTitle),commonData.actionsTitle);
        //webElementActionLibrary.verifyExactText(nodeDetailsPage.getInDGTitle(nodeDetailsPage.esmsContent()),commonData.inDGTitle);
        //webElementActionLibrary.verifyExactText(nodeDetailsPage.getESMTempTitle(nodeDetailsPage.esmsContent()),commonData.tempTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getESMLifeTitle(nodeDetailsPage.esmsContent()),commonData.esmLifeTitle);
        //webElementActionLibrary.verifyExactText(nodeDetailsPage.getStatusTitle(nodeDetailsPage.esmsContent()),commonData.statusTitle);
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(nodeDetailsPage.esmsContent(),3);
    },

    selectAndVerifyFansTabElements: function () {
        nodeDetailsPage.selectInventoryTabAndVerifyTabTextSelected(commonData.fansTabText);
        //commonAssetsControlsPage.verifyPresenceOfExportButtons(nodeDetailsPage.fansContent());
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIDTitle(nodeDetailsPage.fansContent()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getFailureReasonTitle(nodeDetailsPage.fansContent()),commonData.failureReasonTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getHealthStatusTitle(nodeDetailsPage.fansContent()),commonData.healthStatusTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getLocationTitle(nodeDetailsPage.fansContent()),commonData.locationTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getStateTitle(nodeDetailsPage.fansContent()),commonData.stateTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getPresentTitle(nodeDetailsPage.fansContent()),commonData.presentTitle);
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(nodeDetailsPage.fansContent(),5);
    },

    selectAndVerifySCAPTabElements: function () {
        nodeDetailsPage.selectInventoryTabAndVerifyTabTextSelected(commonData.superCapTabText);
        //commonAssetsControlsPage.verifyPresenceOfExportButtons(nodeDetailsPage.supercapContent());
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIDTitle(nodeDetailsPage.supercapContent()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getLocationTitle(nodeDetailsPage.supercapContent()),commonData.locationTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getHealthStatusTitle(nodeDetailsPage.supercapContent()),commonData.healthStatusTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getPresentTitle(nodeDetailsPage.supercapContent()),commonData.presentTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getStateTitle(nodeDetailsPage.supercapContent()),commonData.stateTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getBackUpTimeLeftTitle(nodeDetailsPage.supercapContent()),commonData.backUpTimeLeftTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getChargeStatusTitle(nodeDetailsPage.supercapContent()),commonData.chargeStatusTitle);
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(nodeDetailsPage.supercapContent(),5);
    },

    selectAndVerifyNetworksTabElements: function () {
        nodeDetailsPage.selectInventoryTabAndVerifyTabTextSelected(commonData.networksTabText);
        //commonAssetsControlsPage.verifyPresenceOfExportButtons(nodeDetailsPage.networksContent());
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(nodeDetailsPage.networksContent()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getIPAddrNameTitle(nodeDetailsPage.networksContent()),commonData.ipAddrNameTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getNetMaskTitle(nodeDetailsPage.networksContent()),commonData.netMaskTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getGatewayTitle(nodeDetailsPage.networksContent()),commonData.gatewayTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getIPAddTitle(nodeDetailsPage.networksContent()),commonData.ipAddressTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getMacAddTitle(nodeDetailsPage.networksContent()),commonData.macAddressTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getLinkDetectedTitle(nodeDetailsPage.networksContent()),commonData.linkDetectedTitle);
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(nodeDetailsPage.networksContent(),7);
    },

    selectAndVerifyPowerTabElements: function () {
        //nodeDetailsPage.selectInventoryTabAndVerifyTabTextSelected(commonData.powerTabText);
        nodeDetailsPage.powerTab().click();
        webElementActionLibrary.wait(2);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.selectedTabText().last(),commonData.powerTabText);
        //commonAssetsControlsPage.verifyPresenceOfExportButtons(nodeDetailsPage.powerSupplyContent());
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIDTitle(nodeDetailsPage.powerSupplyContent()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getLocationTitle(nodeDetailsPage.powerSupplyContent()),commonData.locationTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getStateTitle(nodeDetailsPage.powerSupplyContent()),commonData.stateTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getHealthStatusTitle(nodeDetailsPage.powerSupplyContent()),commonData.healthStatusTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getPresentTitle(nodeDetailsPage.powerSupplyContent()),commonData.presentTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getVoltageTitle(nodeDetailsPage.powerSupplyContent()),commonData.voltageTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getCapacityTitle(nodeDetailsPage.powerSupplyContent()),commonData.capacityTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getICTitle(nodeDetailsPage.powerSupplyContent()),commonData.inputCurrentTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getOCTitle(nodeDetailsPage.powerSupplyContent()),commonData.outputCurrentTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getAPTitle(nodeDetailsPage.powerSupplyContent()),commonData.availablePowerTitle);
        webElementActionLibrary.verifyExactText(nodeDetailsPage.getCPTitle(nodeDetailsPage.powerSupplyContent()),commonData.consumedPowerTitle);
        //commonAssetsControlsPage.verifyGridColumnsValuesExistence(nodeDetailsPage.powerSupplyContent(),10);
    },

    verifyPresenceOfExportButtons: function (section) {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getExportToPdfButton(section));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getExportToExcelButton(section));
    },

    verifyEventsSectionColumnTitlesAndValues: function (tabText) {
        var section;
        if(tabText==commonData.alarmsTabText)
            section = nodeDetailsPage.alarmsContent();
        else if(tabText==commonData.errorsTabText)
            section = nodeDetailsPage.errorsContent();
        else if(tabText==commonData.warningsTabText)
            section = nodeDetailsPage.warningsContent();
        else if(tabText==commonData.allTabText)
            section = nodeDetailsPage.allContent();
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getTimeTitle(section),commonData.timeTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSourceTitle(section),commonData.sourceTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSeverityTitle(section),commonData.severityTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getMessageTitle(section),commonData.messageTitle);
        commonAssetsControlsPage.getGridPagerInfoLabel(section).getText().then(function (val) {
            if(val!=commonData.noItemsToDisplay){
                var columns = nodeDetailsPage.eventsSectionFirstRowColumns(section);
                columns.count().then(function (count) {
                    expect(count).toBe(4);
                    for(var i=0;i<count;i++)
                        columns.get(i).getText().then(function (val) {
                            expect(val).not.toBe("");
                        });
                });
            }
        });
    },

    verifyPresenceOfNodeSVGElements: function () {
        webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.nodeStatusText());
        webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.nodePowerText());
        webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.nodeHealthIcon());
        webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.nodeHealthStatusValue());
        //commonAssetsControlsPage.verifyEventIconsSectionElementsPresence(nodeDetailsPage.nodeSection(),1);
        nodeDetailsPage.verifyPresenceOf2IOCSVGs();
        nodeDetailsPage.verifyPresenceOf16PortSVGs();
        nodeDetailsPage.verifyPresenceOf16ESMSVGs();
        nodeDetailsPage.verifyPresenceOfFanSVGs();
        nodeDetailsPage.verifyPresenceOf2SCAPSVGs();
        nodeDetailsPage.verifyPresenceOf2IPMISVGs();
        nodeDetailsPage.verifyPresenceOf4MGMTSVGs();
        nodeDetailsPage.verifyPresenceOf4PSUSVGs();
    },

    verifyPresenceOf2IOCSVGs: function () {
        nodeDetailsPage.iocSVGs().count().then(function (val) {
            expect(val).toBe(2);
            for(var i=0;i<val;i++)
                webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.iocSVGs().get(i));
        })
    },

    verifyPresenceOf16PortSVGs: function () {
        nodeDetailsPage.portsSVG().count().then(function (val) {
            expect(val).toBe(16);
            for(var i=0;i<val;i++)
            webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.portsSVG().get(i));
        })
    },

    verifyPresenceOf16ESMSVGs: function () {
        nodeDetailsPage.esmsSVG().count().then(function (val) {
            expect(val).toBe(16);
            for(var i=0;i<val;i++)
                webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.esmsSVG().get(i));
        })
    },

    verifyPresenceOf2SCAPSVGs: function () {
        nodeDetailsPage.scapsSVG().count().then(function (val) {
            expect(val).toBe(2);
            for(var i=0;i<val;i++)
                webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.scapsSVG().get(i));
        })
    },

    verifyPresenceOf4PSUSVGs: function () {
        nodeDetailsPage.psusSVG().count().then(function (val) {
            expect(val).toBe(4);
            for(var i=0;i<val;i++)
                webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.psusSVG().get(i));
        })
    },

    verifyPresenceOfFanSVGs: function () {
        webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.fanFRU0SVG());
        webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.fanFRU1SVG());
    },

    verifyPresenceOf2IPMISVGs: function () {
        nodeDetailsPage.ipmiSVGs().count().then(function (val) {
            expect(val).toBe(2);
            for(var i=0;i<val;i++)
                webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.ipmiSVGs().get(i));
        })
    },

    verifyPresenceOf4MGMTSVGs: function () {
        nodeDetailsPage.mgmtSVGs().count().then(function (val) {
            expect(val).toBe(4);
            for(var i=0;i<val;i++)
                webElementActionLibrary.verifyPresenceOfElement(nodeDetailsPage.mgmtSVGs().get(i));
        })
    },

    portsSVG: function () {
      return $$('g[id^="port"]');
    },

    esmsSVG: function () {
        return $$('g[id*="esm"]');
    },

    scapsSVG: function () {
      return $$('g[id*="supercap"]');
    },

    psusSVG: function () {
        return $$('g[ng-if*="powerSupply"]');
    },

    fanFRU1SVG: function () {
        return $('[id*="PSU_fru"]');
    },

    fanFRU0SVG: function () {
        return $('[id*="fan_1"]');
    },

    iocSVGs: function () {
        return $$('g[id^="IOC"]');
    },

    ipmiSVGs: function () {
        return $$('g[id*="IPMI"]');
    },

    mgmtSVGs: function () {
        return $$('g[id*="_management_"]');
    },

    eventsSectionFirstRowColumns: function (grid) {
        return grid.$("tbody tr").$$("td");
    },

    eventsSection: function () {
        return $('.vx-transparent-container-hw-events-panel');
    },

    inventorySection: function () {
        return $('.vx-transparent-container-inventory-panel');
    },

    nodeSection: function () {
        return $('.vx-vTheme-chassis-left-panel');
    },

    alarmsContent: function () {
        return nodeDetailsPage.eventsSection().$('[id$="-1"]');
    },

    errorsContent: function () {
        return nodeDetailsPage.eventsSection().$('[id$="-2"]');
    },

    warningsContent: function () {
        return nodeDetailsPage.eventsSection().$('[id$="-3"]');
    },

    allContent: function () {
        return nodeDetailsPage.eventsSection().$('[id$="-4"]');
    },

    getTabElement: function (tabText) {
        return webElementActionLibrary.getElementByXpath('//*[text()="'+tabText+'"]');
    },

    controllersTab: function () {
        return webElementActionLibrary.getElementByXpath('//label[text()="CONTROLLERS"]');
    },

    controllersContent: function () {
        return $('.vx-tab-ioControllers');
    },

    portsTab: function () {
        return webElementActionLibrary.getElementByXpath('//label[text()="PORTS"]');
    },

    portsContent: function () {
        return $('.vx-tab-ports');
    },

    esmsTab: function () {
        return webElementActionLibrary.getElementByXpath('//label[text()="ESMS"]');
    },

    esmsContent: function () {
        return $('.vx-tab-esms');
    },

    fansTab: function () {
        return webElementActionLibrary.getElementByXpath('//label[text()="FANS"]');
    },

    fansContent: function () {
        return $('.vx-tab-fans');
    },

    superCapTab: function () {
        return webElementActionLibrary.getElementByXpath('//label[text()="SUPER CAP"]');
    },

    supercapContent: function () {
        return $('.vx-tab-supercap');
    },

    networksTab: function () {
        return webElementActionLibrary.getElementByXpath('//label[text()="NETWORKS"]');
    },

    networksContent: function () {
        return $('.vx-tab-networks');
    },

    powerTab: function () {
        return $('#PowersupplyTab');
    },

    powerSupplyContent: function () {
        return $('.vx-tab-powerSupply');
    },

    nodeStatusText: function () {
        return webElementActionLibrary.getElementByXpath('//*[contains(text(),"STATUS")]');
    },

    nodeHealthStatusValue: function () {
        return $('[ng-if^="nodeData.status"]');
    },

    nodeHealthIcon: function () {
        return $('[ng-if^="nodeData.status"] i');
    },

    nodePowerText: function () {
        return webElementActionLibrary.getElementByXpath('//*[contains(text(),"POWER")]');
    },

    nodePowerValue: function () {

    },
    
    getIOCIdTitle: function (section) {
        return section.$('[data-title="IOC Id"] .k-link');
    },

    getUpSinceTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.upSinceDataTitle);
    },

    getSystemTimeTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.sysTimeDataTitle);
    },

    getModelTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.modelDataTitle);
    },

    getSWVersionTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.swVersionDataTitle);
    },

    getBusMasterTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.busMasterDataTitle);
    },
    
    getStatusTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.statusDataTitle);
    },

    getInDGTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.inDGDataTitle);
    },

    getESMUUIDTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.esmuuidDataTitle);
    },

    getESMLifeTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.esmLifeDataTitle);
    },

    getESMTempTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.esmTempDataTitle);
    },

    getActionsTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.actionsDataTitle);
    },

    getLinkSpeedTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.portLinkSpeedDataTitle);
    },

    getIdTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.idDataTitle);
    },

    getSlotTitle:function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.slotDataTitle);
    },

    getdgTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.dgDataTitle);
    },

    getTotalCapacityTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.totalCapacityDataTitle);
    },

    getFailureReasonTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.failureReasonDataTitle);
    },

    getHealthStatusTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.healthStatusDataTitle);
    },

    getMgmtRoleTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.mgmtRoleDataTitle);
    },

    getLocationTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.locationDataTitle);
    },

    getPresenceTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.presenceDataTitle);
    },

    getPresentTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.presentDataTitle);
    },

    getVoltageTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.voltageDataTitle);
    },

    getCapacityTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.capacityDataTitle);
    },

    getICTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.inputCurrentDataTitle);
    },

    getOCTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.outputCurrentDataTitle);
    },

    getAPTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.availablePowerDataTitle);
    },

    getCPTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.consumedPowerDataTitle);
    },

    getIPAddrNameTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.ipAddrDataTitle);
    },

    getNetMaskTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.netMaskDataTitle);
    },

    getGatewayTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.gatewayDataTitle);
    },

    getIPAddTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.ipAddressDataTitle);
    },

    getMacAddTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.macAddressDataTitle);
    },

    getLinkDetectedTitle: function (section) {
        return commonAssetsControlsPage.getColumnTitle(section,commonData.linkDetectedDataTitle);
    }
};
module.exports= nodeDetailsPage;