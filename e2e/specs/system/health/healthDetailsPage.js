/**
 * Created by Ajay(Vexata) on 30-06-2016.
 */

'use strict';

var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData=dependencies.getCommonData();

var healthDetailsPage =  {

    verifyGridColumnTitles: function () {
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.activeHeaderInBreadCrumb(),commonData.healthDetailsHeader);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(healthDetailsPage.healthDetailsGrid(),commonData.componentNameDataTitle),commonData.componentNameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(healthDetailsPage.healthDetailsGrid(),commonData.serviceTypeDataTitle),commonData.serviceTypeTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(healthDetailsPage.healthDetailsGrid(),commonData.ioc0DataTitle),commonData.ioc0Title);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(healthDetailsPage.healthDetailsGrid(),commonData.ioc1DataTitle),commonData.ioc1Title);
    },

    verifyComponentsCount: function () {
        expect(commonAssetsControlsPage.getGridRows(healthDetailsPage.healthDetailsGrid()).count()).toBe(commonData.health.count);
    },

    verifyPresenceOfComponentDetails: function () {
        var rows = commonAssetsControlsPage.getGridRows(healthDetailsPage.healthDetailsGrid());
        var j =0;
        rows.count().then(function (count) {
            expect(count).toBe(commonData.health.count);
            for(var i=0; i<count; i++)
            webElementActionLibrary.verifyTextPresence(rows.get(j),commonData.health.components[j]);
            webElementActionLibrary.verifyTextPresence(rows.get(j),commonData.health.serviceType[j]);
            webElementActionLibrary.verifyTextPresence(rows.get(j),commonData.health.IOC0[j]);
            webElementActionLibrary.verifyTextPresence(rows.get(j),commonData.health.IOC1[j]);
        });
    },

    healthDetailsGrid: function () {
        return $('[feature="healthDetailsGrid"]');
    }
};

module.exports=healthDetailsPage;