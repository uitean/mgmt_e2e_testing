/**
 * Created by Ajay(Vexata) on 30-06-2016.
 */

'use strict';
var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    commonData = dependencies.getCommonData(),
    healthDetailsPage = dependencies.getHealthDetailsPage();

describe("Tests for verifying presence of health details page elements", function() {

    describe("Tests for readonly user", function () {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
            commonAssetsControlsPage.navigateToHealthDetailsPage();
        });

        it("verify Health Page column titles", function () {
            healthDetailsPage.verifyGridColumnTitles();
        });

        it("verify Health Page column values and count", function () {
            healthDetailsPage.verifyComponentsCount();
            healthDetailsPage.verifyPresenceOfComponentDetails();
        });

        it("verify Health Page export buttons presence", function () {
            commonAssetsControlsPage.verifyPresenceOfExportButtons(healthDetailsPage.healthDetailsGrid());
        });

        it("verify Health Page search by component name", function () {
            commonAssetsControlsPage.searchByValueAndVerifyPresenceInSearchResults(healthDetailsPage.healthDetailsGrid(),commonData.health.components[3]);
        });

        it("verify Health Page search by service type", function () {
            commonAssetsControlsPage.searchByValueAndVerifyPresenceInSearchResults(healthDetailsPage.healthDetailsGrid(),commonData.health.serviceType[5]);
        });

        it("verify Health Page search by healthy status", function () {
            commonAssetsControlsPage.searchByValueAndVerifyPresenceInSearchResults(healthDetailsPage.healthDetailsGrid(),commonData.health.IOC0[2]);
        });


    });

    describe("Tests for admin user", function () {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToHealthDetailsPage();
        });

        it("verify Health Page column titles", function () {
            healthDetailsPage.verifyGridColumnTitles();
        });

        it("verify Health Page column values and count", function () {
            healthDetailsPage.verifyComponentsCount();
            healthDetailsPage.verifyPresenceOfComponentDetails();
        });

        it("verify Health Page export buttons presence", function () {
            commonAssetsControlsPage.verifyPresenceOfExportButtons(healthDetailsPage.healthDetailsGrid());
        });

        it("verify Health Page search by component name", function () {
            commonAssetsControlsPage.searchByValueAndVerifyPresenceInSearchResults(healthDetailsPage.healthDetailsGrid(),commonData.health.components[3]);
        });

        it("verify Health Page search by service type", function () {
            commonAssetsControlsPage.searchByValueAndVerifyPresenceInSearchResults(healthDetailsPage.healthDetailsGrid(),commonData.health.serviceType[5]);
        });

        it("verify Health Page search by healthy status", function () {
            commonAssetsControlsPage.searchByValueAndVerifyPresenceInSearchResults(healthDetailsPage.healthDetailsGrid(),commonData.health.IOC0[2]);
        });
    });
});

