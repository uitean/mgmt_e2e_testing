/**
 * Created by Ajay(Vexata) on 11-05-2017.
 */

var webElementActionLibrary = dependencies.getWebElementActionLibrary();

var settingsPage =  {

    preferenceTab:function() {
        return $('#prefTab');
    },

    localBrowserCacheText: function () {
      return $$('.vx-events-severity-container').get(0);
    },

    unSelectedChoiceText: function () {
        return $$('.vx-events-severity-container').get(0);
    },

    noOfEventsInput: function () {
      return $('[ng-model="eventsMaxLimit"]');
    },

    noOfEventsSuffix: function () {
        return $('sup');
    },

    base2RadioBtn: function () {
        return $('[for="base2"] div');
    },

    base10RadioBtn: function () {
        return $('[for="base10"] div');
    },

    decimalUnitsLabel: function () {
        return $('[for="base10"]');
    },

    binaryUnitsLabel: function () {
        return $('[for="base2"]');
    },

    sysInfoTab: function () {
        return $('#sysTab');
    },

    repoTab: function () {
        return $('#repoTab');
    },

    emailTab: function () {
        return $('#emailTab');
    },

    passwordTab: function () {
        return $('#chngPasswordTab');
    },

    proxyTab: function () {
        return  $('#proxyTab')
    },

    inlineRequiredLabels: function () {
        return $$('.elementlabel [class="inline required"]');
    },

    inlineLabels: function () {
        return  $$('.elementlabel [class="inline"]');
    },

    sysUserName: function () {
        return settingsPage.inlineRequiredLabels().get(0);
    },

    emailServer: function () {
        return settingsPage.inlineRequiredLabels().get(1);
    },

    emailPort: function () {
        return settingsPage.inlineRequiredLabels().get(2);
    },

    emailUserName: function () {
        return settingsPage.inlineRequiredLabels().get(3);
    },

    emailPassword: function () {
        return settingsPage.inlineRequiredLabels().get(4);
    },

    currentPassword: function () {
        return settingsPage.inlineRequiredLabels().get(0);
    },

    currentPasswordInput: function () {
       return $('[name="currentPassword"]')
    },

    newPasswordInput: function () {
        return $('#newPassword')
    },

    confirmPasswordInput: function () {
        return $('[name="confirmPassword"]');
    },

    newPassword: function () {
        return settingsPage.inlineRequiredLabels().get(1);
    },

    confirmPassword: function () {
        return settingsPage.inlineRequiredLabels().get(2);
    },

    hprHostName: function () {
        return settingsPage.inlineRequiredLabels().get(8);
    },

    hprPort: function () {
        return settingsPage.inlineRequiredLabels().get(9);
    },

    hprUserName: function () {
        return settingsPage.inlineRequiredLabels().get(10);
    },

    hprPassword: function () {
        return settingsPage.inlineRequiredLabels().get(11);
    },

    sprHostName: function () {
        return settingsPage.inlineRequiredLabels().get(10);
    },

    sprPort: function () {
        return settingsPage.inlineRequiredLabels().get(11);
    },

    sprUserName: function () {
        return settingsPage.inlineLabels().get(11);
    },

    sprPassword: function () {
        return settingsPage.inlineLabels().get(12);
    },

    deptName: function () {
        return settingsPage.inlineLabels().get(0);
    },

    cloudUserName: function () {
        return settingsPage.inlineLabels().get(1);
    },

    accessKey: function () {
        return settingsPage.inlineLabels().get(2);
    },

    secretKey: function () {
        return settingsPage.inlineLabels().get(3);
    },

    scpHostName: function () {
        return settingsPage.inlineLabels().get(4);
    },

    scpUserName: function () {
        return settingsPage.inlineLabels().get(5);
    },

    location: function () {
        return settingsPage.inlineLabels().get(6);
    },

    cEmailFrom: function () {
        return settingsPage.inlineLabels().get(7);
    },

    vxEmailFrom: function () {
        return settingsPage.inlineLabels().get(9);
    },

    cEmailTo: function () {
        return settingsPage.inlineLabels().get(8);
    },

    httpProxyLabel: function () {
        return $('[for="vx_http_proxy_selection"]');
    },

    sockProxyLabel: function () {
        return $('[for="vx_sock_proxy_selection"]');
    },

    httpRadioBtn: function () {
        return $('input[value="HTTP"]');
    },

    socksRadioBtn: function () {
        return $('input[value="SOCK"]');
    },

    sysUsernameInput: function () {
        return $('[name="identity_hostname"]');
    },

    deptInput: function () {
        return $('[name="identity_depName"]')
    },

    saveSysInfoBtn: function () {
        return $('[ng-click="savesysInfoConfigurationForm(sysInfoConfiguration)"]');
    },

    saveRepoBtn: function () {
        return $('[ng-click="saveRepositoryConfigurationForm(repositoryConfiguration)"]');
    },

    testCloudBtn: function () {
        return $('[ng-click="validateCloudStorageAccess(repositoryConfiguration.customerName, repositoryConfiguration.vexataCloudStorage)"]');
    },

    testOnPremBtn: function () {
        return $('[ng-click="validateOnPremiseStorageAccess(repositoryConfiguration.onPremiseStorage)"]');
    },

    cloudUserNameInput: function () {
        return $('[name="cloud_cusname"]');
    },

    accessKeyInput: function () {
        return $('[name="cloud_akey"]');
    },

    secretKeyInput: function () {
        return $('[name="cloud_skey"]');
    },

    scpHostNameInput: function () {
        return $('[name="onpremises_hostname"]');
    },

    onPremUserNameInput: function () {
        return $('[name="onpremises_username"]');
    },

    onPremLocationInput: function () {
        return $('[name="onpremises_location"]');
    },

    cloudToggleBtn: function () {
        return $('[for="supportSaveCloudStorage"]');
    },

    onPremToggleBtn: function () {
        return $('[for="supportSaveOnPremises"]');
    },

    cloudIcon: function () {
        return $('.vx-ss-cstorage-icon');
    },

    onPremIcon: function () {
        return $('.vx-ss-onpremises-icon');
    },

    headings: function () {
        return $$('.heading');
    },

    cloudStorageBlock: function () {
        return $('.cstorage-block');
    },

    onPremBlock: function () {
        return $('.onpremises-block');
    },

    cloudStorageLabel: function () {
        return $('.cloud-storage');
    },

    onPremLabel: function () {
        return $('.on-premises-info');
    },

    vexataEamilServiceLabel: function () {
        return $('[for="vxServiceSelection"]');
    },

    vexataEamilServiceRadioBtn: function () {
        return settingsPage.vexataEamilServiceLabel().$("span");
    },

    smtpLabel: function () {
        return $('[for="smtpSelection"]');
    },

    smtpRadioBtn: function () {
        return settingsPage.smtpLabel().$('span');
    },

    smtpServerInput: function () {
        return $('[name="hostName"]');
    },

    smtpPortInput: function () {
        return $('[name="Port"]');
    },

    smtpUserNameInput: function () {
        return $('[name="username"]');
    },

    smtpPasswordInput: function () {
        return $('[name="password"]');
    },

    tlsCheckbox: function () {
        return $('[for="startTLSEnable"]');
    },

    authenticateCheckBox: function () {
        return $('[for="authenticate"]');
    },

    fromInput: function () {
        return $('[name="from"]');
    },

    toInput: function () {
        return $('[name="to"]');
    },

    sendTestMailBtn: function () {
        return $('[ng-click="sendTestMail(emailConfiguration)"]');
    },

    vxFromInput: function () {
      return $('[name="smtp_from"]');
    },

    saveEmailBtn: function () {
        return $('[ng-click="saveEmailConfiguration(emailConfiguration)"]');
    },

    savePasswordBtn: function () {
        return $('[ng-click="changePasswordSubmit(changePassword, changePassForm)"]');
    },

    customerEmailToggelButton: function () {
        return $('[for="customerEmailConfiguration"]');
    },

    vexataMailToggeleBtn: function () {
        return $('[for="enableVexataEmail"]');
    },

    httpProxyLabel: function () {
      return $('[for="vx_http_proxy_selection"]');
    },

    httpProxyRadioBtn: function () {
        return settingsPage.httpProxyLabel().$('span');
    },

    sockProxyLabel: function () {
        return $('[for="vx_sock_proxy_selection"]');
    },

    httpHostNameInput: function () {
      return $('[name="http_hostName"]');
    },

    httpPortInput: function () {
        return $('[name="http_Port"]');
    },

    httpUserNameInput: function () {
        return $('[name="http_username"]');
    },

    httpPasswordInput: function () {
        return $('[name="http_password"]');
    },

    sockHostNameInput: function () {
        return $('[name="sock_hostName"]');
    },

    sockPortInput: function () {
        return $('[name="sock_Port"]');
    },

    sockUserNameInput: function () {
        return $('[name="sock_username"]');
    },

    sockPasswordInput: function () {
        return $('[name="sock_password"]');
    },

    sockProxyRadioBtn: function () {
        return settingsPage.sockProxyLabel().$('span');
    },

    saveProxyBtn: function () {
      return $('[ng-click="saveProxyConfigurations(proxyConfigurations)"]');
    },

    savePreferenceBtn: function () {
      return $('[ng-click="savePreference()"]');
    },

    savePreferenceDisabledBtn: function () {
        return $("[ng-click='savePreference()'][disabled='disabled']");
    },

    hprUseAuthentication: function () {
        return $('[for="http_proxy_authenticate"]');
    },

    sprUseAuthentication: function () {
        return $('[for="sock_proxy_authenticate"]');
    },

    selectBinaryPreference: function () {
        settingsPage.selectPreferenceTab();
        settingsPage.base2RadioBtn().click();
        if(browser.isElementPresent(settingsPage.savePreferenceDisabledBtn()))
            console.log("binary");
        else
            settingsPage.savePreferenceBtn().click();

    },

    selectDecimalPreference: function () {
        settingsPage.selectPreferenceTab();
        //settingsPage.base10RadioBtn().click();
        if(browser.isElementPresent(settingsPage.savePreferenceDisabledBtn()))
                        console.log("decimal");
        else
        settingsPage.savePreferenceBtn().click();

    },

    selectPreferenceTab: function () {
        settingsPage.preferenceTab().click();
        webElementActionLibrary.waitForElement(settingsPage.base2RadioBtn());
    },

    selectRepoTab: function () {
        settingsPage.repoTab().click();
        webElementActionLibrary.waitForElement(settingsPage.headings().get(0));
    },

    selectEmailTab: function () {
        settingsPage.emailTab().click();
        webElementActionLibrary.waitForElement(settingsPage.headings().get(2));
    },

    selectPasswordTab: function () {
        settingsPage.passwordTab().click();
        webElementActionLibrary.waitForElement(settingsPage.currentPassword());
    },

    selectProxyTab: function () {
        settingsPage.proxyTab().click();
        webElementActionLibrary.waitForElement(settingsPage.httpProxyLabel());
    }
};
module.exports=settingsPage;