/**
 * Created by Ajay(Vexata) on 18-05-2017.
 */

'use strict';

//includes all the js file dependencies required for execution of scenarios
var exec = require("ssh-exec");
var webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage(),
    commonData = dependencies.getCommonData(),
    settingsPage = dependencies.getSettingsPage();


describe("Tests For Settings", function() {

    describe("As admin", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToSettingsPage();
        });

        describe("Tests to validate tab fields", function () {

            xit("Test to validate system info tab fields", function () {
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.sysInfoTabHeader);
                webElementActionLibrary.verifyTextPresence(settingsPage.sysUserName(), commonData.userName);
                webElementActionLibrary.verifyTextPresence(settingsPage.deptName(), commonData.dept);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.sysUsernameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.deptInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.saveSysInfoBtn());
            });

            xit("Test to validate repository tab fields ", function () {
                settingsPage.selectRepoTab();
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.repoTabHeader);
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(0), commonData.ssDestinations);
                webElementActionLibrary.verifyTextPresence(settingsPage.cloudStorageBlock(), commonData.vxCloud);
                webElementActionLibrary.verifyTextPresenceUsingContain(settingsPage.onPremBlock(), commonData.onPremFS);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.cloudIcon());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.onPremIcon());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.cloudToggleBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.onPremToggleBtn());
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(1), commonData.credentials);
                webElementActionLibrary.verifyTextPresence(settingsPage.cloudStorageLabel(), commonData.vxCloud);
                webElementActionLibrary.verifyTextPresence(settingsPage.onPremLabel(), commonData.onPrem);
                webElementActionLibrary.verifyTextPresence(settingsPage.cloudUserName(), commonData.userName);
                webElementActionLibrary.verifyTextPresence(settingsPage.accessKey(), commonData.accessKey);
                webElementActionLibrary.verifyTextPresence(settingsPage.secretKey(), commonData.secretKey);
                webElementActionLibrary.verifyTextPresence(settingsPage.scpHostName(), commonData.scpHostName);
                webElementActionLibrary.verifyTextPresence(settingsPage.scpUserName(), commonData.userName);
                webElementActionLibrary.verifyTextPresence(settingsPage.location(), commonData.location);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.cloudUserNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.accessKeyInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.secretKeyInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.scpHostNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.onPremUserNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.onPremLocationInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.testCloudBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.testOnPremBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.saveRepoBtn());
            });

            it("Test to validate password tab fields ", function () {
                settingsPage.selectPasswordTab();
                webElementActionLibrary.verifyTextPresence(settingsPage.currentPassword(), commonData.currentPassword);
                webElementActionLibrary.verifyTextPresence(settingsPage.newPassword(), commonData.newPassword);
                webElementActionLibrary.verifyTextPresence(settingsPage.confirmPassword(), commonData.confirmPassword);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.currentPasswordInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.newPasswordInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.confirmPasswordInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.savePasswordBtn());
            });

            xit("Test to validate proxy tab fields ", function () {
                settingsPage.selectProxyTab();
                webElementActionLibrary.verifyTextPresence(settingsPage.httpProxyLabel(), commonData.httpProxy);
                webElementActionLibrary.verifyTextPresence(settingsPage.hprHostName(), commonData.hostName);
                webElementActionLibrary.verifyTextPresence(settingsPage.hprPort(), commonData.port);
                webElementActionLibrary.verifyTextPresence(settingsPage.hprUserName(), commonData.userName);
                webElementActionLibrary.verifyTextPresence(settingsPage.hprPassword(), commonData.password);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sockProxyLabel(), commonData.socksProxy);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprHostName(), commonData.hostName);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprPort(), commonData.port);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprUserName(), commonData.userName);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprPassword(), commonData.password);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.httpHostNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.httpPortInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.httpUserNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.httpPasswordInput());
                //webElementActionLibrary.verifyPresenceOfElement(settingsPage.sockHostNameInput());
                //webElementActionLibrary.verifyPresenceOfElement(settingsPage.sockPortInput());
                //webElementActionLibrary.verifyPresenceOfElement(settingsPage.sockUserNameInput());
                //webElementActionLibrary.verifyPresenceOfElement(settingsPage.sockPasswordInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.hprUseAuthentication(),commonData.useAuthentication);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprUseAuthentication(),commonData.useAuthentication);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.saveProxyBtn());
            });

            it("Test to validate preferences tab fields ", function () {
                settingsPage.selectPreferenceTab();
                //webElementActionLibrary.verifyTextPresence(settingsPage.localBrowserCacheText(), commonData.unselectedChoiceLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.unSelectedChoiceText(), commonData.unselectedChoiceLabel);
                webElementActionLibrary.verifyTextPresenceUsingContain(settingsPage.binaryUnitsLabel(), commonData.binaryText);
                webElementActionLibrary.verifyTextPresenceUsingContain(settingsPage.decimalUnitsLabel(), commonData.decimalText);
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(0), commonData.displayUnit);
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(1), commonData.eventConfig);
                webElementActionLibrary.verifyPresenceOfElement(webElementActionLibrary.getElementByLabel(commonData.noOfEventsLabel));
                webElementActionLibrary.verifyPresenceOfElement(webElementActionLibrary.getElementContainsText(commonData.selectUnitLabel));
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.base2RadioBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.base10RadioBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.noOfEventsInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.noOfEventsSuffix(), commonData.eventsInputSuffix);
            });

            xit("Test to validate email tab fields ", function () {
                settingsPage.selectEmailTab();
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.emailTabHeader);
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(2), commonData.deliverMailUsingLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.vexataEamilServiceLabel(), commonData.vxemailServiceLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.smtpLabel(), commonData.smtpConfig);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.vexataEamilServiceRadioBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpRadioBtn());
                webElementActionLibrary.verifyTextPresence(settingsPage.emailServer(), commonData.server);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpServerInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.emailPort(), commonData.port);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpPortInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.tlsCheckbox(), commonData.useTLSLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.authenticateCheckBox(), commonData.authenticateLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.emailUserName(), commonData.userName);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpUserNameInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.emailPassword(), commonData.password);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpPasswordInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(3), commonData.recipientsLabel);
                webElementActionLibrary.verifyPresenceOfElement(webElementActionLibrary.getElementByLabel(commonData.cEmailLabel));
                webElementActionLibrary.verifyTextPresence(settingsPage.cEmailFrom(), commonData.from);
                webElementActionLibrary.verifyTextPresence(settingsPage.cEmailTo(), commonData.to);
                webElementActionLibrary.verifyTextPresenceUsingContain(settingsPage.headings().get(4), commonData.vexatasupportMailIdLabel);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.vexataMailToggeleBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.sendTestMailBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.customerEmailToggelButton());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.saveEmailBtn());
                settingsPage.smtpRadioBtn().click();
                webElementActionLibrary.wait(1);
                webElementActionLibrary.verifyTextPresence(settingsPage.vxEmailFrom(), commonData.from);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.vxFromInput());
            });

        });
    });


    describe("As read only user", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
            commonAssetsControlsPage.navigateToSettingsPage();
        });

        describe("Tests to validate tab fields", function () {

            xit("Test to validate system info tab fields", function () {
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.sysInfoTabHeader);
                webElementActionLibrary.verifyTextPresence(settingsPage.sysUserName(), commonData.userName);
                webElementActionLibrary.verifyTextPresence(settingsPage.deptName(), commonData.dept);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.sysUsernameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.deptInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.saveSysInfoBtn());
            });

            xit("Test to validate repository tab fields ", function () {
                settingsPage.selectRepoTab();
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.repoTabHeader);
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(0), commonData.ssDestinations);
                webElementActionLibrary.verifyTextPresence(settingsPage.cloudStorageBlock(), commonData.vxCloud);
                webElementActionLibrary.verifyTextPresenceUsingContain(settingsPage.onPremBlock(), commonData.onPremFS);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.cloudIcon());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.onPremIcon());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.cloudToggleBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.onPremToggleBtn());
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(1), commonData.credentials);
                webElementActionLibrary.verifyTextPresence(settingsPage.cloudStorageLabel(), commonData.vxCloud);
                webElementActionLibrary.verifyTextPresence(settingsPage.onPremLabel(), commonData.onPrem);
                webElementActionLibrary.verifyTextPresence(settingsPage.cloudUserName(), commonData.userName);
                webElementActionLibrary.verifyTextPresence(settingsPage.accessKey(), commonData.accessKey);
                webElementActionLibrary.verifyTextPresence(settingsPage.secretKey(), commonData.secretKey);
                webElementActionLibrary.verifyTextPresence(settingsPage.scpHostName(), commonData.scpHostName);
                webElementActionLibrary.verifyTextPresence(settingsPage.scpUserName(), commonData.userName);
                webElementActionLibrary.verifyTextPresence(settingsPage.location(), commonData.location);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.cloudUserNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.accessKeyInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.secretKeyInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.scpHostNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.onPremUserNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.onPremLocationInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.testCloudBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.testOnPremBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.saveRepoBtn());
            });

           xit("Test to validate email tab fields ", function () {
                settingsPage.selectEmailTab();
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.emailTabHeader);
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(2), commonData.deliverMailUsingLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.vexataEamilServiceLabel(), commonData.vxemailServiceLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.smtpLabel(), commonData.smtpConfig);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.vexataEamilServiceRadioBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpRadioBtn());
                webElementActionLibrary.verifyTextPresence(settingsPage.emailServer(), commonData.server);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpServerInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.emailPort(), commonData.port);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpPortInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.tlsCheckbox(), commonData.useTLSLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.authenticateCheckBox(), commonData.authenticateLabel);
                webElementActionLibrary.verifyTextPresence(settingsPage.emailUserName(), commonData.userName);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpUserNameInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.emailPassword(), commonData.password);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.smtpPasswordInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(3), commonData.recipientsLabel);
                webElementActionLibrary.verifyPresenceOfElement(webElementActionLibrary.getElementByLabel(commonData.cEmailLabel));
                webElementActionLibrary.verifyTextPresence(settingsPage.cEmailFrom(), commonData.from);
                webElementActionLibrary.verifyTextPresence(settingsPage.cEmailTo(), commonData.to);
                webElementActionLibrary.verifyTextPresenceUsingContain(settingsPage.headings().get(4), commonData.vexatasupportMailIdLabel);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.vexataMailToggeleBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.sendTestMailBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.customerEmailToggelButton());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.saveEmailBtn());
            });

            it("Test to validate password tab fields ", function () {
                settingsPage.selectPasswordTab();
                webElementActionLibrary.verifyTextPresence(settingsPage.currentPassword(), commonData.currentPassword);
                webElementActionLibrary.verifyTextPresence(settingsPage.newPassword(), commonData.newPassword);
                webElementActionLibrary.verifyTextPresence(settingsPage.confirmPassword(), commonData.confirmPassword);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.currentPasswordInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.newPasswordInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.confirmPasswordInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.savePasswordBtn());
            });

            xit("Test to validate proxy tab fields ", function () {
                settingsPage.selectProxyTab();
                webElementActionLibrary.verifyTextPresence(settingsPage.httpProxyLabel(), commonData.httpProxy);
                webElementActionLibrary.verifyTextPresence(settingsPage.hprHostName(), commonData.hostName);
                webElementActionLibrary.verifyTextPresence(settingsPage.hprPort(), commonData.port);
                webElementActionLibrary.verifyTextPresence(settingsPage.hprUserName(), commonData.userName);
                webElementActionLibrary.verifyTextPresence(settingsPage.hprPassword(), commonData.password);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sockProxyLabel(), commonData.socksProxy);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprHostName(), commonData.hostName);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprPort(), commonData.port);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprUserName(), commonData.userName);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprPassword(), commonData.password);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.httpHostNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.httpPortInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.httpUserNameInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.httpPasswordInput());
                //webElementActionLibrary.verifyPresenceOfElement(settingsPage.sockHostNameInput());
                //webElementActionLibrary.verifyPresenceOfElement(settingsPage.sockPortInput());
                //webElementActionLibrary.verifyPresenceOfElement(settingsPage.sockUserNameInput());
                //webElementActionLibrary.verifyPresenceOfElement(settingsPage.sockPasswordInput());
                webElementActionLibrary.verifyTextPresence(settingsPage.hprUseAuthentication(),commonData.useAuthentication);
                //webElementActionLibrary.verifyTextPresence(settingsPage.sprUseAuthentication(),commonData.useAuthentication);
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.saveProxyBtn());
            });

            it("Test to validate preferences tab fields ", function () {
                settingsPage.selectPreferenceTab();
                //webElementActionLibrary.verifyTextPresence(settingsPage.localBrowserCacheText(), commonData.localBrowserCacheDisclaimer);
                webElementActionLibrary.verifyTextPresence(settingsPage.unSelectedChoiceText(), commonData.unselectedChoiceLabel);
                webElementActionLibrary.verifyTextPresenceUsingContain(settingsPage.binaryUnitsLabel(), commonData.binaryText);
                webElementActionLibrary.verifyTextPresenceUsingContain(settingsPage.decimalUnitsLabel(), commonData.decimalText);
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(0), commonData.displayUnit);
                webElementActionLibrary.verifyTextPresence(settingsPage.headings().get(1), commonData.eventConfig);
                webElementActionLibrary.verifyPresenceOfElement(webElementActionLibrary.getElementByLabel(commonData.noOfEventsLabel));
                webElementActionLibrary.verifyPresenceOfElement(webElementActionLibrary.getElementContainsText(commonData.selectUnitLabel));
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.base2RadioBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.base10RadioBtn());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.noOfEventsInput());
                webElementActionLibrary.verifyPresenceOfElement(settingsPage.noOfEventsSuffix(), commonData.eventsInputSuffix);
            });

        });
    });
});


