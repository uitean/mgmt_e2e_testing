/**
 * Created by Ajay(Vexata) on 21-04-2017.
 */

var exec = require('ssh-exec');
var cliData = require('./cliData.js');
params= process.argv.slice(2).toString().split(",");
bup = params[0];
hostForUpgrade  = params[1];
if(params[2]==undefined || params[2]=="") {
    repoLocation = undefined;
    console.assert(false,"repo location details are undefined and is mandatory parameter. Please run by passing valid repo location")
}
else{
    repoLocation=params[2].split(";")[2];
    userName=params[2].split(";")[0];
    repoServer=params[2].split(";")[1];
}
dgCleanRequired=params[3];
if(params[4]=="RAID5" || params[4]=="RAID6")
    raidLevel= params[4];
else
    raidLevel=undefined;
if(params[5]==undefined || params[5]=="")
    esmList= undefined;
else{
    esmList=params[5];
    esms=esmList.split(" ");
    var esmListCommand="vxcli esm list | grep '"+esms[0]+" ";
    console.log(esms);
    for (var i=1;i<esms.length;i++){
        esmListCommand = esmListCommand+"\\|"+esms[i]+" ";
    }
    esmListCommand = esmListCommand+"'";
}
if(params[6]==undefined || params[6]=="")
    esmInitScript= undefined;
else
    esmInitScript=params[6];

if(params[7]==undefined || params[7]=="")
    egCreateScript= undefined;
else
    egCreateScript=params[7];
if(params[8]==undefined || params[8]=="")
    initiators = undefined;
else
    initiators=params[8].split(";");
if(params[9]==undefined || params[9]=="")
    ioScript= undefined;
else
    ioScript=params[9];
if(params[10]==undefined || params[10]=="")
    pcscript= undefined;
else
    pcscript=params[10];
console.log(bup+hostForUpgrade+repoLocation+dgCleanRequired+raidLevel+esmList+esmInitScript+egCreateScript+initiators+ioScript+pcscript);

//set repo ssh to the given location

console.log("validating accessibility of "+cliData.getCLIConfig().host);
exec("ping -c 2 "+cliData.getCLIConfig().host,cliData.getKochiConfig(), function (err,vipLog) {
    //will check whether VIP is accessible
    console.assert(vipLog.includes("0% packet loss"),cliData.getCLIConfig().host+" is not accessible");
    if(bup=="VSUINSTALL") {
        setTimeout(function () {
            console.log("setting repo ssh location to " + repoLocation + "using " + cliData.vsuRepoSetSSH + userName + "@" + repoServer + " " + repoLocation);
            exec(cliData.vsuRepoSetSSH + userName + "@" + repoServer + " " + repoLocation, cliData.getCLIConfig(), function (err, stdout) {
                console.assert(!stdout.includes("Error"));
                //get vsu repo list from server
                console.log("listing available repo package");
                exec(cliData.vsuRepoList, cliData.getCLIConfig(), function (err, repoList) {
                    var repo = repoList.split("Packages:")[1].trim();
                    //get local available packages on chassis
                    console.log("listing available local packages");
                    exec(cliData.vsuList, cliData.getCLIConfig(), function (err, vsuListBeforeUpgrade) {
                        var currentPackage = getCurrentPackage(vsuListBeforeUpgrade);
                        //verifies whether teh current package and the latest package are same or not. If same, then installation will be stopped, if not installation will go through next steps
                        console.log("verifying whether the installed package is latest package");
                        if (!(currentPackage == repo)) {
                            console.log("installed package is not latest");
                            removePackagesMoreThanFourAndInstall();
                            //will check if available packages are 4. If so will remove the first package
                            function removePackagesMoreThanFourAndInstall(){
                                exec(cliData.vsuList, cliData.getCLIConfig(), function (err, vsuListBeforeUpgrade) {
                                    if (vsuListBeforeUpgrade.split("Packages:")[1].split("v").length > 4) {
                                        console.log("Chassis has more than 4 or 4 available packages.");
                                        var firstPackage = vsuListBeforeUpgrade.split("Packages:")[1].split("v")[1];
                                        var packageToRemove;
                                        if (!firstPackage.includes("[Current]")) {
                                            packageToRemove = "v" + vsuListBeforeUpgrade.split("Packages:")[1].split("v")[1];
                                            console.log("Removing the first package");
                                        }
                                        else {
                                            packageToRemove = "v" + vsuListBeforeUpgrade.split("Packages:")[1].split("v")[2];
                                            console.log("first package is current installed package. Hence removing second package")
                                        }
                                        exec(cliData.vsuRemove + packageToRemove, cliData.getCLIConfig(), function (err, stdout) {
                                            removePackagesMoreThanFourAndInstall();
                                        }).pipe(process.stdout);
                                    }
                                    else
                                        getRepoAndInstall(repo);
                                }).pipe(process.stdout);
                            }
                        }
                        else {
                            console.log("current installed package is the latest package. Hence aborting the installation");
                        }
                    }).pipe(process.stdout);
                }).pipe(process.stdout);
            }).pipe(process.stdout);
        }, 5000);
    }
    else if (bup=="PKGINSTALL")
    updateUsingPKGInstall();
}).pipe(process.stdout);


function getRepoAndInstall(repo){
    if(bup=="VSUINSTALL")
        updateUsingVSUInstall(repo);
    else
        console.assert(false,"update method is not valid");
}

function pingInitiators(i){
    console.log(initiators[i]);
    exec("ping -c 2 "+cliData.getConf(initiators[i]).host,cliData.getKochiConfig(), function (err,iniLog) {
        //will check whether initiator is accessible
        console.assert(iniLog.includes("0% packet loss"), initiators[i] + " is not accessible");
    }).pipe(process.stdout);
}

function runIO(j){
    setTimeout(function () {
        exec("ps -ef | grep maim", cliData.getConf(initiators[j]), function (err, stdout3) {
            exec(ioScript, cliData.getConf(initiators[j]), function (err, stdout3) {
                exec("ps -ef | grep maim", cliData.getConf(initiators[j]), function (err, stdout3) {
                }).pipe(process.stdout);
            }).pipe(process.stdout);
        }).pipe(process.stdout);
    },30000);
}

function saEnable(){
    exec(cliData.saEnable, cliData.getCLIConfig(), function (err, stdout2) {
        console.assert(stdout2.includes('SA Enable successful'));
    }).pipe(process.stdout);
}

function updateUsingPKGInstall(){
    //get and install repo on IOC0
    console.log("copying repo on IOC0");
    exec("cd /tmp/;scp " + userName + "@" + repoServer + ":" + repoLocation + "pkg* .", cliData.getCLIConfig(), function (err, repoGetLog) {
        exec("cd /tmp; ls -lt pkg*", cliData.getCLIConfig(), function (err, repoGetLog) {
            var package = "pkg-" + repoGetLog.split(".tgz")[0].split("pkg-")[1] + ".tgz";
            var repo = "v" + repoGetLog.split(".tgz")[0].split("pkg-")[1];
            exec("cd /packages; tar -xvzf /tmp/" + package, cliData.getCLIConfig(), function (err, repoGetLog) {
                exec("rm -rf /tmp/"+package, cliData.getCLIConfig(), function (err, repoGetLog) {
                }).pipe(process.stdout);
                console.log("installing package on IOC0");
                exec("/packages/" + repo + "/pkg_postinstall.sh " + repo, cliData.getCLIConfig(), function (err, installationLog) {
                    console.assert(installationLog.includes("VSU:BEGIN:vxd_install"),"installation is not successful");
                    console.assert(installationLog.includes("VSU:END:mgmt_update"),"installation is not successful");
                    //get and install repo on IOC1
                    processFromDGClean(repo);
                }).pipe(process.stdout);
            }).pipe(process.stdout);
        }).pipe(process.stdout);
    }).pipe(process.stdout);
}

function updateUsingVSUInstall(repo){
    //get and install repo on IOC0
    console.log("getting repo on IOC0");
    exec(cliData.vsuRepoGet + repo, cliData.getCLIConfig(), function (err, repoGetLog) {
        console.assert(repoGetLog.includes("Done"),"repo get is not successful");
        console.log("installing package on IOC0 using vsu install "+repo+" -l");
        exec("vsu install " + repo + " -l", cliData.getCLIConfig(), function (err, installationLog) {
            console.assert(installationLog.includes("Please reboot IOC to update versions"),"installation is not successful");
            //install repo on IOC1
            processFromDGClean(repo);
        }).pipe(process.stdout);
    }).pipe(process.stdout);
}

function getCurrentPackage(vsuListop){
    var packages =vsuListop.split("Packages:")[1].split("\n");
    var currentPackage;
    for(var i=0;i<packages.length;i++){
        if(packages[i].includes("[Current]")) {
            currentPackage = packages[i].split("[Current]")[0].trim();
            break;
        }
    }
    return currentPackage;
}

function getLatestPackage(vsuListop){
    var packages =vsuListop.split("Packages:")[1].split("\n");
    var latestPackage;
    for(var i=0;i<packages.length;i++){
        if(packages[i].includes("[Latest]")) {
            latestPackage = packages[i].split("[Latest]")[0].trim();
            break;
        }
    }
    return latestPackage;
}

function checkUpdatedVersion(repo){
    exec("vsu list", cliData.getCLIConfig(), function (err, vsuList) {
        console.assert(getCurrentPackage(vsuList).includes(repo),"update is not successful");
    }).pipe(process.stdout);
}
function checkNDUUpdateStatus(repo) {
    var count = 0;
    if (count <= 300) {
        console.log(count +". validating ndu update status");
        setTimeout(function () {
            exec("vsu update status", cliData.getCLIConfig(), function (err, installationLog) {
                if(!(installationLog.includes("MasterSwapWait") && installationLog.includes("Ready")) || installationLog.includes("Started") || installationLog.includes("StandbyRebooting"))
                    checkNDUUpdateStatus();
                else
                    checkUpdatedVersion(repo);
            }).pipe(process.stdout);
        }, 20000);
    }
    else
        console.assert(false,"waited for 100 mins to check the update status to ready. But is not ready, hence failing the build");
}
function waitForOnlineStatus(host) {
    var count = 0;
    if (count <= 50) {
        setTimeout(function (host) {
            exec("ping -c 2 " + cliData.getConf(host), cliData.getKochiConfig(), function (err, iniLog) {
                //will check whether initiator is accessible
                if(!(iniLog.includes("0% packet loss")))
                    waitForOnlineStatus(host);
            }).pipe(process.stdout);
        }, 10000);
    }
    else
        console.assert(false,"waited for 10 mins for "+host+" to come online");
}

function processFromDGClean(repo) {
    if(dgCleanRequired=="YES" && esmList != undefined) {
        setTimeout(function () {
            console.log("dg clean is being triggered. Waiting for iocs to come online");
            exec("printf \"y\ny\ny\" | dgclean --esms "+esmList, cliData.getCLIConfig(), function (err, dgCleanLog) {
                if(pcscript!=undefined){
                    console.log("power cycling using script\n" +pcscript);
                    exec(pcscript, cliData.getKochiConfig(), function (err, iniLog) {
                    }).pipe(process.stdout);
                }
                //run dg clean after reboot
                var count=0;
                waitForIOC0OnlineStatus();
                function waitForIOC0OnlineStatus() {
                    if (count <= 25) {
                        setTimeout(function () {
                            exec("ping -c 2 " + hostForUpgrade, cliData.getKochiConfig(), function (err, iniLog) {
                                //will check whether initiator is accessible
                                if ((iniLog.includes("100% packet loss"))) {
                                    count++;
                                    console.log(count+"waiting for IOC0 to come online");
                                    waitForIOC0OnlineStatus();
                                }
                                else {
                                    setTimeout(function () {
                                        console.log("triggering dg clean on IOC0 after recovery");
                                        exec("printf \"y\ny\ny\" | dgclean", cliData.getCLIConfig(), function (err, dgCleanLog1) {
                                            console.assert(dgCleanLog1.includes("Success ! Please wait for ESMs to come online to create the DG."), "dg clean is not successful");
                                            setTimeout(function () {
                                                checkUpdatedVersion(repo);
                                            },10000);
                                            if(esmInitScript!= undefined){
                                                setTimeout(function () {
                                                    exec(esmInitScript, cliData.getCLIConfig(), function (err, esmInitLog) {
                                                        console.assert(esmInitLog.includes("ESM in successful"), "esm init is not successful");
                                                        console.assert(esmInitLog.includes("ESM powerond successful"), "esm init is not successful");
                                                        if (esmList != undefined && raidLevel != undefined) {
                                                            var checkCount = 0;
                                                            checkEsmStatus();
                                                            function checkEsmStatus() {
                                                                if (checkCount <= 50) {
                                                                    console.log(esmListCommand);
                                                                    console.log("checking for esms status");
                                                                    exec(esmListCommand, cliData.getCLIConfig(), function (err, stdout) {
                                                                        if (!(stdout.split("Online").length == esms.length + 1)) {
                                                                            setTimeout(function () {
                                                                                console.log("waiting for esms to come online");
                                                                                checkEsmStatus();
                                                                                checkCount++;
                                                                            }, 10000);
                                                                        }
                                                                        else {
                                                                            setTimeout(function () {
                                                                                exec("vxcli dg create dg0 "+raidLevel+" NO_ENCRYPT list "+esmList, cliData.getCLIConfig(), function (err, stdout) {
                                                                                    console.assert(stdout.includes('DG Create Successful'));
                                                                                    // console.assert(stdout.includes('DG ID =  0'));
                                                                                    setTimeout(function () {
                                                                                        exec(cliData.saCreate, cliData.getCLIConfig(), function (err, stdout1) {
                                                                                            console.assert(stdout1.includes('SA Create successful'));
                                                                                            // console.assert(stdout1.includes('saId =  0'));
                                                                                            saEnable();
                                                                                            if (egCreateScript != undefined) {
                                                                                                exec(egCreateScript, cliData.getCLIConfig(), function (err, stdout3) {
                                                                                                    console.assert(stdout3.includes('EG create successful'));
                                                                                                    if (initiators != undefined && ioScript != undefined) {
                                                                                                        for (var j = 0; j < initiators.length; j++) {
                                                                                                            pingInitiators(j);
                                                                                                            runIO(j);
                                                                                                        }
                                                                                                        setTimeout(function () {
                                                                                                            exec(cliData.portPerfShow, cliData.getCLIConfig(), function (err, stdout8) {
                                                                                                                var temp = stdout8.split("r+w")[1].split("0.0M");
                                                                                                                console.log(temp.length);
                                                                                                                console.assert(!(temp.length == 18), "IO is not triggered");
                                                                                                            }).pipe(process.stdout);
                                                                                                        }, 20000);
                                                                                                    }
                                                                                                    else console.log("either initiators list or ioscript are not provided. Hence ignoring io run");
                                                                                                }).pipe(process.stdout);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                console.log("eg script is not given. Hence ignoring eg create");
                                                                                            }
                                                                                        }).pipe(process.stdout);
                                                                                    }, 600000);
                                                                                }).pipe(process.stdout);
                                                                            }, 15000);
                                                                        }
                                                                    }).pipe(process.stdout);
                                                                }
                                                                else
                                                                    console.assert(false,"waited for 10 mins for esms to come online. But esms are still not online. Hence ignoring dg creation and failing the build");
                                                            }

                                                        }
                                                        else console.log("esm list is empty hence ignoring dg creation");
                                                    }).pipe(process.stdout);
                                                },60000);
                                            }
                                        }).pipe(process.stdout);
                                    }, 10000);
                                }

                            }).pipe(process.stdout);
                        }, 20000);
                    }
                    else
                        console.assert(false, "waited for 10 mins for " + hostForUpgrade + " to come online");
                }
            }).pipe(process.stdout);
        }, 5000);
    }
    else
        console.log("build is upgraded through "+bup +" stop the IO and power cycle to get the system back with upgraded build and data persistency");
}

