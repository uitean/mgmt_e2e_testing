/**
 * Created by Ajay(Vexata) on 30-06-2016.
 */

'use strict';

var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData=dependencies.getCommonData();

var eventCatalogPage = {

    verifyGridColumnTitlesAndNonGridElements: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.eventCatalogHeader);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(eventCatalogPage.eventCatalogGrid(),commonData.tagDataTitle), commonData.tagTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIDTitle(eventCatalogPage.eventCatalogGrid()), commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(eventCatalogPage.eventCatalogGrid(),commonData.facilityDataTitle), commonData.facilityTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(eventCatalogPage.eventCatalogGrid(),commonData.severityDataTitle), commonData.severityTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(eventCatalogPage.eventCatalogGrid(),commonData.textDataTitle), commonData.textTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(eventCatalogPage.eventCatalogGrid(),commonData.causeDataTitle), commonData.causeTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(eventCatalogPage.eventCatalogGrid(),commonData.resolutionDataTitle), commonData.resolutionTitle);
    },

    verifySearchByValueAndResults: function () {
        var eventDetails=commonAssetsControlsPage.getGridFirstRowColumns(eventCatalogPage.eventCatalogGrid());
        commonAssetsControlsPage.getGridFirstRowColumns(eventCatalogPage.eventCatalogGrid()).count().then(function (count) {
            for(var i=0;i<count;i++) {
                eventDetails.get(i).getText().then(function (val) {
                    if(val!="")
                        commonAssetsControlsPage.searchByValueAndVerifyPresenceInSearchResults(eventCatalogPage.eventCatalogGrid(), val);
                });
            }
        });

    },

    verifySelectDropDown: function () {
      webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.kendoDropDown(),"Select");
        commonAssetsControlsPage.kendoDropDown().click();
        var j=0;
        commonAssetsControlsPage.kendoDDOptions(eventCatalogPage.eventActionSelectDropDown()).count().then(function (count) {
           for(var i=0;i<count;i++) {
               webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getKendoDDOption(eventCatalogPage.eventActionSelectDropDown(), j), commonData.eventCatalogDD[j]);
               webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getKendoDDOption(eventCatalogPage.eventActionSelectDropDown(),i));
               //eventCatalogPage.eventCatalogDDCheckBoxes().get(i).click();
               j++;
           }
        });
    },

    settingsIcon: function () {
      return $('.settings');
    },
    
    actionText: function () {
      return webElementActionLibrary.getElementByLabel("ACTION:");
    },

    eventCatalogDDCheckBoxes: function () {
      return eventCatalogPage.eventActionSelectDropDown().$$('input');
    },

    individualConfigureText: function () {
        return webElementActionLibrary.getElementByLabel("Individual Configure");
    },

    globalConfigureLink: function () {
        return $('[ng-click="onGlobalConfiguration()"]');
    },

    eventActionSelectDropDown: function () {
        return $('#vx-fault-catalog-dd_listbox');
    },

    eventCatalogGrid: function () {
        return $('[feature="eventsCatalogDetailsGrid"]');
    }


};
module.exports = eventCatalogPage;