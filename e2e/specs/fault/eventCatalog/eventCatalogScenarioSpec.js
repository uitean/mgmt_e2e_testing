/**
 * Created by Ajay(Vexata) on 30-06-2016.
 */


'use strict';
var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    commonData = dependencies.getCommonData(),
    eventCatalogPage = dependencies.getEventCatalogPage();

describe("Tests for verifying presence of event catalog page elements", function() {

    describe("Tests for readonly user", function () {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
            commonAssetsControlsPage.navigateToEventCatalogPage();
        });

        it("verify grid column titles", function () {
            eventCatalogPage.verifyGridColumnTitlesAndNonGridElements();
        });

        it("verify export buttons presence", function () {
            commonAssetsControlsPage.verifyPresenceOfExportButtons(eventCatalogPage.eventCatalogGrid());
        });

        it("verify event catalog page search by all the column values", function () {
            eventCatalogPage.verifySearchByValueAndResults();
        });



    });

    describe("Tests for admin user", function () {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToEventCatalogPage();
        });

        it("verify grid column titles", function () {
            eventCatalogPage.verifyGridColumnTitlesAndNonGridElements();
        });

        it("verify export buttons presence", function () {
            commonAssetsControlsPage.verifyPresenceOfExportButtons(eventCatalogPage.eventCatalogGrid());
        });

        it("verify event catalog page search by all the column values", function () {
            eventCatalogPage.verifySearchByValueAndResults();
        });



    });

});

