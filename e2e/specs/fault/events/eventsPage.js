/**
 * Created by Ajay(Vexata) on 04-04-2016.
 */
'use strict';

var  commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData();

var eventsPage = {

    verifyPresenceOfElementsForReadOnlyUser: function () {
        webElementActionLibrary.verifyAbsenceOfElement(eventsPage.ackClrLabel());
        webElementActionLibrary.verifyAbsenceOfElement(eventsPage.applyButtons().first());
        eventsPage.verifyPresenceOfElementsOnEventsPage();
        eventsPage.verifyGridColumnTitles();
        eventsPage.verifyGridColumnValues();
    },

    verifyPresenceOfElementsForAdmin: function () {
        webElementActionLibrary.verifyExactText(eventsPage.ackClrLabel(),commonData.ackClrLabel);
        webElementActionLibrary.verifyPresenceOfElement(eventsPage.applyButtons().first());
        eventsPage.verifyPresenceOfElementsOnEventsPage();
        eventsPage.verifyGridColumnTitles();
        eventsPage.verifyGridColumnValues();
    },

    verifyPresenceOfElementsOnEventsPage: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getSearchBox(eventsPage.eventsFeature()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.exportToExcelButton());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.exportToPdfButton());
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.eventsHeader);
        webElementActionLibrary.verifyExactText(eventsPage.defaultLabel(),commonData.defaultTimeRangeLabel);
        //webElementActionLibrary.verifyPresenceOfElement(eventsPage.defaultRadioButton());
        eventsPage.verifyTimeSelectorDropDownOptions();
        webElementActionLibrary.verifySelectedDropDownValue(eventsPage.timeSelectorDropDown(),"3 days");
    },

    timeSelectorSection: function () {
        return $('[onsubmit="onTimeSelectorSubmit(selectedTime)"]');
    },

    timeRangeLabel: function () {
        return browser.driver.findElement(By.xpath('//span[contains(text(),"Recent Time Range")]'));
    },

    defaultLabel: function () {
        return eventsPage.timeSelectorSection().$('[for="eventDefault"]');
    },

    defaultRadioButton: function () {
        return $('#eventDefault');
    },

    timeSelectorDropDown: function () {
        return eventsPage.timeSelectorSection().$('select');
    },

    timeSelectorDropDownOptions: function () {
        return $$('[onsubmit="onTimeSelectorSubmit(selectedTime)"] select option');
    },

    verifyTimeSelectorDropDownOptions: function () {
        var options = eventsPage.timeSelectorDropDownOptions();
        expect(options.count()).toBe(9);
        options.count().then(function (count) {
            var expectedOptions = ["6 Hours","12 Hours","1 day","2 days","3 days","4 days","5 days","6 days","7 days"];
            var j=0;
            for(var i=0;i<count;i++){
                options.get(i).getText().then(function (val) {
                    expect(val).toBe(expectedOptions[j]);
                    j++;
                });
            }
        });
    },

    ackClrLabel: function () {
        return $$('.pull-right label').get(1);
    },

    applyButtons: function () {
        return $$('[name="apply"]');
    },

    eventsFeature: function () {
        return $('[feature="events"]');
    },

    acknowledgedCheckBoxInHeader: function () {
        return $('[for="ackCheckAll"] span');
    },

    clearedCheckBoxInHeader: function () {
        return $('[for="clrCheckAll"] span');
    },

    eventsFirstRow: function () {
        return eventsPage.eventsFeature().$$('tbody tr').first();
    },

    eventsFirstRowColumns: function () {
        return eventsPage.eventsFirstRow().$$("td");
    },



    verifyGridColumnTitles: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getTimeTitle(eventsPage.eventsFeature()),commonData.timeTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSourceTitle(eventsPage.eventsFeature()),commonData.sourceTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSeverityTitle(eventsPage.eventsFeature()),commonData.severityTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getCategoryTitle(eventsPage.eventsFeature()),commonData.categoryTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getMessageTitle(eventsPage.eventsFeature()),commonData.messageTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getAcknowledgedTitle(eventsPage.eventsFeature()), commonData.acknowledgedTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getClearedTitle(eventsPage.eventsFeature()), commonData.clearedTitle);
        webElementActionLibrary.verifyPresenceOfElement(eventsPage.acknowledgedCheckBoxInHeader());
        webElementActionLibrary.verifyPresenceOfElement(eventsPage.clearedCheckBoxInHeader());
    },

    verifyGridColumnValues: function () {
        commonAssetsControlsPage.getGridPagerInfoLabel(eventsPage.eventsFeature()).getText().then(function (val) {
            if(val!=commonData.noItemsToDisplay){
                var columns = eventsPage.eventsFirstRowColumns();
                columns.count().then(function (count) {
                    expect(count).toBe(Number(commonData.eventsColumnsNumber));
                    for(var i=0;i<5;i++)
                        columns.get(i).getText().then(function (val) {
                            expect(val).not.toBe("");
                        });
                });
            }
        });
    }
};
module.exports= eventsPage;