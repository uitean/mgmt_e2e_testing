/**
 * Created by Ajay(Vexata) on 04-04-2016.
 */
'use strict';

var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    eventsPage = dependencies.getEventsPage();


describe("Tests for verifying presence of fault page elements", function() {

    it("Verifying fault page elements for readonly user ", function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
        commonAssetsControlsPage.navigateToEventsPage();
        eventsPage.verifyPresenceOfElementsForReadOnlyUser();
    });

    it("Verifying fault page elements for admin ", function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToEventsPage();
        eventsPage.verifyPresenceOfElementsForAdmin();
    });


});