'use strict';

//includes all the js file dependencies required for execution of scenarios


var
    exportGroupsPage = dependencies.getExportGroupsPage(),
    commonData = dependencies.getCommonData(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    portsPage = dependencies.getPortsPage(),
    volumePage=dependencies.getVolumePage(),
    initiatorPage = dependencies.getInitiatorPage(),
    volumeGroupsPage = dependencies.getVolumeGroupsPage(),
    initiatorGroupsPage = dependencies.getInitiatorGroupsPage(),
    portGroupsPage = dependencies.getPortGroupsPage();


describe("Tests for readonly User", function() {

    beforeAll(function(){
        commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
    });

    it("readonly user verify volumes and VG grid titles and values existence and absence of create, edit, delete and remove buttons", function () {
        commonAssetsControlsPage.navigateToVolumesPage();
        webElementActionLibrary.verifyAbsenceOfElement(volumePage.createNewButton());
        webElementActionLibrary.verifyAbsenceOfElement(volumePage.editButton());
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.deleteSelectedButton(volumePage.volumeGrid()));
        volumePage.verifyGridColumnTitles(volumePage.volumeGrid());
        //volumePage.verifyGridValues();
        volumePage.volumeDetailedGridExpansionLinkFirstRow().click();
        volumePage.verifyDetailedGridColumnTitles();
        volumePage.verifyDetailedGridValues();
        volumeGroupsPage.selectVolumeGroupsTab();
        webElementActionLibrary.verifyAbsenceOfElement(volumeGroupsPage.createNewButton());
        webElementActionLibrary.verifyAbsenceOfElement(volumeGroupsPage.editButton());
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.deleteSelectedButton(volumeGroupsPage.volumeGroupGrid()));
        volumeGroupsPage.verifyGridColumnTitles();
        volumeGroupsPage.verifyGridValues();
        volumeGroupsPage.vgDetailedGridExpansionLinkFirstRow().click();
        // volumeGroupsPage.verifyDetailedGridColumnTitles();
        // volumeGroupsPage.verifyDetailedGridValues();
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.getRemoveButton(volumeGroupsPage.detailedGrid()));
    });

    it("readonly user verify initiators and IG grid titles and values existence and absence of create, edit, delete and remove buttons", function () {
        commonAssetsControlsPage.navigateToInitiatorsPage();
        webElementActionLibrary.verifyAbsenceOfElement(initiatorPage.createNewButton());
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.deleteSelectedButton(initiatorPage.initiatorGrid()));
        initiatorPage.verifyGridColumnTitles();
        initiatorPage.verifyGridValues();
        initiatorPage.initiatorDetailedGridExpansionLinkFirstRow().click();
        initiatorPage.verifyDetailedGridColumnTitles();
        initiatorPage.verifyDetailedGridValues();
        initiatorGroupsPage.selectInitiatorGroupsTab();
        webElementActionLibrary.verifyAbsenceOfElement(initiatorGroupsPage.createNewButton());
        webElementActionLibrary.verifyAbsenceOfElement(initiatorGroupsPage.editButton());
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.deleteSelectedButton(initiatorGroupsPage.initiatorGroupGrid()));
        initiatorGroupsPage.verifyGridColumnTitles();
        initiatorGroupsPage.verifyGridValues();
        initiatorGroupsPage.igDetailedGridExpansionLinkFirstRow().click();
        initiatorGroupsPage.verifyDetailedGridColumnTitles();
        initiatorGroupsPage.verifyDetailedGridValues();
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.getRemoveButton(initiatorGroupsPage.detailedGrid()));
    });

    it("readonly user verify ports and PG grid titles and values existence and absence of create, edit, delete and remove buttons", function () {
        commonAssetsControlsPage.navigateToPortsPage();
        portsPage.verifyGridColumnTitles();
        portsPage.verifyPortGridValuesExistence();
        portsPage.verifyTotalPortCount(16);
        portGroupsPage.selectPortGroupsTab();
        webElementActionLibrary.verifyAbsenceOfElement(portGroupsPage.createNewButton());
        webElementActionLibrary.verifyAbsenceOfElement(portGroupsPage.editButton());
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.deleteSelectedButton(portGroupsPage.portGroupGrid()));
        portGroupsPage.verifyGridColumnTitles();
        portGroupsPage.verifyGridValues();
        portGroupsPage.pgDetailedGridExpansionLinkFirstRow().click();
        portGroupsPage.verifyDetailedGridColumnTitles();
        portGroupsPage.verifyDetailedGridValues();
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.getRemoveButton(portGroupsPage.detailedGrid()));
    });

    it("readonly user verify EG grid titles and values existence and absence of create, edit, clone, delete and remove buttons", function () {
        commonAssetsControlsPage.navigateToExportGroupsPage();
        webElementActionLibrary.verifyAbsenceOfElement(exportGroupsPage.createNewButton());
        webElementActionLibrary.verifyAbsenceOfElement(exportGroupsPage.editButton());
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.deleteSelectedButton(exportGroupsPage.exportGroupGrid()));
        webElementActionLibrary.verifyAbsenceOfElement(exportGroupsPage.cloneButton());
        exportGroupsPage.verifyGridColumnTitles();
        exportGroupsPage.verifyGridValues();
        exportGroupsPage.egDetailedGridExpansionLinkFirstRow().click();
        webElementActionLibrary.wait(2);
        // exportGroupsPage.verifyDetailedGridVolumeTabTitles();
        // exportGroupsPage.verifyDetailedGridVolumeTabValues();
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.getRemoveButton(exportGroupsPage.detailedGrid()));
        exportGroupsPage.initiatorsLinkInDetailedGrid().click();
        webElementActionLibrary.wait(1);
        exportGroupsPage.verifyDetailedGridInitiatorTabTitles();
        exportGroupsPage.verifyDetailedGridInitiatorTabValues();
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.getRemoveButton(exportGroupsPage.detailedGrid()));
        exportGroupsPage.portsLinkInDetailedGrid().click();
        webElementActionLibrary.wait(1);
        // exportGroupsPage.verifyDetailedGridPortTabTitles();
        // exportGroupsPage.verifyDetailedGridPortTabValues();
        webElementActionLibrary.verifyAbsenceOfElement(commonAssetsControlsPage.getRemoveButton(exportGroupsPage.detailedGrid()));
    });

});