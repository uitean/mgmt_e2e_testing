
/**
 * Created by Ajay(Vexata) on 20-04-2017.
 */

var saPage = dependencies.getSAPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage(),
    commonData = dependencies.getCommonData();
var exec = require('ssh-exec');

describe("Tests to create DG / SA / EG / Run IO", function() {


    beforeAll(function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToSAPage();
    });


    describe("Tests to create SA and DG",function(){

        it("Test to create SA and DG", function(){
            saPage.createDGAndSA(["5","4"],"FALSE","RAID5");
        });
    });
});
