/**
 * Created by Ajay(Vexata) on 04-10-2018.
 */

'use strict';
var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    pmPage= dependencies.getPmPage();


describe("Tests for verifying presence of policy management elements", function() {

    describe("Tests for admin", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToPMPage();
        });

        it("Verifying policy management page header", function () {
            pmPage.verifyPMPageHeader();
        });

        it("Verifying policy management page mandatory Elements like create, clone and delete buttons with local policy check", function () {
            pmPage.verifyPresenceOfMandatoryElements();
        });

        it("Verifying policy management page general tab elements", function () {
            pmPage.verifyGeneralTabElements();
        });

        it("Verifying policy management page communications tab elements", function () {
            pmPage.verifyCommunicationsTabElements();
        });

        it("Verifying policy management page communications tab cloud connect elements", function () {
            pmPage.verifyCloudConnectElements();
        });

        it("Verifying policy management page communications tab > internet connection elements", function () {
            pmPage.verifyInternetConnectionElements();
        });

        it("Verifying policy management page communications tab email elements", function () {
            pmPage.verifyEmailElements();
        });

        it("Verifying policy management page communications tab archive elements", function () {
            pmPage.verifyArchiveElements();
        });

        it("Verifying policy management page subscriptions tab cloud connect elements", function () {
            pmPage.verifySubscriptionTabElements();
        });

        it("Verifying policy management page event actions tab elements", function () {
            pmPage.verifyEventActionTabElements();
        });

        it("Verifying policy management page sys log tab elements", function () {
            pmPage.verifySysLogTabElements();
        });

        it("Verifying policy management page snmp trap tab elements", function () {
            pmPage.verifySnmpTrapTabElements();
        });

        it("Verifying policy management page ldap tab elements", function () {
            pmPage.verifySnmpTrapTabElements();
        });
    });
});

