/**
 * Created by Ajay(Vexata) on 04-10-2018.
 */

'use strict';

var  commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData();

var pmPage= {


    verifyPMPageHeader: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.policyManagementHeader);
    },

    verifyPresenceOfMandatoryElements: function () {
        webElementActionLibrary.verifyPresenceOfElement(pmPage.createNewPolicyBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.saveButton());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.getPolicyCloneButtonUsingIndex(0));
        webElementActionLibrary.verifyTextPresence(pmPage.getPolicyTitleUsingIndex(0),"local");
        webElementActionLibrary.verifyAbsenceOfElement(pmPage.getPolicyDeleteBtnUsingIndex(0));
    },

    verifyGeneralTabElements: function () {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(),commonData.generalTabText);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.policyNameInput());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.policyDescInput());
        webElementActionLibrary.verifyExactTextUsingContainByGetAttribute(pmPage.policyNameInput(),"local");
        webElementActionLibrary.verifyExactTextUsingContainByGetAttribute(pmPage.policyDescInput(),"default local policy");
    },

    verifyCommunicationsTabElements: function () {
        pmPage.communicationsTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(),commonData.communicationsTabHeader);
    },

    verifyCloudConnectElements: function () {
        pmPage.cloudConnectTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyExactText(pmPage.communicationsActiveInternalTabHeader(),commonData.cloudConnectHeader);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.cloudUserName());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.cloudAccessKey());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.cloudSecretKey());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.cloudDepartment());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.cloudTestBtn());
    },

    verifyInternetConnectionElements: function () {
        pmPage.internetConnectionTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyExactText(pmPage.communicationsActiveInternalTabHeader(),commonData.icTabHeader);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.directInternetConnectionRadioBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.httpProxyRadioBtn());
        pmPage.httpProxyRadioBtn().click();
        webElementActionLibrary.verifyPresenceOfElement(pmPage.icHostName());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.icPortNum());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.icUseAuthenticationCheckBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.icUserName());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.icPassword());
    },

    verifyEmailElements: function () {
        pmPage.emailTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyExactText(pmPage.communicationsActiveInternalTabHeader(),commonData.eventCatalogDD[0]);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.vexataEmailServiceRadioBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.vexataEmailServiceFrom());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerEmailServiceRadioBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerEmailServiceServer());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerEmailServicePort());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerEmailServiceFrom());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerServiceTLSCheckBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerServiceAuthenticateCheckBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerServiceUserName());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerServicePassword());

    },

    verifyArchiveElements: function () {
        pmPage.archiveTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyExactText(pmPage.communicationsActiveInternalTabHeader(),commonData.archiveHeader);
        webElementActionLibrary.verifyExactText(pmPage.archiveHeader(),"On-Premise Archive");
        webElementActionLibrary.verifyPresenceOfElement(pmPage.archiveHostName());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.archiveUserName());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.archiveLocation());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.archiveTestBtn());
    },

    verifyEventActionTabElements: function () {
        pmPage.eventActionsLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(),commonData.eventActionsTabHeader);
        webElementActionLibrary.verifyTextPresence(pmPage.eventActionsTableHeaderColumns().get(0),"SEVERITY");
        webElementActionLibrary.verifyTextPresence(pmPage.eventActionsTableHeaderColumns().get(1),"VEXATA EMAIL");
        webElementActionLibrary.verifyTextPresence(pmPage.eventActionsTableHeaderColumns().get(2),"CUSTOMER EMAIL");
        webElementActionLibrary.verifyTextPresence(pmPage.eventActionsTableHeaderColumns().get(3),"SUPPORT SAVE");
        webElementActionLibrary.verifyTextPresence(pmPage.eventActionsTableRows().get(0),"Emergency");
        webElementActionLibrary.verifyTextPresence(pmPage.eventActionsTableRows().get(1),"Alert");
        webElementActionLibrary.verifyTextPresence(pmPage.eventActionsTableRows().get(2),"Critical");
    },

    verifySysLogTabElements: function () {
        pmPage.sysLogTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(),commonData.sysLogTAbHeader);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.addNewSysLogBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.sysLogSearchBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.deleteSelectedSysLogBtn());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getExportToExcelButton(pmPage.sysLogGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getExportToPdfButton(pmPage.sysLogGrid()));
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.sysLogGrid(),commonData.hostNameDataTitle),commonData.hostNameDataTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.sysLogGrid(),commonData.portDataTitle),commonData.portDataTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.sysLogGrid(),commonData.protocolDataTitle),commonData.protocolDataTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.sysLogGrid(),commonData.logLevelDataTitle),commonData.logLevelDataTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.sysLogGrid(),commonData.activeDataTitle),commonData.activeDataTitle);
    },

    verifySnmpTrapTabElements: function () {
        pmPage.snmpTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(),commonData.snmpTabHeader);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.addNewSnmpBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.snmpSearchBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.deleteSelectedSnmpBtn());
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getExportToExcelButton(pmPage.snmpGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getExportToPdfButton(pmPage.snmpGrid()));
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.snmpGrid(),commonData.hostNameDataTitle),commonData.hostNameDataTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.snmpGrid(),commonData.udpPortDataTitle),commonData.udpPortDataTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.snmpGrid(),commonData.communityDataTitle),commonData.communityDataTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.snmpGrid(),commonData.versionTitle),commonData.versionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.snmpGrid(),commonData.logLevelDataTitle),commonData.logLevelDataTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getColumnTitle(pmPage.snmpGrid(),commonData.activeDataTitle),commonData.activeDataTitle);
    },

    verifyLdapTabElements: function () {
        pmPage.ldapTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(),commonData.ldapTabHeader);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.localLdapRadioBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.externalLdapRadioBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.linuxLdapRadioBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.primaryLdap());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.secondaryLdap());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.adRadioBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.useTLSCheckBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.groupsDN());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.adminGroupName());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.operatorGroupName());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.usersDN());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.userNameAttribute());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.userMembershipAttribute());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.connectTimeOut());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.readTimeout());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.ldapTestConnectivity());
    },

    verifySubscriptionTabElements: function () {
        pmPage.subscriptionsTabLink().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(),commonData.subscriptionsTabHeader);
        webElementActionLibrary.verifyExactText(pmPage.subscriptionHeaderColumns().get(0),"Subscriptions");
        webElementActionLibrary.verifyExactText(pmPage.subscriptionHeaderColumns().get(1),"Customer");
        webElementActionLibrary.verifyExactText(pmPage.subscriptionHeaderColumns().get(2),"Vexata");
        webElementActionLibrary.verifyExactText(pmPage.eventsHeader(),"Events");
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerEventsSlider());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.vexataEventsSlider());
        webElementActionLibrary.verifyExactText(pmPage.supportSaveHeader(),"Support Save");
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerSSSlider());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.vexataSSSlider());
        webElementActionLibrary.verifyExactText(pmPage.telemetryHeader(),"Telemetry");
        webElementActionLibrary.verifyPresenceOfElement(pmPage.customerTelemetrySlider());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.vexataTelemetrySlider());
        pmPage.eventsSectionCollapser().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.eventsCustomerFromEmail());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.eventsCustomerToEmail());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.eventsCustomerTestMailBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.eventsVexataFromEmail());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.eventsVexataToEmail());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.eventsVexataTestMailBtn());
        pmPage.eventsSectionCollapser().click();
        webElementActionLibrary.wait(1);
        pmPage.ssSectionCollapser().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.ssCustomerParentFolder());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.ssCustomerSubFolder());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.ssCustomerTestBtn());
        pmPage.ssSectionCollapser().click();
        webElementActionLibrary.wait(1);
        pmPage.telemetrySectionCollapser().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryOnPremisesCheckBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryCustomerFromEmail());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryCustomerToEmail());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryCustomerTestEmailBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryVexataEmailCheckBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryVexataFromEmail());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryVexataToEmail());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryVexataTestEmailBtn());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryOnPremisesCheckBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryCloudConnectCheckBox());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryOnPremisesParentFolder());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryOnPremisesSubFolder());
        webElementActionLibrary.verifyPresenceOfElement(pmPage.telemetryOnPremisesTestBtn());
    },

    subscriptionHeaderColumns: function () {
        return $$('.heading [class*="column"]');
    },

    columnHeaders: function () {
        return $$('.column-1');
    },

    eventsHeader: function () {
        return pmPage.columnHeaders().get(1);
    },

    customerEventsSlider: function () {
        return $('[name="eventSubscriptionCustomer"]');
    },

    vexataEventsSlider: function () {
        return $('[name="eventSubscriptionVexata"]');
    },

    supportSaveHeader: function () {
        return pmPage.columnHeaders().get(3);
    },

    customerSSSlider: function () {
        return $('[name="supportSaveSubscriptionCustomer"]');
    },

    vexataSSSlider: function () {
        return $('[name="supportSaveSubscriptionVexata"]');
    },

    telemetryHeader: function () {
        return pmPage.columnHeaders().get(5);
    },

    customerTelemetrySlider: function () {
        return $('[name="telemetrySubscriptionCustomer"]');
    },

    vexataTelemetrySlider: function () {
        return $('[name="telemetrySubscriptionVexata"]');
    },

    eventsSectionCollapser: function () {
        return $('[ng-click="collapsed=!collapsed;"]');
    },

    ssSectionCollapser: function () {
        return $('[ng-click="ssCollapsed=!ssCollapsed;"]');
    },

    telemetrySectionCollapser: function () {
        return $('[ng-click="telCollapsed=!telCollapsed;"]');
    },

    eventsCustomerFromEmail: function () {
        return $$('[name="subscription_events_customer_from"]').get(0);
    },

    eventsVexataFromEmail: function () {
        return $$('[name="subscriptions_events_vexata_from"]').get(0);
    },

    eventsCustomerToEmail: function () {
        return $('[name="subscription_events_customer_to"]');
    },

    eventsVexataToEmail: function () {
        return $('[name="subscriptions_events_vexata_to"]');
    },

    eventsCustomerTestMailBtn: function () {
        return $('[ng-click="sceSendTestEMail()"]');
    },

    eventsVexataTestMailBtn: function () {
        return $('[ng-click="sveSendTestEmail()"]');
    },

    ssCustomerParentFolder: function () {
        return $$('[ng-model="getArchiveScope().dataDir"]').get(1);
    },

    ssCustomerSubFolder: function () {
        return $$('[placeholder="/<Sub Folder name>"]').get(0);
    },

    ssCustomerTestBtn: function () {
        return $('[ng-click="scssTestConnectivity()"]');
    },

    telemetryCustomerEmailCheckBox: function () {
        return $('[for="telemetry-customer-email-recipients"]');
    },

    telemetryVexataEmailCheckBox: function () {
        return $('[for="telemetry-vexata-email-recipients"]');
    },

    telemetryCustomerFromEmail: function () {
        return $$('[name="subscription_events_customer_from"]').get(1);
    },

    telemetryCustomerToEmail: function () {
        return $('[name="subscription_telemetry_customer_to"]');
    },

    telemetryVexataFromEmail: function () {
        return $$('[name="subscriptions_events_vexata_from"]').get(1);
    },

    telemetryVexataToEmail: function () {
        return $('[name="sub_vexata_telemetry_to"]');
    },

    telemetryCustomerTestEmailBtn: function () {
        return $('[ng-click="sctSendTestEMail()"]');
    },

    telemetryVexataTestEmailBtn: function () {
        return $('[ng-click="svtSendTestEMail(emailConfiguration)"]');
    },

    telemetryOnPremisesCheckBox: function () {
        return $('[for="telemetry-customer-on-premises"]');
    },

    telemetryCloudConnectCheckBox: function () {
        return $('[for="telemetry-vexata-cc"]');
    },

    telemetryOnPremisesParentFolder: function () {
        return $$('[ng-model="getArchiveScope().dataDir"]').get(2);
    },

    telemetryOnPremisesSubFolder: function () {
        return $$('[placeholder="/<Sub Folder name>"]').get(1);
    },

    telemetryOnPremisesTestBtn: function () {
        return $('[ng-click="sctTestConnectivity()"]');
    },

    localLdapRadioBtn: function () {
        return $('[for="vxIntLdapSelection"]');
    },

    externalLdapRadioBtn: function () {
        return $('[for="extLdapSelection"]');
    },

    linuxLdapRadioBtn: function () {
        return $('[for="ldapTypeLinux"]');
    },

    adRadioBtn: function () {
        return $('[for="ldapTypeAD"]');
    },

    primaryLdap: function () {
        return $('[name="external.primary"]');
    },

    secondaryLdap: function () {
        return $('[name="external.secondary"]');
    },

    useTLSCheckBox: function () {
        return $('[for="tlsEnabled"]');
    },

    groupsDN: function () {
        return $('[ng-model="getLDAPConfigScope().external.groupsDN"]');
    },

    adminGroupName: function () {
        return $('[name="adminGroupName"]');
    },

    operatorGroupName: function () {
        return $('[ng-model="getLDAPConfigScope().external.operatorGroupDN"]');
    },

    usersDN: function () {
        return $('[ng-model="getLDAPConfigScope().external.usersDN"]');
    },

    userNameAttribute: function () {
        return $('[ng-model="getLDAPConfigScope().external.userNameAttribute"]');
    },

    userMembershipAttribute: function () {
        return $('[ng-model="getLDAPConfigScope().external.userMembershipAttribute"]');
    },

    connectTimeOut: function () {
        return $('[name="connectTimeout"]');
    },

    readTimeout: function () {
        return $('[name="readTimeout"]');
    },

    ldapTestConnectivity: function () {
        return $('[ng-click="openTestConnectivityPopup()"]');
    },

    snmpSearchBox: function () {
        return $('[ng-model="snmp_trap_policy.search.nm"]');
    },

    snmpGrid: function () {
        return $('[feature="snmp_trap_policy"]');
    },

    addNewSnmpBtn: function () {
        return $('#snmp_trap_policy-create-btn-id');
    },

    deleteSelectedSnmpBtn: function () {
        return $('#snmp_trap_policy-delete-btn-id');
    },

    sysLogSearchBox: function () {
        return $('[ng-model="syslog_policy.search.nm"]');
    },

    addNewSysLogBtn: function () {
        return $('#syslog_policy-create-btn-id');
    },

    deleteSelectedSysLogBtn: function () {
        return $('#syslog_policy-delete-btn-id');
    },

    sysLogGrid: function () {
        return $('[feature="syslog_policy"]');
    },

    eventActionsTableHeaderColumns: function () {
        return $$('[feature="event_actions"] table tr th');
    },

    eventActionsTableRows: function () {
        return $$('[feature="event_actions"] table tr td:nth-child(1)');
    },


    archiveHeader: function () {
        return $('[ng-form="archiveForm"] .heading');
    },

    archiveHostName: function () {
        return $('#pm_ec_archive_hostname');
    },

    archiveUserName: function () {
        return $('[name="archive_user_name"]');
    },

    archiveLocation: function () {
        return $('[name="archive_location"]');
    },

    archiveTestBtn: function () {
        return $('[ng-click="ecaTestConnectivity()"]');
    },

    vexataEmailServiceRadioBtn: function () {
        return $('[for="vexata_email_settings_radio_id"]');
    },

    vexataEmailServiceFrom: function () {
        return $('[name="vexataEmailServiceFrom"]');
    },

    customerEmailServiceRadioBtn: function () {
        return $('[for="customer_email_settings_radio_id"]');
    },

    customerEmailServiceServer: function () {
        return $('[name="hostName"]');
    },

    customerEmailServicePort: function () {
        return $('[name="Port"]');
    },

    customerEmailServiceFrom: function () {
        return $('[name="customerEmailServiceFrom"]');
    },

    customerServiceTLSCheckBox: function () {
        return $('[for="external-comm_email_customer_startTLSEnable"]');
    },

    customerServiceAuthenticateCheckBox: function () {
        return $('[for="external-comm_email_customer_enableAuth"]');
    },

    customerServiceUserName: function () {
        return $('[name="username"]');
    },

    customerServicePassword: function () {
        return $('[name="password"]');
    },

    directInternetConnectionRadioBtn: function () {
        return $('[for="vx_no_proxy_selection"]');
    },

    httpProxyRadioBtn: function () {
        return $('[for="vx_http_proxy_selection"]');
    },

    icHostName: function () {
        return $('#pm_ec_internet_hostname');
    },

    icPortNum: function () {
        return $('#pm_ec_internet_port');
    },

    icUseAuthenticationCheckBox: function () {
        return $('[for="http_proxy_authenticate"]');
    },

    icUserName: function () {
        return $('[name="http_username"]');
    },

    icPassword: function () {
        return $('[name="http_password"]');
    },


    cloudConnectTabLink: function () {
        return $('[ng-class="{active: activeTab === \'cloudConnect\'}"]');
    },

    internetConnectionTabLink: function () {
        return $('[ng-class="{active: activeTab === \'internetConnection\'}"]');
    },

    emailTabLink: function () {
        return $('[ng-class="{active: activeTab === \'email\'}"]');
    },

    archiveTabLink: function () {
        return $('[ng-class="{active: activeTab === \'archive\'}"]');
    },

    communicationsActiveInternalTabHeader: function () {
        return $('li.active a[ng-click*="activeTab"]');
    },

    cloudUserName: function () {
        return $('input[name="cc_username"]');
    },

    cloudAccessKey: function () {
        return $('input[name="cc_akey"]');
    },

    cloudSecretKey: function () {
        return $('input[name="cc_skey"]');
    },

    cloudDepartment: function () {
        return $('input[name="cc_depName"]');
    },

    cloudTestBtn: function () {
        return $('[ng-click="ecccTestConnectivity()"]');
    },

    policyNameInput: function () {
        return $('input[name="name"]');
    },

    policyDescInput: function () {
        return $('textarea[name="description"]');
    },

    generalTabLink: function () {
        return $('#generalTab');
    },

    communicationsTabLink: function () {
        return $('#communicationsTab');
    },

    subscriptionsTabLink: function () {
        return $('#subscriptionsTab');
    },

    eventActionsLink: function () {
        return $('#eventsTab');
    },

    sysLogTabLink: function () {
        return $('#syslogsTab');
    },

    snmpTabLink: function () {
        return $('#snmpTrapsTab');
    },

    ldapTabLink: function () {
        return $('#ldapConfigurationTab');
    },

    policyGroupPanel: function () {
        return $('.panel-group');
    },

    policyPanels: function () {
        return pmPage.policyGroupPanel().$$('.panel-default');
    },

    getPolicyTitleUsingIndex: function (index) {
        return pmPage.policyPanels().get(index).$('.panel-title');
    },

    getPolicyDeleteBtnUsingIndex: function (index) {
        return pmPage.policyPanels().get(index).$('[ng-click="deletePolicy(policy.getId())"]');
    },

    getPolicyCloneButtonUsingIndex: function (index) {
        return pmPage.policyPanels().get(index).$('[ng-click="createPolicy(policy.getId())"]');
    },

    getPolicyDetailsLinkUsingIndex: function () {
        return pmPage.policyPanels().get(index).$('[ng-click="policyClick(policy.getId())"]');
    },

    createNewPolicyBtn: function () {
        return $('[ng-click="createPolicy()"]');
    },

    saveButton: function () {
        return $('[ng-click="savePolicy()"]');
    }



};
module.exports = pmPage;
