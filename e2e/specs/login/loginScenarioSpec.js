//'use strict';

//includes all the js file dependencies required for execution of scenarios
var loginPage = dependencies.getLoginPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    layoutPage = dependencies.getLayoutPage(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage(),
    commonData = dependencies.getCommonData();

describe("Verify the login page", function () {

    beforeAll(function () {
        webElementActionLibrary.loadUrl(commonData.homePageUrl);
    });

    it ("index.html should take to login screen and verify the login page objects display", function () {
        webElementActionLibrary.verifyCurrentUrl(commonData.loginPageUrl());
        webElementActionLibrary.verifyPresenceOfElement(loginPage.vexataLogo());
        webElementActionLibrary.verifyPresenceOfElement(loginPage.userNamePlaceHolder());
        webElementActionLibrary.verifyPresenceOfElement(loginPage.passwordPlaceHolder());
    });

    it("Give the correct admin username and password and verify whether it goes to dashboard screen and verifies the logout functionality", function() {
        loginPage.loginAs(commonData.validUserName,commonData.validPassword);
        //webElementActionLibrary.verifyCurrentUrl(commonData.dashBoardPageUrl());
        webElementActionLibrary.verifyExactText(layoutPage.userInfo(),commonData.adminUserNameInHeader);
        //Logout from the application
        webElementActionLibrary.mouseOver(layoutPage.userInfo());
        webElementActionLibrary.wait(1);
        layoutPage.logOutLink().click();
        //verify the login page
        webElementActionLibrary.verifyPresenceOfElement(loginPage.userNameField());
        webElementActionLibrary.verifyCurrentUrl(commonData.loginPageUrl());
    });

    it("login functionality validation as a readonly user", function() {
        loginPage.loginAs(commonData.readOnlyUserName,commonData.readOnlyUserPassword);
        //webElementActionLibrary.verifyCurrentUrl(commonData.dashBoardPageUrl());
        webElementActionLibrary.verifyExactText(layoutPage.userInfo(),commonData.readOnlyUserNameInHeader);
        //Logout from the application
        webElementActionLibrary.mouseOver(layoutPage.userInfo());
        webElementActionLibrary.wait(1);
        layoutPage.logOutLink().click();
        //verify the login page
        webElementActionLibrary.verifyPresenceOfElement(loginPage.userNameField());
        webElementActionLibrary.verifyCurrentUrl(commonData.loginPageUrl());
    });

    it("UI error validation for empty and invalid user details", function() {
        //error validation for empty user name and password
        loginPage.loginButton().click();
        webElementActionLibrary.verifyTextPresence(loginPage.blankUserNameError(),commonData.emptyUserNameError);
        webElementActionLibrary.verifyTextPresence(loginPage.blankPasswordError(),commonData.emptyPasswordError);
        //alert notification validation for wrong user name and password
        loginPage.loginAs(commonData.wrongUserName,commonData.wrongPassword);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.loginErrorNotification(),commonData.invalidLoginError);
        commonAssetsControlsPage.closeIcon().click();
    });

    it(" Error message validation on continuous clicks on login button", function() {
        //error validation for empty user name and password
        loginPage.loginAs(commonData.wrongUserName,commonData.wrongPassword);
        loginPage.loginButton().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.loginErrorNotifications(),commonData.invalidLoginError);
        webElementActionLibrary.verifyElementsCount(commonAssetsControlsPage.loginErrorNotifications(),1);
    });

});

