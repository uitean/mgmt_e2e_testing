/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var webElementActionLibrary = dependencies.getWebElementActionLibrary();

var loginPage =  {

    userNameField: function () {
        return element(by.model('username'));
    },

    passwordField: function () {
        return element(by.model('password'));
    },

    loginButton: function () {
        return $(".vx-login-glowy-button");
    },

    vexataLogo: function () {
        return element(by.css('.vx-login-logo'));
    },

    panelTitle: function () {
        return $('.panel-title');
    },

    userNamePlaceHolder: function () {
        return $('[placeholder="Username"]');
    },

    passwordPlaceHolder: function () {
        return $('[placeholder="Password"]');
    },

    blankUserNameError: function () {
        return $('[ng-if*="loginForm.username"]');
    },

    blankPasswordError: function () {
        return $('[ng-if*="loginForm.password"]');
    },


    loginPasswordHelp: function(){
        return $('.vx-login-password-help');
    },

    loginAs: function(userName,password){
        webElementActionLibrary.type(this.userNameField(),userName);
        webElementActionLibrary.type(this.passwordField(),password);
        this.loginButton().click();
        webElementActionLibrary.waitForAngular();
    }
};

module.exports = loginPage;