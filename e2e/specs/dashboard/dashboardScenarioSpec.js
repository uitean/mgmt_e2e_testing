/**
 * Created by Ajay(Vexata) on 01-04-2016.
 */

'use strict';
var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    dashboardPage = dependencies.getDashboardPage();


describe("Tests for verifying presence of dashboard elements", function() {

    describe("Tests for readonly user", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
            commonAssetsControlsPage.navigateToDashboardPage();
        });

        it("Verifying dashboard page header", function () {
            dashboardPage.verifyDashboardPageHeader();
        });

        it("Verifying hardware summary elements like health tick mark, alarms and warnings if present ", function () {
            dashboardPage.verifyPresenceOfHardwareSummarySectionElements();
        });

        it("Verifying dg section elements like health tick mark  ", function () {
            dashboardPage.verifyPresenceOfDGSectionElements();
        });

        it("Verifying sa capacity section elements like donut, used and total values adn tooltips, used %", function () {
            dashboardPage.verifyPresenceOfCapacitySectionElements();
        });

        it("Verifying analytics section elements like header, r/w graph labels, avg values and tooltips, graph and tooltip, legends ", function () {
            dashboardPage.verifyPresenceOfAnalyticsSectionElements();
        });

        it("Verifying time selector elements like default selected, and other selectors and +/- buttons ", function () {
            dashboardPage.verifyPresenceOfTimeSelectorElements();
        });

        xit("Verifying +/- icon changes, graph label changes, graph elements, selected interval while changing time selector ", function () {
            commonAssetsControlsPage.verifyGraphAndLabelsOnChangeOfTimeSelector(dashboardPage.bandwidthSection(),"Bandwidth");
        });

        it("Verifying time selector changes on clicking +/- icons", function () {
            commonAssetsControlsPage.verifyPlusMinusIconClickChanges();
        });

        it("Verifying drill down menu navigation of sa capacity section", function () {
            dashboardPage.verifyCapacityDrillDownFunctionality();
        });

        it("Verifying drill down menu navigation of dg section", function () {
            dashboardPage.verifyDGDrillDownFunctionality();
        });

        it("Verifying drill down menu navigation of analytics section", function () {
            dashboardPage.verifyAnalyticsDrillDownFunctionality();
        });

        it("Verifying drill down menu navigation of hardware summary section", function () {
            dashboardPage.verifyHardwareDrillDownFunctionality();
        });


    });

    describe("Tests for admin", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToDashboardPage();
        });


        it("Verifying dashboard page header", function () {
            dashboardPage.verifyDashboardPageHeader();
        });

        it("Verifying hardware summary elements like health tick mark, alarms and warnings if present ", function () {
            dashboardPage.verifyPresenceOfHardwareSummarySectionElements();
        });

        it("Verifying dg section elements like health tick mark  ", function () {
            dashboardPage.verifyPresenceOfDGSectionElements();
        });

        it("Verifying sa capacity section elements like donut, used and total values adn tooltips, used %", function () {
            dashboardPage.verifyPresenceOfCapacitySectionElements();
        });

        it("Verifying analytics section elements like header, r/w graph labels, avg values and tooltips, graph and tooltip, legends ", function () {
            dashboardPage.verifyPresenceOfAnalyticsSectionElements();
        });

        it("Verifying time selector elements like default selected, and other selectors and +/- buttons ", function () {
            dashboardPage.verifyPresenceOfTimeSelectorElements();
        });

        xit("Verifying +/- icon changes, graph label changes, graph elements, selected interval while changing time selector ", function () {
            commonAssetsControlsPage.verifyGraphAndLabelsOnChangeOfTimeSelector(dashboardPage.bandwidthSection(),"Bandwidth");
        });

        it("Verifying time selector changes on clicking +/- icons", function () {
            commonAssetsControlsPage.verifyPlusMinusIconClickChanges();
        });

        it("Verifying drill down menu navigation of sa capacity section", function () {
            dashboardPage.verifyCapacityDrillDownFunctionality();
        });

        it("Verifying drill down menu navigation of dg section", function () {
            dashboardPage.verifyDGDrillDownFunctionality();
        });

        it("Verifying drill down menu navigation of analytics section", function () {
            dashboardPage.verifyAnalyticsDrillDownFunctionality();
        });

        it("Verifying drill down menu navigation of hardware summary section", function () {
            dashboardPage.verifyHardwareDrillDownFunctionality();
        });

    });
});