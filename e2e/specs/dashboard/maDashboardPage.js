/**
 * Created by Ajay(Vexata) on 01-04-2016.
 */
'use strict';

var  commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData();

var maDashboardPage = {

    verifyMADashboardPageHeader: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.maHeader);
    },

    verifyGridColumnTitles: function (grid) {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getHardwareTitle(grid),commonData.hardwareSummary);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDGTitle(grid),commonData.dg);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getCapacityTitle(grid),commonData.capacity);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getVersionTitle(grid),commonData.versionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getHostTitle(grid),commonData.hostTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getMAEventsTitle(grid),commonData.maEventsTitle);
    },

    verifyLocalArrayPresence: function () {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getTableContent(maDashboardPage.maGrid()),hostName);
    },

    registerArray: function (array) {
        maDashboardPage.registerArrayButton().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.type(maDashboardPage.hostNameInput(),array);
        maDashboardPage.okButton().click();
        webElementActionLibrary.wait(2);
        maDashboardPage.continueRegistrationButton().click();
        webElementActionLibrary.wait(10);
        //webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
        //commonAssetsControlsPage.notification().getText().then(function (val) {
        //    console.log(val);
        //    webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.notification(),"Array "+array+" is successfully registered");
        //    commonAssetsControlsPage.closeIcon().click();
        //
        //});
        //maDashboardPage.verifyRegistrationSuccessMessage(array);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getTableContent(maDashboardPage.maGrid()),array);
    },

    goToArray: function (array) {
        webElementActionLibrary.verifyPresenceOfElement(maDashboardPage.getGoToArrayIcon(array));
        maDashboardPage.getGoToArrayIcon(array).click();
        webElementActionLibrary.wait(2);
        maDashboardPage.continueGoToArrayButton().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.verifyTextPresence(maDashboardPage.currentlyManagingArrayContent(),"Currently Managing: "+array);
        webElementActionLibrary.verifyTextPresence(maDashboardPage.currentlyManagingArrayContent(),"Go to: "+hostName);
        maDashboardPage.mainArrayLink().click();
        webElementActionLibrary.wait(2);
        maDashboardPage.goToMainArrayButton().click();
        webElementActionLibrary.wait(5);
        webElementActionLibrary.verifyTextAbsenceUsingContains(maDashboardPage.currentlyManagingArrayContent(),array);
        commonAssetsControlsPage.navigateToMADashboardPage();
    },

    unRegisterArray: function (array) {
        webElementActionLibrary.verifyPresenceOfElement(maDashboardPage.getGoToArrayIcon(array));
        maDashboardPage.unRegisterArrayIcon(array).click();
        webElementActionLibrary.wait(2);
        maDashboardPage.continueUnRegistrationButton().click();
        webElementActionLibrary.wait(10);
        //webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
        //commonAssetsControlsPage.notification().getText().then(function (val) {
        //    console.log(val);
        //    webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.notification(),"Array "+array+" is successfully unregistered");
        //    commonAssetsControlsPage.closeIcon().click();
        //});
        //maDashboardPage.verifyUnRegistrationSuccessMessage(array);
        webElementActionLibrary.verifyAbsenceOfElement(maDashboardPage.getGoToArrayIcon(array));
    },

    verifyRegistrationSuccessMessage: function (array) {
        commonAssetsControlsPage.notification().getText().then(function (val) {
            console.log(val);
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.notification(),"Array "+array+" is successfully registered");
            commonAssetsControlsPage.closeIcon().click();
        });
    },

    verifyUnRegistrationSuccessMessage: function (array) {
        webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
            commonAssetsControlsPage.notification().getText().then(function (val) {
                console.log(val);
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.notification(),"Array "+array+" is successfully unregistered");
                commonAssetsControlsPage.closeIcon().click();
            });
    },

    verifyPresenceOfRegisterButton: function () {
        webElementActionLibrary.verifyPresenceOfElement(maDashboardPage.registerArrayButton());
    },

    mainArrayLink: function () {
      return $('[ng-click="goToArrayPopup()"]');
    },

    registerArrayButton: function () {
        return $('[ng-click="onRegisterArrayBtnClick()"]');
    },

    registerArrayForm: function () {
      return $('#vx_multi_array_management_add_array_feature_form_id');
    },

    hostNameInput: function () {
      return $('[name="host_name"]');
    },

    getGoToArrayIcon: function (array) {
        return $('[ng-click*="gotoArray"][ng-click*="'+array+'"]');
    },

    unRegisterArrayIcon: function (array) {
        return $('[ng-click*="unregisterArray"][ng-click*="'+array+'"]');
    },

    okButton: function () {
      return $('#vx-multi_array_management_add_array_feature-submit-btn');
    },

    continueRegistrationButton: function () {
      return $('[ng-click*="continueRegistration"]');
    },

    continueUnRegistrationButton: function () {
        return $('[ng-click*="continueUnregistration"]');
    },

    continueGoToArrayButton: function () {
        return $('[ng-click*="continueGotoArray"]');
    },

    currentlyManagingArrayContent: function () {
      return $('[ng-show="isCurrentlyManagedArrayConnected"]');
    },

    cancelButton: function () {
      return $('#vx-multi_array_management_add_array_feature-cancel-btn');
    },

    maGrid: function () {
        return $('#multi-array-grid-container');
    },

    goToMainArrayButton: function () {
        return $('[ng-click*="goToArrayConfirm"]')
    }


};
module.exports = maDashboardPage;

