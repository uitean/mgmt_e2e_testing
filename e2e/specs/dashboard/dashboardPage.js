/**
 * Created by Ajay(Vexata) on 01-04-2016.
 */
'use strict';

var  commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData();

var dashboardPage = {

    verifyDashboardPageHeader: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.dashboardHeader);
    },

    verifyPresenceOfCapacitySectionElements: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(dashboardPage.capacitySection()),commonData.capacity);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLabelHeadings(dashboardPage.provisionedSection()).get(0),commonData.provisionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLabelHeadings(dashboardPage.provisionedSection()).get(1),commonData.used);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLabelHeadings(dashboardPage.provisionedSection()).get(2),commonData.free);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLabelHeadings(dashboardPage.physicalSection()).get(0),commonData.physical);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLabelHeadings(dashboardPage.physicalSection()).get(1),commonData.used);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getLabelHeadings(dashboardPage.physicalSection()).get(2),commonData.free);
        dashboardPage.capacityPercentage(dashboardPage.provisionedSection()).getText().then(function (val) {
            expect(val).toContain("%");
            console.log("allocated percentage is "+val);
        });
        dashboardPage.capacityPercentage(dashboardPage.physicalSection()).getText().then(function (val) {
            expect(val).toContain("%");
            console.log("used percentage is "+val);
        });
        for(var i=0;i<4;i++){
            dashboardPage.capacityValues().get(i).getText().then(function (val) {
                expect(val).not.toEqual("");
                console.log("value is "+val);
            });
        }
    },

    verifyPresenceOfHardwareSummarySectionElements: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(dashboardPage.hardwareSection()),commonData.hardwareSummary);
        webElementActionLibrary.verifyPresenceOfElement(dashboardPage.hardwareDonut());
        //dashboardPage.alarms().getAttribute("class").then(function (val) {
        //    if(val.indexOf("ng-hide")==false)
        //        webElementActionLibrary.verifyPresenceOfElement(dashboardPage.alarms());
        //});
        //dashboardPage.warnings().getAttribute("class").then(function (val) {
        //    if(val.indexOf("ng-hide")==false)
        //        webElementActionLibrary.verifyPresenceOfElement(dashboardPage.warnings());
        //});
        //dashboardPage.errors().getAttribute("class").then(function (val) {
        //    if(val.indexOf("ng-hide")==false)
        //        webElementActionLibrary.verifyPresenceOfElement(dashboardPage.errors());
        //});
    },

    verifyPresenceOfDGSectionElements: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(dashboardPage.dgSection()),commonData.dg);
        webElementActionLibrary.verifyPresenceOfElement(dashboardPage.dgDonut());
    },

    verifyPresenceOfAnalyticsSectionElements: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(dashboardPage.analyticsSection()),commonData.analytics);
        dashboardPage.verifyPresenceOfLatencySectionElements();
        dashboardPage.verifyPresenceOfBandwidthSectionElements();
        dashboardPage.verifyPresenceOfIOPSSectionElements();
    },

    verifyPresenceOfTimeSelectorElements: function () {
        webElementActionLibrary.verifyPresenceOfElement(dashboardPage.timeSelector());
        commonAssetsControlsPage.verifyPresenceOfTimeSelectorElements();
    },

    verifyPresenceOfLatencySectionElements: function () {
        webElementActionLibrary.verifyExactText(dashboardPage.getSectionTitle(dashboardPage.latencySection()),commonData.latency);
        commonAssetsControlsPage.verifyGraphSectionElements(dashboardPage.latencySection(),"Latency");
    },

    verifyPresenceOfBandwidthSectionElements: function () {
        webElementActionLibrary.verifyExactText(dashboardPage.getSectionTitle(dashboardPage.bandwidthSection()),commonData.bandwidth);
        commonAssetsControlsPage.verifyGraphSectionElements(dashboardPage.bandwidthSection(),"Bandwidth");
    },

    verifyPresenceOfIOPSSectionElements: function () {
        webElementActionLibrary.verifyExactText(dashboardPage.getSectionTitle(dashboardPage.iopsSection()),commonData.iops);
        commonAssetsControlsPage.verifyGraphSectionElements(dashboardPage.iopsSection(),"IOPS");
    },

    verifyHardwareDrillDownFunctionality: function () {
        browser.ignoreSynchronization = true;
        dashboardPage.getDrillDown(dashboardPage.hardwareSection()).click();
        webElementActionLibrary.wait(3);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.hardwareDetailsHeader);
        browser.ignoreSynchronization = false;
        //webElementActionLibrary.refresh();
        //commonAssetsControlsPage.navigateToDashboardPage();
    },

    verifyCapacityDrillDownFunctionality: function () {
        dashboardPage.getDrillDown(dashboardPage.capacitySection()).click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.saHeader);
        commonAssetsControlsPage.navigateToDashboardPage();
    },

    verifyDGDrillDownFunctionality: function () {
        dashboardPage.getDrillDown(dashboardPage.dgSection()).click();
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.dgHeader);
        commonAssetsControlsPage.navigateToDashboardPage();
    },

    verifyAnalyticsDrillDownFunctionality: function () {
        dashboardPage.getDrillDown(dashboardPage.analyticsSection()).click();
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.activeHeaderInBreadCrumb(),commonData.portAnalyticsHeader);
        commonAssetsControlsPage.navigateToDashboardPage();
    },

    getDrillDown: function (section) {
        return  section.$('.db-menu-navigation-icon');
    },

    getLeftPanels: function () {
      return $$(".vx-transparent-container-db-left-panel");
    },

    hardwareSection: function () {
        return dashboardPage.getLeftPanels().get(0);
    },

    dgSection: function () {
        return dashboardPage.getLeftPanels().get(1);
    },

    capacitySection: function () {
        return dashboardPage.getLeftPanels().get(2);
    },

    analyticsSection: function () {
        return $('[class$="right-panel"]');
    },

    hardwareDonut: function () {
        return dashboardPage.hardwareSection().$('i[ng-if*="nodeStatus"]');
    },

    dgDonut: function () {
        return dashboardPage.dgSection().$('i[ng-if*="driveGroupData.offlineDrives.length"]');
    },

    capacityDonut: function () {
        return dashboardPage.capacitySection().$('path[fill*="url(#kde"]');
    },

    capacityDonutElements: function () {
        return dashboardPage.capacitySection().$$('path[fill*="url(#kde"]');
    },

    capacityPercentage: function (section) {
        return section.$('[class*="donut-overlay"]');
    },

    capacityValues: function () {
      return $$('.vx-value');
    },

    getSummaryMessageBox: function (section) {
        return section.$('.summary-message-box');
    },

    capacitySections: function () {
        return $$('.col-xs-6');
    },

    provisionedSection: function () {
        return dashboardPage.capacitySections().first();
    },

    physicalSection: function () {
        return dashboardPage.capacitySections().last();
    },

    alarms: function () {
        return dashboardPage.hardwareSection().$('[ng-if="alarmCount > 0"]')
    },

    errors: function () {
        return dashboardPage.hardwareSection().$('[ng-if="errorCount > 0"]')
    },

    warnings: function () {
        return dashboardPage.hardwareSection().$('[ng-if="warningCount > 0"]')
    },

    getAnalyticsSections: function () {
      return $$('.chart-container-db');
    },

    latencySection: function () {
        return dashboardPage.getAnalyticsSections().get(2);
    },

    bandwidthSection: function () {
        return dashboardPage.getAnalyticsSections().get(0);
    },

    iopsSection: function () {
        return dashboardPage.getAnalyticsSections().get(1);
    },

    timeSelector: function () {
        return $('[class*="time-range"]');
    },

    getSectionTitle: function (section) {
        return section.$('[class*="title"]');
    }

};
module.exports = dashboardPage;