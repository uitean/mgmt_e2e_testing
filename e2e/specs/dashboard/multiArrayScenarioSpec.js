'use strict';
var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    maDashboardPage = dependencies.getMADashboardPage();


describe("Tests for verifying presence of multi array dashboard elements", function() {

    var array="hyperloop03-a";

    describe("Tests for admin", function() {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
            commonAssetsControlsPage.navigateToMADashboardPage();
        });


        it("Verifying multi array page header", function () {
            maDashboardPage.verifyMADashboardPageHeader();
        });

        it("Verifying grid column titles, values and register button", function () {
            maDashboardPage.verifyPresenceOfRegisterButton();
            maDashboardPage.verifyGridColumnTitles(maDashboardPage.maGrid());
            maDashboardPage.verifyLocalArrayPresence();
        });

        it("register array and verify the value in the grid", function () {
            maDashboardPage.registerArray(array);
        });

        it("verify go to array option", function () {
            maDashboardPage.goToArray(array);
        });

        it("unregister array and verify the value absence in the grid", function () {
            maDashboardPage.unRegisterArray(array);
        });

    });
});
