/**
 * Created by Ajay(Vexata) on 21-04-2017.
 */

var cliData={
//cliData
    dgCreation: function () {
        return "vxcli dg create dg0 "+raidLevel+" list "+esmList;
    },
    saCreate:"vxcli sa create Vsa0",
    saEnable:"vxcli sa enable 0",
    egCreation:
    'vxcli volume create vol1 500 GiB; ' +
    'vxcli volume create vol2 400 GiB; ' +
    'vxcli volume create vol3 300 GiB; ' +
    'vxcli vg create vg0 vol1 vol2 vol3;' +
    ' vxcli ig create ig0 0 1 2 3;' +
    ' vxcli eg create eg0 vg0:ig0:pg_all_ports;',

    portList:"vxcli port list",
    portPerfShow:"vxcli port perfshow 1 1",
    vsuRepoSetSSH:"vsu repo set ssh ",
    vsuRepoGet:"vsu repo get ",
    vsuList:"vsu list",
    vsuRemove:"vsu remove ",
    vsuRepoList:"vsu repo list",
    linuxUser:"root",
    validPassword:"vxiovxio",
    dgClean:"printf \"y\ny\ny\" | dgclean",
    rescanCommand:"rescan-scsi-bus.sh -i -r -f",
    multipathCommand:"systemctl stop multipathd; multipath -F;multipath;multipath -r -p multibus;multipath -v3;multipath -ll;",
    maimCommand:"catapult -now -p maim -Y1 -t1 -M0 -b1M -B0 -Q1 -l35 --full-device -O -%r70 &",

    getCLIConfig: function () {
        return {
            user: this.linuxUser,
            host: hostForUpgrade,
            password: this.validPassword
        }
    },

    getCLIConfigForIOC0: function () {
        return {
            user: this.linuxUser,
            host: IOC0,
            password: this.validPassword
        }
    },

    getCLIConfigForIOC1: function () {
        return {
            user: this.linuxUser,
            host: IOC1,
            password: this.validPassword
        }
    },


    getKochiConfig: function () {
        return {
            user: "vxbuild",
            host: "kochi",
            password: this.validPassword
        }
    },

    getConf: function(host){
        return {
            user: this.linuxUser,
            host: host,
            password: this.validPassword
        }
    }
};

module.exports = cliData;
