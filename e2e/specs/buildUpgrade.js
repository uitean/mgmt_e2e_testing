/**
 * Created by Ajay(Vexata) on 21-04-2017.
 */

var exec = require('ssh-exec');
var cliData = require('./cliData.js');
var master,standby,temp;
params= process.argv.slice(2).toString().split(",");
bup = params[0];
hostForUpgrade  = params[1];
if(params[2]==undefined || params[2]==""){
    console.log("ioc details are not mentioned. Hence considering this is single ioc machine and continuing to install on hostname mentioned");
    IOC0= hostForUpgrade;
    IOC1=undefined;
}
else
{
    IOC0= params[2].split(";")[0];
    IOC1= params[2].split(";")[1];
}
if(params[3]==undefined || params[3]=="") {
    repoLocation = undefined;
    console.assert(false,"repo location details are undefined and is mandatory parameter. Please run by passing valid repo location")
}
else{
    repoLocation=params[3].split(";")[2];
    userName=params[3].split(";")[0];
    repoServer=params[3].split(";")[1];
}
dgCleanRequired=params[4];
if(params[5]=="RAID5" || params[5]=="RAID6")
    raidLevel= params[5];
else
    raidLevel=undefined;
if(params[6]==undefined || params[6]=="")
    esmList= undefined;
else{
    esmList=params[6];
    esms=esmList.split(" ");
    var esmListCommand="vxcli esm list | grep '"+esms[0]+" ";
    console.log(esms);
    for (var i=1;i<esms.length;i++){
        esmListCommand = esmListCommand+"\\|"+esms[i]+" ";
    }
    esmListCommand = esmListCommand+"'";
}

if(params[7]==undefined || params[7]=="")
    egCreateScript= undefined;
else
    egCreateScript=params[7];
if(params[8]==undefined || params[8]=="")
    initiators = undefined;
else
    initiators=params[8].split(";");
if(params[9]==undefined || params[9]=="")
    ioScript= undefined;
else
    ioScript=params[9];
if(params[10]==undefined || params[10]=="")
    pcscript= undefined;
else
    pcscript=params[10];

console.log(bup+hostForUpgrade+IOC0+IOC1+repoLocation+dgCleanRequired+raidLevel+esmList+egCreateScript+initiators+ioScript+pcscript);

//set repo ssh to the given location

if (initiators != undefined && ioScript != undefined) {
    for (var j = 0; j < initiators.length; j++) {
        rebootInitiators(j);
    }
}

console.log("validating accessibility of "+cliData.getCLIConfigForIOC0().host);
exec("ping -c 2 "+cliData.getCLIConfigForIOC0().host,cliData.getKochiConfig(), function (err,ioc0Log) {
    //will check whether IOC0 is accessible
    console.assert(ioc0Log.includes("0% packet loss"),cliData.getCLIConfigForIOC0().host+" is not accessible");
    console.log("validating accessibility of "+cliData.getCLIConfigForIOC1().host);
    exec("ping -c 2 "+cliData.getCLIConfigForIOC1().host,cliData.getKochiConfig(), function (err,ioc1Log) {
        //will check whether IOC1 is accessible
        console.assert(ioc1Log.includes("0% packet loss"),cliData.getCLIConfigForIOC1().host+" is not accessible");
        if(bup=="VSUINSTALL" || bup=="NDU") {
                exec("vxmeminfo --role",cliData.getCLIConfigForIOC0(), function (err,rolelog) {
                    if(rolelog.includes("master")){
                        master = IOC0;
                        standby=IOC1;
                    }
                    else if(rolelog.includes("standby")) {
                        standby=IOC0;
                        master = IOC1;
                    }
                    else if(rolelog.includes("owner"))
                        console.assert(false,"System running in owner mode. Kindly verify vxos services are running in both IOC's and trigger NDU installation");
                    else
                        console.assert(false,"Kindly verify vxos services are running in both IOC's and trigger NDU installation");
                }).pipe(process.stdout);
            setTimeout(function () {
                console.log(master);
                console.log("setting repo ssh location to " + repoLocation + "using " + cliData.vsuRepoSetSSH + userName + "@" + repoServer + " " + repoLocation);
                exec(cliData.vsuRepoSetSSH + userName + "@" + repoServer + " " + repoLocation, cliData.getConf(master), function (err, stdout) {
                    console.assert(!stdout.includes("Error"));
                    //get vsu repo list from server
                    console.log("listing available repo package");
                    exec(cliData.vsuRepoList, cliData.getConf(master), function (err, repoList) {
                        var repo = repoList.split("Packages:")[1].trim();
                        //get local available packages on chassis
                        console.log("listing available local packages");
                        exec(cliData.vsuList, cliData.getConf(master), function (err, vsuListBeforeUpgrade) {
                            var currentPackage = getCurrentPackage(vsuListBeforeUpgrade);
                            //verifies whether teh current package and the latest package are same or not. If same, then installation will be stopped, if not installation will go through next steps
                            console.log("verifying whether the installed package is latest package");
                            if (!(currentPackage == repo)) {
                                console.log("installed package is not latest");
                                removePackagesMoreThanFourAndInstall();
                                //will check if available packages are 4. If so will remove the first package
                                function removePackagesMoreThanFourAndInstall(){
                                    exec(cliData.vsuList, cliData.getConf(master), function (err, vsuListBeforeUpgrade) {
                                        if (vsuListBeforeUpgrade.split("Packages:")[1].split("v").length > 4) {
                                            console.log("Chassis has more than 4 or 4 available packages.");
                                            var firstPackage = vsuListBeforeUpgrade.split("Packages:")[1].split("v")[1];
                                            var packageToRemove;
                                            if (!firstPackage.includes("[Current]")) {
                                                packageToRemove = "v" + vsuListBeforeUpgrade.split("Packages:")[1].split("v")[1];
                                                console.log("Removing the first package");
                                            }
                                            else {
                                                packageToRemove = "v" + vsuListBeforeUpgrade.split("Packages:")[1].split("v")[2];
                                                console.log("first package is current installed package. Hence removing second package")
                                            }
                                            exec(cliData.vsuRemove + packageToRemove, cliData.getConf(master), function (err, stdout) {
                                                removePackagesMoreThanFourAndInstall();
                                            }).pipe(process.stdout);
                                        }
                                        else
                                            getRepoAndInstall(repo);
                                    }).pipe(process.stdout);
                                }
                            }
                            else {
                                console.log("current installed package is the latest package. Hence aborting the installation");
                            }
                        }).pipe(process.stdout);
                    }).pipe(process.stdout);
                }).pipe(process.stdout);
            }, 20000);
        }
        else if(bup == "PKGINSTALL")
            updateUsingPKGInstall();
    }).pipe(process.stdout);
}).pipe(process.stdout);



function getRepoAndInstall(repo){
    if(bup=="VSUINSTALL")
        updateUsingVSUInstall(repo);
    else if(bup=="NDU")
        updateUsingNDU(repo);
    else
        console.assert(false,"update method is not valid");
}

function pingInitiators(i){
    console.log(initiators[i]);
    exec("ping -c 2 "+cliData.getConf(initiators[i]).host,cliData.getKochiConfig(), function (err,iniLog) {
        //will check whether initiator is accessible
        console.assert(iniLog.includes("0% packet loss"), initiators[i] + " is not accessible");
    }).pipe(process.stdout);
}

function runIO(j){
    setTimeout(function () {
        exec("ps -ef | grep maim", cliData.getConf(initiators[j]), function (err, stdout3) {
            exec(ioScript, cliData.getConf(initiators[j]), function (err, stdout3) {
                exec("ps -ef | grep maim", cliData.getConf(initiators[j]), function (err, stdout3) {
                }).pipe(process.stdout);
            }).pipe(process.stdout);
        }).pipe(process.stdout);
    },30000);
}

function rebootInitiators(j){
    console.log("rebooting initiator "+ initiators[j] );
        exec("reboot", cliData.getConf(initiators[j]), function (err, stdout3) {
    },30000);
}

function saEnable(){
    exec(cliData.saEnable, cliData.getCLIConfig(), function (err, stdout2) {
        console.assert(stdout2.includes('SA Enable successful'));
    }).pipe(process.stdout);
}

function updateUsingPKGInstall() {
    //get and install repo on IOC0
    console.log("copying repo on IOC0");
    exec("cd /tmp/;scp " + userName + "@" + repoServer + ":" + repoLocation + "pkg* .", cliData.getCLIConfigForIOC0(), function (err, repoGetLog) {
        console.assert(repoGetLog.includes(''));
        console.log("copied package and listing the recent ones under /tmp");
        exec("cd /tmp; ls -lt pkg*", cliData.getCLIConfigForIOC0(), function (err, repoGetLog) {
            var package = "pkg-" + repoGetLog.split(".tgz")[0].split("pkg-")[1] + ".tgz";
            var repo = "v" + repoGetLog.split(".tgz")[0].split("pkg-")[1];
            console.log("unzipping the tar file");
            exec("cd /packages; tar -xvzf /tmp/" + package, cliData.getCLIConfigForIOC0(), function (err, repoGetLog) {
                console.assert(repoGetLog.includes(repo+'/pkg_info.txt'),"repo untar is not successful");
                console.log("removing the pkg file from /tmp as it is successfully unzipped");
                exec("rm -rf /tmp/"+package, cliData.getCLIConfigForIOC0(), function (err, repoGetLog) {
                }).pipe(process.stdout);
                console.log("installing package on IOC0");
                exec("/packages/" + repo + "/pkg_postinstall.sh " + repo, cliData.getCLIConfigForIOC0(), function (err, installationLog) {
                    console.assert(installationLog.includes("VSU:BEGIN:vxd_install"), "installation is not successful");
                    console.assert(installationLog.includes("VSU:END:mgmt_update"), "installation is not successful");
                    console.log("copying repo on IOC1");
                    exec("cd /tmp/;scp " + userName + "@" + repoServer + ":" + repoLocation + "pkg* .", cliData.getCLIConfigForIOC1(), function (err, repoGetLog) {
                        console.log("copied package and listing the recent ones under /tmp");
                        exec("cd /tmp; ls -lt pkg*", cliData.getCLIConfigForIOC1(), function (err, repoGetLog) {
                            console.log("unzipping the tar file");
                            exec("cd /packages; tar -xvzf /tmp/" + package, cliData.getCLIConfigForIOC1(), function (err, repoGetLog) {
                                console.assert(repoGetLog.includes(repo+'/pkg_info.txt'),"repo untar is not successful");
                                console.log("removing the pkg file from /tmp as it is successfully unzipped");
                                exec("rm -rf /tmp/"+package, cliData.getCLIConfigForIOC1(), function (err, repoGetLog) {
                                }).pipe(process.stdout);
                                console.log("installing package on IOC1");
                                exec("/packages/" + repo + "/pkg_postinstall.sh " + repo, cliData.getCLIConfigForIOC1(), function (err, installationLog) {
                                    console.assert(installationLog.includes("VSU:BEGIN:vxd_install"), "installation is not successful");
                                    console.assert(installationLog.includes("VSU:END:mgmt_update"), "installation is not successful");
                                    processFromDGClean(repo);
                                }).pipe(process.stdout);
                            }).pipe(process.stdout);
                        }).pipe(process.stdout);
                    }).pipe(process.stdout);
                }).pipe(process.stdout);
            }).pipe(process.stdout);
        }).pipe(process.stdout);
    }).pipe(process.stdout);
}

function updateUsingVSUInstall(repo){
    //get and install repo on IOC0
    console.log("getting repo on IOC0");
    exec(cliData.vsuRepoGet + repo, cliData.getCLIConfigForIOC0(), function (err, repoGetLog) {
        console.assert(repoGetLog.includes("Done"),"repo get is not successful");
        console.log("installing package on IOC0 using vsu install "+repo+" -l");
        exec("vsu install " + repo + " -l", cliData.getCLIConfigForIOC0(), function (err, installationLog) {
            console.assert(installationLog.includes("Please reboot IOC to update versions"),"installation is not successful");
            //install repo on IOC1
            console.log("getting repo on IOC1");
            exec(cliData.vsuRepoGet + repo, cliData.getCLIConfigForIOC1(), function (err, repoGetLog) {
                console.assert(repoGetLog.includes("Done"),"repo get is not successful");
                console.log("installing package on IOC1 using vsu install "+repo+" -l");
                exec("vsu install " + repo + " -l", cliData.getCLIConfigForIOC1(), function (err, installationLog) {
                    console.assert(installationLog.includes("Please reboot IOC to update versions"),"installation is not successful");
                    //run dg clean command before reboot
                    processFromDGClean(repo);
                }).pipe(process.stdout);
            }).pipe(process.stdout);
        }).pipe(process.stdout);
    }).pipe(process.stdout);
}

function updateUsingNDU(repo){
    //get and install repo on IOC0
    console.log("getting repo on master IOC");
    exec(cliData.vsuRepoGet + repo, cliData.getConf(master), function (err, repoGetLog) {
        console.assert(repoGetLog.includes("Done"),"repo get is not successful");
        console.log("installing package using NDU");
        exec("vsu install " + repo, cliData.getConf(master), function (err, installationLog) {
            console.assert(installationLog.includes("Please run vsu update to update versions"),"installation is not successful");
            //install repo on IOC1
            console.log("running vsu update ");
            exec("printf \"y\ny\ny\" | vsu update start", cliData.getConf(master), function (err, installationLog) {
                console.assert(installationLog.includes("Rolling update was started"),"update start is not successful");
                console.assert(installationLog.includes("Use \'vsu update status\' to check on the progress"),"update start is not successful");
                checkNDUUpdateStatus(repo,0);
            }).pipe(process.stdout);
        }).pipe(process.stdout);
    }).pipe(process.stdout);
}

function getCurrentPackage(vsuListop){
    var packages =vsuListop.split("Packages:")[1].split("\n");
    var currentPackage;
    for(var i=0;i<packages.length;i++){
        if(packages[i].includes("[Current]")) {
            currentPackage = packages[i].split("[Current]")[0].trim();
            break;
        }
    }
    return currentPackage;
}

function getLatestPackage(vsuListop){
    var packages =vsuListop.split("Packages:")[1].split("\n");
    var latestPackage;
    for(var i=0;i<packages.length;i++){
        if(packages[i].includes("[Latest]")) {
            latestPackage = packages[i].split("[Latest]")[0].trim();
            break;
        }
    }
    return latestPackage;
}

function checkUpdatedVersion(repo){
    exec("vsu list", cliData.getCLIConfig(), function (err, vsuList) {
        console.log(getCurrentPackage(vsuList)+","+repo);
        console.assert(getCurrentPackage(vsuList).includes(repo),"update is not successful");
    }).pipe(process.stdout);
}
function checkNDUUpdateStatus(repo,count) {
    if (count <= 300) {
        console.log(count +". validating ndu update status");
        exec("ping -c 2 "+cliData.getConf(master),cliData.getKochiConfig(), function (err,vipLog) {
            //will check whether VIP is accessible
            if(!(vipLog.includes("0% packet loss"))) {
                temp =master;
                master = standby;
                standby=temp;
            }
            exec("vsu update status", cliData.getConf(master), function (err, installationLog) {
                if(!(installationLog.includes("MasterSwapWait") && installationLog.includes("Ready")) || installationLog.includes("Started") || installationLog.includes("StandbyRebooting")) {
                    setTimeout(function () {
                        count++;
                        checkNDUUpdateStatus(repo,count);
                    }, 20000);
                }
                else
                    checkUpdatedVersion(repo);
            }).pipe(process.stdout);
        }).pipe(process.stdout);

    }
    else
        console.assert(false,"waited for 100 mins to check the update status to ready. But is not ready, hence failing the build");
}
function waitForOnlineStatus(host) {
    var count = 0;
    if (count <= 50) {
        setTimeout(function (host) {
            exec("ping -c 2 " + cliData.getConf(host), cliData.getKochiConfig(), function (err, iniLog) {
                //will check whether initiator is accessible
                if(!(iniLog.includes("0% packet loss")))
                    waitForOnlineStatus(host);
            }).pipe(process.stdout);
        }, 10000);
    }
    else
        console.assert(false,"waited for 10 mins for "+host+" to come online");
}

function verifyIORunning(){
    exec(cliData.portPerfShow, cliData.getCLIConfig(), function (err, stdout8) {
        var temp = stdout8.split("r+w")[1].split("0.0M");
        console.log(temp.length);
        console.assert(!(temp.length == 18), "IO is not running");
    }).pipe(process.stdout);
}

function processFromDGClean(repo) {
    if(dgCleanRequired=="YES") {
        setTimeout(function () {
            console.log("dg clean is being triggered. Waiting for iocs to come online");
            exec("printf \"y\ny\ny\" | dgclean", cliData.getCLIConfigForIOC0(), function (err, dgCleanLog) {
                if(pcscript!=undefined){
                    console.log("power cycling using script\n" +pcscript);
                    exec(pcscript, cliData.getKochiConfig(), function (err, iniLog) {
                    }).pipe(process.stdout);
                }
                //run dg clean after reboot
                var count = 0,count1=0;
                waitForIOC0OnlineStatus();
                function waitForIOC0OnlineStatus() {
                    if (count <= 50) {
                        setTimeout(function () {
                            exec("ping -c 2 " + IOC0, cliData.getKochiConfig(), function (err, iniLog) {
                                //will check whether initiator is accessible
                                if ((iniLog.includes("100% packet loss"))) {
                                    count++;
                                    console.log(count+"waiting for IOC0 to come online");
                                    waitForIOC0OnlineStatus();
                                }
                                else {
                                    waitForIOC1OnlineStatus();
                                    function waitForIOC1OnlineStatus() {
                                        if (count1 <= 50) {
                                            setTimeout(function () {
                                                exec("ping -c 2 " + IOC1, cliData.getKochiConfig(), function (err, iniLog) {
                                                    //will check whether initiator is accessible
                                                    if ((iniLog.includes("100% packet loss"))) {
                                                        count1++;
                                                        console.log(count+"waiting for IOC1 to come online");
                                                        waitForIOC1OnlineStatus();
                                                    }
                                                    else {
                                                        setTimeout(function () {
                                                            console.log("triggering dg clean on IOC0 after recovery");
                                                            exec("printf \"y\ny\ny\" | dgclean", cliData.getCLIConfigForIOC0(), function (err, dgCleanLog1) {
                                                                console.log(dgCleanLog1);
                                                                console.assert(dgCleanLog1.includes("Success ! Please wait for ESMs to come online to create the DG."), "dg clean is not successful");
                                                                if (esmList != undefined && raidLevel != undefined) {
                                                                    var checkCount = 0;
                                                                    checkEsmStatus();
                                                                    function checkEsmStatus() {
                                                                        if (checkCount <= 50) {
                                                                            console.log(esmListCommand);
                                                                            console.log("checking for esms status");
                                                                            exec(esmListCommand, cliData.getCLIConfig(), function (err, stdout) {
                                                                                if (!(stdout.split("Online").length == esms.length + 1)) {
                                                                                    setTimeout(function () {
                                                                                        console.log("waiting for esms to come online");
                                                                                        checkCount++;
                                                                                        checkEsmStatus();
                                                                                    }, 10000);
                                                                                }
                                                                                else {
                                                                                    checkUpdatedVersion(repo);
                                                                                    setTimeout(function () {
                                                                                        exec(cliData.dgCreation(), cliData.getCLIConfig(), function (err, stdout) {
                                                                                            console.assert(stdout.includes('DG Create Successful'));
                                                                                            // console.assert(stdout.includes('DG ID =  0'));
                                                                                            setTimeout(function () {
                                                                                                exec(cliData.saCreate, cliData.getCLIConfig(), function (err, stdout1) {
                                                                                                    console.assert(stdout1.includes('SA Create successful'));
                                                                                                    // console.assert(stdout1.includes('saId =  0'));
                                                                                                    saEnable();
                                                                                                    if (egCreateScript != undefined) {
                                                                                                        exec(egCreateScript, cliData.getCLIConfig(), function (err, stdout3) {
                                                                                                            console.assert(stdout3.includes('EG create successful'));

                                                                                                            if (initiators != undefined && ioScript != undefined) {
                                                                                                                for (var j = 0; j < initiators.length; j++) {
                                                                                                                    pingInitiators(j);
                                                                                                                    runIO(j);
                                                                                                                }
                                                                                                                setTimeout(function () {
                                                                                                                    verifyIORunning();
                                                                                                                }, 150000);
                                                                                                            }
                                                                                                            else console.log("either initiators list or ioscript are not provided. Hence ignoring io run");
                                                                                                        }).pipe(process.stdout);
                                                                                                    }
                                                                                                    else {
                                                                                                        console.log("eg script is not given. Hence ignoring eg create");
                                                                                                    }
                                                                                                }).pipe(process.stdout);
                                                                                            }, 10000);
                                                                                        }).pipe(process.stdout);
                                                                                    }, 60000);
                                                                                }
                                                                            }).pipe(process.stdout);
                                                                        }
                                                                        else
                                                                            console.assert(false, "waited for 10 mins for esms to come online. But esms are still not online. Hence ignoring dg creation and failing the build");
                                                                    }

                                                                }
                                                                else {
                                                                    console.log("esm list is empty hence ignoring dg creation");
                                                                    checkUpdatedVersion(repo);
                                                                }
                                                            }).pipe(process.stdout);
                                                        }, 10000);
                                                    }

                                                }).pipe(process.stdout);
                                            }, 10000);
                                        }
                                        else
                                            console.assert(false, "waited for 10 mins for " + IOC1 + " to come online");
                                    }
                                }

                            }).pipe(process.stdout);
                        }, 10000);
                    }
                    else
                        console.assert(false, "waited for 10 mins for " + IOC0 + " to come online");
                }
            }).pipe(process.stdout);
        }, 5000);
    }
    else
        console.log("build is upgraded through "+bup +" stop the IO and power cycle to get the system back with upgraded build and data persistency");
}


