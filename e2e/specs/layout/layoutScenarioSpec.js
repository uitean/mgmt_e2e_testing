/**
 * Created by Ajay(Vexata) on 28-06-2016.
 */

'use strict';
var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    layoutPage = dependencies.getLayoutPage();

describe("Tests for verifying presence of header and footer elements", function() {

    describe("Tests for readonly user", function () {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
        });

        it("Dashboard page : Verifying header and footer section", function () {
            layoutPage.verifyHeaderSectionElements();
            layoutPage.verifyFooterSectionElements();
        });

        describe("SA page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToSAPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("EG page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToExportGroupsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Volumes page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToVolumesPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Initiators page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToInitiatorsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Ports page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToPortsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Port Analytics page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToPortAnalyticsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Volume analytics page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToVolumeAnalyticsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Capacity Planing page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToCapacityPlaningPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Hardware Details page", function () {

            xit("Verifying header and footer section elements", function () {
                commonAssetsControlsPage.navigateToHardwareDetailsPage();
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("DG Details page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToDGDetailsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Events page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToEventsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Health page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToHealthDetailsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Settings page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToSettingsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Event Catalog page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToEventCatalogPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        xdescribe("Event Policy page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToEventPolicyPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

    });


    describe("Tests for admin user", function () {

        beforeAll(function () {
            commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        });

        it("Dashboard page : Verifying header and footer section", function () {
            layoutPage.verifyHeaderSectionElements();
            layoutPage.verifyFooterSectionElements();
        });

        describe("SA page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToSAPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("EG page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToExportGroupsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Volumes page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToVolumesPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Initiators page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToInitiatorsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Ports page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToPortsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Port Analytics page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToPortAnalyticsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Volume analytics page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToVolumeAnalyticsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Capacity Planing page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToCapacityPlaningPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Hardware Details page", function () {

            xit("Verifying header and footer section elements", function () {
                commonAssetsControlsPage.navigateToHardwareDetailsPage();
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("DG Details page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToDGDetailsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Events page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToEventsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Health page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToHealthDetailsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Settings page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToSettingsPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        describe("Event Catalog page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToEventCatalogPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

        xdescribe("Event Policy page", function () {
            beforeAll(function () {
                commonAssetsControlsPage.navigateToEventPolicyPage();
            });

            it("Verifying header and footer section elements", function () {
                layoutPage.verifyHeaderSectionElements();
                layoutPage.verifyFooterSectionElements();
            });
        });

    });

});

