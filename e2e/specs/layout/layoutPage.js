/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData=dependencies.getCommonData();

var layoutPage =  {

    verifyHeaderSectionElements: function () {
        webElementActionLibrary.verifyPresenceOfElement(layoutPage.vexataLogo());
        webElementActionLibrary.verifyPresenceOfElement(layoutPage.verticalLine());
        webElementActionLibrary.verifyPresenceOfElement(layoutPage.downArrow());
        webElementActionLibrary.verifyPresenceOfElement(layoutPage.userLogo());
        commonAssetsControlsPage.verifyEventIconPresence(layoutPage.healthIconSectionRed(),layoutPage.healthIconRed());
        commonAssetsControlsPage.verifyEventIconPresence(layoutPage.healthIconSectionAmber(),layoutPage.healthIconAmber());
        commonAssetsControlsPage.verifyEventIconsSectionElementsPresence(layoutPage.headerSection());
        webElementActionLibrary.mouseOver(layoutPage.userInfo());
        webElementActionLibrary.wait(2);
        webElementActionLibrary.verifyExactText(layoutPage.aboutUsLink(),commonData.aboutUs);
        webElementActionLibrary.verifyExactText(layoutPage.apiLink(),commonData.apiExplorer);
    },

    verifyFooterSectionElements: function () {
        webElementActionLibrary.verifyTextPresence(layoutPage.footerSection(),commonData.vexataCopyRights);
        webElementActionLibrary.verifyTextPresence(layoutPage.footerSection(),commonData.lastRefreshText);
        webElementActionLibrary.verifyTextPresence(layoutPage.footerSection(),commonData.connectedToText);
        webElementActionLibrary.verifyExactText(layoutPage.autoRefreshLabel(),commonData.autoRefreshText);
        webElementActionLibrary.verifyTextPresence(layoutPage.footerSection(),commonData.footerSystemName);
        //webElementActionLibrary.verifyTextPresence(layoutPage.footerSection(),commonData.today);
        layoutPage.verifyAutoRefreshIntervals(commonData.autoRefreshDropDown);
        //layoutPage.verifySelectedAutoRefreshInterval(commonData.autoRefreshDropDown[1]);
        webElementActionLibrary.verifyPresenceOfElement(layoutPage.refreshIcon());
        webElementActionLibrary.verifyPresenceOfElement(layoutPage.plugIcon());
    },

    verifySelectedAutoRefreshInterval: function (selectedOption) {
        webElementActionLibrary.verifyExactText(layoutPage.selectedAutoRefresh(),selectedOption);
    },

    verifyAutoRefreshIntervals: function (intervals) {
        var actual = layoutPage.autoRefreshOptions();
        expect(actual.count()).toBe(Number(intervals.length));
        var j=0;
        actual.count().then(function (count) {
            for(var i=0;i<count;i++){
                actual.get(i).getText().then(function (val) {
                    expect(val).toBe(intervals[j]);
                    j++;
                });
            }
        });
    },

    apiLink: function () {
        return $('[ui-sref="api"]');
    },

    aboutUsLink: function () {
        return $('[ng-show="!aboutVisible"]');
    },

    selectedAutoRefresh: function () {
        return layoutPage.footerSection().$('[selected="selected"]');
    },

    autoRefreshOptions: function () {
        return commonAssetsControlsPage.autoRefreshDropDown().$$('option');
    },

    plugIcon: function () {
        return layoutPage.footerSection().$('.fa-plug');
    },

    refreshIcon: function () {
        return layoutPage.footerSection().$('.fa-refresh');
    },

    autoRefreshSection: function () {
        return layoutPage.footerSection().$('.rightAutoRefresh');
    },

    autoRefreshLabel: function () {
        return layoutPage.autoRefreshSection().$('label');
    },

    footerSection: function () {
        return $('.vx-main-footer');
    },

    downArrow: function () {
        return layoutPage.headerSection().$('.down-arrow');
    },

    userLogo: function () {
        return layoutPage.headerSection().$('.avatar');
    },

    verticalLine: function () {
        return layoutPage.headerSection().$('.verticalLine');
    },

    healthIconSectionRed: function () {
        return $('[ng-show="areServerComponentStatus === \'RED\'"]');
    },

    healthIconSectionAmber: function () {
        return $('[ng-show="areServerComponentStatus === \'YELLOW\'"]');
    },


    healthIconRed: function () {
        return commonAssetsControlsPage.getEventIcon(layoutPage.healthIconSectionRed());
    },

    healthIconAmber: function () {
        return commonAssetsControlsPage.getEventIcon(layoutPage.healthIconSectionAmber());
    },

    headerSection: function () {
        return $('.vx-main-header');
    },

    vexataLogo: function () {
        return layoutPage.headerSection().$('.header-logo');
    },

    userInfo: function () {
        return $('.user-name');
    },

    logOutLink: function () {
        return $('[ng-click="logout()"]');
    }

};

module.exports = layoutPage;