/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    dataForExportGroups = dependencies.getDataForExportGroups(),
    volumeGroupsPage = dependencies.getVolumeGroupsPage(),
    initiatorGroupsPage = dependencies.getInitiatorGroupsPage(),
    portGroupsPage = dependencies.getPortGroupsPage(),
    volumePage = dependencies.getVolumePage(),
    initiatorPage = dependencies.getInitiatorPage(),
    commonData = dependencies.getCommonData(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage();

var searchBoxCSSPath='[ng-model*="search.nm"]'; // declared as variable to use it in multiple places with multiple parent elements

var exportGroupsPage =  {

    createEGWithNewVolumeAndInitiator: function (volName,iniName, wwn,pgName, egName) {
        exportGroupsPage.createNewButton().click();
        exportGroupsPage.enterEGNameAndDesc(egName,dataForExportGroups.egDesc);
        //webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.volumesTabText);
        exportGroupsPage.createVolumeForEG(volName,dataForExportGroups.volDescForEG,dataForExportGroups.volSizeForEG);
        exportGroupsPage.initiatorsLinkInEGForm().click();
        webElementActionLibrary.wait(1);
        //webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.initiatorsTabText);
        exportGroupsPage.createInitiatorForEG(iniName,wwn);
        exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("pg",pgName);
        exportGroupsPage.submitButtonInCreateAndEditPanel().click();
        webElementActionLibrary.waitForAngular();
        exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,dataForExportGroups.egDesc);
    },

    createEGWithNewVolumeAndInitiatorAndPort0: function (volName,iniName, wwn, egName) {
        exportGroupsPage.createNewButton().click();
        exportGroupsPage.enterEGNameAndDesc(egName,dataForExportGroups.egDesc);
        //webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.volumesTabText);
        exportGroupsPage.createVolumeForEG(volName,dataForExportGroups.volDescForEG,dataForExportGroups.volSizeForEG);
        exportGroupsPage.initiatorsLinkInEGForm().click();
        webElementActionLibrary.wait(1);
        //webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.initiatorsTabText);
        exportGroupsPage.createInitiatorForEG(iniName,wwn);
        exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("port",0);
        exportGroupsPage.submitButtonInCreateAndEditPanel().click();
        webElementActionLibrary.waitForAngular();
        exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,dataForExportGroups.egDesc);
    },

    createVolumeForEG: function(volumeName,description,size,unit) {
        exportGroupsPage.createNewButtonForVolumeInEGForm().click();
        webElementActionLibrary.type(volumePage.volName(),volumeName);
        webElementActionLibrary.type(volumePage.volDescription(), description);
        if(unit=="MiB" || unit == "TiB")
            volumePage.selectSizeRadioButton(unit);
        volumePage.selectSizeRadioButton("MiB");
        volumePage.enterVolumeSize(size);
        exportGroupsPage.okButtonForVolumeInCreateNewFormInEGForm().click();
        exportGroupsPage.verifyVolumeAddedInVolumesGridInEGForm(volumeName);
    },

    selectAssetOrAssetGroupForEG: function (asset,parentElement) {
        webElementActionLibrary.type(exportGroupsPage.getSearchBox(parentElement),asset);
        exportGroupsPage.firstCheckBoxInEGForm(parentElement).click();
    },

    selectVGForEG: function(vgName) {
        exportGroupsPage.chooseExistingVolumesButton().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.vgTabText);
        webElementActionLibrary.type(exportGroupsPage.getSearchBox(exportGroupsPage.volumeGroupFeature()),vgName);
        exportGroupsPage.firstCheckBoxInEGForm(exportGroupsPage.volumeGroupFeature()).click();
        exportGroupsPage.okButtonForVolumeInCreateNewFormInEGForm().click();
        exportGroupsPage.verifyVolumeAddedInVolumesGridInEGForm();
    },

    selectVolumeForEG: function(volName) {
        exportGroupsPage.chooseExistingVolumesButton().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.vgTabText);
        exportGroupsPage.volumesLinkInEGChooseGrid().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.volumesTabText);
        webElementActionLibrary.type(exportGroupsPage.getSearchBox(exportGroupsPage.volumeFeature()),volName);
        exportGroupsPage.firstCheckBoxInEGForm(exportGroupsPage.volumeFeature()).click();
        exportGroupsPage.okButtonForVolumeInCreateNewFormInEGForm().click();
        exportGroupsPage.verifyVolumeAddedInVolumesGridInEGForm(volName);
    },

    volumeRadioBtn: function () {
        return $('[for="vx_eg_volumes_selection"]');
    },

    vgRadioBtn: function () {
        return $('[for="vx_eg_volume_group_selection"]');
    },

    initiatorRadioButton: function () {
        return $('[for="vx_eg_initiators_selection"]');
    },

    igRadioBtn: function () {
        return $('[for="vx_eg_initiator_group_selection"]');
    },

    portRadioButton: function () {
        return $('[for="vx_eg_port_selection"]');
    },

    pgRadioBtn: function () {
        return $('[for="vx_eg_port_group_selection"]');
    },

    volumeGroupFeature: function () {
        return $('[feature="volume_group_selector"]');
    },

    volumeFeature: function () {
        return $('[feature="volume_selector"]');
    },

    initiatorGroupFeature: function () {
        return $('[feature="initiator_group_selector"]');
    },

    initiatorFeature: function () {
        return $('[feature="initiator_selector"]');
    },

    portFeature: function () {
        return $('[feature="port_selector"]');
    },

    portGroupFeature: function () {
        return $('[feature="port_group_selector"]');
    },

    searchForEG: function (egName) {
        webElementActionLibrary.type(exportGroupsPage.searchBoxInEGPage(),egName);
        webElementActionLibrary.waitForAngular();
    },

    getSearchBox: function (grid) {
        return grid.$('[ng-model*="search.nm"]');
    },

    searchBoxInEGPage : function () {
        return $(searchBoxCSSPath);
    },

    createInitiatorForEG: function(iniName, memberId) {
        exportGroupsPage.createNewButtonForInitiatorInEGForm().click();
        webElementActionLibrary.type(initiatorPage.initiatorName(),iniName);
        webElementActionLibrary.type(initiatorPage.initiatorMemberId(),memberId);
        exportGroupsPage.okButtonForInitiatorInCreateNewFormInEGForm().click();
        exportGroupsPage.verifyInitiatorAddedInInitiatorsGridInEGForm(memberId);
    },

    selectIGForEG: function(igName) {
        exportGroupsPage.chooseExistingInitiatorsButton().click();
        exportGroupsPage.igLinkInEGChooseGrid().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.igTabText);
        webElementActionLibrary.type(exportGroupsPage.getSearchBox(exportGroupsPage.initiatorGroupFeature()),igName);
        exportGroupsPage.firstCheckBoxInEGForm(exportGroupsPage.initiatorGroupFeature()).click();
        exportGroupsPage.okButtonForInitiatorInCreateNewFormInEGForm().click();
        exportGroupsPage.verifyInitiatorAddedInInitiatorsGridInEGForm();
    },

    selectInitiatorForEG: function(wwn) {
        exportGroupsPage.chooseExistingInitiatorsButton().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.initiatorsTabText);
        webElementActionLibrary.type(exportGroupsPage.getSearchBox(exportGroupsPage.initiatorFeature()),wwn);
        exportGroupsPage.firstCheckBoxInEGForm(exportGroupsPage.initiatorFeature()).click();
        exportGroupsPage.okButtonForInitiatorInCreateNewFormInEGForm().click();
        exportGroupsPage.verifyInitiatorAddedInInitiatorsGridInEGForm(wwn);
    },

    selectFirstPG: function () {
        exportGroupsPage.chooseExistingPortsButtonInEGForm().click();
        exportGroupsPage.firstPGRadioButton().click();
        exportGroupsPage.okButtonForPGInChooseExistingPortsInEGForm().click();
    },

    firstPGRadioButton: function () {
        return $('#port_group_selector0');
    },

    firstCheckBoxInEGForm: function (parentElement) {
        return parentElement.$$('[for*="selector"]').first();
    },

    okButtonForPGInChooseExistingPortsInEGForm: function () {
        return $('#vx-port_export_groups-submit-btn');
    },

    okButtonForInitiatorInCreateNewFormInEGForm: function () {
        return $('#vx-initiator_export_groups-submit-btn');
    },

    okButtonForVolumeInCreateNewFormInEGForm: function () {
        return $('#vx-volume_export_groups-submit-btn');
    },

    verifyPortsAddedInPortsGridInEGForm: function(){
        expect(exportGroupsPage.portsFirstRowInEGForm().count()).not.toBe(0);
    },

    verifyInitiatorAddedInInitiatorsGridInEGForm: function(memberId){
        if(memberId!=undefined){
            exportGroupsPage.getAllTableRowsInEGForm(exportGroupsPage.initiatorsGridInEGForm()).getText().then(function (val) {
                expect(val).toMatch(String(memberId));
            });

        }
        else{
            expect(exportGroupsPage.getAllTableRowsInEGForm(exportGroupsPage.initiatorsGridInEGForm()).count()).not.toBe(0);
        }
    },

    verifyVolumeAddedInVolumesGridInEGForm: function (volumeName) {

        if(volumeName!=undefined){
            exportGroupsPage.getAllTableRowsInEGForm(exportGroupsPage.volumeGridInEGForm()).getText().then(function (val) {
                expect(val).toMatch(String(volumeName));
            });
        }
        else{
            expect(exportGroupsPage.getAllTableRowsInEGForm(exportGroupsPage.volumeGridInEGForm()).count()).not.toBe(0);
        }
    },

    volumesFirstRowInEGForm: function () {
        return exportGroupsPage.volumeGridInEGForm().$$('tbody tr').first();
    },

    getAllTableRowsInEGForm : function (grid) {
        return grid.$$('tbody tr');
    },

    getFirstTableRowInEGForm : function (grid) {
        return grid.$$('tbody tr').first();
    },

    getTableContentInEGForm : function (grid) {
        return grid.$('tbody');
    },

    volumeGridInEGForm: function () {
        return $('[feature="volume_export_groups"]');
    },

    chooseExistingVolumesButton: function () {
        return $('#volume_export_groups-choose_existing-btn-id');
    },

    chooseExistingInitiatorsButton: function () {
        return $('#initiator_export_groups-choose_existing-btn-id');
    },

    createNewButtonForVolumeInEGForm:  function () {
        return $('#volume_export_groups-create-btn-id');
    },

    createNewButtonForInitiatorInEGForm:  function () {
        return $('#initiator_export_groups-create-btn-id');
    },

    chooseExistingPortsButtonInEGForm: function () {
        return $('#port_export_groups-choose_existing-btn-id');
    },

    initiatorsFirstRowInEGForm: function () {
        return exportGroupsPage.initiatorsGridInEGForm().$$('tbody tr').first();
    },

    initiatorsGridInEGForm: function () {
        return $('[feature="initiator_export_groups"]');
    },

    initiatorsLinkInEGForm: function () {
        return $('li[id="1"]');
    },

    volumesLinkInEGForm: function () {
        return $('li[id="0"]');
    },

    portsFirstRowInEGForm: function () {
        return exportGroupsPage.portsGridInEGForm().$$('tbody tr').first();
    },

    portsGridInEGForm: function () {
        return $('[feature="port_export_groups"]');
    },

    portsLinkInEGForm: function () {
        return $('li[id="2"]');
    },

    navigateToPortsAndSelectFirstPG: function () {
        exportGroupsPage.portsLinkInEGForm().click();
        webElementActionLibrary.wait(1);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.portsTabText);
        exportGroupsPage.selectFirstPG();
    },

    createEGWithExistingAssetGroups: function(egName,egDesc,vgName,igName,pgName){
        exportGroupsPage.createNewButton().click();
        exportGroupsPage.enterEGNameAndDesc(egName,egDesc);
        exportGroupsPage.selectVolumesTabAndSelectExistingVGOrVol("vg",vgName);
        exportGroupsPage.selectInitiatorsTabAndSelectExistingIGOrIni("ig",igName);
        exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("pg",pgName);
        exportGroupsPage.submitButtonInCreateAndEditPanel().click();
        webElementActionLibrary.wait(2);
        exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,egDesc);
    },

    createEGWithExistingAssets: function(egName,egDesc,volName,iniName,portId){
        exportGroupsPage.createNewButton().click();
        exportGroupsPage.enterEGNameAndDesc(egName,egDesc);
        exportGroupsPage.selectVolumesTabAndSelectExistingVGOrVol("volume",volName);
        exportGroupsPage.selectInitiatorsTabAndSelectExistingIGOrIni("initiator",iniName);
        exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("port",portId);
        exportGroupsPage.submitButtonInCreateAndEditPanel().click();
        webElementActionLibrary.wait(2);
        exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,egDesc);
    },

    selectVolumesTabAndSelectExistingVGOrVol: function (type,name) {
        exportGroupsPage.volumesLinkInEGForm().click();
        webElementActionLibrary.wait(1);
        exportGroupsPage.chooseExistingVolumesButton().click();
        webElementActionLibrary.wait(2);
        if(type=="vg")
        exportGroupsPage.selectVG(name);
        if(type=="volume")
            exportGroupsPage.selectVolume(name);
        exportGroupsPage.okButtonForVolumeInCreateNewFormInEGForm().click();
    },

    selectVG: function (vgName) {
        exportGroupsPage.vgRadioBtn().click();
        exportGroupsPage.selectAssetOrAssetGroupForEG(vgName, exportGroupsPage.volumeGroupFeature());
    },

    selectVolume: function (volName) {
        exportGroupsPage.volumeRadioBtn().click();
        exportGroupsPage.selectAssetOrAssetGroupForEG(volName, exportGroupsPage.volumeFeature());
    },

    selectInitiatorsTabAndSelectExistingIGOrIni: function (type,name) {
        exportGroupsPage.initiatorsLinkInEGForm().click();
        webElementActionLibrary.wait(2);
        exportGroupsPage.chooseExistingInitiatorsButton().click();
        webElementActionLibrary.wait(2);
        if(type=="ig")
            exportGroupsPage.selectIG(name);
        if(type=="initiator")
            exportGroupsPage.selectInitiator(name);
        exportGroupsPage.okButtonForInitiatorInCreateNewFormInEGForm().click();
    },

    selectIG: function (name) {
        exportGroupsPage.igRadioBtn().click();
        exportGroupsPage.selectAssetOrAssetGroupForEG(name,exportGroupsPage.initiatorGroupFeature());
    },

    selectInitiator: function (name) {
        exportGroupsPage.initiatorRadioButton().click();
        exportGroupsPage.selectAssetOrAssetGroupForEG(name,exportGroupsPage.initiatorFeature());
    },

    selectPortsTabAndSelectExistingPGOrPort: function (type,name) {
        exportGroupsPage.portsLinkInEGForm().click();
        webElementActionLibrary.wait(2);
        //webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.portsTabText);
        exportGroupsPage.chooseExistingPortsButtonInEGForm().click();
        webElementActionLibrary.wait(2);
        if(type=="pg")
            exportGroupsPage.selectPG(name);
        if(type=="port")
            exportGroupsPage.selectPort(name);
        exportGroupsPage.okButtonForPGInChooseExistingPortsInEGForm().click();
    },

    selectPG: function (name) {
        exportGroupsPage.pgRadioBtn().click();
        exportGroupsPage.selectAssetOrAssetGroupForEG(name,exportGroupsPage.portGroupFeature());
    },

    selectPort: function (name) {
        exportGroupsPage.portRadioButton().click();
        exportGroupsPage.selectAssetOrAssetGroupForEG(name,exportGroupsPage.portFeature());
    },


    selectVGAndIGAndVerifyVolAndInitiatorDetails: function (vgName,igName,volName,memberId) {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.volumesTabText);
        exportGroupsPage.selectVGForEG(vgName);
        exportGroupsPage.verifyVolumeAddedInVolumesGridInEGForm(volName);
        exportGroupsPage.initiatorsLinkInEGForm().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.initiatorsTabText);
        exportGroupsPage.selectIGForEG(igName);
        exportGroupsPage.verifyInitiatorAddedInInitiatorsGridInEGForm(memberId);
    },

    selectVolAndInitiatorAndVerifyAddedMemberDetails: function (volName,memberId) {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.volumesTabText);
        exportGroupsPage.selectVolumeForEG(volName);
        exportGroupsPage.initiatorsLinkInEGForm().click();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.initiatorsTabText);
        exportGroupsPage.selectInitiatorForEG(memberId);
    },

    verifySuccessMessageAndEGPresenceInTheGrid: function (egName,egDesc) {
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var egId = val.split('#')[1].split('has')[0].trim();
            //validate eg creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForExportGroups.egCreationSuccessMessage1(egId));
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForExportGroups.egCreationSuccessMessage2(egName));
            commonAssetsControlsPage.closeIcon().click();
            exportGroupsPage.verifyEGDetails(egName,egDesc);
        });
    },

    enterEGNameAndDesc: function (egName,egDesc) {
        webElementActionLibrary.type(exportGroupsPage.exportGroupName(),egName);
        webElementActionLibrary.type(exportGroupsPage.exportGroupDescription(), egDesc);
    },

    deleteEG: function(egName,serviceCheckRequired){
        exportGroupsPage.searchForEG(egName);
        webElementActionLibrary.waitForAngular();
        exportGroupsPage.exportGroupsFirstRow().getText().then(function (val) {
            var egDetails = exportGroupsPage.getEGDetailsFromUI(val);
            commonAssetsControlsPage.selectAndDeleteAsset(exportGroupsPage.exportGroupGrid(),exportGroupsPage.egDeletePopup());
            webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyDeletedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForExportGroups.egDeleteSuccessMessage(egDetails.id));
            commonAssetsControlsPage.closeIcon().click();
            if(serviceCheckRequired) {
                webElementActionLibrary.refresh();
                //webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(exportGroupsPage.exportGroupGrid()), egName);
                exportGroupsPage.searchForEG(egName);
                expect(exportGroupsPage.exportGroupRows().count()).toBe(0);
            }
        });
    },

    editEG:function(egName,editEGName,egDescriptionEdited,serviceCheckRequired){
        exportGroupsPage.searchForEG(egName);
        exportGroupsPage.exportGroupsFirstRow().getText().then(function (val) {
            var egDetails = exportGroupsPage.getEGDetailsFromUI(val);
            exportGroupsPage.editButton().click();
            exportGroupsPage.enterEGNameAndDesc(editEGName,egDescriptionEdited);
            exportGroupsPage.submitButtonInCreateAndEditPanel().click();
            //validate eg update success message
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.notification(), dataForExportGroups.egEditSuccessMessage(egDetails.id,editEGName));
            commonAssetsControlsPage.closeIcon().click();
            exportGroupsPage.verifyEGDetails(editEGName, egDescriptionEdited,serviceCheckRequired);
        });
    },

    verifyEGDetails:function(egname,description,serviceCheckRequired) {
        if(serviceCheckRequired)
            webElementActionLibrary.refresh();
        //webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(exportGroupsPage.exportGroupGrid()), egname);
        exportGroupsPage.searchForEG(egname);
        exportGroupsPage.exportGroupsFirstRow().getText().then(function (val) {
            var egDetails = exportGroupsPage.getEGDetailsFromUI(val,description);
            expect(egDetails.name).toBe(egname);
            expect(egDetails.description).toBe(description);
        });
    },

    verifyEGValue: function (value) {
        exportGroupsPage.exportGroupsFirstRow().getText().then(function (val) {
            expect(val).toContain(value);
        });
    },

    verifyGridColumnTitles: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(exportGroupsPage.exportGroupGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(exportGroupsPage.exportGroupGrid()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(exportGroupsPage.exportGroupGrid()),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(exportGroupsPage.egCombinationTitle(),dataForExportGroups.egTuppleIdTitle);
        webElementActionLibrary.verifyExactText(exportGroupsPage.vgNameTitle(),dataForExportGroups.vgNameTitleInEGGrid);
        webElementActionLibrary.verifyExactText(exportGroupsPage.igNameTitle(),dataForExportGroups.igNameTitleInEGGrid);
        webElementActionLibrary.verifyExactText(exportGroupsPage.pgNameTitle(),dataForExportGroups.pgNameTitleInEGGrid);
    },

    verifyGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(exportGroupsPage.exportGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(exportGroupsPage.exportGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(exportGroupsPage.exportGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(exportGroupsPage.exportGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(exportGroupsPage.exportGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get7thColumnValue(exportGroupsPage.exportGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get8thColumnValue(exportGroupsPage.exportGroupGrid()));
    },

    verifyDetailedGridVolumeTabTitles : function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIDTitle(exportGroupsPage.detailedGridVolumesTab()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(exportGroupsPage.detailedGridVolumesTab()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getProvisionedTitle(exportGroupsPage.detailedGridVolumesTab()),commonData.provisionedTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPortsTitle(exportGroupsPage.detailedGridVolumesTab()),commonData.noOfPortsTitle);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText().last(),commonData.volumesTabText);
    },

    verifyDetailedGridVolumeTabValues: function (volName,volSize,unit) {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(exportGroupsPage.detailedGridVolumesTab()));
        if(volName!==undefined)
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.get2ndColumnValue(exportGroupsPage.detailedGridVolumesTab()),volName);
        else
            webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(exportGroupsPage.detailedGridVolumesTab()));
        if(volSize!==undefined) {
            if (unit === undefined)
                webElementActionLibrary.verifyExactText(commonAssetsControlsPage.get4thColumnValue(exportGroupsPage.detailedGridVolumesTab()), volSize + " GiB");
            else
                webElementActionLibrary.verifyExactText(commonAssetsControlsPage.get4thColumnValue(exportGroupsPage.detailedGridVolumesTab()), volSize + " " + unit);
        }
        else
            webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(exportGroupsPage.detailedGridVolumesTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(exportGroupsPage.detailedGridVolumesTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(exportGroupsPage.detailedGridVolumesTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(exportGroupsPage.detailedGridVolumesTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getITNexusButtonFirstRow(exportGroupsPage.detailedGridVolumesTab()));
    },

    compareEGDetailedGridVolumeTabValues: function (volDetails) {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getIdValue(exportGroupsPage.detailedGridVolumesTab()),volDetails.id);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getNameValue(exportGroupsPage.detailedGridVolumesTab()),volDetails.name);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getVolumeUUIDValue(exportGroupsPage.detailedGridVolumesTab()),volDetails.uuid);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getDescriptionValue(exportGroupsPage.detailedGridVolumesTab()),volDetails.description);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.get5thColumnValue(exportGroupsPage.detailedGridVolumesTab()),volDetails.size);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.get6thColumnValue(exportGroupsPage.detailedGridVolumesTab()),volDetails.used);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.get7thColumnValue(exportGroupsPage.detailedGridVolumesTab()),String(Number(volDetails.noOfVGs)+1));
    },

    verifyDetailedGridInitiatorTabTitles : function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(exportGroupsPage.detailedGridInitiatorsTab()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getWWNTitle(exportGroupsPage.detailedGridInitiatorsTab()),commonData.wwnTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDiscoveryMethodTitle(exportGroupsPage.detailedGridInitiatorsTab()),commonData.discoveryMethodTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPortsTitle(exportGroupsPage.detailedGridInitiatorsTab()),commonData.noOfPortsTitle);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText().last(),commonData.initiatorsTabText);
    },

    verifyDetailedGridInitiatorTabValues: function (wwn) {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(exportGroupsPage.detailedGridInitiatorsTab()));
        if(wwn!==undefined)
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.get2ndColumnValue(exportGroupsPage.detailedGridInitiatorsTab()),wwn);
        else
            webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(exportGroupsPage.detailedGridInitiatorsTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(exportGroupsPage.detailedGridInitiatorsTab()));
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.get5thColumnValue(exportGroupsPage.detailedGridInitiatorsTab()),"Auto");
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(exportGroupsPage.detailedGridInitiatorsTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getITNexusButtonFirstRow(exportGroupsPage.detailedGridInitiatorsTab()));
    },

    compareEGDetailedGridInitiatorDetails: function (initiatorDetails) {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getIdValue(exportGroupsPage.detailedGridInitiatorsTab()),initiatorDetails.id);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getWWNValue(exportGroupsPage.detailedGridInitiatorsTab()),initiatorDetails.wwn);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getInitiatorTypeValue(exportGroupsPage.detailedGridInitiatorsTab()),initiatorDetails.type);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getInitiatorMethodValue(exportGroupsPage.detailedGridInitiatorsTab()),initiatorDetails.discoveryMethod);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getInitiatorLoggedInPortValue(exportGroupsPage.detailedGridInitiatorsTab()),initiatorDetails.loggedInPort);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.get6thColumnValue(exportGroupsPage.detailedGridInitiatorsTab()),String(Number(initiatorDetails.noOfIGs)+1));
    },

    verifyDetailedGridPortTabTitles : function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortIdTitle(exportGroupsPage.detailedGridPortsTab()),commonData.portIdTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getWWNTitle(exportGroupsPage.detailedGridPortsTab()),commonData.wwnTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getStateTitle(exportGroupsPage.detailedGridPortsTab()),commonData.stateTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfInitiatorsTitle(exportGroupsPage.detailedGridPortsTab()),commonData.noOfInitiatorsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfVolumesTitle(exportGroupsPage.detailedGridPortsTab()),commonData.noOfVolumesTitle);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText().last(),commonData.portsTabText);
    },

    verifyDetailedGridPortTabValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(exportGroupsPage.detailedGridPortsTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(exportGroupsPage.detailedGridPortsTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(exportGroupsPage.detailedGridPortsTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(exportGroupsPage.detailedGridPortsTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(exportGroupsPage.detailedGridPortsTab()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getITNexusButtonFirstRow(exportGroupsPage.detailedGridPortsTab()));
    },

    compareEGDetailedGridPortDetails: function (portDetails){
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getIdValue(exportGroupsPage.detailedGridPortsTab()),portDetails.id);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getNameValue(exportGroupsPage.detailedGridPortsTab()),portDetails.name);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getPortStateValue(exportGroupsPage.detailedGridPortsTab()),portDetails.state);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getPortTypeValue(exportGroupsPage.detailedGridPortsTab()),portDetails.type);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getPortPhysicalIdValue(exportGroupsPage.detailedGridPortsTab()),portDetails.ppid);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.get6thColumnValue(exportGroupsPage.detailedGridPortsTab()),String(Number(portDetails.noOfPGs)+1));
    },

    initiatorsLinkInEGChooseGrid: function () {
        return webElementActionLibrary.getElementByXpath('//form[@id="vx_initiator_export_groups_form_id"]//span[contains(text(),"INITIATORS")]');
    },

    igLinkInEGChooseGrid: function () {
        return webElementActionLibrary.getElementByXpath('//span[contains(text(),"INITIATOR GROUPS")]');
    },

    vgLinkInEGChooseGrid: function () {
        return webElementActionLibrary.getElementByXpath('//span[contains(text(),"VOLUME GROUPS")]');
    },

    volumesLinkInEGChooseGrid: function () {
        return webElementActionLibrary.getElementByXpath('//form[@id="vx_volume_export_groups_form_id"]//span[contains(text(),"VOLUMES")]');
    },

    portsLinkInEGChooseGrid: function () {
        return webElementActionLibrary.getElementByXpath('//span[contains(text(),"PORTS")]');
    },

    detailedGridVolumesTab: function () {
        return $('[feature="volume_export_group_details"]');
    },

    detailedGridInitiatorsTab: function () {
        return $('[feature="initiator_export_group_details"] div');
    },

    volumesLinkInDetailedGrid: function () {
        return webElementActionLibrary.getElementByXpath('//span[contains(text(),"VOLUMES")]');
    },

    initiatorsLinkInDetailedGrid: function () {
        return webElementActionLibrary.getElementByXpath('//span[contains(text(),"INITIATORS")]');
    },

    portsLinkInDetailedGrid: function () {
        return webElementActionLibrary.getElementByXpath('//span[contains(text(),"PORTS")]');
    },

    detailedGridPortsTab: function () {
        return $('[feature="port_export_group_details"]');
    },

    egDetailedGridExpansionLinkFirstRow: function () {
        return commonAssetsControlsPage.getDetailedGridExpansionLink(exportGroupsPage.exportGroupsFirstRow());
    },

    detailedGrid : function () {
        return commonAssetsControlsPage.getDetailedGrid(exportGroupsPage.exportGroupGrid());
    },

    egCombinationTitle: function(){
        return commonAssetsControlsPage.getColumnTitle(exportGroupsPage.exportGroupGrid(),"VGID:IGID:PGID");
    },

    vgNameTitle: function(){
        return commonAssetsControlsPage.getColumnTitle(exportGroupsPage.exportGroupGrid(),"Volume Group Name");
    },

    igNameTitle: function(){
        return commonAssetsControlsPage.getColumnTitle(exportGroupsPage.exportGroupGrid(),"Initiator Group Name");
    },

    pgNameTitle: function(){
        return commonAssetsControlsPage.getColumnTitle(exportGroupsPage.exportGroupGrid(),"Port Group Name");
    },

    vgNameValue : function(){
        return commonAssetsControlsPage.getValueUsingNGBind(exportGroupsPage.exportGroupGrid(),"dataItem.vgColumn");
    },

    igNameValue : function(){
        return commonAssetsControlsPage.getValueUsingNGBind(exportGroupsPage.exportGroupGrid(),"dataItem.igColumn");
    },

    pgNameValue : function(){
        return commonAssetsControlsPage.getValueUsingNGBind(exportGroupsPage.exportGroupGrid(),"dataItem.pgColumn");
    },

    exportGroupName: function(){
        return $('#vx_eg_form_name');
    },

    exportGroupDescription: function(){
        return $('#vx_eg_form_description');
    },

    exportGroupsFirstRow:  function () {
        return exportGroupsPage.exportGroupGrid().$$('tbody tr').first();
    },

    exportGroupRows:  function () {
        return exportGroupsPage.exportGroupGrid().$$('tbody tr');
    },

    exportGroupGrid:  function () {
        return $('#exportGroupGrid');
    },

    exportGroupCreateEditPanel:  function () {
        return $('[kendo-window="exportGroupWindow"]');
    },

    createNewButton:  function () {
        return $('#export_groups-create-btn-id');
    },

    submitButtonInCreateAndEditPanel: function(){
        return $('#vx-export_groups-submit-btn');
    },

    cancelButtonInCreateAndEditPanel: function(){
        return $('#vx-export_groups-cancel-btn');
    },

    selectAssetHeader: function(){
        return $('.section_title');
    },

    editButton:  function () {
        return commonAssetsControlsPage.getEditButton(exportGroupsPage.exportGroupGrid());
    },

    nameErrorMessage:  function () {
        return $('[data-container-for="name"] .k-invalid-msg');
    },

    deleteButton:  function () {
        return exportGroupsPage.exportGroupGrid().$('.k-grid-delete');
    },

    cloneButton:  function () {
        return exportGroupsPage.exportGroupGrid().$('.k-grid-clone');
    },

    deleteEGAssets : function (vgName, volName, igName, wwn, pgName) {
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.assetsLogo());
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.mouseOver(commonAssetsControlsPage.advancedDropDown());
        webElementActionLibrary.waitForAngular();
        if(vgName!="" || volName != "") {
            commonAssetsControlsPage.manageVolumesLink().click();
            webElementActionLibrary.waitForAngular();
            if(vgName!="") {
                volumeGroupsPage.selectVolumeGroupsTab();
                volumeGroupsPage.deleteVG(vgName);
            }
            if(volName!="") {
                volumePage.selectVolumesTab();
                volumePage.deleteVolume(volName);
                webElementActionLibrary.waitForAngular();
            }
        }
        commonAssetsControlsPage.initiatorsLinkBreadCrumb().click();
        webElementActionLibrary.waitForAngular();
        if(igName!="" || wwn!="") {
            if(igName!="") {
                initiatorGroupsPage.selectInitiatorGroupsTab();
                initiatorGroupsPage.deleteIG(igName);
            }
            if(wwn!="") {
                initiatorPage.selectInitiatorsTab();
                initiatorPage.deleteInitiator(wwn);
            }
        }
        commonAssetsControlsPage.portsLinkBreadCrumb().click();
        webElementActionLibrary.waitForAngular();
        if(pgName!="" ) {
            portGroupsPage.selectPortGroupsTab();
            portGroupsPage.deletePG(pgName);
        }
    },

    getEGDetailsFromUI: function(val,description){
        var details;
        if(description=="") {
            details = {
                id: val.split(' ')[0],
                name: val.split(' ')[1],
                description: "",
                groupCombination:val.split(' ')[2],
                tuppleId:val.split(' ')[3],
                vg: val.split(' ')[4],
                ig: val.split(' ')[5],
                pg: val.split(' ')[6]
            };
        }
        else{
            details = {
                id: val.split(' ')[0],
                name: val.split(' ')[1],
                description: val.split(' ')[2],
                groupCombination:val.split(' ')[3],
                tuppleId:val.split(' ')[4],
                vg: val.split(' ')[5],
                ig: val.split(' ')[6],
                pg: val.split(' ')[7]
            };
        }
        return details;
    },

    egDeletePopup: function () {
        return $('[kendo-window="egPopupWindow"]');
    }

};

module.exports = exportGroupsPage;