var dataForExportGroups={

  //successMessages
  egCreationSuccessMessage1: function(id){return 'Details: Export Group #'+id},
  egCreationSuccessMessage2: function(name){return name+' is created successfully'},
  egDeleteSuccessMessage: function(id){return 'Details: Export Group #('+id+') has been deleted.'},
  egEditSuccessMessage: function(id){return 'Details: Export Group #('+id+') has been updated'},

  //String Constants
  defaultPgForEG:"pg_all_ports",
  iNameForEg:"TAIForEg",
  iName2ForEg:"TAI2ForEg",
  volNameForEG:"TAVForEg",
  volName2ForEG:"TAV2ForEg",
  volDescForEG:"TAVForEgDesc",
  volSizeForEG:"1",
  vgNameForEG:"TAVGForEG",
  vgDescForEG:"TAVGDescForEG",
  igDescForEG:"TAIGDescForEG",
  pgDescForEG:"TAPGDescForEG",
  igNameForEG:"TAIGForEG",
  pgNameForEG:"TAPGForEG",
  egName:"TAEG",
  egDesc:"TAEGDesc",
  exportGroupsHeader:"Export Groups",
  egTuppleIdTitle:"VGID:IGID:PGID",
  vgNameTitleInEGGrid:"VOLUME GROUP NAME",
  igNameTitleInEGGrid:"INITIATOR GROUP NAME",
  pgNameTitleInEGGrid:"PORT GROUP NAME",


  createEGPopUpTitle: "Create Export Group",
  editEGPopUpTitle: "Edit",
  editedEGName: "TAEGE",
  editedEGDescription: "TA_egDescEdited",
  emptyEGNameError: "name is required",
  selectAsset:"Select Asset",

  egName28Char:"EGNameForUIAutomation28Chara",
  egDesc63Char:"EGDescriptionForUIAutomation63CharactersToTest63CharactersTetet",
  editedEGName28Char:"EditedEGNameForUIAutomatio28",
  editedEGDesc63Char:"EditedEGDescriptionForUIAutomation63CharactersToTest63Character",

  blankVolumeListError:"Please select Volume(s) that should be part of the Export Group",
  blankInitiatorListError:"Please select Initiator(s) that should be part of the Export Group"


};

module.exports = dataForExportGroups;