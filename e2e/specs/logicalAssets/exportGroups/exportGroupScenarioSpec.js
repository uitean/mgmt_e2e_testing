'use strict';

//includes all the js file dependencies required for execution of scenarios


var
    exportGroupsPage = dependencies.getExportGroupsPage(),
    commonData = dependencies.getCommonData(),
    dataForExportGroups = dependencies.getDataForExportGroups(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    portsPage = dependencies.getPortsPage(),
    volumePage=dependencies.getVolumePage(),
    initiatorPage = dependencies.getInitiatorPage(),
    volumeGroupsPage = dependencies.getVolumeGroupsPage(),
    initiatorGroupsPage = dependencies.getInitiatorGroupsPage(),
    portGroupsPage = dependencies.getPortGroupsPage(),
    volumeService = dependencies.getVolumeService(),
    vgService=dependencies.getVolumeGroupsService(),
    initiatorService = dependencies.getInitiatorService(),
    igService = dependencies.getInitiatorGroupService(),
    pgService=dependencies.getPortGroupService();



describe("Tests for Export Groups", function() {

    var volName, firstVolDetails, volName2, iName, firstInitiatorDetails, iName2, vgName, wwn, wwn2, igName, pgName, egName, editedEGName, volDetails, initiatorDetails, runTimeWWN, runTimeVol, runTimeIni;
    volName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.volNameForEG);
    volName2 = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.volName2ForEG);
    iName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.iNameForEg);
    iName2 = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.iName2ForEg);
    wwn = initiatorPage.getInitiatorWWN();
    wwn2 = initiatorPage.getInitiatorWWN();

    beforeAll(function () {
        volumeService.createVolume(volName, dataForExportGroups.volDescForEG, dataForExportGroups.volSizeForEG);
        //volumeService.createVolume(volName2,dataForExportGroups.volDescForEG,dataForExportGroups.volSizeForEG);
        initiatorService.createInitiator(iName, "desc", wwn);
        //initiatorService.createInitiator(iName2,"desc",wwn2);
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToVolumesPage();
        volumePage.searchForVolume(volName);
        volumePage.volumeFirstRow().getText().then(function (val) {
            firstVolDetails = volumePage.getVolumeDetailsFromUI(val);
        });
        commonAssetsControlsPage.navigateToInitiatorsPage();
        initiatorPage.searchForInitiator(iName);
        initiatorPage.initiatorsFirstRow().getText().then(function (val) {
            firstInitiatorDetails = initiatorPage.getInitiatorDetailsFromUI(val);
        });
    });

    describe("Create EG", function () {

        beforeAll(function () {
            vgName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.vgNameForEG);
            igName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.igNameForEG);
            pgName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.pgNameForEG);
            vgService.createVG(vgName, dataForExportGroups.vgDescForEG, [Number(firstVolDetails.id)]);
            igService.createIG(igName, dataForExportGroups.igDescForEG, [Number(firstInitiatorDetails.id)]);
            pgService.createPG(pgName, "pg_desc", [Number("0")]);
            commonAssetsControlsPage.navigateToExportGroupsPage();
        });

        beforeEach(function () {
            runTimeVol = undefined;
            runTimeWWN = undefined;
            egName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.egName);
            commonAssetsControlsPage.navigateToExportGroupsPage();
            webElementActionLibrary.refresh();
        });

        describe("Create EG with new assets", function () {

            it("Test to verify successful EG Creation creating new volume and new initiator and selecting port", function () {
                runTimeWWN = initiatorPage.getInitiatorWWN();
                runTimeVol = webElementActionLibrary.getTimeStampAttached("rtVol");
                runTimeIni = webElementActionLibrary.getTimeStampAttached("rtIni");
                exportGroupsPage.createEGWithNewVolumeAndInitiatorAndPort0(runTimeVol, runTimeIni, runTimeWWN, egName);
                exportGroupsPage.verifyGridColumnTitles();
            });

            afterEach(function () {
                exportGroupsPage.deleteEG(egName);
                if (runTimeVol != undefined && runTimeWWN != undefined) {
                    exportGroupsPage.deleteEGAssets("vg_" + egName, runTimeVol, "ig_" + egName, runTimeWWN, "pg_" + egName);
                    commonAssetsControlsPage.navigateToExportGroupsPage();
                }
            });
        });


        describe("Create EG with existing assets", function () {

            it("Test to verify successful EG Creation selecting existing assets", function () {
                exportGroupsPage.createEGWithExistingAssets(egName, dataForExportGroups.egDesc, volName, iName, 0);
            });

            afterEach(function () {
                exportGroupsPage.deleteEG(egName);
                exportGroupsPage.deleteEGAssets("vg_" + egName, "", "ig_" + egName, "", "pg_" + egName);
                commonAssetsControlsPage.navigateToExportGroupsPage();
            });
        });

        describe("Create EG with new volume, existing Ig and PG", function () {

            it("Test to verify successful EG Creation ", function () {
                runTimeVol = webElementActionLibrary.getTimeStampAttached("rtVol");
                exportGroupsPage.createNewButton().click();
                exportGroupsPage.enterEGNameAndDesc(egName,dataForExportGroups.egDesc);
                exportGroupsPage.createVolumeForEG(runTimeVol,dataForExportGroups.volDescForEG,dataForExportGroups.volSizeForEG);
                exportGroupsPage.selectInitiatorsTabAndSelectExistingIGOrIni("ig",igName);
                exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("pg",pgName);
                exportGroupsPage.submitButtonInCreateAndEditPanel().click();
                webElementActionLibrary.wait(2);
                exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,dataForExportGroups.egDesc);

            });

            afterEach(function () {
                exportGroupsPage.deleteEG(egName);
                exportGroupsPage.deleteEGAssets("vg_" + egName, runTimeVol, "", "", "");
                commonAssetsControlsPage.navigateToExportGroupsPage();
            });
        });

        describe("Create EG with new volume, existing initiator and PG", function () {

            it("Test to verify successful EG Creation ", function () {
                runTimeVol = webElementActionLibrary.getTimeStampAttached("rtVol");
                exportGroupsPage.createNewButton().click();
                exportGroupsPage.enterEGNameAndDesc(egName,dataForExportGroups.egDesc);
                exportGroupsPage.createVolumeForEG(runTimeVol,dataForExportGroups.volDescForEG,dataForExportGroups.volSizeForEG);
                exportGroupsPage.selectInitiatorsTabAndSelectExistingIGOrIni("initiator",iName);
                exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("pg",pgName);
                exportGroupsPage.submitButtonInCreateAndEditPanel().click();
                webElementActionLibrary.wait(2);
                exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,dataForExportGroups.egDesc);

            });

            afterEach(function () {
                exportGroupsPage.deleteEG(egName);
                exportGroupsPage.deleteEGAssets("vg_" + egName, runTimeVol, "ig_"+egName, "", "");
                commonAssetsControlsPage.navigateToExportGroupsPage();
            });
        });

        describe("Create EG with new volume, existing initiator and port", function () {

            it("Test to verify successful EG Creation ", function () {
                runTimeVol = webElementActionLibrary.getTimeStampAttached("rtVol");
                exportGroupsPage.createNewButton().click();
                exportGroupsPage.enterEGNameAndDesc(egName,dataForExportGroups.egDesc);
                exportGroupsPage.createVolumeForEG(runTimeVol,dataForExportGroups.volDescForEG,dataForExportGroups.volSizeForEG);
                exportGroupsPage.selectInitiatorsTabAndSelectExistingIGOrIni("initiator",iName);
                exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("port",0);
                exportGroupsPage.submitButtonInCreateAndEditPanel().click();
                webElementActionLibrary.wait(2);
                exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,dataForExportGroups.egDesc);

            });

            afterEach(function () {
                exportGroupsPage.deleteEG(egName);
                exportGroupsPage.deleteEGAssets("vg_" + egName, runTimeVol, "ig_"+egName, "", "pg_"+egName);
                commonAssetsControlsPage.navigateToExportGroupsPage();
            });

        });

        describe("Create EG with new volume, existing ig and port", function () {

            it("Test to verify successful EG Creation ", function () {
                runTimeVol = webElementActionLibrary.getTimeStampAttached("rtVol");
                exportGroupsPage.createNewButton().click();
                exportGroupsPage.enterEGNameAndDesc(egName,dataForExportGroups.egDesc);
                exportGroupsPage.createVolumeForEG(runTimeVol,dataForExportGroups.volDescForEG,dataForExportGroups.volSizeForEG);
                exportGroupsPage.selectInitiatorsTabAndSelectExistingIGOrIni("ig",igName);
                exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("port",0);
                exportGroupsPage.submitButtonInCreateAndEditPanel().click();
                webElementActionLibrary.wait(2);
                exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,dataForExportGroups.egDesc);

            });

            afterEach(function () {
                exportGroupsPage.deleteEG(egName);
                exportGroupsPage.deleteEGAssets("vg_" + egName, runTimeVol, "", "", "pg_"+egName);
                commonAssetsControlsPage.navigateToExportGroupsPage();
            });

        });

        describe("Create EG with new volume, new initiator and existing pg", function () {

            it("Test to verify successful EG Creation ", function () {
                runTimeVol = webElementActionLibrary.getTimeStampAttached("rtVol");
                exportGroupsPage.createNewButton().click();
                exportGroupsPage.enterEGNameAndDesc(egName,dataForExportGroups.egDesc);
                exportGroupsPage.createVolumeForEG(runTimeVol,dataForExportGroups.volDescForEG,dataForExportGroups.volSizeForEG);
                exportGroupsPage.selectInitiatorsTabAndSelectExistingIGOrIni("initiator",iName);
                exportGroupsPage.selectPortsTabAndSelectExistingPGOrPort("pg",pgName);
                exportGroupsPage.submitButtonInCreateAndEditPanel().click();
                webElementActionLibrary.wait(2);
                exportGroupsPage.verifySuccessMessageAndEGPresenceInTheGrid(egName,dataForExportGroups.egDesc);

            });

            afterEach(function () {
                exportGroupsPage.deleteEG(egName);
                exportGroupsPage.deleteEGAssets("vg_" + egName, runTimeVol, "ig_"+egName, "", "");
                commonAssetsControlsPage.navigateToExportGroupsPage();
            });

        });

        it("Test to verify successful EG Creation selecting existing asset groups and deletion", function () {
            exportGroupsPage.createEGWithExistingAssetGroups(egName, dataForExportGroups.egDesc, vgName, igName, pgName);
            exportGroupsPage.deleteEG(egName);
        });

        afterAll(function () {
            exportGroupsPage.deleteEGAssets(vgName, volName, igName, iName, pgName);
        });

    });

    xdescribe("Tests to update  EG name and description with different inputs", function () {

        beforeAll(function () {
            volName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.volNameForEG);
            wwn = initiatorPage.getInitiatorWWN();
        });

        beforeEach(function () {
            commonAssetsControlsPage.navigateToExportGroupsPage();
            exportGroupsPage.createEGWithNewVolumeAndInitiator(volName, wwn, egName);
            editedEGName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.editedEGName);
        });

        it("Tests to Update EG name and description only", function () {
            exportGroupsPage.editEG(egName, editedEGName, dataForExportGroups.editedEGDescription, true);
        });

        it("Test to update EG with name as 28 characters and description as 63 characters", function () {
            editedEGName = dataForExportGroups.editedEGName28Char;
            exportGroupsPage.editEG(egName, editedEGName, dataForExportGroups.editedEGDesc63Char);
        });

        it("Test to update EG with blank description", function () {
            exportGroupsPage.editEG(egName, editedEGName, "");
        });

        afterEach(function () {
            exportGroupsPage.deleteEG(editedEGName);
            exportGroupsPage.deleteEGAssets("vg_" + egName, volName, "ig_" + egName, wwn);
        })
    });

    xdescribe("Tests to Validate EG search functionality using different EG parameters", function () {
        var egDetails, egNameForSearch;

        beforeAll(function () {
            commonAssetsControlsPage.navigateToExportGroupsPage();
            egNameForSearch = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.egName);
            igName = "ig_" + egNameForSearch;
            vgName = "vg_" + egNameForSearch;
            volName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.volNameForEG);
            wwn = initiatorPage.getInitiatorWWN();
            exportGroupsPage.createEGWithNewVolumeAndInitiator(volName, wwn, egNameForSearch);
            exportGroupsPage.exportGroupsFirstRow().getText().then(function (val) {
                egDetails = exportGroupsPage.getEGDetailsFromUI(val);
            });
        });

        beforeEach(function () {
            webElementActionLibrary.refresh();
        });

        it("Tests to search EG using id", function () {
            exportGroupsPage.searchForEG(egDetails.id);
            exportGroupsPage.verifyEGValue(egDetails.id);
        });

        it("Tests to search EG using desc", function () {
            exportGroupsPage.searchForEG(egDetails.description);
            exportGroupsPage.verifyEGValue(egDetails.description);
        });

        it("Tests to search EG using asset group Combination", function () {
            exportGroupsPage.searchForEG(egDetails.groupCombination);
            exportGroupsPage.verifyEGValue(egDetails.groupCombination);
        });

        it("Tests to search EG using asset group tupple id", function () {
            exportGroupsPage.searchForEG(egDetails.tuppleId);
            exportGroupsPage.verifyEGValue(egDetails.tuppleId);
        });

        it("Tests to search EG using vg name", function () {
            exportGroupsPage.searchForEG(egDetails.vg);
            exportGroupsPage.verifyEGValue(egDetails.vg);
        });

        it("Tests to search EG using ig name", function () {
            exportGroupsPage.searchForEG(egDetails.ig);
            exportGroupsPage.verifyEGValue(egDetails.ig);
        });

        it("Tests to search EG using pg name", function () {
            exportGroupsPage.searchForEG(egDetails.pg);
            exportGroupsPage.verifyEGValue(egDetails.pg);
        });

        it("Test to verify EG detailed grid volumes tab column titles, values", function () {
            exportGroupsPage.searchForEG(egNameForSearch);
            exportGroupsPage.egDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            exportGroupsPage.verifyDetailedGridVolumeTabTitles();
            exportGroupsPage.verifyDetailedGridVolumeTabValues(volName, dataForExportGroups.volSizeForEG);
        });

        it("Test to verify EG detailed grid initiators tab column titles, values", function () {
            exportGroupsPage.searchForEG(egNameForSearch);
            exportGroupsPage.egDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            exportGroupsPage.initiatorsLinkInDetailedGrid().click();
            webElementActionLibrary.waitForAngular();
            exportGroupsPage.verifyDetailedGridInitiatorTabTitles();
            exportGroupsPage.verifyDetailedGridInitiatorTabValues(wwn);
        });

        it("Test to verify EG detailed grid ports tab column titles, values", function () {
            exportGroupsPage.searchForEG(egNameForSearch);
            exportGroupsPage.egDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            exportGroupsPage.portsLinkInDetailedGrid().click();
            webElementActionLibrary.waitForAngular();
            exportGroupsPage.verifyDetailedGridPortTabTitles();
            exportGroupsPage.verifyDetailedGridPortTabValues();
        });

        afterAll(function () {
            exportGroupsPage.deleteEG(egNameForSearch);
            exportGroupsPage.deleteEGAssets(vgName, volName, igName, wwn);
        });
    });

    xdescribe("Tests to delete  EG with different inputs", function () {

        beforeEach(function () {
            volName = webElementActionLibrary.getTimeStampAttached(dataForExportGroups.volNameForEG);
            wwn = initiatorPage.getInitiatorWWN();
            commonAssetsControlsPage.navigateToExportGroupsPage();
            exportGroupsPage.createEGWithNewVolumeAndInitiator(volName, wwn, egName);
        });

        it("Test: Verify delete disability and ability functionality of VG, IG and PG when associated to EG and successful deletion of EG", function () {
            vgName = "vg_" + egName;
            igName = "ig_" + egName;
            commonAssetsControlsPage.navigateToVolumesPage();
            webElementActionLibrary.waitForAngular();
            volumeGroupsPage.selectVolumeGroupsTab();
            volumeGroupsPage.searchForVG(vgName);
            webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getDisabledDeleteButton(volumeGroupsPage.volumeGroupGrid()));
            commonAssetsControlsPage.initiatorsLinkBreadCrumb().click();
            webElementActionLibrary.waitForAngular();
            initiatorGroupsPage.selectInitiatorGroupsTab();
            initiatorGroupsPage.searchForIG(igName);
            webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.getDisabledDeleteButton(initiatorGroupsPage.initiatorGroupGrid()));
            commonAssetsControlsPage.navigateToExportGroupsPage();
            webElementActionLibrary.waitForAngular();
            exportGroupsPage.deleteEG(egName, true);
            commonAssetsControlsPage.navigateToVolumesPage();
            webElementActionLibrary.waitForAngular();
            volumeGroupsPage.selectVolumeGroupsTab();
            volumeGroupsPage.deleteVG(vgName);
            volumePage.selectVolumesTab();
            volumePage.deleteVolume(volName);
            commonAssetsControlsPage.initiatorsLinkBreadCrumb().click();
            webElementActionLibrary.waitForAngular();
            initiatorGroupsPage.selectInitiatorGroupsTab();
            initiatorGroupsPage.deleteIG(igName);
            initiatorPage.selectInitiatorsTab();
            initiatorPage.deleteInitiator(wwn);
        });
    });

    xdescribe("Tests to validate UI Error for Create EG Panel'", function () {

        beforeAll(function () {
            commonAssetsControlsPage.navigateToExportGroupsPage();
            exportGroupsPage.createNewButton().click();
        });

        it("Test: Verify Elements in Create EG Panel", function () {
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForExportGroups.createEGPopUpTitle);
            webElementActionLibrary.verifyTextPresence(exportGroupsPage.selectAssetHeader(), dataForExportGroups.selectAsset);
            webElementActionLibrary.mouseOver(exportGroupsPage.cancelButtonInCreateAndEditPanel());
            webElementActionLibrary.verifyPresenceOfElement(exportGroupsPage.cancelButtonInCreateAndEditPanel());
        });

        it("Test: Try creating EG with no name, description ", function () {
            exportGroupsPage.submitButtonInCreateAndEditPanel().click();
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().first(), commonData.assetNameError);
        });

        it("Test: Try creating EG with proper name and verify UI error messages for volume and initiator", function () {
            webElementActionLibrary.type(exportGroupsPage.exportGroupName(), egName);
            exportGroupsPage.submitButtonInCreateAndEditPanel().click();
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForExportGroups.blankVolumeListError);
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForExportGroups.blankInitiatorListError);
        });

        it("Test: Try creating VG with improper name and proper description, and verify UI error messages", function () {
            webElementActionLibrary.type(exportGroupsPage.exportGroupName(), egName);
            exportGroupsPage.createVolumeForEG("test", dataForExportGroups.volDescForEG, dataForExportGroups.volSizeForEG);
            exportGroupsPage.submitButtonInCreateAndEditPanel().click();
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForExportGroups.blankInitiatorListError);
            webElementActionLibrary.verifyTextAbsenceUsingContains(commonAssetsControlsPage.notification(), dataForExportGroups.blankVolumeListError);
        });

        afterAll(function () {
            commonAssetsControlsPage.closeWindow();
        });
    });

});