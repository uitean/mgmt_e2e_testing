/**
 * Created by Ajay(Vexata) on 10-04-2017.
 */

/**
 * Created by Ajay(Vexata) on 10-04-2017.
 */
'use strict';
var frisby = require('frisby');
var commonData=dependencies.getCommonData();
var commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage();
var pgApiUrl = commonData.pgApiUrl();

var portGroupsService = {


    createPG: function (name,desc,portsIdArray) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.post(pgApiUrl,
            {
                addPorts:portsIdArray,
                description:desc,
                name:name
            }, {json: true})
            .expect('status',201)
            .then(function(res){
                expect(res._json.name).toBe(name);
                expect(res._json.description).toBe(desc);
                expect(res._json.currPorts).toEqual(portsIdArray);
                console.log("pg "+res._json.name +" with id "+res._json.id+" created through rest api");
            });
    },

    getPGDetails : function (pgId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.get(pgApiUrl+pgId)
            .expect('status',200)
            .then(function (res) {
                console.log(res);
            });
    },

    deletePG: function (pgId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.del(pgApiUrl+pgId)
            .expect('status',204).then(function (res) {
                console.log("pg "+pgId+" deleted through rest api");
            })
    }
};



module.exports = portGroupsService;

