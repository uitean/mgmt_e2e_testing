/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    dataForPortGroups = dependencies.getDataForPortGroups(),
    commonData=dependencies.getCommonData(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage();


var portGroupsPage =  {

    createPG : function(pgName,pgDesc,portId,portName,serviceCheckRequired){
        webElementActionLibrary.waitForElement(portGroupsPage.createNewButton());
        portGroupsPage.createNewButton().click();
        webElementActionLibrary.type(portGroupsPage.portGroupName(), pgName);
        webElementActionLibrary.type(portGroupsPage.portGroupDescription(), pgDesc);
        //commonAssetsControlsPage.applyNameFilter(portName);
        //commonAssetsControlsPage.applyIdFilter(portId);
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBoxInForm().last(),portName);
        portGroupsPage.portCheckBox().click();
        portGroupsPage.submitButton().click();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var pgId = commonAssetsControlsPage.getIdFromNotification(val);
            //validate pg creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForPortGroups.pgCreationSuccessMessage(pgId));
            commonAssetsControlsPage.closeIcon().click();
            portGroupsPage.verifyPGDetails(pgName,pgDesc,"1",serviceCheckRequired);
        });
    },


    deletePGPopup: function () {
        return $('[kendo-window="portGroupBulkDeletePopupWindow"]')
    },

    deletePG: function(name,serviceCheckRequired){
        portGroupsPage.searchForPG(name);
        webElementActionLibrary.waitForAngular();
        portGroupsPage.portGroupsFirstRow().getText().then(function (val) {
            var pgDetails = portGroupsPage.getPGDetailsFromUI(val);
            commonAssetsControlsPage.selectAndDeleteAsset(portGroupsPage.portGroupGrid(),portGroupsPage.deletePGPopup());
            //webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
            //webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.notification(), dataForPortGroups.pgDeleteSuccessMessage(pgDetails.id, name));
            //commonAssetsControlsPage.closeIcon().click();
            if(serviceCheckRequired){
                webElementActionLibrary.refresh();
                portGroupsPage.selectPortGroupsTab();
                portGroupsPage.searchForPG(name);
                expect(portGroupsPage.portGroupRows().count()).toBe(1);
                webElementActionLibrary.verifyTextPresence(portGroupsPage.portGroupRows(),commonData.noItemsToDisplay);
            }
        });
    },

    editPG:function(pgName,editPGName,pgDescription,anotherPortName,serviceCheckRequired){
        portGroupsPage.searchForPG(pgName);
        portGroupsPage.portGroupsFirstRow().getText().then(function (val) {
            var pgDetails = portGroupsPage.getPGDetailsFromUI(val);
            webElementActionLibrary.mouseOver(portGroupsPage.editButton());
            portGroupsPage.editButton().click();
            webElementActionLibrary.type(portGroupsPage.portGroupName(),editPGName);
            webElementActionLibrary.type(portGroupsPage.portGroupDescription(), pgDescription);
            webElementActionLibrary.type(commonAssetsControlsPage.getSearchBoxInForm().last(), anotherPortName);
            portGroupsPage.portCheckBox().click();
            portGroupsPage.submitButton().click();
            webElementActionLibrary.waitForAngular();
            //validate pg update success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyUpdatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForPortGroups.pgEditSuccessMessage(pgDetails.id));
            commonAssetsControlsPage.closeIcon().click();
            portGroupsPage.verifyPGDetails(editPGName, pgDescription,"2",serviceCheckRequired);
        });
    },

    verifyPGDetails:function(pgname,description,portCount,serviceCheckRequired) {
        if(serviceCheckRequired) {
            webElementActionLibrary.refresh();
            portGroupsPage.selectPortGroupsTab();
        }
        portGroupsPage.searchForPG(pgname);
        webElementActionLibrary.waitForAngular();
        portGroupsPage.portGroupsFirstRow().getText().then(function (val) {
            var pgDetails = portGroupsPage.getPGDetailsFromUI(val,description);
            expect(pgDetails.name).toBe(pgname);
            expect(pgDetails.description).toBe(description);
            expect(pgDetails.numOfPorts).toBe(portCount);
        });
    },

    verifyGridColumnTitles: function () {
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(portGroupsPage.portGroupGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(portGroupsPage.portGroupGrid()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(portGroupsPage.portGroupGrid()),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPortsTitle(portGroupsPage.portGroupGrid()),commonData.noOfPortsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfEGsTitle(portGroupsPage.portGroupGrid()),commonData.noOfEGsTitle);
    },

    verifyGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(portGroupsPage.portGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(portGroupsPage.portGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(portGroupsPage.portGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(portGroupsPage.portGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(portGroupsPage.portGroupGrid()));
    },

    verifyDetailedGridColumnTitles : function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortIdTitle(portGroupsPage.detailedGrid()),commonData.portIdTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getWWNTitle(portGroupsPage.detailedGrid()),commonData.wwnTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getStateTitle(portGroupsPage.detailedGrid()),commonData.stateTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getTypeTitle(portGroupsPage.detailedGrid()),commonData.typeTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortPhysicalPortIdTitle(portGroupsPage.detailedGrid()),commonData.physicalPortIdTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPGsTitle(portGroupsPage.detailedGrid()),commonData.noOfPGsTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfInitiatorsTitle(portGroupsPage.detailedGrid()),commonData.noOfInitiatorsTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfVolumesTitle(portGroupsPage.detailedGrid()),commonData.noOfVolumesTitle);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText().last(),commonData.portsTabText);
    },

    verifyDetailedGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(portGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(portGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(portGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(portGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(portGroupsPage.detailedGrid()));

    },

    compareDetailedGridPortDetails: function (portDetails){
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdValue(portGroupsPage.detailedGrid()),portDetails.id);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameValue(portGroupsPage.detailedGrid()),portDetails.name);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortStateValue(portGroupsPage.detailedGrid()),portDetails.state);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortIOCIdValue(portGroupsPage.detailedGrid()),portDetails.iocId);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortPhysicalIdValue(portGroupsPage.detailedGrid()),portDetails.ppid);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getITNInitiatorsValue(portGroupsPage.detailedGrid()),portDetails.noOfInitiators);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getITNVolumesValue(portGroupsPage.detailedGrid()),portDetails.noOfVolumes);
        webElementActionLibrary.verifyExactText(portGroupsPage.detailedGridNoOfPGs(),String(Number(portDetails.noOfPGs)+1));
    },

    verifyPGValue: function (value) {
        portGroupsPage.portGroupsFirstRow().getText().then(function (val) {
            expect(val).toContain(value);
        });
    },

    detailedGrid: function () {
        return commonAssetsControlsPage.getDetailedGrid(portGroupsPage.portGroupGrid());
    },

    searchForPG: function (pgName) {
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(portGroupsPage.portGroupGrid()), pgName);
        webElementActionLibrary.waitForAngular();
    },

    portGroupName: function(){
        return $('#vx_port_grp_form_name');
    },

    portGroupDescription: function(){
        return $('#vx_port_grp_form_description');
    },

    portGroupsFirstRow:  function () {
        return portGroupsPage.portGroupGrid().$$('tbody tr').first();
    },

    portGroupRows:  function () {
        return portGroupsPage.portGroupGrid().$$('tbody tr');
    },


    portGroupGrid:  function () {
        return $('[feature="portGroupsGrid"]');
    },

    portGroupCreateAndEditPanel:  function () {
        return $('[kendo-window="portGroupWindow"]');
    },

    createNewButton:  function () {
        return $('#portGroupsGrid-create-btn-id');
    },

    portCheckBox:function(){
        return $$('[for*="port_selector"]').get(1);
    },

    submitButton: function(){
        return $('#vx-portGroupsGrid-submit-btn');
    },

    cancelButton: function(){
        return $('#vx-portGroupsGrid-cancel-btn');
    },

    editPanelPortGroupId: function () {
        return portGroupsPage.portGroupCreateAndEditPanel().$('[ng-if="portGroupData.id"]');
    },

    editPanelPortId: function () {
        return portGroupsPage.editVolumeGroupPanel().$('[ng-bind="dataItem.id"]');
    },

    editButton:  function () {
        return commonAssetsControlsPage.getEditButton(portGroupsPage.portGroupGrid());
    },

    nameErrorMessage:  function () {
        return $('[data-container-for="name"] .k-invalid-msg');
    },

    deleteButton:  function () {
        return portGroupsPage.portGroupGrid().$('.k-grid-delete');
    },

    portGroupGridSearchBox:  function () {
        return $('#vx-PortGroups-grid-search-input');
    },

    portGroupCreateEditFormSearchBox:  function () {
        return $('#vx-PortGroups-create-edit-form-search-input');
    },

    selectPortGroupsTab: function () {
        portGroupsPage.pgTab().click();
        webElementActionLibrary.waitForAngular();
    },

    pgTab: function () {
      return $('#portsGroupTab');
    },


    getPGDetailsFromUI: function(val,description){

        var details;
        if(description=="") {
            details = {
                id: val.split(' ')[0],
                name: val.split(' ')[1],
                description:"",
                numOfPorts:val.split(' ')[2],
                exportGroups: val.split(' ')[3]
            };
        }
        else{
            details = {
                id: val.split(' ')[0],
                name: val.split(' ')[1],
                description: val.split(' ')[2],
                numOfPorts:val.split(' ')[3],
                exportGroups: val.split(' ')[4]
            };
        }
        return details;
    },

    detailedGridNoOfPGs: function(){
        return commonAssetsControlsPage.get7thColumnValue(portGroupsPage.detailedGrid());
    },

    pgDetailedGridExpansionLinkFirstRow: function () {
        return commonAssetsControlsPage.getDetailedGridExpansionLink(portGroupsPage.portGroupsFirstRow());
    },

    beOnPGTab: function(){
        commonAssetsControlsPage.selectedTabText().getText().then(function (val) {
            if(val!=dataForPortGroups.pgTabText){
                portGroupsPage.selectPortGroupsTab();
                webElementActionLibrary.wait(2);
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.pgTabText);
            }
        });
    },

    verifyPGEGCount: function (name, count) {
        portGroupsPage.searchForPG(name);
        portGroupsPage.portGroupsFirstRow().getText().then(function (val) {
            expect(val.split(' ')[4]).toBe(count);
        });
    }

};

module.exports = portGroupsPage;