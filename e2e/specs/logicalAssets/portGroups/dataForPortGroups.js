var dataForPortGroups={

  //successMessages
  pgCreationSuccessMessage: function(id){return 'Details: Port Group #('+id+') has been created.'},
  pgDeleteSuccessMessage: function(id){return 'Details: Port Group(s) #('+id+') has been deleted.'},
  pgEditSuccessMessage: function(id){return 'Details: Port Group #('+id+') has been updated'},

  //String Constants
  pgTabText: "Port Group",
  createPGPopUpTitle: "Create Port Group",
  editPGPopUpTitle: "Edit Port Group",
  pgName: "TAPG",
  editedPGName: "TAPGE",
  editedPGDescription: "TA_pgDescEdited",
  pgDescription: "TA_pgDesc",
  emptyPGNameError: "name is required",
  detailedGridPortsTabText:"Ports",
  pgName31Char:"PGNameFor31Chr",
  pgDesc63Char:"PGDescriptionForUIAutomation6CharactersToTest63CharactersTetet",
  editedPGName31Char:"EPGNameFor31Ch",
  editedPGDesc63Char:"EditedPGDescriptionForUIAutomation63CharactersToTest63Character",
  port1Name:"10:00:3c:91:2b:02:ff:00",
  port2Name:"10:00:3c:91:2b:02:ff:01"

};

module.exports = dataForPortGroups;