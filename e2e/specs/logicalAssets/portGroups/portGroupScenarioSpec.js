'use strict';

//includes all the js file dependencies required for execution of scenarios


var
    portGroupsPage = dependencies.getPortGroupsPage(),
    portsPage=dependencies.getPortsPage(),
    commonData=dependencies.getCommonData(),
    dataForPortGroups = dependencies.getDataForPortGroups(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    pgService=dependencies.getPortGroupService();


describe("Tests for Port Groups", function() {

    var pgName,  editedPGName,initialPGCountForPort1,firstPortDetails,secondPortDetails;


    beforeAll(function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToPortsPage();
        portsPage.portRows().get(1).getText().then(function(val){
            firstPortDetails = portsPage.getPortDetailsFromUI(val);
        });
        portsPage.portRows().get(2).getText().then(function(val){
            secondPortDetails = portsPage.getPortDetailsFromUI(val);
        });
        portGroupsPage.selectPortGroupsTab();
        webElementActionLibrary.wait(2);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.pgTabText);
    });


    beforeEach(function(){
        pgName = webElementActionLibrary.getTimeStampAttached(dataForPortGroups.pgName);
    });

    describe("Tests to create PG with different inputs",function(){

        beforeEach(function(){
            pgName = webElementActionLibrary.getTimeStampAttached(dataForPortGroups.pgName);
            webElementActionLibrary.refresh();
            webElementActionLibrary.wait(2);
            portGroupsPage.beOnPGTab();
        });

        it("Test to verify successful PG Creation", function(){
            portGroupsPage.createPG(pgName,dataForPortGroups.pgDescription,firstPortDetails.id,firstPortDetails.name);
        });

        it("Test to verify successful PG Creation with name as 31 characters and description as 63 characters", function(){
            pgName=webElementActionLibrary.getTimeStampAttached(dataForPortGroups.pgName31Char);
            portGroupsPage.createPG(pgName,dataForPortGroups.pgDesc63Char,firstPortDetails.id,firstPortDetails.name);
        });

        it("Test to verify successful PG Creation with name only", function(){
            portGroupsPage.createPG(pgName,"",firstPortDetails.id,firstPortDetails.name);
        });

        afterEach(function () {
            portGroupsPage.searchForPG(pgName);
            portGroupsPage.portGroupsFirstRow().getText().then(function (val) {
                var pgDetails = portGroupsPage.getPGDetailsFromUI(val,dataForPortGroups.pgDescription);
                pgService.deletePG(pgDetails.id);
            });
        })

    });

    xdescribe("Tests to update  PG with different inputs",function(){

        beforeEach(function(){
            pgService.createPG(pgName,dataForPortGroups.pgDescription,[Number(firstPortDetails.id)]);
            editedPGName=webElementActionLibrary.getTimeStampAttached(dataForPortGroups.editedPGName);
            webElementActionLibrary.refresh();
            portGroupsPage.beOnPGTab();
        });

        it("Tests to Update PG", function() {
            portGroupsPage.editPG(pgName,editedPGName,dataForPortGroups.editedPGDescription,secondPortDetails.name);
        });

        it("Test to update PG with name as 31 characters and description as 63 characters", function() {
            editedPGName = webElementActionLibrary.getTimeStampAttached(dataForPortGroups.editedPGName31Char);
            portGroupsPage.editPG(pgName,editedPGName,dataForPortGroups.editedPGDesc63Char,secondPortDetails.name);
        });

        it("Test to update PG with blank description", function() {
            portGroupsPage.editPG(pgName,editedPGName,"",secondPortDetails.name);
        });

        afterEach(function(){
            portGroupsPage.searchForPG(editedPGName);
            portGroupsPage.portGroupsFirstRow().getText().then(function (val) {
                var pgDetails = portGroupsPage.getPGDetailsFromUI(val,dataForPortGroups.pgDescription);
                pgService.deletePG(pgDetails.id);
            });
        })
    });

    xdescribe("Tests to Validate PG search functionality using different PG parameters",function(){
        var pgDetails, pgNameForSearch;

        beforeAll( function(){
            pgNameForSearch = webElementActionLibrary.getTimeStampAttached(dataForPortGroups.pgName);
            pgService.createPG(pgNameForSearch,dataForPortGroups.pgDescription,[Number(firstPortDetails.id)]);
            webElementActionLibrary.refresh();
            portGroupsPage.selectPortGroupsTab();
            portGroupsPage.searchForPG(pgNameForSearch);
            portGroupsPage.portGroupsFirstRow().getText().then(function (val) {
                pgDetails = portGroupsPage.getPGDetailsFromUI(val);
            });
        });


        it("Tests to search PG using id", function() {
            portGroupsPage.searchForPG(pgDetails.id);
            portGroupsPage.verifyPGValue(pgDetails.id);
        });

        it("Tests to search PG using desc", function() {
            portGroupsPage.searchForPG(pgDetails.description);
            portGroupsPage.verifyPGValue(pgDetails.description);
        });

        it("Tests to search PG using no of ports", function() {
            portGroupsPage.searchForPG(pgDetails.numOfPorts);
            portGroupsPage.verifyPGValue(pgDetails.numOfPorts);
        });

        it("Tests to search PG using no of EG's", function() {
            portGroupsPage.searchForPG(pgDetails.exportGroups);
            portGroupsPage.verifyPGValue(pgDetails.exportGroups);
        });

        it("Test to compare the port details with the port details in the PG detailed grid and verify PG grid, PG detailed grid column titles", function () {
            portGroupsPage.verifyGridColumnTitles();
            portGroupsPage.searchForPG(pgNameForSearch);
            portGroupsPage.pgDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            //portGroupsPage.verifyDetailedGridColumnTitles();
            //portGroupsPage.verifyDetailedGridValues();
            //portGroupsPage.compareDetailedGridPortDetails(firstPortDetails);
        });

        it("Test to verify PG count of a port", function(){
            portsPage.selectPortsTab();
            commonAssetsControlsPage.firstGridFilterIcon().click();
            webElementActionLibrary.type(commonAssetsControlsPage.filterInput(),firstPortDetails.id);
            webElementActionLibrary.waitForAngular();
            commonAssetsControlsPage.filterBtn().click();
            portsPage.verifyPortPGCount(firstPortDetails.id,Number(firstPortDetails.noOfPGs)+1);
            portGroupsPage.selectPortGroupsTab();
        });

        describe("Tests to validate UI Error for Edit PG Panel",function(){
            beforeAll(function(){
                portGroupsPage.searchForPG(pgNameForSearch);
                portGroupsPage.editButton().click();
                webElementActionLibrary.waitForAngular();
            });

            it("Test: Verify Elements in Edit PG Panel",function(){
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForPortGroups.editPGPopUpTitle);
            });

        });

        afterAll( function(){
            pgService.deletePG(pgDetails.id);
        });
    });


    describe("Tests to delete  PG with different inputs",function(){

        beforeEach( function(){
            pgName = webElementActionLibrary.getTimeStampAttached(dataForPortGroups.pgName);
            pgService.createPG(pgName,dataForPortGroups.pgDescription,[Number(firstPortDetails.id)]);
            webElementActionLibrary.refresh();
            portGroupsPage.selectPortGroupsTab();

        });

        it("Tests to Delete PG", function(){
            portGroupsPage.deletePG(pgName,true);
        });
    });

    describe("Tests to validate UI Error for Create PG Panel'",function(){

        beforeAll(function(){
            portGroupsPage.createNewButton().click();
            webElementActionLibrary.waitForAngular();
        });

        it("Test: Verify Elements in Create PG Panel",function(){
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForPortGroups.createPGPopUpTitle);
            webElementActionLibrary.verifyPresenceOfElement(portGroupsPage.cancelButton());

        });

        afterAll(function(){
            commonAssetsControlsPage.closeWindow();
        });
    });


});



