/**
 * Created by Ajay(Vexata) on 19-04-2017.
 */

'use strict';

//includes all the js file dependencies required for execution of scenarios
var exec = require("ssh-exec");
var saPage = dependencies.getSAPage(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage(),
    commonData = dependencies.getCommonData(),
    cliData = dependencies.getCLIData(),
    settingsPage = dependencies.getSettingsPage();


var saDetailsFromCli;
exec("vxcli sa show",cliData.getConf(hostName), function (err,stdout) {
    saDetailsFromCli=
    {
        name:stdout.split("Name:")[1].split("\n")[0].trim(),
        desc:stdout.split("Desc:")[1].split("\n")[0].trim(),
        id:stdout.split("id:")[1].split("\n")[0].trim(),
        state:stdout.split("State:")[1].split("\n")[0].trim(),
        uuid:stdout.split("Uuid:")[1].split("\n")[0].trim(),
        numPorts:stdout.split("Num Ports:")[1].split("\n")[0].trim(),
        physicalCapacity:stdout.split("Physical Capacity:")[1].split("\n")[0].trim(),
        maxOPF:stdout.split("Max OverProv Factor:")[1].split("\n")[0].trim(),
        maxPL:stdout.split("Max Provision Limit:")[1].split("\n")[0].trim(),
        provisionedSize:stdout.split("Provisioned Size:")[1].split("\n")[0].trim(),
        provisionedSizeLeft:stdout.split("Provision Size left:")[1].split("\n")[0].trim(),
        usedCapacity:stdout.split("Used Capacity:")[1].split("\n")[0].trim(),
        threshold:stdout.split("TP Warn Threshold:")[1].split("\n")[0].trim(),
        volumes:stdout.split("numVol:")[1].split("(M")[0].trim(),
        vgs:stdout.split("numVg:")[1].split("(M")[0].trim(),
        initiators:stdout.split("numInitiators:")[1].split("(M")[0].trim(),
        igs:stdout.split("numIg:")[1].split("(M")[0].trim(),
        pgs:stdout.split("numPg:")[1].split("(M")[0].trim(),
        egs:stdout.split("numEg:")[1].split("(M")[0].trim()
    },
        console.log(saDetailsFromCli);
});

describe("Tests For SA as Admin", function() {


    beforeAll(function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        //commonAssetsControlsPage.navigateToSettingsPage();
        //settingsPage.selectBinaryPreference();
        commonAssetsControlsPage.navigateToSAPage();
    });

    describe("Tests to Validate SA page Elements after creation",function(){

        it("Test to verify Array Info section", function(){
            var saDetails =[saDetailsFromCli.name,saDetailsFromCli.desc,saDetailsFromCli.maxOPF.split("%")[0],saDetailsFromCli.threshold.split("%")[0],saDetailsFromCli.id, saDetailsFromCli.uuid,saDetailsFromCli.state,saDetailsFromCli.numPorts];
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSectionHeader(saPage.arrayInfoSection()),commonData.arrayInfo);
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.generalTabText);
        });

        xit("Test to verify SA Details", function(){
            var saDetails =[saDetailsFromCli.name,saDetailsFromCli.desc,saDetailsFromCli.maxOPF.split("%")[0],saDetailsFromCli.threshold.split("%")[0],saDetailsFromCli.id, saDetailsFromCli.uuid,saDetailsFromCli.state,saDetailsFromCli.numPorts];
            for(var i=0;i<4;i++){
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saElementLabels().get(i),commonData.saCreateLabels[i]);
                if(i==1 || i==0)
                    webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saInputFields().get(i),saDetails[i]);
                else
                    webElementActionLibrary.verifyTextPresenceUsingContainByGetAttribute(saPage.saEditableInputFields().get(2-i),saDetails[i]);
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saElementLabels().get(4+i),commonData.saViewLabels[i]);
                if(i==2)
                    webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saInputFields().get(4+i),saDetails[4+i].toUpperCase());
                else
                    webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saInputFields().get(4+i),saDetails[4+i]);
            }
        });

        it("Test to verify radial guage section details", function(){
            var assetDetails =[saDetailsFromCli.volumes,saDetailsFromCli.vgs,saDetailsFromCli.egs,saDetailsFromCli.initiators,saDetailsFromCli.igs,saDetailsFromCli.pgs];
            for(var i=0;i<6;i++){
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.getRadialSectionHeader(saPage.radialGuageSections().get(i)),commonData.saRadialSectionDetails[i].Name);
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.getRadialSectionFillerValue(saPage.radialGuageSections().get(i)),assetDetails[i]);
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.getRadialSectionMaxAssetCount(saPage.radialGuageSections().get(i)),commonData.saRadialSectionDetails[i].Max);
                webElementActionLibrary.verifyPresenceOfElement(saPage.getRadialSectionInnerBar(saPage.radialGuageSections().get(i)));
                webElementActionLibrary.verifyPresenceOfElement(saPage.getRadialSectionOuterBar(saPage.radialGuageSections().get(i)));
                webElementActionLibrary.verifyPresenceOfElement(saPage.getRadialSectionFillerBar(saPage.radialGuageSections().get(i)));
                if(commonData.saRadialSectionDetails[i].MaxPerG!=undefined)
                    webElementActionLibrary.verifyTextPresenceUsingContain(saPage.getRadialSectionMaxAssetGroupCount(saPage.radialGuageSections().get(i)),commonData.saRadialSectionDetails[i].MaxPerG);
            }
        });

        xit("Test to verify storage bar section details", function(){
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(0),commonData.physicalStorage);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(1),commonData.used);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(2),commonData.free);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(4),commonData.provisionedStorage);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(5),commonData.allocated);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(6),commonData.available);
            saPage.storageSectionLabels().get(3).getText().then(function (text) {
                expect(text).toEqual((Number(saDetailsFromCli.physicalCapacity.split(" ")[0])).toFixed("2")+" "+saDetailsFromCli.physicalCapacity.split(" ")[1]);
            });
            saPage.storageSectionLabels().get(7).getText().then(function (text) {
                expect(text).toEqual((Number(saDetailsFromCli.maxPL.split(" ")[0])).toFixed("2")+" "+saDetailsFromCli.maxPL.split(" ")[1]);
            });
            for (var i=0;i<2;i++) {
                webElementActionLibrary.verifyPresenceOfElement(saPage.freeValues().get(i));
                webElementActionLibrary.verifyPresenceOfElement(saPage.usedValues().get(i));
            }

        });

        it("Test to update sa over provisioning factor and threshold ", function(){
            webElementActionLibrary.type(saPage.saEditableInputFields().get(0),"1601");
            webElementActionLibrary.type(saPage.saEditableInputFields().get(1),"96");
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().first(), commonData.saOverProvisionError);
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().last(), commonData.saThresholdError);
            webElementActionLibrary.type(saPage.saEditableInputFields().get(0),"99");
            webElementActionLibrary.type(saPage.saEditableInputFields().get(1),"49");
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().first(), commonData.saOverProvisionError);
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().last(), commonData.saThresholdError);

        });

        xit("Test to update sa over provisioning factor and threshold ", function(){
            saPage.updateSA("1600","95");
            //webElementActionLibrary.wait(2);
            saPage.updateSA("300","75");
        });

    });
});

describe("Tests For SA as operator", function() {


    beforeAll(function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsReadOnlyUser();
        //commonAssetsControlsPage.navigateToSettingsPage();
        //settingsPage.selectBinaryPreference();
        commonAssetsControlsPage.navigateToSAPage();
    });


    describe("Tests to Validate SA page Elements after creation",function(){

        xit("Test to verify SA Details", function(){
            var saDetails =[saDetailsFromCli.name,saDetailsFromCli.desc,saDetailsFromCli.maxOPF.split("%")[0],saDetailsFromCli.threshold.split("%")[0],saDetailsFromCli.id, saDetailsFromCli.uuid,saDetailsFromCli.state,saDetailsFromCli.numPorts];
            for(var i=0;i<4;i++){
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saElementLabels().get(i),commonData.saCreateLabels[i]);
                if(i==1 || i==0)
                    webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saInputFields().get(i),saDetails[i]);
                else
                    webElementActionLibrary.verifyTextPresenceUsingContainByGetAttribute(saPage.saEditableInputFields().get(2-i),saDetails[i]);
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saElementLabels().get(4+i),commonData.saViewLabels[i]);
                if(i==2)
                    webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saInputFields().get(4+i),saDetails[4+i].toUpperCase());
                else
                    webElementActionLibrary.verifyTextPresenceUsingContain(saPage.saInputFields().get(4+i),saDetails[4+i]);
            }
        });

        it("Test to verify radial guage section details", function(){
            var assetDetails =[saDetailsFromCli.volumes,saDetailsFromCli.vgs,saDetailsFromCli.egs,saDetailsFromCli.initiators,saDetailsFromCli.igs,saDetailsFromCli.pgs];
            for(var i=0;i<6;i++){
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.getRadialSectionHeader(saPage.radialGuageSections().get(i)),commonData.saRadialSectionDetails[i].Name);
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.getRadialSectionFillerValue(saPage.radialGuageSections().get(i)),assetDetails[i]);
                webElementActionLibrary.verifyTextPresenceUsingContain(saPage.getRadialSectionMaxAssetCount(saPage.radialGuageSections().get(i)),commonData.saRadialSectionDetails[i].Max);
                webElementActionLibrary.verifyPresenceOfElement(saPage.getRadialSectionInnerBar(saPage.radialGuageSections().get(i)));
                webElementActionLibrary.verifyPresenceOfElement(saPage.getRadialSectionOuterBar(saPage.radialGuageSections().get(i)));
                webElementActionLibrary.verifyPresenceOfElement(saPage.getRadialSectionFillerBar(saPage.radialGuageSections().get(i)));
                if(commonData.saRadialSectionDetails[i].MaxPerG!=undefined)
                    webElementActionLibrary.verifyTextPresenceUsingContain(saPage.getRadialSectionMaxAssetGroupCount(saPage.radialGuageSections().get(i)),commonData.saRadialSectionDetails[i].MaxPerG);
            }
        });

        xit("Test to verify storage bar section details", function(){
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(0),commonData.physicalStorage);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(1),commonData.used);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(2),commonData.free);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(4),commonData.provisionedStorage);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(5),commonData.allocated);
            webElementActionLibrary.verifyExactText(saPage.storageSectionLabels().get(6),commonData.available);
            saPage.storageSectionLabels().get(3).getText().then(function (text) {
                expect(text).toEqual((Number(saDetailsFromCli.physicalCapacity.split(" ")[0])).toFixed("2")+" "+saDetailsFromCli.physicalCapacity.split(" ")[1]);
            });
            saPage.storageSectionLabels().get(7).getText().then(function (text) {
                expect(text).toEqual((Number(saDetailsFromCli.maxPL.split(" ")[0])).toFixed("2")+" "+saDetailsFromCli.maxPL.split(" ")[1]);
            });
            for (var i=0;i<2;i++) {
                webElementActionLibrary.verifyPresenceOfElement(saPage.freeValues().get(i));
                webElementActionLibrary.verifyPresenceOfElement(saPage.usedValues().get(i));
            }

        });

        it("Test to verify disabled inputs", function(){
            webElementActionLibrary.verifyPresenceOfElement(saPage.saDisabledEditableInputFields().get(0));
            webElementActionLibrary.verifyPresenceOfElement(saPage.saDisabledEditableInputFields().get(1));
        });

    });



});




