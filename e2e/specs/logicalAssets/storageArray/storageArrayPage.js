/**
 * Created by Ajay(Vexata) on 19-04-2017.
 */

/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData=dependencies.getCommonData(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage();

var SAPage =  {

    arrayInfoSection: function () {
        return $('.vx-transparent-container-array-info-panel');
    },

    encryptionCheckBox :function () {
        return $('label[for="dg-encryption"]');
    },

    generalTab: function () {

    },

    advancedTab: function () {

    },

    nextButton: function () {
        return $('[ng-click="onClickNextStep()"]');
    },

    raid6RadioButton: function () {
        return $('[for="vx_dg_raid6_level_selection"] span');
    },


    raid5RadioButton: function () {
        return $('[for="vx_dg_raid5_level_selection"] span');
    },

    getCheckBoxForEsm: function (esmId) {
        return $('label[for="esm-'+esmId+'"] span');
    },

    selectEsms: function (esmList) {
        for(var i=0;i<esmList.length;i++){
            console.log(esmList[i]);
            SAPage.getCheckBoxForEsm(esmList[i]).click();
        }

    },

    selectAllCheckBox: function () {
        return $('label[for="select-all"] span');
    },

    saNotCreatedText: function () {
        return $('.vx-events-severity-container');
    },

    heading: function () {
        return $$('.heading');
    },

    selectEsmText: function () {
        return $('.small-text');
    },

    previousButton: function () {
        return $('[ng-click="isStep1Completed = false;"]');
    },

    createButton: function () {
        return $('[ng-click="onCreate()"]');
    },

    saElementLabels: function () {
        return $$('.storage-array-creation .elementlabel');
    },

    saInputFields: function () {
        return $$('.storage-array-creation .elementinput');
    },

    saEditableInputFields: function () {
        return $$('.sa-view-panel .elementinput input');
    },

    saDisabledEditableInputFields: function () {
        return $$('.sa-view-panel .elementinput input[disabled="disabled"]');
    },

    radialGuageSections: function () {
        return $$('.vx-radial-container');
    },

    getRadialSectionHeader: function (radialSection) {
        return radialSection.$$("label").first();
    },

    getRadialSectionMaxAssetCount: function (radialSection) {
        return radialSection.$$("label").get(1);
    },

    getRadialSectionOuterBar: function (radialSection) {
        return radialSection.$(".vx-radial-outer-bar");
    },

    getRadialSectionInnerBar: function (radialSection) {
        return radialSection.$(".vx-radial-inner-bar");
    },

    getRadialSectionFillerBar: function (radialSection) {
        return radialSection.$(".vx-radial-filler-bar");
    },

    getRadialSectionFillerValue: function (radialSection) {
        return radialSection.$(".vx-radial-progress-value");
    },

    getRadialSectionMaxAssetGroupCount: function (radialSection) {
        return radialSection.$$("label").last();
    },

    storageSection: function () {
        return $('.sa-storage-info');
    },

    storageSectionLabels: function () {
        return SAPage.storageSection().$$('label');
    },

    usedValues: function () {
        return SAPage.storageSection().$$(".used-value");
    },

    freeValues: function () {
        return SAPage.storageSection().$$(".free-value");
    },

    saveButton: function () {
        return $('[ng-click="updateSA()"]');
    },

    updateSA : function (overProvisioning, threshold) {
        webElementActionLibrary.type(SAPage.saEditableInputFields().get(0),overProvisioning);
        webElementActionLibrary.type(SAPage.saEditableInputFields().get(1),threshold);
        SAPage.saveButton().click();
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.notification(),"Details: Storage Array has been updated successfully!");
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.notification(),"Successfully Updated!");
        webElementActionLibrary.verifyExactTextUsingContainByGetAttribute(SAPage.saEditableInputFields().get(0),overProvisioning);
        webElementActionLibrary.verifyExactTextUsingContainByGetAttribute(SAPage.saEditableInputFields().get(1),threshold);
    },

    createDGAndSA: function (esmList,encryptionFlag,raidMode) {
        SAPage.selectAllCheckBox().click();
        SAPage.selectEsms(esmList);
        if(encryptionFlag=="FALSE")
            SAPage.encryptionCheckBox().click();
        if(raidMode=="RAID5")
            SAPage.raid5RadioButton().click();
        SAPage.nextButton().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.assetsHeader(),commonData.saHeader);
        webElementActionLibrary.verifyExactText(SAPage.previousButton(),"Previous");
        webElementActionLibrary.verifyExactText(SAPage.createButton(),"Create");
        SAPage.createButton().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.notification(),commonData.saCreateSuccessMsg);
    }

};

module.exports = SAPage;
