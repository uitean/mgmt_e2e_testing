/**
 * Created by Ajay(Vexata) on 10-04-2017.
 */
'use strict';
var frisby = require('frisby');
var commonData=dependencies.getCommonData();
var commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage();
var igApiUrl = commonData.igApiUrl();

var initiatorGroupsService = {


    createIG: function (name,desc,iniIdArray) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.post(igApiUrl,
            {
                addInitiators:iniIdArray,
                description:desc,
                name:name
            }, {json: true})
            .expect('status',201)
            .then(function(res){
                expect(res._json.name).toBe(name);
                expect(res._json.description).toBe(desc);
                expect(res._json.currInitiators).toEqual(iniIdArray);
                console.log("ig "+res._json.name +" with id "+res._json.id+" created through rest api");
            });
    },

    getIGDetails : function (igId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.get(igApiUrl+igId)
            .expect('status',200)
            .then(function (res) {
                console.log(res);
            });
    },

    deleteIG: function (igId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.del(igApiUrl+igId)
            .expect('status',204).then(function (res) {
                console.log("ig "+igId+" deleted through rest api");
            });
    }
};



module.exports = initiatorGroupsService;
