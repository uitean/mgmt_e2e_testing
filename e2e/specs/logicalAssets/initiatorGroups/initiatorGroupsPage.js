/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    dataForInitiatorGroups = dependencies.getDataForInitiatorGroups(),
    commonData=dependencies.getCommonData(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage();


var initiatorGroupsPage =  {

    beOnIGTab: function(){
        commonAssetsControlsPage.selectedTabText().getText().then(function (val) {
            if(val!==dataForInitiatorGroups.igTabText){
                initiatorGroupsPage.selectInitiatorGroupsTab();
                webElementActionLibrary.wait(2);
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.igTabText);
            }
        });
    },

    createIG : function(igName,igDesc,iniName,serviceCheckRequired){
        webElementActionLibrary.waitForElement(initiatorGroupsPage.createNewButton());
        initiatorGroupsPage.createNewButton().click();
        webElementActionLibrary.type(initiatorGroupsPage.initiatorGroupName(), igName);
        webElementActionLibrary.type(initiatorGroupsPage.initiatorGroupDescription(), igDesc);
        //commonAssetsControlsPage.applyNameFilter(iniName);
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBoxInForm().last(),iniName);
        initiatorGroupsPage.initiatorCheckBox().click();
        initiatorGroupsPage.submitButton().click();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var igId = commonAssetsControlsPage.getIdFromNotification(val);
            //validate ig creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForInitiatorGroups.igCreationSuccessMessage(igId));
            commonAssetsControlsPage.closeIcon().click();
            initiatorGroupsPage.verifyIGDetails(igName,igDesc,"1",serviceCheckRequired);
        });

    },


    deleteIG: function(name,serviceCheckRequired){
        initiatorGroupsPage.searchForIG(name);
        webElementActionLibrary.waitForAngular();
        initiatorGroupsPage.initiatorGroupsFirstRow().getText().then(function (val) {
            var igDetails = initiatorGroupsPage.getIGDetailsFromUI(val);
            commonAssetsControlsPage.selectAndDeleteAsset(initiatorGroupsPage.initiatorGroupGrid(),initiatorGroupsPage.igDeletePopup());
            webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyDeletedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForInitiatorGroups.igDeleteSuccessMessage(igDetails.id));
            if(serviceCheckRequired) {
                webElementActionLibrary.refresh();
                initiatorGroupsPage.selectInitiatorGroupsTab();
                initiatorGroupsPage.searchForIG(name);
                expect(initiatorGroupsPage.initiatorGroupRows().count()).toBe(1);
            }
        });
    },

    editIG:function(igName,editIGName,igDescription,anotherInitiator,serviceCheckRequired){
        webElementActionLibrary.type(initiatorGroupsPage.igGridSearchBox(), igName);
        initiatorGroupsPage.initiatorGroupsFirstRow().getText().then(function (val) {
            var igDetails = initiatorGroupsPage.getIGDetailsFromUI(val);
            initiatorGroupsPage.editButton().click();
            webElementActionLibrary.type(initiatorGroupsPage.initiatorGroupName(), editIGName);
            webElementActionLibrary.type(initiatorGroupsPage.initiatorGroupDescription(), igDescription);
            webElementActionLibrary.type(commonAssetsControlsPage.getSearchBoxInForm(), anotherInitiator);
            initiatorGroupsPage.initiatorCheckBox().click();
            initiatorGroupsPage.submitButton().click();
            webElementActionLibrary.waitForAngular();
            //validate ig update success message
            //webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.notification(), dataForInitiatorGroups.igEditSuccessMessage(igDetails.id, editIGName));
            //commonAssetsControlsPage.closeIcon().click();
            initiatorGroupsPage.verifyIGDetails(editIGName, igDescription,"2",serviceCheckRequired);
        });
    },


    verifyIGDetails:function(igname,description,initiatorCount,serviceCheckRequired) {
        if(serviceCheckRequired) {
            webElementActionLibrary.refresh();
            initiatorGroupsPage.selectInitiatorGroupsTab();
        }
        initiatorGroupsPage.searchForIG(igname);
        initiatorGroupsPage.initiatorGroupsFirstRow().getText().then(function (val) {
            var igDetails = initiatorGroupsPage.getIGDetailsFromUI(val,description);
            expect(igDetails.name).toBe(String(igname));
            expect(igDetails.description).toBe(description);
            expect(igDetails.numOfInitiators).toBe(initiatorCount);
        });
    },


    verifyGridColumnTitles: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(initiatorGroupsPage.initiatorGroupGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(initiatorGroupsPage.initiatorGroupGrid()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(initiatorGroupsPage.initiatorGroupGrid()),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfInitiatorsTitle(initiatorGroupsPage.initiatorGroupGrid()),commonData.noOfInitiatorsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfEGsTitle(initiatorGroupsPage.initiatorGroupGrid()),commonData.noOfEGsTitle);
    },

    verifyGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get7thColumnValue(initiatorGroupsPage.initiatorGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(initiatorGroupsPage.initiatorGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(initiatorGroupsPage.initiatorGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(initiatorGroupsPage.initiatorGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(initiatorGroupsPage.initiatorGroupGrid()));
    },

    verifyDetailedGridColumnTitles : function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(initiatorGroupsPage.detailedGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getWWNTitle(initiatorGroupsPage.detailedGrid()),commonData.wwnTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getInitiatorTypeTitle(initiatorGroupsPage.detailedGrid()),commonData.initiatorTypeTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDiscoveryMethodTitle(initiatorGroupsPage.detailedGrid()),commonData.discoveryMethodTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfIGsTitle(initiatorGroupsPage.detailedGrid()),commonData.noOfIGsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortListTitle(initiatorGroupsPage.detailedGrid()),commonData.portListTitle);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText().last(),commonData.initiatorsTabText);
    },

    verifyDetailedGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(initiatorGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(initiatorGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(initiatorGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(initiatorGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(initiatorGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(initiatorGroupsPage.detailedGrid()));
    },


    verifyIGValue: function (value) {
        initiatorGroupsPage.initiatorGroupsFirstRow().getText().then(function (val) {
            expect(val).toContain(value);
        });
    },

    searchForIG: function (igName) {
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(initiatorGroupsPage.initiatorGroupGrid()), igName);
    },

    detailedGrid: function () {
        return commonAssetsControlsPage.getDetailedGrid(initiatorGroupsPage.initiatorGroupGrid());
    },

    selectInitiatorGroupsTab: function(){
        initiatorGroupsPage.igTab().click();
        webElementActionLibrary.waitForAngular();
    },

    igTab: function () {
      return $('#initiatorsGroupTab');
    },

    initiatorGroupName: function(){
        return $('#vx_initiator_grp_form_name');
    },

    initiatorGroupDescription: function(){
        return $('#vx_initiator_grp_form_description');
    },

    initiatorGroupsFirstRow:  function () {
        return initiatorGroupsPage.initiatorGroupGrid().$$('tbody tr').first();
    },

    initiatorGroupRows:  function () {
        return initiatorGroupsPage.initiatorGroupGrid().$$('tbody tr');
    },


    initiatorGroupGrid:  function () {
        return $('[feature="initiatorGroupGrid"]');
    },

    initiatorGroupCreateAndEditPanel:  function () {
        return $('[kendo-window="initiatorGroupWindow"]');
    },
    createNewButton:  function () {
        return $('#initiatorGroupGrid-create-btn-id');
    },

    initiatorCheckBox:function(){
        return $('[for*="initiator_selector"]');
    },

    submitButton: function(){
        return $('#vx-initiatorGroupGrid-submit-btn');
    },

    cancelButton: function(){
        return $('#vx-initiatorGroupGrid-cancel-btn');
    },


    editButton:  function () {
        //return initiatorGroupsPage.initiatorGroupGrid().$('.k-edit');
        return commonAssetsControlsPage.getEditButton(initiatorGroupsPage.initiatorGroupGrid());
    },

    nameErrorMessage:  function () {
        return $('[data-container-for="name"] .k-invalid-msg');
    },

    igPopUpSearchBox:  function () {
        return $('#vx-InitiatorGroups-create-edit-form-search-input');
    },

    igGridSearchBox:  function () {
        return $('#vx-InitiatorGroups-grid-search-input');
    },


    deleteButton:  function () {
        return initiatorGroupsPage.initiatorGroupGrid().$('.k-grid-delete');
    },



    getIGDetailsFromUI: function(val,description){
        var details;
        if(description=="") {
            details = {
                id: val.split(' ')[0],
                name: val.split(' ')[1],
                description: "",
                numOfInitiators:val.split(' ')[2],
                hostProfileType:val.split(' ')[3],
                exportGroups: val.split(' ')[4]
            };
        }
        else{
            details = {
                id: val.split(' ')[0],
                name: val.split(' ')[1],
                description: val.split(' ')[2],
                numOfInitiators:val.split(' ')[3],
                hostProfileType:val.split(' ')[4],
                exportGroups: val.split(' ')[5]
            };
        }
        return details;
    },

    detailedGridNoOfIGs: function(){return initiatorGroupsPage.detailedGrid().$('td:nth-child(6)')},

    igDetailedGridExpansionLinkFirstRow: function () {
        return commonAssetsControlsPage.getDetailedGridExpansionLink(initiatorGroupsPage.initiatorGroupsFirstRow());
    },

    verifyIGEGCount: function (name, count) {
        initiatorGroupsPage.searchForIG(name);
        initiatorGroupsPage.initiatorGroupsFirstRow().getText().then(function (val) {
            expect(val.split(' ')[4]).toBe(count);
        });
    },

    igDeletePopup: function () {
        return $('[kendo-window="initiatorGroupBulkDeletePopupWindow"]');
    }

};

module.exports = initiatorGroupsPage;