var dataForInitiatorGroups={

  //successMessages
  igCreationSuccessMessage: function(id){return 'Details: Initiator Group #('+id+') has been created.'},
  igDeleteSuccessMessage: function(id){return 'Details: Initiator Group(s) #('+id+') has been deleted.'},
  igEditSuccessMessage: function(id){return 'Details: Initiator Group #('+id+') has been updated'},

  //String Constants
  iName:"TAI",
  iName2:"TAI2",
  iDesForIg:"TAIGIniDesc",
  igTabText: "Initiator Group",
  createIGPopUpTitle: "Create Initiator Group",
  editIGPopUpTitle: "Edit Initiator Group",
  igName: "TAIG",
  editedIGName: "TAIGE",
  editedIGDescription: "TA_igDescEdited",
  igDescription: "TA_igDesc",
  emptyIGNameError: "name is required",
  detailedGridInitiatorsTabText:"Initiators",
  igName31Char:"IGNameFor31Chr",
  igDesc63Char:"IGDescriptionForUIAutomation63CharactersToTest63CharactersTetet",
  editedIGName31Char:"EIGNameFor31Ch",
  editedIGDesc63Char:"EditedIGDescriptionForUIAutomation63CharactersToTest63Character"

};

module.exports = dataForInitiatorGroups;