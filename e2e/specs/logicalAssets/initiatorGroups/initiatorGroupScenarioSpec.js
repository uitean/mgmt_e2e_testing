'use strict';

//includes all the js file dependencies required for execution of scenarios


var
    initiatorGroupsPage = dependencies.getInitiatorGroupsPage(),
    dataForInitiatorGroups = dependencies.getDataForInitiatorGroups(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    commonData= dependencies.getCommonData(),
    initiatorPage = dependencies.getInitiatorPage(),
    initiatorService = dependencies.getInitiatorService(),
    igService = dependencies.getInitiatorGroupService();

describe("Tests for Initiator Groups", function() {

    var igName,  editedIGName,firstInitiatorDetails,secondInitiatorDetails,
        iName=webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.iName),
        iName2=webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.iName2),
        initiatorWWN1=initiatorPage.getInitiatorWWN(),
        initiatorWWN2=initiatorPage.getInitiatorWWN();

    beforeAll(function () {
        initiatorService.createInitiator(iName,dataForInitiatorGroups.iDesForIg,initiatorWWN1);
        initiatorService.createInitiator(iName2,dataForInitiatorGroups.iDesForIg,initiatorWWN2);
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToInitiatorsPage();
        initiatorPage.searchForInitiator(iName);
        initiatorPage.initiatorsFirstRow().getText().then(function (val) {
            firstInitiatorDetails = initiatorPage.getInitiatorDetailsFromUI(val);
        });
        initiatorPage.searchForInitiator(iName2);
        initiatorPage.initiatorsFirstRow().getText().then(function (val) {
            secondInitiatorDetails = initiatorPage.getInitiatorDetailsFromUI(val);
        });
        initiatorGroupsPage.selectInitiatorGroupsTab();
        webElementActionLibrary.wait(2);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.igTabText);
    });


    beforeEach(function(){
        igName = webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.igName);
    });

    describe("Tests to create IG with different inputs",function(){

        beforeEach(function(){
            igName = webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.igName);
            webElementActionLibrary.refresh();
            webElementActionLibrary.wait(2);
            initiatorGroupsPage.beOnIGTab();
        });

        it("Test to verify successful IG Creation", function(){
            initiatorGroupsPage.createIG(igName,dataForInitiatorGroups.igDescription,firstInitiatorDetails.name);
        });

        it("Test to verify successful IG Creation with name as 31 characters and description as 63 characters", function(){
            igName=webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.igName31Char);
            initiatorGroupsPage.createIG(igName,dataForInitiatorGroups.igDesc63Char,firstInitiatorDetails.name);
        });

        it("Test to verify successful IG Creation with name only", function(){
            initiatorGroupsPage.createIG(igName,"",firstInitiatorDetails.name);
        });

        afterEach(function () {
            initiatorGroupsPage.searchForIG(igName);
            initiatorGroupsPage.initiatorGroupsFirstRow().getText().then(function (val) {
                var igDetails = initiatorGroupsPage.getIGDetailsFromUI(val,dataForInitiatorGroups.igDescription);
                igService.deleteIG(igDetails.id);
            });
        })

    });

    xdescribe("Tests to update  IG with different inputs",function(){

        beforeEach(function(){
            initiatorGroupsPage.createIG(igName,dataForInitiatorGroups.igDescription,initiatorWWN1);
            editedIGName=webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.editedIGName);
        });

        it("Tests to Update IG", function() {
            initiatorGroupsPage.editIG(igName,editedIGName,dataForInitiatorGroups.editedIGDescription,initiatorWWN2);
        });

        it("Test to update IG with name as 31 characters and description as 63 characters", function() {
            editedIGName = webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.editedIGName31Char);
            initiatorGroupsPage.editIG(igName,editedIGName,dataForInitiatorGroups.editedIGDesc63Char,initiatorWWN2);
        });

        it("Test to update IG with blank description", function() {
            initiatorGroupsPage.editIG(igName,editedIGName,"",initiatorWWN2);
        });

        afterEach(function(){
            initiatorGroupsPage.deleteIG(editedIGName);
        })
    });

    xdescribe("Tests to Validate IG search functionality using different IG parameters",function(){
        var igDetails, igNameForSearch;

        beforeAll( function(){
            igNameForSearch = webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.igName);
            igService.createIG(igNameForSearch,dataForInitiatorGroups.igDescription,[Number(firstInitiatorDetails.id)]);
            webElementActionLibrary.refresh();
            initiatorGroupsPage.selectInitiatorGroupsTab();
            initiatorGroupsPage.searchForIG(igNameForSearch);
            initiatorGroupsPage.initiatorGroupsFirstRow().getText().then(function (val) {
                igDetails = initiatorGroupsPage.getIGDetailsFromUI(val);
            });
        });

        it("Tests to search IG using id", function() {
            initiatorGroupsPage.searchForIG(igDetails.id);
            initiatorGroupsPage.verifyIGValue(igDetails.id);
        });

        it("Tests to search IG using desc", function() {
            initiatorGroupsPage.searchForIG(igDetails.description);
            initiatorGroupsPage.verifyIGValue(igDetails.description);
        });

        it("Tests to search IG using no of Initiators", function() {
            initiatorGroupsPage.searchForIG(igDetails.numOfInitiators);
            initiatorGroupsPage.verifyIGValue(igDetails.numOfInitiators);
        });

        it("Tests to search IG using no of EG's", function() {
            initiatorGroupsPage.searchForIG(igDetails.exportGroups);
            initiatorGroupsPage.verifyIGValue(igDetails.exportGroups);
        });

        it("Test to compare the initiator details with the initiator details in the IG detailed grid and verify IG grid, IG detailed grid titles", function () {
            initiatorGroupsPage.verifyGridColumnTitles();
            initiatorGroupsPage.searchForIG(igNameForSearch);
            initiatorGroupsPage.igDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            //initiatorGroupsPage.verifyDetailedGridColumnTitles();
            //initiatorGroupsPage.verifyDetailedGridValues();
            //initiatorGroupsPage.compareDetailedGridInitiatorDetails(firstInitiatorDetails);
        });

        it("Test to compare the IG details with the IG details in the initiators detailed grid and IG count of a initiator in Initiator Grid", function () {
            initiatorPage.selectInitiatorsTab();
            initiatorPage.searchForInitiator(firstInitiatorDetails.name);
            initiatorPage.verifyInitiatorIGCount(firstInitiatorDetails.name,String(Number(firstInitiatorDetails.noOfIGs)+1));
            initiatorPage.initiatorDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            //initiatorPage.compareDetailedGridIGDetails(igDetails);
            initiatorGroupsPage.selectInitiatorGroupsTab();
            webElementActionLibrary.waitForAngular();
        });

        describe("Tests to validate UI Error for Edit IG Pane'",function(){
            beforeAll(function(){
                initiatorGroupsPage.searchForIG(igNameForSearch);
                initiatorGroupsPage.editButton().click();
                webElementActionLibrary.waitForAngular();
            });

            xit("Test: Verify Elements in Edit IG Panel",function(){
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForInitiatorGroups.editIGPopUpTitle);
            });

            xit("Test: Try updating IG with no name, description and zero size, and verify empty field UI error messages",function(){

            });

            xit("Test: Try updating IG with proper name and improper description, and verify UI error messages",function(){

            });

            xit("Test: Try updating IG with improper name and proper description, and verify UI error messages",function(){

            });

            xit("Test: Try updating IG with proper name, description and with improper size, and verify UI error messages",function(){

            });
        });

        afterAll( function(){
            igService.deleteIG(igDetails.id);
        });
    });

    describe("Tests to delete  IG with different inputs",function(){

        beforeEach( function(){
            igName = webElementActionLibrary.getTimeStampAttached(dataForInitiatorGroups.igName);
            igService.createIG(igName,dataForInitiatorGroups.igDescription,[Number(firstInitiatorDetails.id)]);
            webElementActionLibrary.refresh();
            initiatorGroupsPage.selectInitiatorGroupsTab()
        });


        it("Test to verify initiator delete disability which is associated with a IG and to delete IG", function() {
            initiatorGroupsPage.deleteIG(igName,true);
        });
    });

    describe("Tests to validate UI Error for Create IG Panel'",function(){

        beforeAll(function(){
            initiatorGroupsPage.selectInitiatorGroupsTab();
            initiatorGroupsPage.createNewButton().click();
            webElementActionLibrary.waitForAngular();
        });

        it("Test: Verify Elements in Create IG Panel",function(){
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForInitiatorGroups.createIGPopUpTitle);
            webElementActionLibrary.verifyPresenceOfElement(initiatorGroupsPage.cancelButton());
        });

        xit("Test: Try creating IG with no name, description and zero size, and verify empty field UI error messages",function(){

        });

        xit("Test: Try creating IG with proper name and improper description, and verify UI error messages",function(){

        });

        xit("Test: Try creating IG with improper name and proper description, and verify UI error messages",function(){

        });

        xit("Test: Try creating IG with proper name, description and with improper size, and verify UI error messages",function(){

        });
        afterAll(function(){
            commonAssetsControlsPage.closeWindow();
        });
    });

    afterAll(function () {
        initiatorService.deleteInitiator(firstInitiatorDetails.id);
        initiatorService.deleteInitiator(secondInitiatorDetails.id);
    });

});



