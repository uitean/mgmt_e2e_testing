var dataForInitiators={

 //successMessages
 initiatorCreationSuccessMessage: function(id){return 'Details: Initiator #('+id+') has been created.'},
 initiatorUpdateSuccessMessage: function(id){return 'Details: Initiator #('+id+') has been updated.'},
 initiatorDeleteSuccessMessage: function(id){return 'Details: Initiator(s) #('+id+') has been deleted.'},

 //String Constants
 initiatorName:"TAI",
 editedInitiatorName:"TAIE",
 initiatorDescription:"TAIDesc",
 editedInitiatorDescription:"TAIEDesc",
 initiatorsTabText: "Initiators",
 iniName31Char:"InitiaName31Ch",
 iniDesc63Char:"InitiaDescriptionForUIAutomation64CharactersToTest63CharactersT",
 editedIniName31Char:"EInitiaName31C",
 editedIniDesc63Char:"EdInitiaDescriptionForUIAutomation63CharactersToTest63Character",
 createInitiatorPopUpTitle: "Create Initiator",
    editInitiatorPopUpTitle: "Edit Initiator",
 emptyMemberIdErrorMessage:"This field is mandatory",
 invalidMemberId:"dsadsadasda",
 detailedGridIGTabText:"Initiator Groups",
 invalidMemberIdErrorMessage:"Must contains hexadecimal characters([0-9] or [A-F]), Example 12:23:45:67:78:98:80:FF"

};

module.exports = dataForInitiators;
