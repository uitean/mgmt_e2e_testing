'use strict';

//includes all the js file dependencies required for execution of scenarios

var initiatorPage = dependencies.getInitiatorPage(),
    dataForInitiators = dependencies.getDataForInitiators(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData = dependencies.getCommonData(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage(),
    initiatorService = dependencies.getInitiatorService();



describe("Tests For Initiators", function() {
    var initiatorWWN, initiatorName, editedInitiatorName;

    beforeAll(function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToInitiatorsPage();
    });

    beforeEach(function(){
        initiatorWWN = initiatorPage.getInitiatorWWN();
        initiatorName = webElementActionLibrary.getTimeStampAttached(dataForInitiators.initiatorName);

    });

    describe("Tests to create initiator with different inputs",function(){
        beforeEach(function(){
            webElementActionLibrary.refresh();
        });

        it("Test to verify successful Initiator Creation", function(){
            initiatorPage.createInitiator(initiatorName,dataForInitiators.initiatorDescription,initiatorWWN,true);
            initiatorPage.verifyGridColumnTitles();
            initiatorPage.initiatorDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            initiatorPage.verifyDetailedGridColumnTitles();
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getPagerInfo(initiatorPage.detailedGrid()),commonData.noItemsToDisplay);
        });

        it("Test to verify successful Initiator Creation with name as 31 characters and description as 63 characters", function(){
            initiatorName=webElementActionLibrary.getTimeStampAttached(dataForInitiators.iniName31Char);
            initiatorPage.createInitiator(initiatorName,dataForInitiators.initiatorDescription,initiatorWWN)
        });

        it("Test to verify successful Initiator Creation with only name", function(){
            initiatorPage.createInitiator(initiatorName,"",initiatorWWN)
        });

        afterEach(function () {
            initiatorPage.searchForInitiator(initiatorName);
            initiatorPage.initiatorsFirstRow().getText().then(function (val) {
                var iniDetails = initiatorPage.getInitiatorDetailsFromUI(val);
                initiatorService.deleteInitiator(iniDetails.id);
            });
        });

    });

    xdescribe("Tests to update Initiator with different inputs",function(){

        beforeEach( function(){
            initiatorService.createInitiator(initiatorName,dataForInitiators.initiatorDescription,initiatorWWN);
            webElementActionLibrary.refresh();
            editedInitiatorName = webElementActionLibrary.getTimeStampAttached(dataForInitiators.editedInitiatorName);
        });


        it("Tests to Update Initiator Name", function() {
            initiatorPage.editInitiator(initiatorName,editedInitiatorName,dataForInitiators.editedInitiatorDescription,initiatorWWN);
        });

        it("Test to update initiator with name as 31 characters and description as 63 characters", function() {
            editedInitiatorName = webElementActionLibrary.getTimeStampAttached(dataForInitiators.editedIniName31Char);
            initiatorPage.editInitiator(initiatorName,editedInitiatorName,dataForInitiators.editedIniDesc63Char,initiatorWWN);
        });

        it("Test to update Initiator with blank description", function() {
            initiatorPage.editInitiator(initiatorName,editedInitiatorName,"",initiatorWWN);
        });

        afterEach(function () {
            initiatorPage.searchForInitiator(editedInitiatorName);
            initiatorPage.initiatorsFirstRow().getText().then(function (val) {
                var iniDetails = initiatorPage.getInitiatorDetailsFromUI(val);
                initiatorService.deleteInitiator(iniDetails.id);
            });
        });
    });

    describe("Tests to delete initiator",function(){
        beforeEach(function(){
            initiatorService.createInitiator(initiatorName,dataForInitiators.initiatorDescription,initiatorWWN);
            webElementActionLibrary.refresh();
        });

        it("Test to verify successful Initiator deletion", function(){
            initiatorPage.deleteInitiator(initiatorName,true) ;
        });

    });

    describe("Tests to Validate Initiator search functionality using different Initiator parameters",function(){
        var initiatorDetails, initiatorWWNForSearch,iNameForSearch;

        beforeAll( function(){
            initiatorWWNForSearch = initiatorPage.getInitiatorWWN();
            iNameForSearch=webElementActionLibrary.getTimeStampAttached(dataForInitiators.initiatorName);
            initiatorService.createInitiator(iNameForSearch,dataForInitiators.initiatorDescription,initiatorWWNForSearch);
            webElementActionLibrary.refresh();
            initiatorPage.searchForInitiator(iNameForSearch);
            initiatorPage.initiatorsFirstRow().getText().then(function (val) {
                initiatorDetails = initiatorPage.getInitiatorDetailsFromUI(val,dataForInitiators.initiatorDescription);
                console.log(initiatorDetails.wwn);
            });
        });

        it("Tests to verify edit popup", function() {
            initiatorPage.editButton().click();
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForInitiators.editInitiatorPopUpTitle);
            webElementActionLibrary.verifyPresenceOfElement(initiatorPage.cancelButton());
            initiatorPage.cancelButton().click();
        });

        it("Tests to search Initiator using id", function() {
            initiatorPage.searchForInitiator(initiatorDetails.id);
            initiatorPage.verifyInitiatorValue(initiatorDetails.id);
        });

        it("Tests to search Initiator using name", function() {
            initiatorPage.searchForInitiator(initiatorDetails.name);
            initiatorPage.verifyInitiatorValue(initiatorDetails.name);
        });

        it("Tests to search Initiator using description", function() {
            initiatorPage.searchForInitiator(initiatorDetails.description);
            initiatorPage.verifyInitiatorValue(initiatorDetails.description);
        });

        xit("Tests to search Initiator using wwn", function() {
            initiatorPage.searchForInitiator(initiatorDetails.wwn);
            initiatorPage.verifyInitiatorValue(initiatorDetails.wwn);
        });

        it("Tests to search Initiator using discovery method", function() {
            initiatorPage.searchForInitiator(initiatorDetails.discoveryMethod);
            initiatorPage.verifyInitiatorValue(initiatorDetails.discoveryMethod);
        });

        it("Tests to search Initiator using no of IG's", function() {
            initiatorPage.searchForInitiator(initiatorDetails.noOfIGs);
            initiatorPage.verifyInitiatorValue(initiatorDetails.noOfIGs);
        });

        afterAll( function(){
            initiatorService.deleteInitiator(initiatorDetails.id);
        });
    });


    xdescribe("Tests to validate UI Error for Create Initiator Panel'",function(){

        beforeAll(function(){
            webElementActionLibrary.wait(2);
            initiatorPage.createNewButton().click();
            webElementActionLibrary.waitForAngular();
        });

        it("Test: Verify Elements in Create Initiator Panel",function(){
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForInitiators.createInitiatorPopUpTitle);
            webElementActionLibrary.verifyPresenceOfElement(initiatorPage.cancelButton());
        });

        it("Test: Verify empty Initiator Member id Error message",function(){
            initiatorPage.submitButton().click();
            webElementActionLibrary.wait(1);
            webElementActionLibrary.waitForElement(commonAssetsControlsPage.invalidErrorMsgs());
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.invalidErrorMsgs().last(),dataForInitiators.emptyMemberIdErrorMessage);
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.invalidErrorMsgs().first(),dataForInitiators.emptyMemberIdErrorMessage);
        });

        it("Test:  Verify invalid Initiator Member id Error message",function(){
            webElementActionLibrary.type(initiatorPage.initiatorMemberId(),dataForInitiators.invalidMemberId);
            initiatorPage.submitButton().click();
            webElementActionLibrary.waitForElement(commonAssetsControlsPage.invalidErrorMsgs());
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.invalidErrorMsgs().last(),dataForInitiators.invalidMemberIdErrorMessage);
            webElementActionLibrary.type(initiatorPage.initiatorMemberId(),initiatorPage.getInitiatorWWN());
        });


        afterAll(function(){
            commonAssetsControlsPage.closeWindow();
        });
    });


});

