/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var
    dataForInitiators = dependencies.getDataForInitiators(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage();


var initiatorPage =  {

    getInitiatorWWN:function(){
        var wwn="";
        var id = (Math.random().toString(16).slice(2,10)+Math.random().toString(16).slice(2,10));
        while(id.length<16){
            id='0'+id;
        }
        for (var i=0;i<id.length-2;i=i+2){
            wwn+= id.slice(i,i+2)+":";
        }
        wwn+= id.slice(14,16);
        return wwn;
    },

    createInitiator: function(name,description,initiatorMemberId,serviceCheckRequired){
        initiatorPage.createNewButton().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.type(initiatorPage.initiatorName(),name);
        webElementActionLibrary.type(initiatorPage.initiatorDescription(),description);
        webElementActionLibrary.type(initiatorPage.initiatorMemberId(),initiatorMemberId);
        initiatorPage.submitButton().click();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var initiatorId = commonAssetsControlsPage.getIdFromNotification(val);
            //validate initiator creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForInitiators.initiatorCreationSuccessMessage(initiatorId));
            commonAssetsControlsPage.closeIcon().click();
            initiatorPage.verifyInitiatorDetails(name,description,initiatorMemberId,serviceCheckRequired);
        });
    },

    editInitiator: function (name,editedName,description,initiatorWWN) {
        webElementActionLibrary.type(initiatorPage.searchBox(), name);
        initiatorPage.editButton().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.type(initiatorPage.initiatorName(),editedName);
        webElementActionLibrary.type(initiatorPage.initiatorDescription(),description);
        initiatorPage.submitButton().click();
        webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var initiatorId = commonAssetsControlsPage.getIdFromNotification(val);
            //validate initiator creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyUpdatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForInitiators.initiatorUpdateSuccessMessage(initiatorId));
            commonAssetsControlsPage.closeIcon().click();
            initiatorPage.verifyInitiatorDetails(editedName,description,initiatorWWN);
        });

    },

    deleteInitiator:function(initiatorId,serviceCheckRequired){
        webElementActionLibrary.type(initiatorPage.searchBox(),initiatorId);
        webElementActionLibrary.waitForAngular();
        initiatorPage.initiatorsFirstRow().getText().then(function (val) {
            var initiatorDetails = initiatorPage.getInitiatorDetailsFromUI(val);
            commonAssetsControlsPage.selectAndDeleteAsset(initiatorPage.initiatorGrid(),initiatorPage.initiatorDeletePopup());
            webElementActionLibrary.wait(commonData.waitTime);
            webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyDeletedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForInitiators.initiatorDeleteSuccessMessage(initiatorDetails.id));
            commonAssetsControlsPage.closeIcon().click();
            if(serviceCheckRequired) {
                webElementActionLibrary.refresh();
                webElementActionLibrary.type(initiatorPage.searchBox(), initiatorId);
                webElementActionLibrary.verifyElementsCount(initiatorPage.initiatorRows(),1);
                expect(initiatorPage.initiatorRows().count()).toBe(1);
                webElementActionLibrary.verifyTextPresence(initiatorPage.initiatorRows(),commonData.noItemsToDisplay);
            }
        });
    },

    verifyInitiatorDetails:function(name,description,initiatorMemberId,serviceCheckRequired) {
        if(serviceCheckRequired)
            webElementActionLibrary.refresh();
        webElementActionLibrary.type(initiatorPage.searchBox(), name);
        initiatorPage.initiatorsFirstRow().getText().then(function (val) {
            var initiatorDetails = initiatorPage.getInitiatorDetailsFromUI(val,description);
            var noOfIGs;
            expect(initiatorDetails.name).toBe(name);
            expect(initiatorDetails.description).toBe(description);
            expect(initiatorDetails.wwn).toBe(initiatorMemberId);
            expect(initiatorDetails.discoveryMethod).toBe("Manual");
            noOfIGs = initiatorDetails.noOfIGs;
            expect(noOfIGs).toBe("0");
        });
        webElementActionLibrary.verifyPresenceOfElement(initiatorPage.itNexusButton());
    },

    verifyInitiatorIGCount:function(initiatorMemberId,igCount) {
        initiatorPage.searchForInitiator(initiatorMemberId);
        initiatorPage.initiatorsFirstRow().getText().then(function (val) {
            var initiatorDetails = initiatorPage.getInitiatorDetailsFromUI(val);
            expect(initiatorDetails.noOfIGs).toBe(igCount);
        });
    },

    verifyGridColumnTitles: function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(initiatorPage.initiatorGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getWWNTitle(initiatorPage.initiatorGrid()),commonData.wwnTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(initiatorPage.initiatorGrid()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(initiatorPage.initiatorGrid()),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDiscoveryMethodTitle(initiatorPage.initiatorGrid()),commonData.discoveryMethodTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfIGsTitle(initiatorPage.initiatorGrid()),commonData.noOfIGsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPortsTitle(initiatorPage.initiatorGrid()),commonData.noOfPortsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortListTitle(initiatorPage.initiatorGrid()),commonData.portListTitle);
    },

    verifyGridValues: function () {
        //webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(initiatorPage.initiatorGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(initiatorPage.initiatorGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(initiatorPage.initiatorGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(initiatorPage.initiatorGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(initiatorPage.initiatorGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get7thColumnValue(initiatorPage.initiatorGrid()));
        //webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get8thColumnValue(initiatorPage.initiatorGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get9thColumnValue(initiatorPage.initiatorGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get10thColumnValue(initiatorPage.initiatorGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get11thColumnValue(initiatorPage.initiatorGrid()));
    },

    verifyDetailedGridColumnTitles : function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(initiatorPage.detailedGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(initiatorPage.detailedGrid()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(initiatorPage.detailedGrid()),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfInitiatorsTitle(initiatorPage.detailedGrid()),commonData.noOfInitiatorsTitle);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText().last(),commonData.igTabText);
    },

    verifyDetailedGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(initiatorPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(initiatorPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(initiatorPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(initiatorPage.detailedGrid()));

    },

    compareDetailedGridIGDetails: function (igDetails) {
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getIdValue(initiatorPage.detailedGrid()),igDetails.id);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getNameValue(initiatorPage.detailedGrid()),igDetails.name);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getDescriptionValue(initiatorPage.detailedGrid()),igDetails.description);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getNoOfInitiatorsValue(initiatorPage.detailedGrid()),igDetails.numOfInitiators);
    },


    verifyInitiatorValue: function (value) {
        initiatorPage.initiatorsFirstRow().getText().then(function (val) {
            expect(val).toContain(value);
        });
    },

    initiatorDetailedGridExpansionLinkFirstRow: function () {
        return commonAssetsControlsPage.getDetailedGridExpansionLink(initiatorPage.initiatorsFirstRow());
    },

    detailedGrid: function () {
        return commonAssetsControlsPage.getDetailedGrid(initiatorPage.initiatorGrid());
    },

    selectInitiatorsTab: function(){
        initiatorPage.initiatorsTab().click();
        webElementActionLibrary.waitForAngular();
    },

    initiatorsTab: function () {
      return $('#initiatorsTab');
    },

    searchForInitiator: function (param) {
        webElementActionLibrary.type(initiatorPage.searchBox(), param);
    },

    initiatorMemberId: function(){
        return $('#vx_initiator_form_memberId');
    },

    initiatorName: function(){
        return $('#vx_initiator_form_name');
    },

    initiatorDescription: function(){
        return $('#vx_initiator_form_description');
    },

    editButton: function () {
        //return initiatorPage.initiatorGrid().$('.k-i-edit');
        return commonAssetsControlsPage.getEditButton(initiatorPage.initiatorGrid());
    },

    submitButton: function(){
        return $('#vx-initiatorGrid-submit-btn');
    },

    cancelButton: function(){
        return $('#vx-initiatorGrid-cancel-btn');
    },

    initiatorGrid:  function() {
        return $('[feature="initiatorGrid"]');
    },

    searchBox:  function () {
        return $('[ng-model="initiatorGrid.search.nm"]');
    },

    initiatorsFirstRow:  function () {
        return initiatorPage.initiatorGrid().$$('tbody tr').first();
    },

    initiatorRows:  function () {
        return initiatorPage.initiatorGrid().$$('tbody tr');
    },

    createNewButton:  function () {
        return $('#initiatorGrid-create-btn-id');
    },

    deleteButton:  function () {
        return initiatorPage.initiatorGrid().$('.k-grid-delete');
    },

    itNexusButton:  function () {
        return initiatorPage.initiatorGrid().$('[name="itnexus"]');
    },

    getInitiatorDetailsFromUI: function(val,description){
        var details;
        if(description=="") {
            details = {
                id: val.split(' ')[0],
                name:val.split(' ')[1],
                description:"",
                wwn: val.split(' ')[2],
                hostProfileType: val.split(' ')[3],
                discoveryMethod: val.split(' ')[4],
                noOfIGs: val.split(' ')[5],
                noOfPorts: val.split(' ')[6]
                //portList: val.split(' ')[7].split('EditIT')[0].trim()
            };
        }
        else{
            details = {
                id: val.split(' ')[0],
                name:val.split(' ')[1],
                description:val.split(' ')[2],
                wwn: val.split(' ')[3],
                hostProfileType: val.split(' ')[4],
                discoveryMethod: val.split(' ')[5],
                noOfIGs: val.split(' ')[6],
                noOfPorts: val.split(' ')[7]
                //portList: val.split(' ')[7].split('EditIT')[0].trim()
            };
        }
        return details;
    },

    initiatorDeletePopup: function () {
        return $('[kendo-window="initiatorPopupWindow"]');
    }

};

module.exports = initiatorPage;