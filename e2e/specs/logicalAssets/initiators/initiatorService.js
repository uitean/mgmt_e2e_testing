/**
 * Created by Ajay(Vexata) on 10-04-2017.
 */
'use strict';

const frisby = require('frisby');
var commonData=dependencies.getCommonData();
var commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage();
var initiatorsAPIURL = commonData.initiatorsApiUrl();
var initiatorService = {

    deleteInitiator: function (initiatorId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.del(initiatorsAPIURL+initiatorId)
            .expect('status',204).then(function (res) {
                console.log("Initiator "+initiatorId+" deleted through rest api");
            });
    },

    createInitiator: function (name,desc,memberId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.post(initiatorsAPIURL,
            {
                name: name,
                description: desc,
                memberId: memberId
            }, {json: true})
            .expect('status',201)
            .then(function(res){
                console.log("Initiator "+res._json.name +" with id "+res._json.id+" created through rest api");
            });
    },

    getInitiatorDetails: function (initiatorId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.get(initiatorsAPIURL+initiatorId).
            expect('status',200)
            .then(function (res) {
                console.log(res);
            });
    },

};



module.exports = initiatorService;
