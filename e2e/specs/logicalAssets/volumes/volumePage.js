/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';


var
    dataForVolumes = dependencies.getDataForVolumes(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData=dependencies.getCommonData(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage(),
    volumeService=dependencies.getVolumeService();


var volumePage =  {

    createVolume: function(volumeName,volDescription,size,serviceCheckRequired){
        volumePage.createNewButton().click();
        webElementActionLibrary.type(volumePage.volName(),volumeName);
        webElementActionLibrary.type(volumePage.volDescription(), volDescription);
        volumePage.selectSizeRadioButton();
        volumePage.enterVolumeSize(size);
        webElementActionLibrary.waitForAngular();
        volumePage.submitButton().click();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            console.log(val);
            var volumeId = commonAssetsControlsPage.getIdFromNotification(val);
            //validate volume creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumes.getVolumeCreationSuccessMessage(volumeId));
            commonAssetsControlsPage.closeIcon().click();
            size=volumePage.getSizeWithUnit(size,dataForVolumes.GiB);
            volumePage.verifyVolumeDetailsInGrid(volumeName,volDescription,size,serviceCheckRequired);
        });
    },


    createVolumeWithSizeUnit: function(volumeName,description,size,unit,serviceCheckRequired){
        volumePage.createNewButton().click();
        webElementActionLibrary.type(volumePage.volName(),volumeName);
        webElementActionLibrary.type(volumePage.volDescription(), description);
        volumePage.selectSizeRadioButton(unit);
        volumePage.enterVolumeSize(size);
        volumePage.submitButton().click();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            console.log(val);
            var volumeId = commonAssetsControlsPage.getIdFromNotification(val);
            //validate volume creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumes.getVolumeCreationSuccessMessage(volumeId));
            commonAssetsControlsPage.closeIcon().click();
            size = volumePage.getSizeWithUnit(size,unit);
            volumePage.verifyVolumeDetailsInGrid(volumeName,description,size,serviceCheckRequired);
        });
    },

    deleteVolume:function(name,serviceCheckRequired){
        webElementActionLibrary.wait(3);
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumePage.volumeGrid()), name);
        webElementActionLibrary.waitForAngular();
        volumePage.volumeFirstRow().getText().then(function (val) {
            var volumeDetails = volumePage.getVolumeDetailsFromUI(val);
            commonAssetsControlsPage.selectAndDeleteAsset(volumePage.volumeGrid(),volumePage.volDeletePopup());
            //webElementActionLibrary.wait(commonData.waitTime);
            //webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
            //webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyDeletedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumes.getVolumeDeleteSuccessMessage(volumeDetails.id));
            commonAssetsControlsPage.closeIcon().click();
            if(serviceCheckRequired){
                webElementActionLibrary.refresh();
                webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumePage.volumeGrid()), name);
                expect(volumePage.volumeRows().count()).toBe(2);
            }
            //validate volume deletion from API service
            //volumeService.verifyVolumeDeletion(id);
        });
    },

    editVolume: function(presentVolName,newVolName,description,serviceCheckRequired){
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumePage.volumeGrid()), presentVolName);
        volumePage.editButton().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.type(volumePage.volName(), newVolName);
        webElementActionLibrary.type(volumePage.volDescription(),description);
        volumePage.submitButton().click();
        webElementActionLibrary.waitForAngular();
        //webElementActionLibrary.waitForElementUsingExpectedConditions(commonAssetsControlsPage.notification());
        //validate volume update success message
        //commonAssetsControlsPage.notification().isPresent().then(function () {
        commonAssetsControlsPage.notification().getText().then(function (val) {
            //        console.log(val);
            var size;
            var volumeId = commonAssetsControlsPage.getIdFromNotification(val);
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyUpdatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumes.getVolumeEditSuccessMessage(volumeId));
            commonAssetsControlsPage.closeIcon().click();
            volumePage.verifyVolumeDetailsInGrid(newVolName,description,size,serviceCheckRequired); //sending size as undefined to avoid size validation
        });
        //});
    },

    verifyGridColumnTitles: function (grid) {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIDTitle(grid),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(grid),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(grid),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getProvisionedTitle(grid),commonData.provisionedTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getVolumeUsedTitle(grid),commonData.usedTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfVGsTitle(grid),commonData.noOfVGsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPortsTitle(grid),commonData.noOfPortsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortListTitle(grid),commonData.portListTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPRTitle(grid),commonData.noOfPRTitle);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPRKeysTitle(grid),commonData.noOfPRKeysTitle);
    },

    createSS: function (volName) {
        volumePage.searchForVolume(volName);
        volumePage.createSSIcon().click();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            console.log(val);
            var ssId = commonAssetsControlsPage.getIdFromNotification(val);
            //validate volume creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumes.getSSCreationSuccessMessage(ssId));
            commonAssetsControlsPage.closeIcon().click();
            volumePage.volumeDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            volumePage.selectSSTab();
            webElementActionLibrary.wait(2);
            volumePage.ssGridTable().getText().then(function (val) {
                volumePage.verifySSDetailsInSnapshotGrid(val,ssId);
            });

            // volumePage.verifyVolumeDetailsInGrid(volumeName,description,size,serviceCheckRequired);
        });

    },

    loadSSGridForVolume: function (volName) {
        volumePage.searchForVolume(volName);
        volumePage.volumeDetailedGridExpansionLinkFirstRow().click();
        webElementActionLibrary.waitForAngular();
        volumePage.selectSSTab();
        webElementActionLibrary.wait(2);
    },


    editSS: function (volName,ssEditedName) {
        volumePage.loadSSGridForVolume(volName);
        volumePage.editSSIcon().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.type(volumePage.ssNameInputField(),ssEditedName);
        webElementActionLibrary.type(volumePage.ssDescInputField(),"ssupdatedesc");
        volumePage.editSSOkButton().click();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var ssId=commonAssetsControlsPage.getIdFromNotification(val);
            console.log(ssId);
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyUpdatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumes.getSSEditSuccessMessage(ssId));
            commonAssetsControlsPage.closeIcon().click();
            webElementActionLibrary.refresh();
            volumePage.loadSSGridForVolume(volName);
            volumePage.ssGridTable().getText().then(function (val) {
                volumePage.verifySSDetailsInSnapshotGrid(val,ssId,ssEditedName,"ssupdatedesc");
            });

            // volumePage.verifyVolumeDetailsInGrid(volumeName,description,size,serviceCheckRequired);
        });

    },

    deleteSS:function (volName) {
        volumePage.loadSSGridForVolume(volName);
        volumePage.deleteSSIcon().click();
        commonAssetsControlsPage.deleteRecord();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var ssId=commonAssetsControlsPage.getIdFromNotification(val);
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumes.getSSDeleteSuccessMessage(ssId));
            commonAssetsControlsPage.closeIcon().click();
        });
    },

    verifySSDetailsInSnapshotGrid:function (val,ssId,ssName,ssDesc) {
        console.log(val);
        volumeService.getSSDetails(ssId).then(function (value) {
            expect(val).toContain(ssId);
            if(ssName==undefined)
                expect(val).toContain(value._json.name);
            else
                expect(val).toContain(ssName);
            if(ssDesc!==undefined)
                expect(val).toContain(value._json.description);
            // webElementActionLibrary.verifyTextPresence(volumePage.ssGridTable(),ssId);
            // webElementActionLibrary.verifyTextPresence(volumePage.ssGridTable(),value._json.name);
            // webElementActionLibrary.verifyTextPresence(volumePage.ssGridTable(),value._json.desc);
        });
    },

    verifyGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(volumePage.volumeGrid()));
        //webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(volumePage.volumeGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(volumePage.volumeGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(volumePage.volumeGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get7thColumnValue(volumePage.volumeGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get8thColumnValue(volumePage.volumeGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get9thColumnValue(volumePage.volumeGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get10thColumnValue(volumePage.volumeGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get11thColumnValue(volumePage.volumeGrid()));
    },

    verifyDetailedGridColumnTitles : function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(volumePage.detailedGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(volumePage.detailedGrid()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(volumePage.detailedGrid()),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfVolumesTitle(volumePage.detailedGrid()),commonData.noOfVolumesTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.selectedTabText().last(),commonData.vgTabText);
    },

    verifyDetailedGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(volumePage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(volumePage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(volumePage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(volumePage.detailedGrid()));
    },

    compareDetailedGridVGDetails: function (vgDetails) {
        //webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getIdValue(volumePage.detailedGrid()),vgDetails.id);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getNameValue(volumePage.detailedGrid()),vgDetails.name);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getDescriptionValue(volumePage.detailedGrid()),vgDetails.description);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.getNoOfVolumesValue(volumePage.detailedGrid()),vgDetails.numOfVols);
    },

    compareVolumeDetailsOnUIAndRestResponse : function(volumeName){
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumePage.volumeGrid()), volumeName);
        volumePage.volumeFirstRow().getText().then(function (val) {
            var volumeDetails = volumePage.getVolumeDetailsFromUI(val);
            //volumeService.verifyVolumeDetailsAndDelete(volumeDetails);
        });
    },

    verifyPhysicalStorageBar: function () {
        expect(volumePage.physicalStorageBar().getAttribute("high")).toBe(dataForVolumes.physicalStorageHighCapacity);
        expect(volumePage.physicalStorageBar().getAttribute("min")).toBe(dataForVolumes.physicalStorageMinCapacity);
        expect(volumePage.physicalStorageBar().getAttribute("max")).toBe(dataForVolumes.physicalStorageMaxCapacity);
        volumePage.physicalStorageBar().getAttribute("value").then(function(val){

        });
    },

    searchForVolume: function (param) {
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumePage.volumeGrid()), param);
    },

    verifyVolumeDetailsInGrid:function(volName,volDescription,size,serviceCheckRequired) {
        if(serviceCheckRequired){
            webElementActionLibrary.refresh();
        }
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumePage.volumeGrid()), volName);
        volumePage.volumeFirstRow().getText().then(function (val) {
            var volumeDetails = volumePage.getVolumeDetailsFromUI(val,volDescription);
            expect(volumeDetails.name).toBe(volName);
            expect(volumeDetails.description).toBe(volDescription);
            if(size!==undefined) {
                var sizeFormatted = volumeDetails.size.replace(/,/g, "").split(".00")[0];
                expect(sizeFormatted+" "+volumeDetails.sizeUnit).toBe(size);
            }
        });
        //webElementActionLibrary.verifyPresenceOfElement(volumePage.itNexusButton());
    },

    verifyVolumeId: function (value) {
        volumePage.volumeFirstRow().getText().then(function (val) {
            expect(value).toContain(val);
        });
    },

    verifyVolumeValue: function (value) {
        volumePage.volumeFirstRow().getText().then(function (val) {
            expect(val).toContain(value);
        });
    },

    verifyVolumeUUID: function (value) {
        commonAssetsControlsPage.getVolumeUUIDValue(volumePage.volumeGrid()).getText().then(function (val) {
            expect(value).toBe(val);
        });
    },

    verifyVolumeDesc: function (value) {
        commonAssetsControlsPage.getDescriptionValue(volumePage.volumeGrid()).getText().then(function (val) {
            expect(value).toBe(val);
        });
    },

    verifyVolumeSize: function (value) {
        commonAssetsControlsPage.get6thColumnValue(volumePage.volumeGrid()).getText().then(function (val) {
            expect(value).toBe(val);
        });
    },

    verifyVolumeUsed: function (value) {
        commonAssetsControlsPage.get7thColumnValue(volumePage.volumeGrid()).getText().then(function (val) {
            expect(value).toBe(val);
        });
    },

    beOnVolumesTab: function(){
        commonAssetsControlsPage.selectedTabText().getText().then(function (val) {
            if(val!==commonData.volumesTabText){
                volumePage.selectVolumesTab();
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), commonData.volumesTabText);
            }
        });
    },


    verifyVolumeVGCount:function(name,vgCount) {
        volumePage.searchForVolume(name);
        volumePage.volumeFirstRow().getText().then(function (val) {
            var volumeDetails = volumePage.getVolumeDetailsFromUI(val);
            console.log(volumeDetails);
            expect(volumeDetails.noOfVGs).toBe(vgCount);
        });
    },

    ssGrid:function () {
        return $('vx-grid[feature="snapshots"]');
    },

    ssGridTable:function () {
        return $$('vx-grid[feature="snapshots"] table').get(1);
    },

    detailedGrid: function () {
        return commonAssetsControlsPage.getDetailedGrid(volumePage.volumeGrid());
    },

    selectVolumesTab: function () {
        volumePage.volumesTab().click();
    },

    selectSSTab: function () {
        volumePage.ssTab().click();
    },

    volumesTab: function () {
        return $('#volumesTab');
    },

    ssTab:function () {
        return webElementActionLibrary.getElementByXpath('//span[text()="SNAPSHOTS"]');
    },

    volName: function(){
        return $('#vx_volume_form_name');
    },

    volDescription: function(){
        return $('#vx_volume_form_description');
    },

    enterVolumeSize: function(size){
        webElementActionLibrary.type($('[ng-model*="VolSize"]'),size);
    },

    enterNoOfVolumes: function(noOfVolumes){
        webElementActionLibrary.type($('[ng-model*="VolumesGridTab.data.numberOfVolumes"]'),noOfVolumes);
    },

    sizeDropDown: function () {
        return volumePage.createorEditVolumeWindow().element(by.css('.k-dropdown-wrap'));
    },

    selectSizeRadioButton: function (unit) {
        if(emuFlag=="true"){
            volumePage.mibSizeRadioButton().click();
        }
        else{
            if(unit==dataForVolumes.MiB)
                volumePage.mibSizeRadioButton().click();
            else if(unit==dataForVolumes.GiB)
                volumePage.gibSizeRadioButton().click();
            else if(unit==dataForVolumes.TiB)
                volumePage.tibSizeRadioButton().click();
        }
    },

    getSizeWithUnit: function (size,unit) {
        var sizeWithUnit;
        if(emuFlag=="true"){
            sizeWithUnit=size+" "+dataForVolumes.MiB;
        }
        else{
            if(unit==dataForVolumes.MiB)
                sizeWithUnit=size+" "+dataForVolumes.MiB;
            else if(unit==dataForVolumes.TiB)
                sizeWithUnit=size+" "+dataForVolumes.TiB;
            else
                sizeWithUnit=size+" "+dataForVolumes.GiB;
        }

        return sizeWithUnit;
    },

    mibSizeRadioButton: function () {
        return $$('label[for="vx_volume_form_mb_unit_b10"] span').first();
    },

    gibSizeRadioButton: function () {
        return $$('label[for="vx_volume_form_gb_unit_b10"] span').first();
    },

    tibSizeRadioButton: function () {
        return $$('label[for="vx_volume_form_tb_unit_b10"] span').first();
    },

    submitButton: function(){
        return $('#vx-VolumesGridTab-submit-btn');
    },

    cancelButton: function(){
        return $('#vx-VolumesGridTab-cancel-btn');
    },

    volumeGrid:  function () {
        return $('#volumeGrid');
    },

    volumeFirstRow:  function () {
        return volumePage.volumeGrid().$$('tbody tr').first();
    },

    volumeDetailedGridExpansionLinkFirstRow: function () {
        return commonAssetsControlsPage.getDetailedGridExpansionLink(volumePage.volumeFirstRow());
    },

    volumeRows:  function () {
        return volumePage.volumeGrid().$$('tbody tr');
    },

    editPanelVolumeId: function () {
        return $('#vx_volume_form_volume_id');
    },

    editPanelVolumeUUID: function () {
        return $('#vx_volume_form_volume_uuid');
    },

    editButton:  function () {
        return commonAssetsControlsPage.getEditIcon(volumePage.volumeGrid());
    },

    createNewButton:  function () {
        return $('#VolumesGridTab-create-btn-id');
    },

    nameErrorMessage:  function () {
        return $('[data-container-for="name"] .k-invalid-msg');
    },

    itNexusButton:  function () {
        return volumePage.volumeGrid().$('[name="itnexus"]');
    },

    getVolumeDetailsFromUI: function(val,description){
        var details;
        if(description=="") {
            details = {
                id: val.split(' ')[0],
                //uuid: val.split(' ')[1],
                name: val.split(' ')[1],
                description: "",
                size: val.split(' ')[2],
                sizeUnit:val.split(' ')[3],
                used: val.split(' ')[4],
                usedUnit: val.split(' ')[5],
                noOfVGs: val.split(' ')[6],
                noOfPorts: val.split(' ')[7],
                portList: val.split(' ')[8],
                noOfInitiators: val.split(' ')[9]
            };
        }
        else{
            details = {
                id: val.split(' ')[0],
                //uuid: val.split(' ')[1],
                name: val.split(' ')[1],
                description: val.split(' ')[2],
                size: val.split(' ')[3],
                sizeUnit:val.split(' ')[4],
                used: val.split(' ')[5],
                usedUnit: val.split(' ')[6],
                noOfVGs: val.split(' ')[7],
                noOfPorts: val.split(' ')[8],
                portList: val.split(' ')[9],
                noOfInitiators: val.split(' ')[10]
            };
        }
        return details;
    },

    detailedGridLabel: function(){
        return commonAssetsControlsPage.getPagerInfo(volumePage.detailedGrid());
    },

    volumeSizeFirstRow: function(){
        return volumePage.volumeFirstRow().$('td:nth-child(6)');
    },

    volumeUsedFirstRow: function(){
        return volumePage.volumeFirstRow().$('td:nth-child(7)');
    },

    volumeGroupsFirstRow: function(){
        return volumePage.volumeFirstRow().$('td:nth-child(8)');
    },

    createorEditVolumeWindow: function () {
        return $('[kendo-window="createOrEditVolume"]');
    },

    physicalStorageBar: function () {
        return $('#vx-VolumePhysicalStorage-bar');
    },

    volDeletePopup: function () {
        return $('[kendo-window="popupWindow"]');
    },

    createSSIcon:function () {
        return $$('i[name="create_snapshot"]').get(0);
    },

    editSSIcon:function () {
        return volumePage.ssGrid().$('.snapshots-edit-btn-cls');
    },

    ssNameInputField:function () {
        return $('#vx_volume_snapshot_form_name');
    },

    ssDescInputField:function () {
        return $('#vx_volume_snapshot_form_description');
    },

    editSSOkButton: function () {
        return $('#vx-snapshots-submit-btn');
    },

    deleteSSIcon: function () {
        return volumePage.ssGrid().$('i[name="destroy"]');
    },

    volumeRefreshButton: function () {
    return $('#VolumesGridTab-refresh-btn-id');
    }





};

module.exports = volumePage;