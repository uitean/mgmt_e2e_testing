'use strict';

const frisby = require('frisby');
var commonData=dependencies.getCommonData();
var commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage();
var volumesAPIURL = commonData.volumesApiUrl();
var ssAPIUrl=commonData.ssAPIUrl();
var volumeService = {

    deleteVolume: function (volumeId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.del(volumesAPIURL+volumeId)
            .expect('status',204).then(function (res) {
                console.log("vol "+volumeId+" deleted through rest api");
            });
    },

    createVolume: function (name,desc,size) {
        console.log(name+desc+size);
        commonAssetsControlsPage.setServiceHeader();
        return frisby.post(volumesAPIURL,
            {
                name: name,
                description: desc,
                volSize: size
            }, {json: true})
            .expect('status',201)
            .then(function(res){
                console.log("vol "+res._json.name +" with id "+res._json.id+" created through rest api");
            });
    },

    getVolumeDetails: function (volId) {

        commonAssetsControlsPage.setServiceHeader();
        return  frisby.get(volumesAPIURL+volId).
        expect('status',200)
            .then(function (res) {
            });
    },

    getSSDetails: function (ssId) {

        commonAssetsControlsPage.setServiceHeader();
        return frisby.get(ssAPIUrl+ssId).
        expect('status',200);
    },

    createSS: function (volId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.post(volumesAPIURL+volId+"/snapshots",
            {
                name: "ss"+volId,
                description: "desc",
                volSize: "1"
            }, {json: true})
            .expect('status',201)
            .then(function (value) {
                console.log("ss " +value._json.id+ " created through rest api");
            });
    },

    getVolumeList: function (volId) {

        commonAssetsControlsPage.setServiceHeader();
        return frisby.get(volumesAPIURL).
        expect('status',200)
            .then(function (res) {
                console.log(res);
            });
    }

};



module.exports = volumeService;
