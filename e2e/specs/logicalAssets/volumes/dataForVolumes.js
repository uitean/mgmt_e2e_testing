var dataForVolumes={

    //successMessages
    getVolumeCreationSuccessMessage: function(id){return 'Details: Volume(s) #('+id+') has been created.'},
    getVolumeDeleteSuccessMessage: function(id){return 'Details: Volume(s) #('+id+') delete request is initiated, Please check the events section to verify the status.'},
    getVolumeEditSuccessMessage: function(id){return 'Details: Volume(s) #('+id+') has been updated'},
    getSSCreationSuccessMessage: function(id){return 'Details: Snapshot #('+id+') has been created.'},
    getSSDeleteSuccessMessage: function(id){return '#('+id+') delete request is initiated, Please check the events section to verify the status.'},
    getSSEditSuccessMessage: function(id){return 'Details: Snapshot #('+id+') has been updated'},

    //String Constants
    volumesTabText: "VOLUMES",
    createVolumePopUpTitle: "Create Volume",
    editVolumePopUpTitle: "Edit Volume",
    volumeName: "TAV",
    editedVolumeName: "TAVE",
    editedVolumeDescription: "TAvolDescEdited",
    volumeDescription: "TAvolDesc",
    volName31Char:"VolumeName31Ch",
    volDesc63Char:"VolumeDescriptionForUIAutomation64CharactersToTest63CharactersT",
    editedVolName31Char:"EVolumeName31C",
    editedVolDesc63Char:"EdVolumeDescriptionForUIAutomation63CharactersToTest63Character",
    volumeSize: "1",
    editedVolumeSize: "3",
    volumeSizeError: "Must be a positive numeric value greater than 0 and less than",
    noOfVolumesError:"Must be a positive numeric value greater than 0 and less than or equal to 50",
    detailedGridVGTabText:"VOLUME GROUPS",
    MiB:"MB",
    GiB:"GB",
    TiB:"TB",
    physicalStorageHighCapacity:"80",
    physicalStorageMinCapacity:"0",
    physicalStorageMaxCapacity:"0.96"

};

module.exports = dataForVolumes;
