'use strict';

//includes all the js file dependencies required for execution of scenarios

var volumePage = dependencies.getVolumePage(),
    dataForVolumes = dependencies.getDataForVolumes(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage(),
    commonData = dependencies.getCommonData(),
    volumeService = dependencies.getVolumeService();

describe("Tests For Volumes", function() {
    var volumeName,  editedVolumeName, volId;

    beforeAll(function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToVolumesPage();
    });


    describe("Tests to create volume with different inputs",function(){

        beforeEach(function() {
            volumeName = webElementActionLibrary.getTimeStampAttached(dataForVolumes.volumeName);
            webElementActionLibrary.refresh();
        });

        it("Test to verify successful Volume Creation with default size unit and to verify the grid and detailed grid column titles", function(){
            volumePage.createVolume(volumeName,dataForVolumes.volumeDescription,dataForVolumes.volumeSize);
            volumePage.verifyGridColumnTitles(volumePage.volumeGrid());
            volumePage.volumeDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            volumePage.verifyDetailedGridColumnTitles();
            webElementActionLibrary.verifyTextPresence(volumePage.detailedGridLabel(),commonData.noItemsToDisplay);
        });

        it("Test to verify successful Volume Creation with name as 31 characters and description as 63 characters and with default size unit", function(){
            volumeName=webElementActionLibrary.getTimeStampAttached(dataForVolumes.volName31Char);
            volumePage.createVolume(volumeName,dataForVolumes.volDesc63Char,dataForVolumes.volumeSize);
        });

        it("Test to verify successful Volume Creation with only name and verify edit popup elements", function(){
            volumePage.createVolume(volumeName,"",dataForVolumes.volumeSize);
            volumePage.editButton().click();
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForVolumes.editVolumePopUpTitle);
            webElementActionLibrary.verifyPresenceOfElement(volumePage.editPanelVolumeId());
            webElementActionLibrary.verifyPresenceOfElement(volumePage.editPanelVolumeUUID());
            webElementActionLibrary.verifyPresenceOfElement(volumePage.cancelButton());
            volumePage.cancelButton().click();
        });

        it("Test to verify successful Volume Creation with size in MiB", function(){
            volumePage.createVolumeWithSizeUnit(volumeName,dataForVolumes.volumeDescription,dataForVolumes.volumeSize,dataForVolumes.MiB);
        });

        it("Test to verify successful Volume Creation with size in TiB", function(){
            volumePage.createVolumeWithSizeUnit(volumeName,dataForVolumes.volumeDescription,dataForVolumes.volumeSize,dataForVolumes.TiB);
        });


        afterEach( function () {
            volumePage.searchForVolume(volumeName);
            volumePage.volumeFirstRow().getText().then(function (val) {
                var volDetails = volumePage.getVolumeDetailsFromUI(val);
                volumeService.deleteVolume(volDetails.id);
            });
        });

    });

    xdescribe("Tests to update Volume with different inputs",function(){

        beforeEach( function(){
            volumeService.createVolume(volumeName,dataForVolumes.volumeDescription,dataForVolumes.volumeSize);
            webElementActionLibrary.refresh();
            editedVolumeName = webElementActionLibrary.getTimeStampAttached(dataForVolumes.editedVolumeName);

        });

        it("Tests to Update Volume", function() {
            volumePage.editVolume(volumeName,editedVolumeName,dataForVolumes.editedVolumeDescription);
        });

        it("Test to update volume with name as 31 characters and description as 63 characters", function() {
            editedVolumeName = webElementActionLibrary.getTimeStampAttached(dataForVolumes.editedVolName31Char);
            volumePage.editVolume(volumeName,editedVolumeName,dataForVolumes.editedVolDesc63Char);
        });

        it("Test to update volume with blank description", function() {
            volumePage.editVolume(volumeName,editedVolumeName,"");
        });

        afterEach( function(){
            volumePage.searchForVolume(volumeName);
            volumePage.volumeFirstRow().getText().then(function (val) {
                var volDetails = volumePage.getVolumeDetailsFromUI(val);
                volumeService.deleteVolume(volDetails.id,done);
            });
        });
    });

    describe("Tests to Validate volume search functionality using different volume parameters and to validate UI Error for Edit Volume Pane",function(){
        var volDetails, volumeNameForSearch;

        beforeAll( function(){
            volumeNameForSearch = webElementActionLibrary.getTimeStampAttached(dataForVolumes.volumeName);
            volumeService.createVolume(volumeNameForSearch,dataForVolumes.volumeDescription,dataForVolumes.volumeSize);
            webElementActionLibrary.refresh();
            volumePage.searchForVolume(volumeNameForSearch);
            volumePage.volumeFirstRow().getText().then(function (val) {
                volDetails = volumePage.getVolumeDetailsFromUI(val);
            });
        });

        beforeEach( function(){
            //webElementActionLibrary.refresh();
        });

        //excluded as uuid is removed from the grid
        xit("Tests to search volume using UUID", function() {
            volumePage.searchForVolume(volDetails.uuid);
            volumePage.verifyVolumeUUID(volDetails.uuid);
        });

        xit("Tests to search volume using id", function() {
            volumePage.searchForVolume(volDetails.id);
            volumePage.verifyVolumeValue(volDetails.id);
        });

        it("Tests to search volume using desc", function() {
            volumePage.searchForVolume(volDetails.description);
            volumePage.verifyVolumeValue(volDetails.description);
        });

        it("Tests to search volume using size", function() {
            volumePage.searchForVolume(volDetails.size);
            volumePage.verifyVolumeValue(volDetails.size);
        });

        it("Tests to search volume using used", function() {
            volumePage.searchForVolume(volDetails.used);
            volumePage.verifyVolumeValue(volDetails.used);
        });

        xdescribe("Tests to validate edit volume panel", function () {

            beforeAll(function(){
                volumePage.searchForVolume(volDetails.name);
                volumePage.editButton().click();
            });

            it("Test: Verify Elements in Edit Volume Panel",function(){
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForVolumes.editVolumePopUpTitle);
                webElementActionLibrary.verifyPresenceOfElement(volumePage.editPanelVolumeId());
                webElementActionLibrary.verifyPresenceOfElement(volumePage.editPanelVolumeUUID());
                webElementActionLibrary.verifyPresenceOfElement(volumePage.cancelButton());
            });

            it("Test: Try updating Volume with no name, and no size, and verify empty field UI error messages",function(){
                webElementActionLibrary.type(volumePage.volName(),"");
                volumePage.enterVolumeSize("");
                volumePage.submitButton().click();
                commonAssetsControlsPage.formErrors().get(1).getText().then(function (val) {
                    webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().first(), commonData.emptyFieldError);
                    webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().get(1), commonData.emptyFieldError)
                });
            });

            it("Test: Try updating Volume with one numeric character name zero size, and verify UI error messages",function(){
                webElementActionLibrary.type(volumePage.volName(),"1");
                volumePage.enterVolumeSize("0");
                volumePage.submitButton().click();
                commonAssetsControlsPage.formErrors().get(1).getText().then(function (val) {
                    webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().first(), commonData.assetNameError);
                    webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.formErrors().get(1), dataForVolumes.volumeSizeError);
                });
            });
        });


        afterAll( function(){
            //webElementActionLibrary.refresh();
            volumeService.deleteVolume(volDetails.id);
        });
    });

    describe("Tests to delete Volume with different inputs",function(){

        beforeEach( function(){
            volumeName = webElementActionLibrary.getTimeStampAttached(dataForVolumes.volumeName);
            volumeService.createVolume(volumeName,dataForVolumes.volumeDescription,dataForVolumes.volumeSize);
            webElementActionLibrary.refresh();
        });

        it("Tests to Delete Volume", function(){
            volumePage.deleteVolume(volumeName,true);
        });


    });

    describe("Tests for snapshots",function(){
        var ssId;
        beforeAll( function(){
            volumeName = webElementActionLibrary.getTimeStampAttached(dataForVolumes.volumeName);
            volumeService.createVolume(volumeName,dataForVolumes.volumeDescription,dataForVolumes.volumeSize).then(function (value) { volId=value._json.id; });
        });

        beforeEach(function () {
           webElementActionLibrary.refresh();
        });

        it("Tests to Create Snapshot", function(){
            volumePage.createSS(volumeName);
        });


        it("Tests to Edit Snapshot", function(){
            volumePage.editSS(volumeName,webElementActionLibrary.getTimeStampAttached("ssEdit"));
        });

        it("Tests to delete Snapshot", function() {
            volumePage.deleteSS(volumeName);
        });

        afterAll( function(){
            volumeService.deleteVolume(volId);
        });


    });

    xdescribe("Tests to validate UI Error for Create Volume Panel'",function(){

        beforeAll(function(){
            volumePage.createNewButton().click();
            webElementActionLibrary.wait(2);
        });

        it("Test: Verify Elements in Create Volume Panel",function(){
            webElementActionLibrary.verifyExactText(commonAssetsControlsPage.popUpTitle(), dataForVolumes.createVolumePopUpTitle);
            webElementActionLibrary.verifyPresenceOfElement(volumePage.cancelButton());
        });

        it("Test: Try creating Volume with no name, and no size, and verify empty field UI error messages",function(){
            webElementActionLibrary.type(volumePage.volName(),"");
            volumePage.enterVolumeSize("");
            volumePage.enterNoOfVolumes("");
            volumePage.submitButton().click();
            commonAssetsControlsPage.formErrors().get(2).getText().then(function (val) {
                webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().first(), commonData.emptyFieldError);
                webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().get(1), commonData.emptyFieldError);
                webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().get(2), commonData.emptyFieldError);
            });
        });

        it("Test: Try creating Volume with one numeric character name zero size, and verify UI error messages",function(){
            webElementActionLibrary.type(volumePage.volName(),"1");
            volumePage.enterVolumeSize("0");
            volumePage.enterNoOfVolumes("0");
            volumePage.submitButton().click();
            commonAssetsControlsPage.formErrors().get(2).getText().then(function (val) {
                webElementActionLibrary.verifyExactText(commonAssetsControlsPage.formErrors().first(), commonData.assetNameError);
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.formErrors().get(2), dataForVolumes.noOfVolumesError);
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.formErrors().get(1), dataForVolumes.volumeSizeError);
            });
        });

    });



});

