/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';


var logicalAssetsDashboardPage =  {

    manageVolumesLink: function(){
        return $('a[ui-sref="volumes"]');
    },

    manageExportGroupsLink: function(){
        return $('a[ui-sref="exportgroups"]');
    },

    manageInitiatorsLink: function(){
        return $('a[ui-sref="initiators"]');
    },

    managePortsLink: function(){
        return $('a[ui-sref="ports"]');
    }

    };

module.exports = logicalAssetsDashboardPage;