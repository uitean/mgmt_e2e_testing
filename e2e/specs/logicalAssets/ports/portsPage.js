/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonData= dependencies.getCommonData(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage();


var portsPage =  {

    verifyGridColumnTitles: function(){
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortIdTitle(portsPage.portGrid()),commonData.portIdTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getWWNTitle(portsPage.portGrid()),commonData.wwnTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getStateTitle(portsPage.portGrid()),commonData.stateTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortsIOCIdTitle(portsPage.portGrid()),commonData.portIOCIdTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getPortPhysicalPortIdTitle(portsPage.portGrid()),commonData.physicalPortIdTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfPGsTitle(portsPage.portGrid()),commonData.noOfPGsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfInitiatorsTitle(portsPage.portGrid()),commonData.noOfInitiatorsTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfVolumesTitle(portsPage.portGrid()),commonData.noOfVolumesTitle);
    },

    verifyPortGridValuesExistence: function(){
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(portsPage.portGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(portsPage.portGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(portsPage.portGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(portsPage.portGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(portsPage.portGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get7thColumnValue(portsPage.portGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get8thColumnValue(portsPage.portGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get9thColumnValue(portsPage.portGrid()));
        webElementActionLibrary.verifyPresenceOfElement(portsPage.itNexusButton());
    },

    verifyTotalPortCount: function(count){
        webElementActionLibrary.verifyElementsCount(portsPage.portRows(),count);
    },

    verifyPortPGCount:function(portName,pgCount) {
        //portsPage.searchForPort(portName);
        portsPage.portsFirstRow().getText().then(function (val) {
            var portDetails = portsPage.getPortDetailsFromUI(val);
            expect(Number(portDetails.noOfPGs)).toBe(pgCount);
        });
    },

    itNexusButton:  function () {
        return portsPage.portGrid().$('[name="itnexus"]');
    },

    selectPortsTab: function () {
      commonAssetsControlsPage.selectTabUsingLabel(commonData.portsTabText);
    },

    portsTab: function () {
      return $('#portsTab');
    },

    portsFirstRow:  function () {
        return portsPage.portGrid().$$('tbody tr').first();
    },

    portRows:  function () {
        return portsPage.portGrid().$$('tbody tr');
    },

    portGrid:  function () {
        return $('[feature="portsGrid"]');
    },

    portGridSearchBox:  function () {
        return $('#vx-Ports-grid-search-input');
    },

    searchForPort: function (param) {
      webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(portsPage.portGrid()),param);
    },

    portNoOfPgs: function () {
        return portsPage.portGrid().$$('tbody tr td:nth-child(6)').first();
    },

    verifyPortValue: function (value) {
        portsPage.portsFirstRow().getText().then(function (val) {
            expect(val).toContain(value);
        });
    },


    getPortDetailsFromUI: function(val){
        var details = {
            id: val.split(' ')[0],
            name: val.split(' ')[1],
            state: val.split(' ')[2],
            iocId:val.split(' ')[3],
            ppid: val.split(' ')[4],
            noOfPGs: val.split(' ')[5],
            noOfInitiators: val.split(' ')[6],
            noOfVolumes: val.split(' ')[7]
        };

        return details;
    }

};

module.exports = portsPage;