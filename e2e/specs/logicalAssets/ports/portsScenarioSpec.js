'use strict';

//includes all the js file dependencies required for execution of scenarios


var
    portsPage = dependencies.getPortsPage(),
    dataForPortGroups = dependencies.getDataForPortGroups(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage();


describe("Tests for Ports", function() {
    var portDetails;

    beforeAll(function () {
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToPortsPage();
        portsPage.portsFirstRow().getText().then(function (val) {
            portDetails = portsPage.getPortDetailsFromUI(val);
            console.log(portDetails.name);
        });

    });

    describe("Tests to verify port grid",function(){

        it("Test to verify port grid elements", function(){
            portsPage.verifyGridColumnTitles();
            portsPage.verifyPortGridValuesExistence();
            portsPage.verifyTotalPortCount(16);
        });

    });

    describe("Tests to Validate port search functionality using different port parameters",function(){

        it("Tests to search port using state", function() {
            portsPage.searchForPort(portDetails.state);
            portsPage.verifyPortValue(portDetails.state);
        });

        it("Tests to search port using id", function() {
            portsPage.searchForPort(portDetails.id);
            portsPage.verifyPortValue(portDetails.id);
        });

        xit("Tests to search port using name", function() {
            portsPage.searchForPort(portDetails.name);
            portsPage.verifyPortValue(portDetails.name);
        });

        it("Tests to search port using ppid", function() {
            portsPage.searchForPort(portDetails.ppid);
            portsPage.verifyPortValue(portDetails.ppid);
        });

        it("Tests to search port using no of Pgs", function() {
            portsPage.searchForPort(portDetails.noOfPGs);
            portsPage.verifyPortValue(portDetails.noOfPGs);
        });
    });

});



