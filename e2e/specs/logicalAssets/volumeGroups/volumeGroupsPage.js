/**
 * Created by Ajay(Vexata) on 09-09-2015.
 */
'use strict';

var
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    dataForVolumeGroups = dependencies.getDataForVolumeGroups(),
    commonData= dependencies.getCommonData(),
    volumePage=dependencies.getVolumePage(),
    vgService=dependencies.getVolumeGroupsService(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage();


var volumeGroupsPage =  {

    beOnVGTab: function(){
        commonAssetsControlsPage.selectedTabText().getText().then(function (val) {
            if(val!==dataForVolumeGroups.vgTabText){
                volumeGroupsPage.selectVolumeGroupsTab();
                webElementActionLibrary.wait(2);
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText(), dataForVolumeGroups.vgTabText);
            }
        });
    },

    createVG : function(vgName,vgDesc,volName,serviceCheckRequired){
        webElementActionLibrary.waitForElement(volumeGroupsPage.createNewButton());
        volumeGroupsPage.createNewButton().click();
        webElementActionLibrary.type(volumeGroupsPage.volumeGroupName(), vgName);
        webElementActionLibrary.type(volumeGroupsPage.volumeGroupDescription(), vgDesc);
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBoxInForm().last(),volName);
        volumeGroupsPage.volumeCheckBox().click();
        volumeGroupsPage.submitButton().click();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var vgId = commonAssetsControlsPage.getIdFromNotification(val);
            //    //validate vg creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumeGroups.vgCreationSuccessMessage(vgId));
            commonAssetsControlsPage.closeIcon().click();
            volumeGroupsPage.verifyVGDetails(vgName,vgDesc,serviceCheckRequired);
        });
    },


    deleteVG: function(name,serviceCheckRequired){
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumeGroupsPage.volumeGroupGrid()), name);
        volumeGroupsPage.volumeGroupsFirstRow().getText().then(function (val) {
            var vgDetails = volumeGroupsPage.getVGDetailsFromUI(val);
            commonAssetsControlsPage.selectAndDeleteAsset(volumeGroupsPage.volumeGroupGrid(),volumeGroupsPage.vgDeletePopup());
            webElementActionLibrary.waitForElement(commonAssetsControlsPage.notification());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyDeletedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumeGroups.vgDeleteSuccessMessage(vgDetails.id));
            commonAssetsControlsPage.closeIcon().click();
            if(serviceCheckRequired) {
                webElementActionLibrary.refresh();
                volumeGroupsPage.selectVolumeGroupsTab();
                webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumeGroupsPage.volumeGroupGrid()), name);
                expect(volumeGroupsPage.volumeGroupRows().count()).toBe(1);
            }
            //validate volume deletion from API service
            //volumeGroupService.verifyVGDeletion(id);
        });
    },

    editVG:function(vgName,editVGName,vgDescription,anotherVolumeForVG,serviceCheckRequired){
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumeGroupsPage.volumeGroupGrid()), vgName);
        volumeGroupsPage.volumeGroupsFirstRow().getText().then(function (val) {
            var vgDetails = volumeGroupsPage.getVGDetailsFromUI(val);
            volumeGroupsPage.editButton().click();
            webElementActionLibrary.type(volumeGroupsPage.volumeGroupName(), editVGName);
            webElementActionLibrary.type(volumeGroupsPage.volumeGroupDescription(), vgDescription);
            webElementActionLibrary.type(commonAssetsControlsPage.getSearchBoxInForm().last(), anotherVolumeForVG);
            volumeGroupsPage.volumeCheckBox().click();
            volumeGroupsPage.submitButton().click();
            webElementActionLibrary.waitForAngular();
            //validate volume update success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyUpdatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumeGroups.vgEditSuccessMessage(vgDetails.id));
            commonAssetsControlsPage.closeIcon().click();
            volumeGroupsPage.verifyVGDetails(editVGName,vgDescription,serviceCheckRequired);
        });
    },

    compareVGDetailsOnUIAndRestResponse: function(vgName){
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumeGroupsPage.volumeGroupGrid()), vgName);
        volumeGroupsPage.volumeGroupsFirstRow().getText().then(function (val) {
            var vgDetails = volumeGroupsPage.getVGDetailsFromUI(val);
            //volumeGroupService.verifyVGDetailsAndDelete(vgDetails);
        });
    },

    verifyVGDetails:function(vgname,description,serviceCheckRequired) {
        if(serviceCheckRequired) {
            webElementActionLibrary.refresh();
            volumeGroupsPage.selectVolumeGroupsTab();
        }
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumeGroupsPage.volumeGroupGrid()), vgname);
        volumeGroupsPage.volumeGroupsFirstRow().getText().then(function (val) {
            var vgDetails = volumeGroupsPage.getVGDetailsFromUI(val,description);
            expect(vgDetails.name).toBe(vgname);
            expect(vgDetails.description).toBe(description);
        });
    },

    verifyGridColumnTitles: function () {
        // webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(volumeGroupsPage.volumeGroupGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(volumeGroupsPage.volumeGroupGrid()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(volumeGroupsPage.volumeGroupGrid()),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfVolumesTitle(volumeGroupsPage.volumeGroupGrid()),commonData.noOfVolumesTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getSizeTitle(volumeGroupsPage.volumeGroupGrid()),commonData.sizeTitle);
        // webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfEGsTitle(volumeGroupsPage.volumeGroupGrid()),commonData.noOfEGsTitle);
    },

    verifyGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(volumeGroupsPage.volumeGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(volumeGroupsPage.volumeGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(volumeGroupsPage.volumeGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(volumeGroupsPage.volumeGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get6thColumnValue(volumeGroupsPage.volumeGroupGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get7thColumnValue(volumeGroupsPage.volumeGroupGrid()));
    },

    verifyDetailedGridColumnTitles : function () {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdTitle(volumeGroupsPage.detailedGrid()),commonData.idTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameTitle(volumeGroupsPage.detailedGrid()),commonData.nameTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionTitle(volumeGroupsPage.detailedGrid()),commonData.descriptionTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getProvisionedTitle(volumeGroupsPage.detailedGrid()),commonData.provisionedTitle);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNoOfVGsTitle(volumeGroupsPage.detailedGrid()),commonData.noOfVGsTitle);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText().last(),commonData.volumesTabText);
    },

    verifyDetailedGridValues: function () {
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get1stColumnValue(volumeGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get2ndColumnValue(volumeGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get3rdColumnValue(volumeGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get4thColumnValue(volumeGroupsPage.detailedGrid()));
        webElementActionLibrary.verifyPresenceOfElement(commonAssetsControlsPage.get5thColumnValue(volumeGroupsPage.detailedGrid()));
    },

    compareDetailedGridVolumeDetails: function (volDetails) {
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getIdValue(volumeGroupsPage.detailedGrid()),volDetails.id);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getNameValue(volumeGroupsPage.detailedGrid()),volDetails.name);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getDescriptionValue(volumeGroupsPage.detailedGrid()),volDetails.description);
        webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getProvisionedValue(volumeGroupsPage.detailedGrid()),volDetails.size+" "+volDetails.sizeUnit);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getUsedValue(volumeGroupsPage.detailedGrid()),volDetails.used+" "+volDetails.usedUnit);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getITNInitiatorsValue(volumeGroupsPage.detailedGrid()),volDetails.noOfInitiators);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.getITNPortsValue(volumeGroupsPage.detailedGrid()),volDetails.noOfPorts);
        //webElementActionLibrary.verifyExactText(commonAssetsControlsPage.get7thColumnValue(volumeGroupsPage.detailedGrid()),String(Number(volDetails.noOfVGs)+1));
    },

    createSG: function (vgName, vgId) {
        console.log(vgName);
        volumeGroupsPage.searchForVG(vgName);
        volumeGroupsPage.createSGIcon().click();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            console.log(val);
            var sgId = commonAssetsControlsPage.getIdFromNotification(val);
            //validate volume creation success message
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyCreatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumeGroups.getSGCreationSuccessMessage(sgId));
            commonAssetsControlsPage.closeIcon().click();
            volumeGroupsPage.vgDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            volumeGroupsPage.selectSGTab();
            webElementActionLibrary.wait(2);
            volumeGroupsPage.sgGridTable().getText().then(function (val) {
                volumeGroupsPage.verifySGDetailsInSGGrid(val,vgId,sgId);
            });

            // volumePage.verifyVolumeDetailsInGrid(volumeName,description,size,serviceCheckRequired);
        });

    },

    editSG: function (vgName,vgId, sgEditedName) {
        volumeGroupsPage.loadSGGridForVG(vgName);
        volumeGroupsPage.editSGIcon().click();
        webElementActionLibrary.waitForAngular();
        webElementActionLibrary.type(volumeGroupsPage.sgNameInputField(),sgEditedName);
        webElementActionLibrary.type(volumeGroupsPage.sgDescInputField(),"ssupdatedesc");
        volumeGroupsPage.editSGOkButton().click();
        webElementActionLibrary.waitForAngular();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var sgId=commonAssetsControlsPage.getIdFromNotification(val);
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), commonAssetsControlsPage.getSuccessfullyUpdatedText());
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumeGroups.getSGEditSuccessMessage(sgId));
            commonAssetsControlsPage.closeIcon().click();
            webElementActionLibrary.refresh();
            volumeGroupsPage.selectVolumeGroupsTab();
            volumeGroupsPage.loadSGGridForVG(vgName);
            volumeGroupsPage.sgGridTable().getText().then(function (val) {
                console.log(sgId);
                volumeGroupsPage.verifySGDetailsInSGGrid(val,vgId,sgId,sgEditedName,"ssupdatedesc");
            });

            // volumePage.verifyVolumeDetailsInGrid(volumeName,description,size,serviceCheckRequired);
        });

    },

    deleteSG:function (vgName) {
        volumeGroupsPage.loadSGGridForVG(vgName);
        volumeGroupsPage.deleteSGIcon().click();
        commonAssetsControlsPage.deleteRecord();
        commonAssetsControlsPage.notification().getText().then(function (val) {
            var sgId=commonAssetsControlsPage.getIdFromNotification(val);
            webElementActionLibrary.verifyTextPresenceUsingContain(commonAssetsControlsPage.notification(), dataForVolumeGroups.getSGDeleteSuccessMessage(sgId));
            commonAssetsControlsPage.closeIcon().click();
        });
    },


    loadSGGridForVG: function (vgName) {
        volumeGroupsPage.searchForVG(vgName);
        volumeGroupsPage.vgDetailedGridExpansionLinkFirstRow().click();
        webElementActionLibrary.waitForAngular();
        volumeGroupsPage.selectSGTab();
        webElementActionLibrary.wait(2);
    },

    verifySGDetailsInSGGrid:function (val,vgId,sgId,sgName,sgDesc) {
        vgService.getSGDetails(vgId,sgId).then(function (value) {
            //console.log(value);
            expect(val).toContain(sgId);
            if(sgName==undefined)
                expect(val).toContain(value._body.name);
            else
                expect(val).toContain(sgName);
            if(sgDesc!==undefined)
                expect(val).toContain(value._body.description);
        });
    },

    selectSGTab: function () {
        volumeGroupsPage.sgTab().click();
    },

    searchForVG: function (vgName) {
        webElementActionLibrary.type(commonAssetsControlsPage.getSearchBox(volumeGroupsPage.volumeGroupGrid()), vgName);
    },

    detailedGrid: function () {
        return commonAssetsControlsPage.getDetailedGrid(volumeGroupsPage.volumeGroupGrid());
    },

    selectVolumeGroupsTab: function(){
        volumeGroupsPage.volumeGroupsTab().click();
        webElementActionLibrary.waitForAngular();
    },

    volumeGroupsTab: function () {
        return $('#volumeGroupsTab');
    },

    volumeGroupName: function(){
        return $('#vx_volume_grp_form_name');
    },

    volumeGroupDescription: function(){
        return $('#vx_volume_grp_form_description');
    },

    volumeGroupsFirstRow:  function () {
        return volumeGroupsPage.volumeGroupGrid().$$('tbody tr').first();
    },

    volumeGroupRows:  function () {
        return volumeGroupsPage.volumeGroupGrid().$$('tbody tr');
    },

    volumeGroupGrid:  function () {
        return $('#volumeGroupGrid');
    },

    volumeGroupCreatePanel:  function () {
        return $$('.k-window').last();
    },

    createNewButton:  function () {
        return $('#VolumeGroupGridTab-create-btn-id span');
    },

    volumeCheckBox:function(){
        return $('[for*="volume_selector"]');
    },

    submitButton: function(){
        return $('#vx-VolumeGroupGridTab-submit-btn');
    },

    cancelButton: function(){
        return $('#vx-VolumeGroupGridTab-cancel-btn');
    },

    editVolumeGroupPanel: function () {
        return $('[kendo-window="volGroup"]');
    },


    editButton:  function () {
        return commonAssetsControlsPage.getEditIcon(volumeGroupsPage.volumeGroupGrid());
    },

    nameErrorMessage:  function () {
        return $('[data-container-for="name"] .k-invalid-msg');
    },

    deleteButton:  function () {
        return volumeGroupsPage.volumeGroupGrid().$('.k-grid-delete');
    },

    verifyVGValue: function (value) {
        volumeGroupsPage.volumeGroupsFirstRow().getText().then(function (val) {
            expect(val).toContain(value);
        });
    },

    getVGDetailsFromUI: function(val,description){
        var details;
        if(description=="") {
            details = {
                id: val.split(' ')[0],
                name: val.split(' ')[1],
                description: "",
                numOfVols:val.split(' ')[2],
                size: val.split(' ')[3],
                exportGroups: val.split(' ')[5]
            };
        }
        else{
            details = {
                id: val.split(' ')[0],
                name: val.split(' ')[1],
                description: val.split(' ')[2],
                numOfVols:val.split(' ')[3],
                size: val.split(' ')[4],
                exportGroups: val.split(' ')[6]
            };
        }
        return details;
    },

    vgDetailedGridExpansionLinkFirstRow: function () {
        return commonAssetsControlsPage.getDetailedGridExpansionLink(volumeGroupsPage.volumeGroupsFirstRow());
    },

    verifyVGEGCount: function (name, count) {
        volumeGroupsPage.searchForVG(name);
        volumeGroupsPage.volumeGroupsFirstRow().getText().then(function (val) {
            expect(val.split(' ')[5]).toBe(count);
        });
    },

    vgDeletePopup: function () {
        return $('[kendo-window="vgPopupWindow"]');
    },

    sgTab:function () {
        return webElementActionLibrary.getElementByXpath('//span[text()="Snapshot Groups"]');
    },

    createSGIcon:function () {
        return $$('i[name="create_snapshot_group"]').get(0);
    },

    sgGrid:function () {
        return $('vx-grid[feature="snapshot_group"]');
    },

    sgGridTable:function () {
        return $$('vx-grid[feature="snapshot_group"] table').get(1);
    },
    vgRefreshButton: function () {
        return $('#VolumeGroupGridTab-refresh-btn-id');
    },

    editSGIcon:function () {
        return volumeGroupsPage.sgGrid().$('.snapshot_group-edit-btn-cls');
    },

    sgNameInputField:function () {
        return $('#vx_volume_group_snapshot_form_name');
    },

    sgDescInputField:function () {
        return $('#vx_volume_group_snapshot_form_description');
    },

    editSGOkButton: function () {
        return $('#vx-snapshot_group-submit-btn');
    },

    deleteSGIcon: function () {
        return volumeGroupsPage.sgGrid().$('i[name="destroy"]');
    },

};

module.exports = volumeGroupsPage;