'use strict';
var frisby = require('frisby');
var commonData=dependencies.getCommonData();
var commonAssetsControlsPage = dependencies.getCommonAssetsControlsPage();
var vgApiUrl = commonData.vgApiUrl();
var sgApiUrl;

var volumeGroupsService = {


    createVG: function (name,desc,volIdArray) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.post(vgApiUrl,
            {
                addVolumes:volIdArray,
                description:desc,
                name:name
            }, {json: true})
            .expect('status',201)
            .then(function(res){
                expect(res._json.name).toBe(name);
                expect(res._json.description).toBe(desc);
                expect(res._json.currVolumes).toEqual(volIdArray);
                console.log("vg "+res._json.name +" with id "+res._json.id+" created through rest api");
            });
    },

    getSGDetails: function (vgId,sgId) {
        sgApiUrl= commonData.sgAPIUrl(vgId);
        commonAssetsControlsPage.setServiceHeader();
        return frisby.get(sgApiUrl+sgId).
            expect('status',200);
    },

    getVGDetails : function (vgId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.get(vgApiUrl+vgId)
            .expect('status',200)
            .then(function (res) {
                console.log(res);
            });

    },

    deleteVG: function (vgId) {
        commonAssetsControlsPage.setServiceHeader();
        return frisby.del(vgApiUrl+vgId)
            .expect('status',204).then(function (res) {
                console.log("vg "+vgId+" deleted through rest api");
            });
    }
};



module.exports = volumeGroupsService;
