var dataForVolumeGroups={

  //successMessages
  vgCreationSuccessMessage: function(id){return 'Details: Volume Group #('+id+') has been created.'},
  vgDeleteSuccessMessage: function(id){return 'Details: Volume Group(s) #('+id+') has been deleted.'},
  vgEditSuccessMessage: function(id){return 'Details: Volume Group #('+id+') has been updated'},
  getSGCreationSuccessMessage: function(id){return 'Details: Snapshot Group #('+id+') has been created.'},
  getSGDeleteSuccessMessage: function(id){return '#('+id+') delete request is initiated, Please check the events section to verify the status.'},
  getSGEditSuccessMessage: function(id){return 'Details: SnapshotGroup #('+id+') has been updated'},

  //String Constants
  volNameForVG:"TAVOne",
  anotherVolNameForVG:"TAVTwo",
  volDescForVG:"TAVGVolDesc",
  volSize:"1",
  vgTabText: "VOLUME GROUPS",
  detailedGridVolumesTabText:"Volumes",
  createVGPopUpTitle: "Create Volume Group",
  editVGPopUpTitle: "Edit Volume Group",
  vgName: "TAVG",
  editedVGName: "TAVGE",
  editedVGDescription: "TA_vgDescEdited",
  vgDescription: "TA_vgDesc",
  emptyVGNameError: "name is required",

  vgName31Char:"VGNameFor31Chr",
  vgDesc63Char:"VGDescriptionForUIAutomation63CharactersToTest63CharactersTetet",
  editedVGName31Char:"EVGNameFor31Ch",
  editedVGDesc63Char:"EditedVGDescriptionForUIAutomation63CharactersToTest63Character",


};

module.exports = dataForVolumeGroups;