'use strict';

//includes all the js file dependencies required for execution of scenarios


var
    volumeGroupsPage = dependencies.getVolumeGroupsPage(),
    dataForVolumeGroups = dependencies.getDataForVolumeGroups(),
    webElementActionLibrary = dependencies.getWebElementActionLibrary(),
    commonAssetsControlsPage= dependencies.getCommonAssetsControlsPage(),
    volumePage = dependencies.getVolumePage(),
    volumeService = dependencies.getVolumeService(),
    vgService=dependencies.getVolumeGroupsService();

describe("Tests for Volume Groups", function() {

    var firstVolDetails, secondVolDetails, vgName, vgName1, vgId, editedVGName, volume1NameForVG, volume2NameForVG;


    beforeAll(function () {
        volume1NameForVG = webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.volNameForVG);
        volume2NameForVG = webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.anotherVolNameForVG);
        volumeService.createVolume(volume1NameForVG,dataForVolumeGroups.volDescForVG,dataForVolumeGroups.volSize);
        volumeService.createVolume(volume2NameForVG,dataForVolumeGroups.volDescForVG,dataForVolumeGroups.volSize);
        commonAssetsControlsPage.loadLoginUrlAndLoginAsAdmin();
        commonAssetsControlsPage.navigateToVolumesPage();
        volumePage.searchForVolume(volume1NameForVG);
        volumePage.volumeFirstRow().getText().then(function (val) {
            firstVolDetails = volumePage.getVolumeDetailsFromUI(val);
        });
        volumePage.searchForVolume(volume2NameForVG);
        volumePage.volumeFirstRow().getText().then(function (val) {
            secondVolDetails = volumePage.getVolumeDetailsFromUI(val);
        });
        webElementActionLibrary.waitForAngular();
        volumeGroupsPage.selectVolumeGroupsTab();
        webElementActionLibrary.wait(2);
        webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.selectedTabText().last(), dataForVolumeGroups.vgTabText);
    });

    beforeEach(function(){
        vgName = webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.vgName);
    });

    describe("Tests to create VG with different inputs",function(){

        beforeEach(function(){
            vgName = webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.vgName);
            webElementActionLibrary.refresh();
            webElementActionLibrary.wait(2);
            volumeGroupsPage.beOnVGTab();
        });

        it("Test to verify successful VG Creation", function(){
            volumeGroupsPage.createVG(vgName,dataForVolumeGroups.vgDescription,volume1NameForVG,true);
        });

        it("Test to verify successful VG Creation with name as 31 characters and description as 63 characters", function(){
            vgName=webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.vgName31Char);
            volumeGroupsPage.createVG(vgName,dataForVolumeGroups.vgDesc63Char,volume1NameForVG);
        });

        it("Test to verify successful VG Creation with name only", function(){
            volumeGroupsPage.createVG(vgName,"",volume1NameForVG);
        });

        afterEach(function () {
            volumeGroupsPage.searchForVG(vgName);
            volumeGroupsPage.volumeGroupsFirstRow().getText().then(function (val) {
                var vgDetails = volumeGroupsPage.getVGDetailsFromUI(val,dataForVolumeGroups.vgDescription);
                vgService.deleteVG(vgDetails.id);
            });
        })

    });

    xdescribe("Tests to update VG with different inputs",function(){

        beforeEach(function(){
            volumeGroupsPage.createVG(vgName,dataForVolumeGroups.vgDescription,volume1NameForVG,true);
            editedVGName=webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.editedVGName);
        });

        it("Tests to Update VG", function() {
            volumeGroupsPage.editVG(vgName,editedVGName,dataForVolumeGroups.editedVGDescription,volume2NameForVG,true);
        });

        it("Test to update VG with name as 31 characters and description as 63 characters", function() {
            editedVGName = webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.editedVGName31Char);
            volumeGroupsPage.editVG(vgName,editedVGName,dataForVolumeGroups.editedVGDesc63Char,volume2NameForVG);
        });

        it("Test to update VG with blank description", function() {
            volumeGroupsPage.editVG(vgName,editedVGName,"",volume2NameForVG);
        });

        afterEach(function(){
            volumeGroupsPage.deleteVG(editedVGName);
        });
    });

    xdescribe("Tests to Validate VG search functionality using different VG parameters",function(){
        var vgDetails, vgNameForSearch;

        beforeAll( function(){
            vgNameForSearch = webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.vgName);
            vgService.createVG(vgNameForSearch,dataForVolumeGroups.vgDescription,[Number(firstVolDetails.id)]);
            webElementActionLibrary.refresh();
            volumeGroupsPage.selectVolumeGroupsTab();
            volumeGroupsPage.searchForVG(vgNameForSearch);
            volumeGroupsPage.volumeGroupsFirstRow().getText().then(function (val) {
                vgDetails = volumeGroupsPage.getVGDetailsFromUI(val);
            });
        });

        it("Tests to search VG using id", function() {
            volumeGroupsPage.searchForVG(vgDetails.id);
            volumeGroupsPage.verifyVGValue(vgDetails.id);
        });

        it("Tests to search VG using desc", function() {
            volumeGroupsPage.searchForVG(vgDetails.description);
            volumeGroupsPage.verifyVGValue(vgDetails.description);
        });

        it("Tests to search VG using size", function() {
            var size = vgDetails.size.split('.')[0];
            volumeGroupsPage.searchForVG(size);
            volumeGroupsPage.verifyVGValue(size);
        });

        it("Tests to search VG using no of volumes", function() {
            volumeGroupsPage.searchForVG(vgDetails.numOfVols);
            volumeGroupsPage.verifyVGValue(vgDetails.numOfVols);
        });

        xit("Tests to search VG using no of EG's", function() {
            volumeGroupsPage.searchForVG(vgDetails.exportGroups);
            volumeGroupsPage.verifyVGValue(vgDetails.exportGroups);
        });

        it("Test to compare the volume details with the volume details in the VG detailed grid and verify VG grid, VG detailed grid titles", function () {
            volumeGroupsPage.verifyGridColumnTitles();
            volumeGroupsPage.searchForVG(vgNameForSearch);
            volumeGroupsPage.vgDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            //volumeGroupsPage.verifyDetailedGridColumnTitles();
            //volumeGroupsPage.verifyDetailedGridValues();
            //volumeGroupsPage.compareDetailedGridVolumeDetails(firstVolDetails);
        });

        it("Test to compare the vg details with the vg details in the volumes detailed grid and VG count of a volume in Volume Grid", function () {
            volumePage.selectVolumesTab();
            volumePage.verifyVolumeVGCount(volume1NameForVG,String(Number(firstVolDetails.noOfVGs)+1));
            volumePage.volumeDetailedGridExpansionLinkFirstRow().click();
            webElementActionLibrary.waitForAngular();
            //volumePage.compareDetailedGridVGDetails(vgDetails);
            volumeGroupsPage.selectVolumeGroupsTab();
        });

        describe("Tests to validate UI Error for Edit VG Pane'",function(){
            beforeAll(function(){
                volumeGroupsPage.searchForVG(vgNameForSearch);
                volumeGroupsPage.editButton().click();
                webElementActionLibrary.waitForAngular();
            });

            it("Test: Verify Elements in Edit VG Panel",function(){
                webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForVolumeGroups.editVGPopUpTitle);
            });

            xit("Test: Try updating VG with no name, and no volumes, and verify empty field UI error messages",function(){

            });

            xit("Test: Try updating VG with proper name and no volume, and verify UI error messages",function(){

            });

            xit("Test: Try updating VG with single character name, and verify UI error messages",function(){

            });

        });

        afterAll( function(){
            vgService.deleteVG(vgDetails.id);
        });
    });

    describe("Tests to delete VG with different inputs",function(){

        beforeEach( function(){
            vgName = webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.vgName);
            vgService.createVG(vgName,dataForVolumeGroups.vgDescription,[Number(firstVolDetails.id)]);
            webElementActionLibrary.refresh();
        });

        it("Test to verify volume delete disability which is associated with a Volume Group and to delete VG", function() {
            volumeGroupsPage.selectVolumeGroupsTab();
            volumeGroupsPage.deleteVG(vgName,true);
        });
    });

    describe("Tests for SG",function(){
        var sgId;
        beforeAll( function(){
            vgName1 = webElementActionLibrary.getTimeStampAttached(dataForVolumeGroups.vgName);
            vgService.createVG(vgName1,dataForVolumeGroups.vgDescription,[Number(firstVolDetails.id)]).then(function (value) { vgId=value._body.id; });
            //volumePage.volumeRefreshButton().click();
        });

        beforeEach(function () {
            webElementActionLibrary.refresh();
            volumeGroupsPage.selectVolumeGroupsTab();
            //volumeGroupsPage.vgRefreshButton().click();
            //webElementActionLibrary.waitForAngular();
        });

        it("Tests to Create SG", function(){
            volumeGroupsPage.createSG(vgName1,vgId);
        });


        it("Tests to Edit SG", function(){
            volumeGroupsPage.editSG(vgName1,vgId, webElementActionLibrary.getTimeStampAttached("sgEdit"));
        });

        it("Tests to delete SG", function() {
            volumeGroupsPage.deleteSG(vgName1);
        });

        afterAll( function(){
            vgService.deleteVG(vgId);
        });


    });

    describe("Tests to validate UI Error for Create VG Panel'",function(){

        beforeAll(function(){
            volumeGroupsPage.createNewButton().click();
            webElementActionLibrary.waitForAngular();
        });

        it("Test: Verify Elements in Create VG Panel",function(){
            webElementActionLibrary.verifyTextPresence(commonAssetsControlsPage.popUpTitle(), dataForVolumeGroups.createVGPopUpTitle);
            webElementActionLibrary.mouseOver(volumeGroupsPage.cancelButton());
            webElementActionLibrary.verifyPresenceOfElement(volumeGroupsPage.cancelButton());
        });

        xit("Test: Try creating VG with no name, and no volumes, and verify empty field UI error messages",function(){

        });

        xit("Test: Try creating VG with proper name and no volume, and verify UI error messages",function(){

        });

        xit("Test: Try creating VG with single character name, and verify UI error messages",function(){

        });
    });



    afterAll(function () {
        volumeService.deleteVolume(firstVolDetails.id);
        volumeService.deleteVolume(secondVolDetails.id);
    });

});



