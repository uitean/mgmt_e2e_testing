     This folder contains the code to do end to end testing of the angular application

Setup Assumptions & installation procedure:
1.  Node/npm should be installed
2.  For UI e-e testing we will be using protractor which is a node.js based e-e testing
    framework.  It uses jasmine test framework and selenium webdriver server to drive the
    browser.  Both protractor and webdriver needs to be installed.  Actually Protractor comes
    with webdriver with chrome support but it needs to be updated.
3. Protractor should be installed globally (npm install protractor -g)
4. Once protractor is installed globally run( webdriver-manager update)
5. Grunt should be installed globally (npm install grunt -g)
6. AUT(Appllication Under Test) should be up and running

If you need setup help, Please follow the youtube video

https://www.youtube.com/watch?v=57134cHJlAs

Once the above setup is done.

Data Setup:

1. Change the specs in conf.js file to run the scripts as per your requirement(default - All tests would run)
2. Change the browser in conf.js file to run the scripts as per your requirement(default - firefox)
3. Change the screen resolution to 1920*1080 and the text size as smallers

How to Run:

There are two ways to run the tests

1. By Grunt:

a. Navigate to project location where gruntfile.js is located(cd \vexata_client_mgmt_view )  and run command (grunt e2e --host "hostname") to run the scripts. Host name is the url. 
Eg: "hyperloop03-a" is the hostname to run scripts against http://hyperloop03-a.vexata.com

2. By Protractor

a. Update the selenium webdriver manager by running command "webdriver-manager update"
b. Start the selenium webdriver manager by running command "webdriver-manager start"
c. Set the hostname in conf.js to run your scripts against hostname
Eg: "hyperloop03-a" is the hostname to run scripts against http://hyperloop03-a.vexata.com
d. Navigate to project location where conf.js file is located(cd \vexata_client_mgmt_view\test\e2e) and run command (protractor conf.js) to run the scripts


Output of the tests results/runs are on the terminal

How to add e2e tests:
1. Categorise the test as per the e2e test folder structure
2. Add the test in the Spec FIle under the feature folder - Eg: like volumeScenarioSpec.js under volumes folder in e2e tests.
3. Add the page objects and reusable flows in the Page file under feature folder - Eg: like volumePage.js under volumes folder in e2e tests.
4. Add the rest calls and rest validation in the service file under feature folder - Eg: like volumeService.js under volumes folder in e2e tests.
5. Add the data specific for the feature in teh data file under feature folder - Eg: like dataForVolumes.js under volumes folder in e2e tests.

Additional Files Significance:
1. webElementActionLibrary.js contains all the consolidated actions on web elements like mouseover, type etc.
2. commonData.js contains all teh data which is common across application.
3. conf.js file contains all the protractor, browser, reporting and specs configurations.
4. dependencies.js file contains all the functions to retrieve required dependencies and use whereever needed (like volume page required to load for running volumeScenarioSpec)
