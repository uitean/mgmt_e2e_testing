'use strict';

var dependencies = {

    getCommonAssetsControlsPage: function(){
        return require('./specs/common/commonAssetsControlsPage.js');
    },

    getDashboardPage: function(){
        return require('./specs/dashboard/dashboardPage.js');
    },

    getMADashboardPage: function(){
        return require('./specs/dashboard/maDashboardPage.js');
    },

    getPmPage: function(){
        return require('./specs/policyManagement/pmPage.js');
    },

    getEventsPage: function(){
        return require('./specs/fault/events/eventsPage.js');
    },

    getNodeDetailsPage: function(){
        return require('./specs/node/nodeDetailsPage.js');
    },

    getVolumeAnalyticsPage: function(){
        return require('./specs/analytics/volumes/volumeAnalyticsPage.js');
    },

    getCapacityPlaningPage: function(){
        return require('./specs/analytics/capacityPlaning/capacityPlaningPage.js');
    },

    getPortAnalyticsPage: function(){
        return require('./specs/analytics/ports/portAnalyticsPage.js');
    },

    getLayoutPage: function(){
        return  require('./specs/layout/layoutPage.js')
    },

    getHealthDetailsPage: function(){
        return  require('./specs/system/health/healthDetailsPage.js')
    },

    getEventCatalogPage: function(){
        return  require('./specs/fault/eventCatalog/eventCatalogPage.js')
    },

    getLogicalAssetsDashboardPage : function () {
        return require('./specs/logicalAssets/dashboard/logicalAssetsDashboardPage.js')
    },

    getVolumePage: function(){
        return  require('./specs/logicalAssets/volumes/volumePage.js');
    },

    getSAPage: function(){
        return  require('./specs/logicalAssets/storageArray/storageArrayPage.js');
    },

    getDataForVolumes: function(){
        return require('./specs/logicalAssets/volumes/dataForVolumes.js');
    },
    getInitiatorPage: function(){
        return  require('./specs/logicalAssets/initiators/initiatorPage');
    },

    getDataForInitiators: function(){
        return require('./specs/logicalAssets/initiators/dataForInitiators.js');
    },

    getInitiatorService: function(){
        return require('./specs/logicalAssets/initiators/initiatorService.js');
    },

    getInitiatorGroupService: function(){
        return require('./specs/logicalAssets/initiatorGroups/initiatorGroupService.js');
    },

    getPortGroupService: function(){
        return require('./specs/logicalAssets/portGroups/portGroupService.js');
    },

    getVolumeGroupsPage: function(){
        return  require('./specs/logicalAssets/volumeGroups/volumeGroupsPage.js');
    },

    getFrisby: function () {
        return  require('./../../node_modules/frisby/lib/frisby.js');
    },

    getDataForVolumeGroups: function(){
        return  require('./specs/logicalAssets/volumeGroups/dataForVolumeGroups.js');
    },

    getInitiatorGroupsPage: function(){
        return  require('./specs/logicalAssets/initiatorGroups/initiatorGroupsPage.js');
    },

    getDataForInitiatorGroups: function(){
        return  require('./specs/logicalAssets/initiatorGroups/dataForInitiatorGroups.js');
    },

    getPortGroupsPage: function(){
        return  require('./specs/logicalAssets/portGroups/portGroupsPage.js');
    },

    getDataForPortGroups: function(){
        return  require('./specs/logicalAssets/portGroups/dataForPortGroups.js');
    },

    getPortsPage: function(){
        return  require('./specs/logicalAssets/ports/portsPage.js');
    },

    getDataForPorts: function(){
        return  require('./specs/logicalAssets/ports/dataForPorts.js');
    },

    getExportGroupsPage: function(){
        return  require('./specs/logicalAssets/exportGroups/exportGroupsPage.js');
    },

    getDataForExportGroups: function(){
        return  require('./specs/logicalAssets/exportGroups/dataForExportGroups.js');
    },

    getLoginPage: function(){
        return  require('./specs/login/loginPage.js');
    },

    getWebElementActionLibrary : function(){
        return require('./webElementActionLibrary.js');
    },

    getCommonData : function(){
        return  require('./commonData.js');
    },

    getSettingsPage : function(){
        return  require('./specs/system/settings/settingsPage.js');
    },

    getCLIData : function(){
        return  require('./specs/cliData.js');
    },

    getVolumeService : function(){
        return require('./specs/logicalAssets/volumes/volumeService.js')
    },

    getVolumeGroupsService : function(){
        return require('./specs/logicalAssets/volumeGroups/volumeGroupsService.js')
    }

};
module.exports=dependencies;